TEMPLATE = app

DEFINES -= USE_CLIPSTER_DEV_SERVER

OUTPUT_EXT =
!isEmpty(QMAKE_TARGET.arch) {
  #OUTPUT_EXT = -$$QMAKE_TARGET.arch
}

CONFIG(debug, debug|release) {
    DESTDIR = debug$$OUTPUT_EXT
    OBJECTS_DIR = debug$$OUTPUT_EXT/.obj
    MOC_DIR = debug$$OUTPUT_EXT/.moc
    RCC_DIR = debug$$OUTPUT_EXT/.rcc
    UI_DIR = debug$$OUTPUT_EXT/.ui
} else {
    DESTDIR = release$$OUTPUT_EXT
    OBJECTS_DIR = release$$OUTPUT_EXT/.obj
    MOC_DIR = release$$OUTPUT_EXT/.moc
    RCC_DIR = release$$OUTPUT_EXT/.rcc
    UI_DIR = release$$OUTPUT_EXT/.ui
}

GIT_VERSION = $$system(git describe  --always --tags)

DEFINES += GIT_VERSION=\\\"$$GIT_VERSION\\\"

CONFIG += $$(DEBUG)

QT += widgets gui gui-private sql webkitwidgets

SOURCES += src/main.cpp \
    src/clipboard.cpp \
    src/clipboarddata.cpp \
    src/mimedatatools.cpp \
    src/trayicon.cpp \
    src/qxtglobalshortcut.cpp \
    src/clipboardmodel.cpp \
    src/clipboardproxymodel.cpp \
    #src/previewimageprovider.cpp \
    src/clipboardlocaldb.cpp \
    src/clipboarddataprovider.cpp \
    src/clipsterconfig.cpp \
    src/webserviceclient.cpp \
    src/commonresponse.cpp \
    src/deviceinfo.cpp \
    src/loginwebview.cpp \
    src/whoamiresponse.cpp \
    src/clipboardapplicationmanager.cpp \
    src/dataentrylistresponse.cpp \
    src/onlineclipboard.cpp \
    src/networkreplyparser.cpp \
    src/clipboarddatamanager.cpp \
    src/transferreplywrapper.cpp \
    src/singleapplication.cpp \
    src/httpheaders.cpp \
    src/stringtools.cpp \
    src/multiparthandler.cpp \
    src/successresponse.cpp \
    src/wrapper.cpp \
    src/clipboardentrydelegate.cpp \
    src/clipboardentryview.cpp \
    src/searchfield.cpp \
    src/teammanagerwebview.cpp \
    src/roundedpopup.cpp \
    src/teamlistresponse.cpp \
    src/team.cpp \
    src/teammanager.cpp \
    src/roundedpopupbutton.cpp
    #src/clipboardbrowser.cpp

INCLUDEPATH += src

unix:!macx {
    SOURCES += \
        src/x11/deviceinfo_x11.cpp \
        src/x11/qxtglobalshortcut_x11.cpp \
        src/x11/sendkeyboardevent_x11.cpp \
        src/x11/xdo/xdo.c \
        src/x11/xdo/xdotool.c \
        src/x11/xdo/cmd_click.c \
        src/x11/xdo/cmd_mousemove.c
        #src/x11/backtrace-symbols.c
    LIBS += -lX11 -lXtst -lXinerama -ludev -lbfd
    DEFINES += LINUX_OS
    QMAKE_LFLAGS += -g -rdynamic "-Wl,-rpath,\'\$$ORIGIN\'"
}
macx {
    SOURCES += \
        src/mac/mouselib_mac.c \
        src/mac/deviceinfo_mac.cpp \
        src/mac/qxtglobalshortcut_mac.cpp \
        src/mac/sendkeyboardevent_mac.cpp
    OBJECTIVE_SOURCES =
    DEFINES += DARWIN_OS
    QMAKE_CFLAGS_RELEASE += $$(EXTRA_COMPILER_FLAGS)
    QMAKE_CXXFLAGS_RELEASE += $$(EXTRA_COMPILER_FLAGS)
    LIBS += "-framework cocoa" "-framework carbon"
    QMAKE_LFLAGS += -Wl,-rpath,@loader_path/.
    ICON = resources/images/clipster-logo.icns
}
win32 {
    SOURCES += \
        src/win/deviceinfo_win.cpp \
        src/win/qxtglobalshortcut_win.cpp \
        src/win/sendkeyboardevent_win.cpp \
        src/win/mouselib_win.c

    DEFINES += WINDOWS_OS
    LIBS += User32.lib userenv.lib dbghelp.lib
    QMAKE_CXXFLAGS_RELEASE -= -Zc:strictStrings
    QMAKE_CFLAGS_RELEASE += $$(EXTRA_COMPILER_FLAGS)
    QMAKE_CXXFLAGS_RELEASE += $$(EXTRA_COMPILER_FLAGS)
    QMAKE_LFLAGS_RELEASE += $$(EXTRA_LINKER_FLAGS)
    QMAKE_CFLAGS_RELEASE += -Zi
    QMAKE_CXXFLAGS_RELEASE += -Zi
    RC_ICONS = resources/images/clipster-logo.ico
    QMAKE_LFLAGS_RELEASE += /DEBUG /OPT:REF
    QMAKE_CXXFLAGS_EXCEPTIONS_ON = /EHa
    QMAKE_CXXFLAGS_STL_ON = /EHa
}

RESOURCES += \
    resources/images.qrc \
    resources/db.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    src/clipboard.h \
    src/clipboarddata.h \
    src/mimedatatools.h \
    src/trayicon.h \
    src/qxtglobalshortcut_p.h \
    src/qxtglobalshortcut.h \
    src/clipboardmodel.h \
    src/clipboardproxymodel.h \
    #src/previewimageprovider.h \
    src/clipboardlocaldb.h \
    src/clipboarddataprovider.h \
    src/deviceinfo.h \
    src/clipsterconfig.h \
    src/webserviceclient.h \
    src/commonresponse.h \
    src/loginwebview.h \
    src/whoamiresponse.h \
    src/clipboardapplicationmanager.h \
    src/dataentrylistresponse.h \
    src/onlineclipboard.h \
    src/networkreplyparser.h \
    src/clipboarddatamanager.h \
    src/transferreplywrapper.h \
    src/singleapplication.h \
    src/httpheaders.h \
    src/stringtools.h \
    src/multiparthandler.h \
    src/successresponse.h \
    src/wrapper.h \
    src/clipboardentrydelegate.h \
    src/clipboardentryview.h \
    src/searchfield.h \
    src/teammanagerwebview.h \
    src/roundedpopup.h \
    src/teamlistresponse.h \
    src/team.h \
    src/teammanager.h \
    src/roundedpopupbutton.h \
    src/clipboardmodelinterface.h \
    src/clipsterhost.h
    #src/clipboardbrowser.h

OBJECTIVE_SOURCES += \
    src/mac/pasteboard.mm

LIBS += \
    -L../lib/cliplib/bin -lcliplib -L../lib/crashlib/bin -lcrashlib -L../lib/logview/bin -llogview

INCLUDEPATH += \
    ../lib/cliplib/include \
    ../lib/crashlib/include \
    ../lib/logview/include

FORMS +=

DEFINES += GIT_VERSION=\\\"$$GIT_VERSION\\\"
DEFINES -= USE_CLIPSTER_DEV_SERVER
