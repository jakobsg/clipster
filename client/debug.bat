set EXTRA_COMPILER_FLAGS=
set EXTRA_LINKER_FLAGS=
set DEBUG=true
cd ..\lib\cliplib
qmake "CONFIG+=debug"
jom clean
jom -j4
cd ..\crashlib
qmake "CONFIG+=debug"
jom clean
jom -j4
cd ..\..\crashinfo
qmake "CONFIG+=debug"
jom clean
jom -j4
cd ..\client
rm debug -Rf
qmake "CONFIG+=debug"
jom clean
jom -j4
copy ..\lib\cliplib\bin\cliplib.dll debug
copy ..\lib\crashlib\bin\crashlib.dll debug
copy ..\crashinfo\bin\crashinfo.exe debug
windeployqt debug\Clipster.exe --debug
copy vcdist\vcredist_%BUILD_PLATFORM%.exe debug
iscc win-deploy-debug.iss
