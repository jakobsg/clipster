set EXTRA_COMPILER_FLAGS=-Zi
set EXTRA_LINKER_FLAGS="/DEBUG /OPT:REF"
set DEBUG=
rm release-%BUILD_PLATFORM% -Rf
qmake
nmake clean
nmake
windeployqt --qmldir src\qml release-%BUILD_PLATFORM%\Clipster.exe --release-with-debug-info
copy vcdist\vcredist_%BUILD_PLATFORM%.exe release-%BUILD_PLATFORM%
iscc win-deploy-release.iss
