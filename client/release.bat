set EXTRA_COMPILER_FLAGS=
set EXTRA_LINKER_FLAGS=
set DEBUG=
cd ..\crashlib
qmake
jom clean
jom -j4
cd ..\lib\cliplib
qmake
jom clean
jom -j4
cd ..\logview
qmake
jom clean
jom -j4
cd ..\..\crashinfo
qmake
jom clean
jom -j4
cd ..\client
' rm release-%BUILD_PLATFORM% -Rf
rm release -Rf
qmake
jom clean
jom -j4
'copy ..\lib\cliplib\bin\cliplib.dll release-%BUILD_PLATFORM%
copy ..\lib\cliplib\bin\cliplib.dll release
copy ..\lib\logview\bin\logview.dll release
copy ..\lib\crashlib\bin\crashlib.dll release
copy ..\crashinfo\bin\crashinfo.exe release
'windeployqt --qmldir src\qml release-%BUILD_PLATFORM%\Clipster.exe --release
windeployqt release\Clipster.exe --release
'copy vcdist\vcredist_%BUILD_PLATFORM%.exe release-%BUILD_PLATFORM%
copy vcdist\vcredist_%BUILD_PLATFORM%.exe release
iscc win-deploy-release.iss
scp -i .ssh\id_rsa release-msi/setup-%BUILD_PLATFORM%.exe jakob@clipster-dev.limbosoft.com:downloads/clipster-dev/setup-%BUILD_PLATFORM%.exe
