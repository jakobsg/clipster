create table cb_entry (
    cb_entry_id INTEGER PRIMARY KEY AUTOINCREMENT,
    data_hash TEXT NOT NULL,
    copytime INTEGER NOT NULL,
    data_available INTEGER NOT NULL DEFAULT 0,
    data_origin INTEGER NOT NULL DEFAULT 0,
    data_type INTEGER NOT NULL DEFAULT 0,
    owner TEXT default NULL,
    hostname TEXT default NULL,
    os_type TEXT default NULL,
    team_name TEXT default NULL,
    cloud_remove_right INT NOT NULL DEFAULT 0,
    text_preview TEXT,
    image_preview BLOB,
    unique(data_hash)
);

create table team (
    team_id INTEGER PRIMARY KEY AUTOINCREMENT,
    team_owner TEXT NOT NULL,
    teamname TEXT NOT NULL
);
CREATE UNIQUE INDEX uni_team ON team(team_owner, teamname);

create table team_cb_entry (
    fk_team_id INTEGER NOT NULL,
    fk_cb_entry_id INTEGER NOT NULL
);

CREATE UNIQUE INDEX uni_team_entry ON team_cb_entry(fk_team_id, fk_cb_entry_id);
