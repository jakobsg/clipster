#include "clipboard.h"
#include "mimedatatools.h"
#include "clipboardlocaldb.h"
#include "clipboarddatamanager.h"
#include "clipboardapplicationmanager.h"
#include "webserviceclient.h"
#include <QApplication>
#include <QMimeData>
#include <QTimer>
#include <QImage>
#include <crashlib.h>

#ifdef DARWIN_OS
#include "mac/pasteboard.h"
#endif

// Singleton list for managing instances of the Clipboard class.
// the static method Clipboard::getClipboardInstances() gives access
// to this list. It is used from other classes to send messages to all
// Clipboard instances.
ClipboardList Clipboard::s_instances;

Clipboard::Clipboard(QObject *parent)
    : ClipboardDataProvider(parent)
    , m_clipboard(QApplication::clipboard()) {
    CALL_LOGGER();
	LOG_WARNING("Creating Clipboard instance");
#ifdef DARWIN_OS
    // Mac OS X lacks a global clipboard-data-ready event. There is no way for an
    // Application to recieve notification about new data on the clipboard from
    // another application. Therefore we must poll for this information constantly.
    // Fortunately there is an easy way to do this using the read-only property
    // [[NSPasteboard generalPasteboard] changeCount] which increments by one
    // every time any program makes a copy.

    // store the initial Pasteboard change count
    m_change_count = getChangeCount();

    // Create a polling timer and connect it to checkMacPasteboardChange() slot.
    m_poll_clipboard = new QTimer(this);
    m_poll_clipboard->setInterval(500);
    m_poll_clipboard->start();
    connect(m_poll_clipboard,SIGNAL(timeout()),this,SLOT(checkMacPasteboardChange()));
#endif

    // Register Clipboard instanse
    Clipboard::s_instances.push_back(this);

    // Initialize current and next data origin to default LocalCopy
    m_current_data_origin = ClipboardData::LocalCopy;
    m_next_data_origin = ClipboardData::LocalCopy;

    // Connect QClipboard::dataChanged() to Clipboard::dataChanged() slot in order to
    // recieve events about clipboard copies.
    QTimer *data_deferrence_timer = new QTimer(this);
    data_deferrence_timer->setInterval(150);
    data_deferrence_timer->setSingleShot(true);
    connect(data_deferrence_timer,SIGNAL(timeout()),this,SLOT(dataChanged()));
    connect(m_clipboard, SIGNAL(dataChanged()), data_deferrence_timer, SLOT(start()));
    connect(WebServiceClient::instance(),SIGNAL(dataUploadDone(const QString&)),this,SLOT(processDataUpload(const QString&)),Qt::QueuedConnection);
    connect(WebServiceClient::instance(),SIGNAL(dataDownloadDone(const QString&)),this,SLOT(processDataDownload(const QString&)),Qt::QueuedConnection);
    connect(WebServiceClient::instance(),SIGNAL(dataTransferProgressChanged(const QString&,float)),this,SLOT(processDataTransferProgress(const QString&, float)),Qt::QueuedConnection);
}

// The following method only applies to Mac OS X systems. It reads the Pasteboards
// changeCount property and calls Clipboard::dataChange() if the value has changed
// since last poll. This method is called by the m_poll_clipboard QTimer and essentially
// is a workaround for the lack of QClipboard::dataChanged() signals on Max OS X.
void Clipboard::checkMacPasteboardChange() {
#ifdef DARWIN_OS
    CALL_LOGGER();
    int cc = getChangeCount();
    if (cc!=m_change_count) {
        // Copy has been made since last poll.
        dataChanged();
        m_change_count = cc;
    }
#endif
}

void Clipboard::processDataDownload(const QString &data_hash) {
    CALL_LOGGER();
    bool ok = setDataAvailable(data_hash,true);
    if (ok) {
        ClipboardData *cb_data = ClipboardDataManager::instance()->clipboardDataByDataHash(data_hash);
        if (cb_data) {
            QMimeData md;
            bool data_available = MimeDataTools::instance()->deserializeMimeData(data_hash,md);
            if (data_available) {
                cb_data->setData(md,ClipboardData::RemoteDistributedCopy);
                activateClipboardData(cb_data);
                ClipboardApplicationManager::instance()->showMessage(tr("Clipster Download"),tr("Internet clipboard data ready"));
            }
        }
    }
}

void Clipboard::processDataUpload(const QString &data_hash) {
    ClipboardData *cb_data = ClipboardDataManager::instance()->clipboardDataByDataHash(data_hash);
    if (cb_data) {
        cb_data->setDataOrigin(ClipboardData::RemoteDistributedCopy);
        ClipboardLocalDB::instance()->updateValue(data_hash,"data_origin",(int) ClipboardData::RemoteDistributedCopy);
        ClipboardDataManager::instance()->processClipboardData(cb_data);
    }
}

void Clipboard::processDataTransferProgress(const QString &data_hash, float progress) {
    CALL_LOGGER();
    ClipboardData *cb_data = ClipboardDataManager::instance()->clipboardDataByDataHash(data_hash);
    if (cb_data) {
        cb_data->setTransferProgress(progress);
        ClipboardDataManager::instance()->processClipboardData(cb_data);
    }
}

// Set data origin of the next incoming clipboard data entry. This will
// Default is ClipboardData::LocalCopy which equals to data from the local
// system clipboard.
void Clipboard::setNextDataOrigin(ClipboardData::DataOrigin data_origin) {
    CALL_LOGGER();
    m_next_data_origin = data_origin;
}

// Handle incoming clipboard data
void Clipboard::dataChanged() {
    CALL_LOGGER();
    // Store the data origin of the incoming clipboard data
    m_current_data_origin = m_next_data_origin;
    // Reset the data origin of the next incoming clipboard data to default LocalCopy
    m_next_data_origin = ClipboardData::LocalCopy;


    QByteArray clipster_data_hash = m_clipboard->mimeData()->data("application/x-clipster-data-hash");
    LOG_TRACE(QString("Clipster clip: %1").arg(clipster_data_hash.data()));
    // Signal that clipboard data is ready via all Clipboard instances.
    if (clipster_data_hash.isNull() || m_current_data_origin!=ClipboardData::InAppSelectionCopy) {
        ClipboardData *entry = new ClipboardData(*(m_clipboard->mimeData()), m_current_data_origin);
        addClipboardData(entry);
        emit clipboardEntryReady();
    }
}

// Inject clipboard data on the system clipboard.
void Clipboard::activateClipboardData(ClipboardData *data) {
    CALL_LOGGER();
    // Simulate crashes - START
    // Segfault:
    //
    // std::exception:
//    char *bla = 0;
//    std::string test = bla;
    // Simulate crashes - END
    if (!data) {
        return;
    }
    if (!data->dataAvailable()) {
        WebServiceClient::instance()->download(data->dataHash());
        return;
    }
    // Tell the clipboard that the next incoming clipboard data isM from the application
    // itself and therefore should be ignored as new clipboard data. Otherwise the
    // data selected in the clipboard manager will be added as yet another clipboard
    // data entry.
    setNextDataOrigin(ClipboardData::InAppSelectionCopy);
    QMimeData *newclip = new QMimeData;
#ifdef DARWIN_OS
    // This is nessecary on Mac OS X to solve the same problem as described above.
    m_change_count++;
#endif
    // Insert the data on the clipboard.
    QString hash = MimeDataTools::instance()->copyMimeData(data->data(),newclip);
    newclip->setData("application/x-clipster-data-hash", data->dataHash().toLatin1());
    m_clipboard->setMimeData(newclip);
    ClipboardApplicationManager::instance()->hide();
}

// Get the singleton list og Clipboard instanses.
ClipboardList &Clipboard::getClipboardInstances() {
    CALL_LOGGER();
    return Clipboard::s_instances;
}
