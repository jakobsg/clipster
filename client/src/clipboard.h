#ifndef CLIPBOARD_H
#define CLIPBOARD_H

#include <QObject>
#include <QClipboard>
#include "clipboarddata.h"
#include "clipboarddataprovider.h"

class QTimer;

class Clipboard;

typedef QList<Clipboard *> ClipboardList;

class Clipboard : public ClipboardDataProvider {

    Q_OBJECT

public:

    explicit Clipboard(QObject *parent=0);
    Q_INVOKABLE void activateClipboardData(ClipboardData *data);
    void setNextDataOrigin(ClipboardData::DataOrigin data_origin=ClipboardData::LocalCopy);
    static ClipboardList &getClipboardInstances();

public Q_SLOTS:
    void dataChanged();
    void checkMacPasteboardChange();
    void processDataDownload(const QString &data_hash);
    void processDataUpload(const QString &data_hash);
    void processDataTransferProgress(const QString &data_hash, float progress);

Q_SIGNALS:
    void clipboardEntryReady();

private:
    ClipboardData::DataOrigin m_next_data_origin;
    ClipboardData::DataOrigin m_current_data_origin;
    QClipboard *m_clipboard;
    static ClipboardList s_instances;
#ifdef DARWIN_OS
    QTimer *m_poll_clipboard;
    int m_change_count;
#endif
};

#endif // CLIPBOARD_H

