#include "clipboardapplicationmanager.h"
#include "clipboarddatamanager.h"
#include "webserviceclient.h"
#include "loginwebview.h"
#include "trayicon.h"
#include "clipboardlocaldb.h"
#include "onlineclipboard.h"
#include "mimedatatools.h"
#include "clipsterconfig.h"
#include "clipboardmodel.h"
#include "clipboardproxymodel.h"
#include "clipboardentrydelegate.h"
#include "clipboardentryview.h"
#include "clipboardbrowser.h"
#include "searchfield.h"
#include "roundedpopup.h"
#include "teammanagerwebview.h"
#include "teammanager.h"
#include "logview.h"

#include <crashlib.h>

#include <QLayout>
#include <QToolButton>
#include <QDesktopWidget>
#include <QLineEdit>
#include <QApplication>
#include <QWindow>
#include <QTimer>
#include <QSystemTrayIcon>
#include <QTimeZone>
#include <QLabel>
#include <QSizeGrip>
#include <QMouseEvent>

#define UPDATE_CLIPBOARD_FREQUENCE 5000
#define CHECK_PENDING_TEAMACTIONS_FREQUENCE 7000

extern "C" int click(int button);
extern "C" int mousemove(int x, int y, int doclick);

ClipboardApplicationManager *ClipboardApplicationManager::s_instance = 0;

MoveGrip::MoveGrip(QWidget *clientwidget, QWidget *parent)
: QLabel(parent) {
    CALL_LOGGER();
    m_client = clientwidget;
    setMouseTracking(true);
}

MoveGrip::~MoveGrip() {
}

void MoveGrip::enterEvent(QEvent *e) {
    QWidget::enterEvent(e);
    QApplication::setOverrideCursor(Qt::SizeAllCursor);
}

void MoveGrip::leaveEvent(QEvent *e) {
    QWidget::leaveEvent(e);
    QApplication::restoreOverrideCursor();
}

void MoveGrip::mousePressEvent(QMouseEvent *me) {
    CALL_LOGGER();
    QLabel::mouseMoveEvent(me);
    m_client_start = m_client->pos();
    m_drag_start = QCursor::pos();
}

void MoveGrip::mouseMoveEvent(QMouseEvent *me) {
    QLabel::mouseMoveEvent(me);
    if ((me->buttons()&Qt::LeftButton)==Qt::LeftButton) {
        QPoint p = QCursor::pos();
        QPoint delta = p-m_drag_start;
        m_client->move(m_client_start+delta);
    }
}

void MoveGrip::mouseReleaseEvent(QMouseEvent *me) {
    CALL_LOGGER();
    QLabel::mouseReleaseEvent(me);
}


ClipboardApplicationManager::ClipboardApplicationManager(QWidget *parent, Qt::WindowFlags f) :
QWidget(parent, f) {
    CALL_LOGGER();
    setupUi();
    m_team_management_invoked = false;
    m_pending_team_invitations = 0;
    m_pending_team_member_accepts = 0;
    m_login_web_view = 0;
    m_team_management_web_view = 0;
    m_online_clipboard = new OnlineClipboard(this);
    connect(WebServiceClient::instance(),SIGNAL(whoamiResponseReady(const WhoamiResponse&)),this,SLOT(processWhoamiResponse(const WhoamiResponse&)));
    connect(WebServiceClient::instance(),SIGNAL(registerDeviceResponseReady(const SuccessResponse&)),this,SLOT(processRegisterDeviceResponse(const SuccessResponse&)));
    connect(WebServiceClient::instance(),SIGNAL(pendingTeamInvitationsResponseReady(int)),this,SLOT(setPendingTeamInvitations(int)));
    connect(WebServiceClient::instance(),SIGNAL(pendingTeamMemberAcceptsResponseReady(int)),this,SLOT(setPendingTeamMemberAccepts(int)));
    connect(TeamManager::instance(),SIGNAL(teamsUpdated()),this,SLOT(teamsUpdated()));
    WebServiceClient::instance()->whoami();
    m_deferred_click = new QTimer(this);
    m_deferred_click->setSingleShot(true);
    connect(m_deferred_click,SIGNAL(timeout()),this,SLOT(clickSearch()));
    m_cb_list_poll_timer = new QTimer(this);
    m_cb_list_poll_timer->setInterval(UPDATE_CLIPBOARD_FREQUENCE);
    m_cb_list_poll_timer->setSingleShot(false);
    m_cb_list_poll_timer->stop();
    connect(m_cb_list_poll_timer,SIGNAL(timeout()),this,SLOT(updateOnlineClipboardData()));
    m_pending_poll_timer = new QTimer(this);
    m_pending_poll_timer->setInterval(CHECK_PENDING_TEAMACTIONS_FREQUENCE);
    m_pending_poll_timer->setSingleShot(false);
    m_pending_poll_timer->stop();
    connect(m_pending_poll_timer,SIGNAL(timeout()),this,SLOT(updatePendingTeamActions()));
    setMouseTracking(true);
    //addImageProvider("preview",previewImageProvider());
}

ClipboardApplicationManager *ClipboardApplicationManager::instance(QWidget *parent, Qt::WindowFlags f) {
    CALL_LOGGER();
//    int *a = 0;
//    *a = 1;
    if (s_instance==0) {
        s_instance = new ClipboardApplicationManager(parent,f);
        s_instance->hide();
        s_instance->m_trayicon = new TrayIcon(s_instance);
    }
    else {
        s_instance->setParent(parent);
    }
	LOG_TRACE("Fetched the ClipboardApplicationManager instance");
    return s_instance;
}

TrayIcon *ClipboardApplicationManager::trayIcon() {
    CALL_LOGGER();
    return m_trayicon;
}

void ClipboardApplicationManager::showMessage(const QString &title, const QString &msg, int duration_msecs) {
    CALL_LOGGER();
    m_trayicon->trayIcon()->showMessage(title, msg, QSystemTrayIcon::Information, duration_msecs);
}

void ClipboardApplicationManager::setupUi() {
    CALL_LOGGER();

    setGeometry(100,100,850,650);
    setWindowIcon(QIcon(":/images/clipboard-icon.icns"));
    setWindowFlags(windowFlags()|Qt::WindowStaysOnTopHint|Qt::FramelessWindowHint);
    setAttribute(Qt::WA_TranslucentBackground);
    m_baseFrame = new QFrame(this);
    m_sizeGripRight = new QSizeGrip(this);
    m_sizeGripRight->setFixedSize(11,11);
    m_sizeGripLeft = new QSizeGrip(this);
    m_sizeGripLeft->setFixedSize(11,11);
    m_sizeGripBottom = new QSizeGrip(this);
    m_sizeGripBottom->setFixedSize(11,11);
    m_widgetLayout = new QVBoxLayout(this);
    setLayout(m_widgetLayout);
    m_widgetLayout->addWidget(m_baseFrame);
    m_baseFrame->setStyleSheet("QFrame {background-color: #eee; border: 1px solid #bbb;border-radius: 7px};");
    m_baseFrame->setAttribute(Qt::WA_MacShowFocusRect, false);
    m_baseFrameLayout = new QGridLayout(m_baseFrame);
    m_baseFrame->setLayout(m_baseFrameLayout);
    m_baseFrameLayout->setContentsMargins(0,9,0,0);
    m_baseFrameLayout->setSpacing(0);
    m_topLayout = new QHBoxLayout(m_baseFrame);
    m_baseFrameLayout->addLayout(m_topLayout,0,1,1,1);
    m_searchIcon = new QLabel(m_baseFrame);
    m_searchIcon->setPixmap(QPixmap(":/images/magnifying-glass.png"));
    m_searchIcon->setStyleSheet("border: 0px;border-radius: 0px;");
    m_topLayout->addWidget(m_searchIcon);
    m_topLayout->addItem(new QSpacerItem(8,2,QSizePolicy::Fixed, QSizePolicy::Fixed));
    m_searchField = new SearchField(m_baseFrame);
    m_searchField->setStyleSheet("background-color: #eee; border: 0px; border-radius: 0px;");
    QFont lef = m_searchField->font();
    lef.setPixelSize(32);
    m_searchField->setFont(lef);
    m_searchField->setSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::Preferred);
    m_searchField->setPlaceholderText(tr("Clipboard Search"));
    m_searchField->setAttribute(Qt::WA_MacShowFocusRect, false);
    m_topLayout->addWidget(m_searchField);
    m_topLayout->addItem(new QSpacerItem(16,2,QSizePolicy::Fixed, QSizePolicy::Fixed));
    m_moveGrip = new MoveGrip(this,m_baseFrame);
    m_moveGrip->setPixmap(QPixmap(":/images/movegrip.png"));
    m_moveGrip->setStyleSheet("QLabel {border: none; margin: 0px; padding: 0px;}");
    m_topLayout->addWidget(m_moveGrip);
    m_topLayout->addItem(new QSpacerItem(16,2,QSizePolicy::Fixed, QSizePolicy::Fixed));
    m_teamButton = new QToolButton(m_baseFrame);
    m_teamButton->setIcon(QIcon(":/images/team_32.png"));
    m_teamButton->setStyleSheet("QToolButton {border: none; margin: 0px; padding: 0px;}");
    m_teamButton->setFixedSize(28,28);
    m_teamButton->setIconSize(QSize(28,28));
    QObject::connect(m_teamButton, SIGNAL(clicked()), this, SLOT(manageTeams()));
    m_topLayout->addWidget(m_teamButton);
    m_pending_team_actions_lbl = new QLabel(" 0 ",m_teamButton);
    m_pending_team_actions_lbl->setStyleSheet("color: white; font-weight: bold; background-color: green; border: 0; border-radius:2px");
    QFont f = m_pending_team_actions_lbl->font();
    f.setPixelSize(10);
    m_pending_team_actions_lbl->setFont(f);
    m_pending_team_actions_lbl->setVisible(false);
    m_topLayout->addItem(new QSpacerItem(16,2,QSizePolicy::Fixed, QSizePolicy::Fixed));
    m_closeButton = new QToolButton(m_baseFrame);
    m_closeButton->setIcon(QIcon(":/images/close.png"));
    m_closeButton->setStyleSheet("QToolButton {border: none; margin: 0px; padding: 0px;}");
    m_closeButton->setFixedSize(28,28);
    m_closeButton->setIconSize(QSize(28,28));
    QObject::connect(m_closeButton, SIGNAL(clicked()), this, SLOT(close()));
    m_topLayout->addWidget(m_closeButton);
    m_topLayout->addItem(new QSpacerItem(8,2,QSizePolicy::Fixed, QSizePolicy::Fixed));
    m_clipboardEntryView = new ClipboardEntryView(m_baseFrame);
    m_clipboardEntryView->setStyleSheet("border: 0px;border-radius: 0px;background-color: #eee;");
    m_clipboardEntryView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_clipboardEntryDelegate = new ClipboardEntryDelegate(m_clipboardEntryView);
    m_clipboardEntryView->setItemDelegate(m_clipboardEntryDelegate);
    m_clipboardEntryView->setAttribute(Qt::WA_MacShowFocusRect, false);
    m_clipboardModel = new ClipboardModel(m_baseFrame);
    m_clipboardProxyModel = new ClipboardProxyModel(m_clipboardModel);
    m_clipboardProxyModel->setSourceClipboardModel(m_clipboardModel);
    m_clipboardEntryView->setModel(m_clipboardProxyModel);
    connect(m_clipboardEntryView, SIGNAL(entered(QModelIndex)), this, SLOT(fastClosePopup()));

    connect(m_searchField, SIGNAL(upPressed()), m_clipboardEntryView, SLOT(moveCurrentUpOne()));
    connect(m_searchField, SIGNAL(downPressed()), m_clipboardEntryView, SLOT(moveCurrentDownOne()));
    connect(m_searchField, SIGNAL(returnPressed()), m_clipboardEntryView, SLOT(activateCurrent()));
    connect(m_searchField, SIGNAL(escapePressed()), this, SLOT(close()));
    connect(m_searchField, SIGNAL(textChanged(QString)), m_clipboardProxyModel, SLOT(setFilterWildcard(QString)));
    connect(m_clipboardEntryView, SIGNAL(onlineSettingsTriggered(ClipboardData*)), this, SLOT(showOnlineSettings(ClipboardData*)));

    m_baseFrameLayout->addItem(new QSpacerItem(2,6,QSizePolicy::Fixed, QSizePolicy::Fixed),1,1,1,1);
    m_baseFrameLayout->addWidget(m_clipboardEntryView,2,1,1,1);
    m_baseFrameLayout->addWidget(m_sizeGripLeft,3,0,1,1,Qt::AlignBottom | Qt::AlignRight);
    m_baseFrameLayout->addWidget(m_sizeGripRight,3,2,1,1,Qt::AlignBottom | Qt::AlignRight);
    //m_baseFrameLayout->addWidget(m_sizeGripBottom,3,1,1,1,Qt::AlignBottom | Qt::AlignCenter);
    move(QApplication::desktop()->screen()->rect().center() - rect().center());
    m_popup = new RoundedPopup(0);
    m_popup->raise();
    connect(m_popup, SIGNAL(actionTriggered(QAction*)), this, SLOT(popupActionTriggered(QAction*)));
    m_share_action = new QAction(QIcon(":/images/team_32.png"),tr("Share with team"),m_popup);
    m_cloud_remove_action = new QAction(QIcon(":/images/cloud-out.png"),tr("Remove from cloud"),m_popup);
    connect(m_share_action, SIGNAL(triggered(bool)), this, SLOT(popupTeams()));
    m_popup->addAction(m_share_action);
    m_popup->addAction(m_cloud_remove_action);
}

ClipboardApplicationManager::~ClipboardApplicationManager() {
    CALL_LOGGER();
}

void ClipboardApplicationManager::fastClosePopup() {
    //m_popup->fastClose();
    m_popup->close();
}

void ClipboardApplicationManager::updatePendingTeamActions() {
    WebServiceClient::instance()->pendingTeamInvitations();
    WebServiceClient::instance()->pendingTeamMemberAccepts();
}

void ClipboardApplicationManager::setPendingTeamInvitations(int number) {
    m_pending_team_invitations = number;
    m_pending_team_actions = m_pending_team_invitations + m_pending_team_member_accepts;
    m_pending_team_actions_lbl->setText(QString(" %1 ").arg(m_pending_team_actions));
    m_pending_team_actions_lbl->setVisible(m_pending_team_actions!=0);
}

void ClipboardApplicationManager::setPendingTeamMemberAccepts(int number) {
    m_pending_team_member_accepts = number;
    m_pending_team_actions = m_pending_team_invitations + m_pending_team_member_accepts;
    m_pending_team_actions_lbl->setText(QString(" %1 ").arg(m_pending_team_actions));
    m_pending_team_actions_lbl->setVisible(m_pending_team_actions!=0);
}

void ClipboardApplicationManager::teamsUpdated() {
    m_teams = TeamManager::instance()->teamList();
}

void ClipboardApplicationManager::popupTeams() {
    foreach (QAction *a, m_popup->actions()) {
        m_popup->removeAction(a);
        if (a->data().toString()=="team") {
            delete a;
        }
    }
    foreach (Team t, m_teams) {
        QAction *team_action = new QAction(QIcon(":/images/team_32.png"), t.teamname(),m_popup);
        team_action->setData("team");
        team_action->setWhatsThis(t.owner());
        m_popup->addAction(team_action);
    }
}

void ClipboardApplicationManager::popupActionTriggered(QAction *action) {
    if (action->data().toString()=="team") {
        QString owner = action->whatsThis();
        QString teamname = action->text();
        QString data_hash = m_current_data_hash;
        WebServiceClient::instance()->teamShareDataEntry(owner, teamname, data_hash);
    }
}

void ClipboardApplicationManager::closeEvent(QCloseEvent *) {
    fastClosePopup();
}

void ClipboardApplicationManager::clickSearch() {
    CALL_LOGGER();
    QPoint lineedit_pos = m_searchField->pos()+QPoint(15,15);
    QPoint old_pos = QCursor::pos();
    int x = m_searchField->mapToGlobal(lineedit_pos).x();
    int y = m_searchField->mapToGlobal(lineedit_pos).y();
    mousemove(x+70,y+20,1);
    // Mouse back to where it came from.
    mousemove(old_pos.x(), old_pos.y(),0);
}

void ClipboardApplicationManager::processWhoamiResponse(const WhoamiResponse &resp) {
    CALL_LOGGER();
    qDebug("whoami resp");
    if (resp.m_res_code==10301) {
        setVisible(false);
        login();
    }
    else if (resp.m_res_code==-1) {
        ClipboardApplicationManager::instance()->showMessage(tr("Network problem"),tr("You do not appear to be internet connected."));
    }
    else {
        updateOnlineClipboardData();
        updatePendingTeamActions();
        TeamManager::instance()->updateTeamList();
        m_cb_list_poll_timer->start();
        m_pending_poll_timer->start();
#ifdef DARWIN_OS
        showMessage(tr("Clipster Usage Hint"),tr("Press Command+Shift+M to hide and show the Clipster Manager."),6000);
#else
        showMessage(tr("Clipster Usage Hint"),tr("Press Control+Shift+M to hide and show the Clipster Manager."),6000);
#endif
    }
}

void ClipboardApplicationManager::processRegisterDeviceResponse(const SuccessResponse &resp) {
    CALL_LOGGER();
    if (resp.m_res_code==0) {
        if (resp.m_success) {
            WebServiceClient::instance()->whoami();
        }
        else {
            qApp->exit();
        }
    }
}

void ClipboardApplicationManager::manageTeams() {
    m_team_management_invoked = true;
    if (m_team_management_web_view==0) {
        m_team_management_web_view = new TeamManagementWebView();
    }
    QRect desktopRect = QApplication::desktop()->availableGeometry();
    QPoint center = desktopRect.center();

    m_team_management_web_view->move(center.x()-m_team_management_web_view->width()*0.5, center.y()-m_team_management_web_view->height()*0.5);
    m_team_management_web_view->startManagement();
    m_team_management_web_view->raise();
    toggleVisible();
}

void ClipboardApplicationManager::updateOnlineClipboardData() {
    CALL_LOGGER();
    WebServiceClient::instance()->listDataEntries();
    WebServiceClient::instance()->listTeamDataEntries();
}

void ClipboardApplicationManager::clearLocalClipboardEntries() {
    CALL_LOGGER();
    ClipboardLocalDB::instance()->deleteClipboardEntries();
    MimeDataTools::instance()->deleteDataDirectory();
	ClipsterConfig::removeConfigValue("data_timestamp");
    ClipboardDataManager::instance()->deleteAll();
}

void ClipboardApplicationManager::simulateSEGVCrash() {
    CALL_LOGGER();
    int *a = 0;
    *a = 123;
}

void ClipboardApplicationManager::simulateSECrash() {
    CALL_LOGGER();
    std::string jakob = NULL;
}

void ClipboardApplicationManager::showOnlineSettings(ClipboardData *cd) {
    foreach (QAction *a, m_popup->actions()) {
        m_popup->removeAction(a);
        if (a->data().toString()=="team") {
            delete a;
        }
    }
    m_current_data_hash = cd->dataHash();
    m_popup->addAction(m_share_action);
    m_popup->addAction(m_cloud_remove_action);
    m_popup->popup();
}

void ClipboardApplicationManager::toggleVisible() {
    CALL_LOGGER();
    LOG_ERROR("The user toggled clipboard manager visibility using CTRL-Shift+M on the freaking keyboard");
    if (!isVisible()) {
        TeamManager::instance()->updateTeamList();
        m_clipboardEntryView->scrollToTop();
        m_deferred_click->start(250);
    }
    setVisible(!isVisible());
}

void ClipboardApplicationManager::login() {
    CALL_LOGGER();
    if (m_login_web_view==0) {
        m_login_web_view = new LoginWebView();
    }
    QRect desktopRect = QApplication::desktop()->availableGeometry();
    QPoint center = desktopRect.center();

    m_login_web_view->move(center.x()-m_login_web_view->width()*0.5, center.y()-m_login_web_view->height()*0.5);
    m_login_web_view->startLogin();
}
