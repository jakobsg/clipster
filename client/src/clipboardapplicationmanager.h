#ifndef CLIPBOARDMANAGERAPPLICATION_H
#define CLIPBOARDMANAGERAPPLICATION_H

#include "whoamiresponse.h"
#include "successresponse.h"
#include "teamlistresponse.h"
#include "team.h"
#include <QWidget>
#include <QLabel>

class WebServiceClient;
class QTimer;
class LoginWebView;
class TeamManagementWebView;
class TrayIcon;
class OnlineClipboard;
class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class SearchField;
class QToolButton;
class QFrame;
class ClipboardEntryView;
class ClipboardEntryDelegate;
class ClipboardModel;
class ClipboardProxyModel;
class QSizeGrip;
class RoundedPopup;
class Clipboard;

class MoveGrip : public QLabel {

    Q_OBJECT

public:
    MoveGrip(QWidget *clientwidget, QWidget *parent=0);
    ~MoveGrip();

protected:
    void enterEvent(QEvent *);
    void leaveEvent(QEvent *);
    void mousePressEvent(QMouseEvent *me);
    void mouseMoveEvent(QMouseEvent *me);
    void mouseReleaseEvent(QMouseEvent *me);

private:
    QWidget *m_client;
    QPoint m_drag_start;
    QPoint m_client_start;
};

class ClipboardApplicationManager : public QWidget {

    Q_OBJECT

public:
    static ClipboardApplicationManager *instance(QWidget *parent=0, Qt::WindowFlags f=0);
    ~ClipboardApplicationManager();
    TrayIcon *trayIcon();
    void showMessage(const QString &title,const QString &msg, int duration_msecs=3000);
    void setupUi();

protected:
    void closeEvent(QCloseEvent *);

public Q_SLOTS:
    void login();
    void toggleVisible();
    void clickSearch();
    void processWhoamiResponse(const WhoamiResponse &resp);
    void processRegisterDeviceResponse(const SuccessResponse &resp);
    void updateOnlineClipboardData();
	void clearLocalClipboardEntries();
    void simulateSEGVCrash();
    void simulateSECrash();
    void manageTeams();
    void showOnlineSettings(ClipboardData *cd);
    void fastClosePopup();
    void updatePendingTeamActions();
    void setPendingTeamInvitations(int number);
    void setPendingTeamMemberAccepts(int number);
    void teamsUpdated();
    void popupTeams();
    void popupActionTriggered(QAction *action);

private:
    ClipboardApplicationManager(QWidget *parent, Qt::WindowFlags f);
    static ClipboardApplicationManager *s_instance;

    QFrame *m_baseFrame;
    QVBoxLayout *m_widgetLayout;
    QGridLayout *m_baseFrameLayout;
    QHBoxLayout *m_topLayout;
    QLabel *m_searchIcon;
    SearchField *m_searchField;
    QToolButton *m_closeButton;
    QToolButton *m_teamButton;
    QLabel *m_pending_team_actions_lbl;
    QSizeGrip *m_sizeGripRight;
    QSizeGrip *m_sizeGripLeft;
    QSizeGrip *m_sizeGripBottom;
    MoveGrip *m_moveGrip;
    ClipboardEntryView *m_clipboardEntryView;
    ClipboardEntryDelegate *m_clipboardEntryDelegate;
    ClipboardModel *m_clipboardModel;
    ClipboardProxyModel *m_clipboardProxyModel;
    RoundedPopup *m_popup;
    QTimer *m_deferred_click;
    TeamManagementWebView *m_team_management_web_view;
    LoginWebView *m_login_web_view;
    OnlineClipboard *m_online_clipboard;
    TrayIcon *m_trayicon;
    QTimer *m_cb_list_poll_timer;
    QTimer *m_pending_poll_timer;
    QAction *m_share_action;
    QAction *m_cloud_remove_action;
    TeamList m_teams;
    QString m_current_data_hash;
    bool m_team_management_invoked;

    int m_pending_team_invitations;
    int m_pending_team_member_accepts;
    int m_pending_team_actions;
};

#endif // CLIPBOARDMANAGERAPPLICATION_H
