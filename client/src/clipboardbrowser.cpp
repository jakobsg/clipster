#include "clipboardmodel.h"
#include "clipboardentrydelegate.h"
#include "clipboardentryview.h"
#include "clipboardbrowser.h"
#include <QLayout>
#include <QApplication>
#include <QToolButton>
#include <QDesktopWidget>
#include <QLineEdit>

ClipboardBrowser::ClipboardBrowser(QWidget *parent, Qt::WindowFlags f)
: QWidget(parent, f){
    setupUi();
}

ClipboardBrowser::~ClipboardBrowser() {

}

void ClipboardBrowser::setupUi() {
    setGeometry(100,100,600,500);
    setWindowFlags(windowFlags()|Qt::WindowStaysOnTopHint|Qt::FramelessWindowHint);
    setAttribute(Qt::WA_TranslucentBackground);
    QFrame *f = new QFrame(this);
    QVBoxLayout *l = new QVBoxLayout(this);
    setLayout(l);
    l->addWidget(f);
    f->setStyleSheet("border: 1px solid #bbb;border-radius: 7px;background-color: #eee;");
    QVBoxLayout *fl = new QVBoxLayout(f);
    f->setLayout(fl);
    QHBoxLayout *top = new QHBoxLayout(f);
    fl->addLayout(top);
    QLineEdit *le = new QLineEdit(f);
    le->setStyleSheet("border: 0px; border-radius: 0px;");
    QFont lef = le->font();
    lef.setPixelSize(32);
    le->setFont(lef);
    le->setSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::Preferred);
    le->setPlaceholderText(tr("Clipboard Search"));
    top->addWidget(le);
    QToolButton *close_button = new QToolButton(f);
    close_button->setIcon(QIcon(":/images/close.png"));
    close_button->setStyleSheet("QToolButton {border: none; margin: 0px; padding: 0px;}");
    QObject::connect(close_button, SIGNAL(clicked()), this, SLOT(close()));
    top->addWidget(close_button);
    ClipboardEntryView *lv = new ClipboardEntryView(f);
    lv->setStyleSheet("border: 0px;border-radius: 0px;background-color: #eee;");
    lv->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ClipboardEntryDelegate *ced = new ClipboardEntryDelegate(lv);
    lv->setItemDelegate(ced);
    ClipboardModel *cm = new ClipboardModel(f);
    lv->setModel(cm);
    fl->addWidget(lv);
    move(QApplication::desktop()->screen()->rect().center() - rect().center());
}

