#ifndef CLIPBOARDBROWSER_H
#define CLIPBOARDBROWSER_H

#include <QWidget>

class ClipboardBrowser : public QWidget
{
    Q_OBJECT

public:
    ClipboardBrowser(QWidget *parent=0, Qt::WindowFlags f=0);
    ~ClipboardBrowser();

    void setupUi();
};

#endif // CLIPBOARDBROWSER_H
