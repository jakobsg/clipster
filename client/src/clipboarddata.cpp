#include "clipboarddata.h"
#include "mimedatatools.h"
#include "clipboardlocaldb.h"
#include "webserviceclient.h"
#include "clipboarddatamanager.h"
#include "clipboardapplicationmanager.h"
#include <QPixmap>
#include <QIcon>
#include <QPainter>
#include <crashlib.h>

#define MAX_PREVIEW_IMAGE_WIDTH 128
#define MAX_PREVIEW_IMAGE_HEIGHT 96
#define MAX_PREVIEW_TEXT_LENGTH 512

void ClipboardData::updatePreviewData() {
    CALL_LOGGER();
    QImage img = qvariant_cast<QImage>(m_data.imageData());
    if (!img.isNull()) {
        setImagePreview(img);
    }
    else {
        ClipsterCore::MediaTypeSet mts(m_data.formats());
        const ClipsterCore::MediaTypeMatchRuleSet *mrs = MimeDataTools::instance()->detectMediaType(mts);
		if (mrs) {
			setImagePreview(QImage(mrs->iconResourceUrl()));
		}
    }
    if (m_text_preview.isEmpty()) {
        setTextPreview( m_data.text().simplified() );
    }
    if (m_text_preview.length()>MAX_PREVIEW_TEXT_LENGTH) {
        m_text_preview = m_text_preview.left(MAX_PREVIEW_TEXT_LENGTH/2) + m_text_preview.right(MAX_PREVIEW_TEXT_LENGTH/2);
    }
}

void ClipboardData::initialize() {
    CALL_LOGGER();
    m_data_available = false;
    m_copytime = QDateTime::currentDateTime();
    m_data_origin = LocalCopy;
    m_transfer_progress = 0.0;
}

ClipboardData::ClipboardData(QObject *parent) : QObject (parent) {
    CALL_LOGGER();
    initialize();
}

ClipboardData::ClipboardData(ClipboardData::DataOrigin data_origin, QObject *parent) : QObject (parent) {
    CALL_LOGGER();
    initialize();
    m_data_origin = data_origin;
}

ClipboardData::ClipboardData(const QMimeData &data, ClipboardData::DataOrigin data_origin, QObject *parent) : QObject (parent) {
    CALL_LOGGER();
    initialize();
    m_data_origin = data_origin;
    setData(data,data_origin);
}

ClipboardData::ClipboardData(const QString &data_hash, QObject *parent) : QObject(parent) {
    CALL_LOGGER();
    initialize();
    m_data_hash = data_hash;
    ClipboardLocalDB::instance()->loadClipboardEntry(data_hash, this);
    m_data_available = MimeDataTools::instance()->deserializeMimeData(data_hash,m_data);
    if (m_data_available) {
        updatePreviewData();
    }
    ClipboardDataManager::instance()->processClipboardData(this);
}

ClipboardData::~ClipboardData() {
    CALL_LOGGER();
}

const QMimeData &ClipboardData::data() const {
    CALL_LOGGER();
    return m_data;
}

void ClipboardData::setData(const QMimeData &mimedata, DataOrigin data_origin) {
    CALL_LOGGER();
    QString old_data_hash = m_data_hash;
    m_data_hash = MimeDataTools::instance()->copyMimeData(mimedata,&m_data);
    if (m_data_hash.isEmpty()) {
        // Clipboard data formats did not match any media type rule sets.
        ClipboardApplicationManager::instance()->showMessage("Unidentified Copy Data","Unable to recognize the media-type passed on by the system clipboard");
        return;
    }
    if (data_origin == ClipboardData::RemoteDistributedCopy) {
        m_data_hash = old_data_hash;
    }
    updatePreviewData();
    MimeDataTools::instance()->serializeMimeData(m_data_hash,m_data);
    m_data_origin = data_origin;
    m_data_available = true;
    if (old_data_hash!=m_data_hash) {
        // Had a data_hash already which has changed.
        ClipboardDataManager::instance()->processClipboardData(this);
    }
}

void ClipboardData::setTransferProgress(float progress) {
    CALL_LOGGER();
    m_transfer_progress = progress;
}

QString ClipboardData::textPreview() const {
    CALL_LOGGER();
    if (!m_text_preview.isEmpty()) {
        return m_text_preview.simplified();
    }
    return QString();
}

QImage ClipboardData::imagePreview() const {
    CALL_LOGGER();
    if (!m_image_preview.isNull()) {
        return m_image_preview;
    }
    return QImage();
}

void ClipboardData::setTextPreview(const QString &text_preview) {
    CALL_LOGGER();
    m_text_preview = text_preview;
}

void ClipboardData::setImagePreview(const QImage &image_preview) {
    CALL_LOGGER();
    if ((float)image_preview.height()/image_preview.width() > 0.75) {
        QPixmap tmp(MAX_PREVIEW_IMAGE_WIDTH,MAX_PREVIEW_IMAGE_HEIGHT);
        tmp.fill(Qt::transparent);
        QImage scaled_img = image_preview.scaledToHeight(MAX_PREVIEW_IMAGE_HEIGHT,Qt::SmoothTransformation);
        QPainter p(&tmp);
        p.drawImage((MAX_PREVIEW_IMAGE_WIDTH-scaled_img.width())/2,0,scaled_img,0,0);
        p.end();
        m_image_preview = tmp.toImage();
    }
    else {
        m_image_preview = image_preview.scaledToWidth(MAX_PREVIEW_IMAGE_WIDTH,Qt::SmoothTransformation);
    }
}

void ClipboardData::setDataOrigin(ClipboardData::DataOrigin data_origin) {
    CALL_LOGGER();
    m_data_origin = data_origin;
}

ClipboardData::DataOrigin ClipboardData::dataOrigin() const {
    CALL_LOGGER();
    return m_data_origin;
}

QString ClipboardData::dataHash() const {
    CALL_LOGGER();
    return m_data_hash;
}

QDateTime ClipboardData::copyTime() const {
    CALL_LOGGER();
    return m_copytime;
}

bool ClipboardData::dataAvailable() const {
    CALL_LOGGER();
    return m_data_available;
}

bool ClipboardData::upload() const {
    CALL_LOGGER();
    return WebServiceClient::instance()->upload(m_data_hash);
}

float ClipboardData::transferProgress() const {
    CALL_LOGGER();
    return m_transfer_progress;
}

void ClipboardData::setCopyTime(const QDateTime &copytime) {
    CALL_LOGGER();
    m_copytime = copytime;
}
