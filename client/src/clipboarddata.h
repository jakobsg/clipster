#ifndef CLIPBOARDENTRY_H
#define CLIPBOARDENTRY_H

#include <QMimeData>
#include <QImage>
#include <QDateTime>

class ClipboardData : public QObject
{
    Q_OBJECT

public:

    enum DataOrigin {
        Unknown = 0,
        InAppSelectionCopy,
        LocalCopy,
        LocalDistributedCopy,
        RemoteDistributedCopy
    };
    Q_ENUMS(DataOrigin)

    ClipboardData(QObject *parent=0);
    ClipboardData(ClipboardData::DataOrigin data_origin, QObject *parent=0);
    ClipboardData(const QMimeData &data, ClipboardData::DataOrigin data_origin, QObject *parent=0);
    ClipboardData(const QString &data_hash, QObject *parent=0);
    ~ClipboardData();

    Q_INVOKABLE QString textPreview() const;
    Q_INVOKABLE QImage imagePreview() const;
    Q_INVOKABLE DataOrigin dataOrigin() const;
    Q_INVOKABLE QString dataHash() const;
    Q_INVOKABLE QDateTime copyTime() const;
    Q_INVOKABLE bool dataAvailable() const;
    Q_INVOKABLE bool upload() const;
    Q_INVOKABLE float transferProgress() const;

    void setCopyTime(const QDateTime &copytime);
    void setTextPreview(const QString &text_preview);
    void setImagePreview(const QImage &image_preview);
    void setDataOrigin(DataOrigin data_origin);
    void setData(const QMimeData &data, DataOrigin data_origin);
    void setTransferProgress(float progress);
    const QMimeData &data() const;

private:
    void updatePreviewData();
    void initialize();

    QMimeData m_data;
    QString m_data_hash;
    DataOrigin m_data_origin;
    QImage m_image_preview;
    QString m_text_preview;
    QString m_username;
    QString m_ostype;
    QString m_hostname;
    QDateTime m_copytime;
    float m_transfer_progress;
    bool m_data_available;
};

#endif // CLIPBOARDENTRY_H
