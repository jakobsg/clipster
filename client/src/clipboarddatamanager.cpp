#include "clipboarddatamanager.h"
#include "clipboarddata.h"
#include "clipsterconfig.h"
#include <QApplication>
#include <crashlib.h>

ClipboardDataManager *ClipboardDataManager::s_cb_data_manager = 0;

ClipboardDataManager::ClipboardDataManager(QObject *parent) : QObject(parent) {
    CALL_LOGGER();
}

ClipboardDataManager *ClipboardDataManager::instance() {
    CALL_LOGGER();
    if (s_cb_data_manager==0) {
        s_cb_data_manager = new ClipboardDataManager(QApplication::instance());
    }
    return s_cb_data_manager;
}

ClipboardDataManager::~ClipboardDataManager() {
    CALL_LOGGER();
}

ClipboardData *ClipboardDataManager::clipboardDataByDataHash(const QString &data_hash) {
    CALL_LOGGER();
    if (m_cb_data_map.contains(data_hash)) {
        return m_cb_data_map[data_hash];
    }
    return 0;
}

ClipboardData *ClipboardDataManager::clipboardDataByHistoryIndex(int history_idx) {
    CALL_LOGGER();
    if (history_idx >= m_cb_data_history.length()) {
        return 0;
    }
    return m_cb_data_map[m_cb_data_history[history_idx]];
}

int ClipboardDataManager::count() const {
    CALL_LOGGER();
    return m_cb_data_history.count();
}

void ClipboardDataManager::deleteAll() {
    CALL_LOGGER();
    m_cb_data_history.clear();
    foreach (ClipboardData *d, m_cb_data_map.values()) {
        delete d;
    }
    m_cb_data_map.clear();
}

void ClipboardDataManager::processClipboardData(ClipboardData *cb_data) {
    CALL_LOGGER();
    ClipboardData *current = 0;
    int cb_idx = m_cb_data_history.indexOf(cb_data->dataHash());
    if (cb_idx>-1) {
        current = m_cb_data_map[cb_data->dataHash()];
        m_cb_data_map[cb_data->dataHash()] = cb_data;
        cb_data->setParent(this);
        if (current && current!=cb_data) {
            // This a new clipboard data entry
            if (cb_idx>0) {
                // It is not first in the history
                m_cb_data_history.move(cb_idx,0);
                emit clipboardHistoryMove(cb_idx,0);
            }
            current->deleteLater();
        }
        else {
            // this is a clipboard data update not new entry
            // Don't change position in history just signal change
            emit clipboardDataChanged(cb_idx);
            emit clipboardDataChanged(cb_data->dataHash());
        }
    }
    else {
        m_cb_data_map[cb_data->dataHash()] = cb_data;
        cb_data->setParent(this);
        m_cb_data_history.insert(0,cb_data->dataHash());
        emit clipboardHistoryInsert(0);
    }
	if (m_cb_data_history.length()>ClipsterConfig::configValue("max_displayed_cb_entries",50).toInt()) {
        QString data_hash = m_cb_data_history.takeLast();
        ClipboardData *oldest = m_cb_data_map[data_hash];
        m_cb_data_map.remove(data_hash);
        if (oldest) {
            oldest->deleteLater();
        }
        emit clipboardHistoryRemove(m_cb_data_history.length());
    }
}
