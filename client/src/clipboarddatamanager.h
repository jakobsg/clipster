#ifndef CLIPBOARDDATAMANAGER_H
#define CLIPBOARDDATAMANAGER_H

#include <QObject>
#include <QMap>
#include <QStringList>

class ClipboardData;

class ClipboardDataManager : public QObject
{
    Q_OBJECT

public:
    static ClipboardDataManager *instance();
    ~ClipboardDataManager();

    void processClipboardData(ClipboardData *cb_data);
    ClipboardData *clipboardDataByDataHash(const QString &data_hash);
    ClipboardData *clipboardDataByHistoryIndex(int history_idx);
    int count() const;
    void deleteAll();

signals:
    void clipboardDataChanged(const QString &data_hash);
    void clipboardDataChanged(int idx);
    void clipboardHistoryMove(int fromidx, int toidx);
    void clipboardHistoryInsert(int idx);
    void clipboardHistoryRemove(int idx);

public slots:

private:
    ClipboardDataManager(QObject *parent = 0);
    static ClipboardDataManager *s_cb_data_manager;
    QMap<QString,ClipboardData *> m_cb_data_map;
    QStringList m_cb_data_history;
};

#endif // CLIPBOARDDATAMANAGER_H
