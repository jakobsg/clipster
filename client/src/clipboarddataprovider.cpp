#include "clipboarddataprovider.h"
#include "clipboardlocaldb.h"
#include "clipboarddata.h"
#include "webserviceclient.h"
#include <crashlib.h>

ClipboardDataProvider::ClipboardDataProvider(QObject *parent) : QObject(parent) {

}

ClipboardDataProvider::~ClipboardDataProvider() {

}

void ClipboardDataProvider::addClipboardData(ClipboardData *cb_data) {
    CALL_LOGGER();
    ClipboardLocalDB::instance()->upsertClipboardEntry(cb_data);
}

bool ClipboardDataProvider::setDataAvailable(const QString &data_hash, bool available) {
    CALL_LOGGER();
    return ClipboardLocalDB::instance()->updateValue(data_hash, "data_available", available);
}
