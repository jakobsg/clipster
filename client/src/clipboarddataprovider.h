#ifndef CLIPBOARDDATAPROVIDER_H
#define CLIPBOARDDATAPROVIDER_H

#include <QObject>

class ClipboardData;

class ClipboardDataProvider : public QObject {
    Q_OBJECT
public:
    explicit ClipboardDataProvider(QObject *parent = 0);
    ~ClipboardDataProvider();

    virtual void addClipboardData(ClipboardData *cb_data);
    virtual bool setDataAvailable(const QString &data_hash, bool available=true);

signals:

};

#endif // CLIPBOARDDATAPROVIDER_H
