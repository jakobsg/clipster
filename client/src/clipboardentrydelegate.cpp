#include "clipboardentrydelegate.h"
#include "clipboardmodel.h"
#include "clipboardentryview.h"

#include <crashlib.h>
#include <QPainter>
#include <QStyleOptionViewItem>

#define PREVIEW_MAX_WIDTH 32
#define PREVIEW_MAX_HEIGHT 24

ClipboardEntryDelegate::ClipboardEntryDelegate(QObject *parent)
: QStyledItemDelegate(parent){
    CALL_LOGGER();
}

ClipboardEntryDelegate::~ClipboardEntryDelegate() {

}

void ClipboardEntryDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const {
    //if (index.data(ClipboardModel::Hovered).toBool()) {
    ClipboardEntryView *cv = (ClipboardEntryView *) option.widget;
    if (cv && cv->currentIndex().row()==index.row()) {
        painter->fillRect(option.rect,QColor(220,220,220));
    }

    const ClipboardModelInterface *mod = dynamic_cast<const ClipboardModelInterface *>(index.model());
    const ClipboardData *cb_data = mod->clipboardData(index.row());
    QRect text_rect = option.rect;
    text_rect.setLeft(text_rect.x()+40);
    text_rect.setRight(text_rect.right()-40);
    painter->setPen(QColor(120,120,120));
    painter->drawText(text_rect.marginsRemoved(QMargins(0,6,0,0)),option.fontMetrics.elidedText(index.data().toString(),Qt::ElideMiddle, text_rect.width()));
    painter->setPen(QColor(40,40,40));
    painter->drawText(text_rect.marginsRemoved(QMargins(0,21,36,0)),cb_data->copyTime().toString());

    QRect text_imagerect = option.rect;
    text_imagerect.setWidth(text_rect.left());
    QImage prev_img;
    if (!cb_data->imagePreview().isNull()) {
        if ((float)cb_data->imagePreview().height()/cb_data->imagePreview().width() > 0.75) {
            prev_img = cb_data->imagePreview().scaledToHeight(PREVIEW_MAX_HEIGHT,Qt::SmoothTransformation);
        }
        else {
            prev_img = cb_data->imagePreview().scaledToWidth(PREVIEW_MAX_WIDTH,Qt::SmoothTransformation);
        }
    }
    QRect source = prev_img.rect();
    source.moveCenter(text_imagerect.center());
    painter->drawImage(source,prev_img);

    QRect icon_rect = option.rect;
    icon_rect.setLeft(text_rect.right());
    QString icon = ":/images/non-shared.png";
    if (cb_data->dataOrigin()==ClipboardData::RemoteDistributedCopy) {
        icon = ":/images/shared.png";
    }
    QImage cloud((index.data(ClipboardModel::MouseOverAction).toInt()==ClipboardModel::ToggleOnline) ? ":/images/mid-shared.png" : icon );
    QRect cloud_rect = cloud.rect();
    cloud_rect.moveCenter(icon_rect.center());
    painter->drawImage(cloud_rect,cloud);
}

QSize ClipboardEntryDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const {
    Q_UNUSED(option);
    Q_UNUSED(index);
    return QSize(100,40);
}
