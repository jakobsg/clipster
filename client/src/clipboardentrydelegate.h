#ifndef CLIPBOARDENTRYDELEGATE_H
#define CLIPBOARDENTRYDELEGATE_H

#include <QStyledItemDelegate>

class ClipboardEntryDelegate : public QStyledItemDelegate {

    Q_OBJECT

public:
    ClipboardEntryDelegate(QObject *parent=0);
    ~ClipboardEntryDelegate();

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;

};

#endif // CLIPBOARDENTRYDELEGATE_H
