#include "clipboardentryview.h"
#include "clipboardmodel.h"
#include "clipboard.h"
#include "crashlib.h"
#include "roundedpopup.h"

#include <QMouseEvent>
#include <QApplication>
#include <QTimer>
#include <QPropertyAnimation>

ClipboardEntryView::ClipboardEntryView(QWidget *parent)
: QListView(parent) {
    CALL_LOGGER();
    setAutoScroll(true);
    setMouseTracking(true);
    setVerticalScrollMode(QListView::ScrollPerPixel);
    connect(this,SIGNAL(entered(const QModelIndex &)),this,SLOT(slotEntered(const QModelIndex &)));
    connect(this,SIGNAL(clicked(const QModelIndex &)),this,SLOT(entryClicked(const QModelIndex &)));
    connect(this, SIGNAL(onlineSettingsTriggered(ClipboardData *)), this, SLOT(showOlineSettings(ClipboardData *)));
    //connect(this,SIGNAL(activated(const QModelIndex &)),this,SLOT(entrySelected(const QModelIndex &)));
}

ClipboardEntryView::~ClipboardEntryView() {
}

void ClipboardEntryView::slotEntered(const QModelIndex &index) {
    setCurrentIndex(index);
    model()->setData(m_prev_hover, QVariant(false), ClipboardModel::Hovered);
    model()->setData(index, QVariant(true), ClipboardModel::Hovered);
}

void ClipboardEntryView::entryClicked(const QModelIndex &index) {
    CALL_LOGGER();
    qDebug("Click on idx: %d, gave: %d", index.row(), index.data(ClipboardModel::MouseOverAction).toInt());
    if (index.data(ClipboardModel::MouseOverAction).toInt()==ClipboardModel::ToggleOnline) {
        ClipboardModelInterface *mod = dynamic_cast<ClipboardModelInterface *>(model());
        ClipboardData *cd = mod->clipboardData(index.row());
        qDebug("Clipboard data: %p", cd);
        cd->upload();
        emit onlineSettingsTriggered(cd);
    }
    else {
        entrySelected(index);
    }
}

void ClipboardEntryView::entrySelected(const QModelIndex &index) {
    CALL_LOGGER();
    ClipboardModelInterface *mod = dynamic_cast<ClipboardModelInterface *>(model());
    ClipboardList &cl = Clipboard::getClipboardInstances();
    ClipboardData *cd = mod->clipboardData(index.row());
    LOG_INFO(QString("Selected: %1, Data available: %2").arg(cd->dataHash()).arg(cd->dataAvailable()));
    cl.first()->activateClipboardData(mod->clipboardData(index.row()));
    this->topLevelWidget()->hide();
}

void ClipboardEntryView::moveCurrentUpOne() {
    int curidx = currentIndex().row();
    int rowcnt = model()->rowCount(model()->index(0,0));
    if (curidx>=rowcnt || curidx<0) {
        curidx = rowcnt-1;
    }
    else if (curidx>0) {
        curidx--;
    }
    setCurrentIndex(model()->index(curidx,0));
}

void ClipboardEntryView::moveCurrentDownOne() {
    int curidx = currentIndex().row();
    int rowcnt = model()->rowCount(model()->index(0,0));
    if (curidx<0) {
        curidx = 0;
    }
    else if (curidx<rowcnt-1) {
        curidx++;
    }
    setCurrentIndex(model()->index(curidx,0));
}

void ClipboardEntryView::activateCurrent() {
    entrySelected(currentIndex());
}

void ClipboardEntryView::showOlineSettings(ClipboardData *cb_data) {
}

void ClipboardEntryView::mouseMoveEvent(QMouseEvent *me) {
    QListView::mouseMoveEvent(me);
    QModelIndex idx = indexAt(me->pos());
    QRect visrect = visualRect(idx);
    QPoint itempos = me->pos() - visrect.topLeft();
    QRect zerorect(QPoint(0,0),visrect.size());
    if (m_prev_mapped!=idx) {
        if (m_prev_mapped.isValid()) {
            ((ClipboardModel *)model())->setData(m_prev_mapped,0,ClipboardModel::MouseOverAction);
        }
        m_prev_mapped = idx;
    }
    if (zerorect.marginsRemoved(QMargins(zerorect.width()-32,10,6,10)).contains(itempos)) {
        ((ClipboardModel *)model())->setData(idx,ClipboardModel::ToggleOnline,ClipboardModel::MouseOverAction);
    }
    else {
        ((ClipboardModel *)model())->setData(idx,0,ClipboardModel::MouseOverAction);
    }
}

void ClipboardEntryView::enterEvent(QEvent *e) {
    QListView::enterEvent(e);
    return;
    if (m_prev_mapped.isValid()) {
        model()->setData(m_prev_mapped,0,ClipboardModel::MouseOverAction);
    }
}

void ClipboardEntryView::keyPressEvent(QKeyEvent *e) {
    if (e->key()==Qt::Key_Return) {
        activateCurrent();
    }
}
