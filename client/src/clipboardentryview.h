#ifndef CLIPBOARDENTRYVIEW_H
#define CLIPBOARDENTRYVIEW_H

#include <QListView>
class ClipboardData;
class RoundedPopup;

class ClipboardEntryView : public QListView
{

    Q_OBJECT

public:
    ClipboardEntryView(QWidget *parent=0);
    ~ClipboardEntryView();

public slots:
    void slotEntered(const QModelIndex &index);
    void entryClicked(const QModelIndex &index);
    void entrySelected(const QModelIndex &index);
    void moveCurrentUpOne();
    void moveCurrentDownOne();
    void activateCurrent();
    void showOlineSettings(ClipboardData *cb_data);

protected:
    void mouseMoveEvent(QMouseEvent *me);
    void keyPressEvent(QKeyEvent * e);
    void enterEvent(QEvent *e);

signals:
    void onlineSettingsTriggered(ClipboardData *cb_data);

private:
    QModelIndex m_prev_hover;
    QModelIndex m_prev_mapped;
};

#endif
