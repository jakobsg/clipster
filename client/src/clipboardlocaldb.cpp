#include "clipboardlocaldb.h"
#include "clipboarddata.h"
#include "clipsterconfig.h"

#include <QApplication>
#include <QStandardPaths>
#include <QSqlQuery>
#include <QDir>
#include <QSqlError>
#include <QRegularExpression>
#include <QDateTime>
#include <QBuffer>
#include <crashlib.h>

ClipboardLocalDB *ClipboardLocalDB::s_instance;

ClipboardLocalDB *ClipboardLocalDB::instance() {
    CALL_LOGGER();
    if (s_instance == 0) {
        s_instance = new ClipboardLocalDB(QApplication::instance());
    }
    return s_instance;
}

ClipboardLocalDB::ClipboardLocalDB(QObject *parent) :
QObject(parent) {
    CALL_LOGGER();
    initDB();
}

void ClipboardLocalDB::initDB(bool force_reset) {
    CALL_LOGGER();
    QString app_data_location = QStandardPaths::writableLocation(QStandardPaths::DataLocation);
    QDir adl(app_data_location);
    adl.mkdir(app_data_location);
    if (force_reset) {
        adl.remove("clipboard.db");
    }
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(adl.filePath("clipboard.db"));
    db.open();
    if (db.tables().contains("cb_entry")) {
        return;
    }

    QRegularExpression rx_create("^create\\s+(\\w+)\\s+(\\w+).*");
    // Initialize local clipboard database
    QFile file(":/db/init.sql");
    file.open(QIODevice::ReadOnly);
    QByteArray dump = file.readAll();
    QList<QByteArray> sql_list = dump.split(';');
    foreach (QByteArray bytearray, sql_list) {
        QString sql = QString::fromUtf8(bytearray.data(),bytearray.length());
        sql = sql.trimmed();
        if (!sql.isEmpty()) {
            QRegularExpressionMatch match = rx_create.match(sql);
            if (match.hasMatch()) {
                qDebug("LocalDB: Creating %s \"%s\"",
                    match.captured(1).toUtf8().data(),
                    match.captured(2).toUtf8().data());
            }
            db.exec(sql.toUtf8().data());
            if (db.lastError().type()!=QSqlError::NoError) {
                qDebug("DB Error: %s" , db.lastError().text().toUtf8().data());
            }
        }
    }
}

void ClipboardLocalDB::startTransaction() {
    CALL_LOGGER();
    QSqlQuery query;
    query.exec("begin");
}

void ClipboardLocalDB::endTransaction() {
    CALL_LOGGER();
    QSqlQuery query;
    query.exec("end");
}


void ClipboardLocalDB::upsertClipboardEntry(ClipboardData *cb_data) {
    CALL_LOGGER();
    QSqlQuery query;
    query.prepare(
        "insert into cb_entry ("
        "  copytime, data_hash, data_available,"
        "  data_origin, data_type, owner,"
        "  team_name, cloud_remove_right,"
        "  text_preview, image_preview)"
        "select "
        "  :copytime, :data_hash, :data_available,"
        "  :data_origin, :data_type, :owner,"
        "  :team_name, :cloud_remove_right,"
        "  :text_preview, :image_preview "
        "where not exists ("
        "  select 1 from cb_entry "
        "  where data_hash=:data_hash )");
    qDebug("DATA_ORIGIN: %d", (int) cb_data->dataOrigin());
    query.bindValue(":copytime", cb_data->copyTime().toMSecsSinceEpoch());
    query.bindValue(":data_hash",cb_data->dataHash());
    query.bindValue(":data_available",cb_data->dataAvailable());
    query.bindValue(":data_origin",(int)cb_data->dataOrigin());
    query.bindValue(":data_type",1);
    query.bindValue(":owner",QVariant());
    query.bindValue(":team_name",QVariant());
    query.bindValue(":cloud_remove_right",1);
    query.bindValue(":text_preview",cb_data->textPreview());
    QImage image_preview = cb_data->imagePreview();
    if (!image_preview.isNull()) {
        QBuffer buf;
        image_preview.save(&buf,"png");
        query.bindValue(":image_preview",buf.data());
    }
    else {
        query.bindValue(":image_preview",QVariant());
    }
    query.exec();
    if (query.lastError().type()!=QSqlError::NoError) {
        qDebug("SQL ERROR: %s", query.lastError().text().toUtf8().data());
    }

    if (query.numRowsAffected()==0) {
        // If no rows are inserted it is assumed that it already existed
        // update the copytime instead.
        query.prepare(
            "update cb_entry "
            "  set copytime=:copytime "
            "where "
            "  data_hash=:data_hash ");
        query.bindValue(":copytime", cb_data->copyTime().toMSecsSinceEpoch());
        query.bindValue(":data_hash",cb_data->dataHash());
        query.exec();
        if (query.lastError().type()!=QSqlError::NoError) {
            qDebug("SQL ERROR: %s", query.lastError().text().toUtf8().data());
        }
        else if (query.numRowsAffected()==0) {
            qDebug("PROBLEM: No clipboard data was inserted in cb_entry and no existing row was updated (data_hash: %s)", cb_data->dataHash().toUtf8().data());
        }
        else {
            qDebug("INFO: cb_entry entry updated (data_hash: %s)", cb_data->dataHash().toUtf8().data());
        }
    }
    else {
        qDebug("INFO: cb_entry entry inserted (data_hash: %s)", cb_data->dataHash().toUtf8().data());
    }
}

bool ClipboardLocalDB::updateValue(const QString &data_hash, const QString &column, const QVariant &value) {
    CALL_LOGGER();
    QSqlQuery query;
    query.prepare(
        QString("update cb_entry "
        "  set %1 = :value "
        "where "
        "  data_hash = :data_hash ").arg(column));
    query.bindValue(":data_hash", data_hash);
    query.bindValue(":value", value);
    query.exec();
    if (query.lastError().type()!=QSqlError::NoError) {
        qDebug("SQL ERROR: %s", query.lastError().text().toUtf8().data());
    }
    if (query.numRowsAffected()>0) {
        return true;
    }
    return false;
}

bool ClipboardLocalDB::loadClipboardEntry(const QString &data_hash, ClipboardData *cb_data) {
    CALL_LOGGER();
    QSqlQuery query;
    query.prepare(
        "select "
        "  copytime, data_origin, data_type, "
        "  owner, team_name, cloud_remove_right, "
        "  text_preview, image_preview "
        "from cb_entry "
        "where "
        "  data_hash=:data_hash ");
    query.bindValue(":data_hash",data_hash);
    bool exec_ok = query.exec();
    if (!exec_ok || !query.first()) {
        return false;
    }
    QByteArray image_preview_bytes = query.value("image_preview").toByteArray();
    if (!image_preview_bytes.isNull()) {
        QImage img = QImage::fromData(image_preview_bytes,"png");
        cb_data->setImagePreview(img);
    }
    cb_data->setTextPreview(query.value("text_preview").toString());
    cb_data->setDataOrigin((ClipboardData::DataOrigin) query.value("data_origin").toInt());
    cb_data->setCopyTime(QDateTime::fromMSecsSinceEpoch(query.value("copytime").toLongLong()));
    return true;
}

QList<ClipboardData *> ClipboardLocalDB::loadClipboardEntries() {
    CALL_LOGGER();
    QSqlQuery query;
    QList <ClipboardData *> result;
    query.prepare(
        "select data_hash "
        "from cb_entry "
		"order by copytime desc "
		"limit :max_displayed_cb_entries ");
	query.bindValue(
		":max_displayed_cb_entries",
		ClipsterConfig::configValue("max_displayed_cb_entries",50).toInt());
    if (!query.exec()) {
        return result;
    }
	if (query.last()) {
        do {
            result << new ClipboardData(query.value("data_hash").toString());
		} while (query.previous());
    }
    return result;
}

bool ClipboardLocalDB::deleteClipboardEntries() {
    CALL_LOGGER();
    QSqlQuery query;
	query.prepare(
		"delete "
		"from cb_entry ");
    return query.exec();
}

ClipboardLocalDB::~ClipboardLocalDB() {
    CALL_LOGGER();
}

