#ifndef CLIPBOARDLOCALDB_H
#define CLIPBOARDLOCALDB_H

#include <QObject>

class ClipboardData;

class ClipboardLocalDB : public QObject {

    Q_OBJECT

private:
    ClipboardLocalDB(QObject *parent=0);

public:
    ~ClipboardLocalDB();
    static ClipboardLocalDB *instance();

    void upsertClipboardEntry(ClipboardData *cb_data);
    bool updateValue(const QString &data_hash, const QString &column, const QVariant &value);
    bool loadClipboardEntry(const QString &data_hash, ClipboardData *cb_data);
	bool deleteClipboardEntries();
    void startTransaction();
    void endTransaction();
    QList<ClipboardData *> loadClipboardEntries();

public Q_SLOTS:
    void initDB(bool force_reset=false);

Q_SIGNALS:

private:
    static ClipboardLocalDB *s_instance;

};

#endif // CLIPBOARDLOCALDB_H
