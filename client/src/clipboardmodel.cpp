#include "clipboardmodel.h"
#include "clipboard.h"
#include "clipboardapplicationmanager.h"
#include "clipboardlocaldb.h"
#include "clipboarddatamanager.h"
#include <crashlib.h>

ClipboardModel::ClipboardModel(QObject *parent) :
QAbstractListModel(parent) {
    CALL_LOGGER();
    connect(ClipboardDataManager::instance(),SIGNAL(clipboardHistoryInsert(int)),this,SLOT(clipboardDataInserted(int)));
    connect(ClipboardDataManager::instance(),SIGNAL(clipboardHistoryMove(int,int)),this,SLOT(clipboardDataMoved(int,int)));
    connect(ClipboardDataManager::instance(),SIGNAL(clipboardHistoryRemove(int)),this,SLOT(clipboardDataRemoved(int)));
    connect(ClipboardDataManager::instance(),SIGNAL(clipboardDataChanged(int)),this,SLOT(clipboardDataChanged(int)));
    m_current_mouseover = -1;
    m_action = (ClipboardDataAction) 0;
    beginResetModel();
    ClipboardLocalDB::instance()->loadClipboardEntries();
    endResetModel();
}

ClipboardModel::~ClipboardModel() {
    CALL_LOGGER();
}

ClipboardData *ClipboardModel::clipboardData(int index) {
    CALL_LOGGER();
    return ClipboardDataManager::instance()->clipboardDataByHistoryIndex(index);
}

const ClipboardData *ClipboardModel::clipboardData(int index) const {
    CALL_LOGGER();
    return ClipboardDataManager::instance()->clipboardDataByHistoryIndex(index);
}

void ClipboardModel::clipboardDataInserted(int idx) {
    CALL_LOGGER();
    beginInsertRows(QModelIndex(),idx,idx);
    endInsertRows();
}

void ClipboardModel::clipboardDataMoved(int fromidx, int toidx) {
    CALL_LOGGER();
    beginMoveRows(QModelIndex(),fromidx,fromidx,QModelIndex(),toidx);
    endMoveRows();
}

void ClipboardModel::clipboardDataRemoved(int idx) {
    CALL_LOGGER();
    beginRemoveRows(QModelIndex(),idx,idx);
    endRemoveRows();
}

void ClipboardModel::clipboardDataChanged(int idx) {
    CALL_LOGGER();
    emit dataChanged(index(idx),index(idx));
}

int ClipboardModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    CALL_LOGGER();
    return ClipboardDataManager::instance()->count();
}

QVariant ClipboardModel::data(const QModelIndex &index, int role) const {
    CALL_LOGGER();
    if (index.row() < 0 || index.row() >= ClipboardDataManager::instance()->count())
        return QVariant();
    ClipboardData *data = ClipboardDataManager::instance()->clipboardDataByHistoryIndex(index.row());
    if (role==TextPreview) {
        return data->textPreview();
    }
    else if (role==DataHash) {
        return data->dataHash();
    }
    else if (role==DataOrigin) {
        return data->dataOrigin();
    }
    else if (role==CopyTime) {
        QString copytime = data->copyTime().toString();
        return copytime;
    }
    else if (role==TransferProgress) {
        return data->transferProgress();
    }
    else if (role==Qt::DisplayRole) {
        return data->textPreview();
    }
    else if (role==MouseOverAction && index.row()==m_current_mouseover) {
        return m_action;
    }
    return QVariant();
}

bool ClipboardModel::setData(const QModelIndex &idx, const QVariant &value, int role) {
    if (role==Hovered) {
        m_current_mouseover = idx.row();
    }
    else if (role==MouseOverAction) {
        if (idx.data(MouseOverAction)!=value) {
            m_action = (ClipboardDataAction) value.toInt();
            emit dataChanged(idx,idx);
            return true;
        }
        return false;
    }
    return QAbstractListModel::setData(idx,value,role);
}


QHash<int, QByteArray> ClipboardModel::roleNames() const {
    CALL_LOGGER();
    QHash<int, QByteArray> roles;
    roles[TextPreview] = "textPreview";
    roles[DataHash] = "dataHash";
    roles[DataOrigin] = "dataOrigin";
    roles[CopyTime] = "copyTime";
    roles[TransferProgress] = "transferProgress";
    roles[Hovered] = "hovered";
    roles[MouseOverAction] = "mouseOverAction";
    return roles;
}
