#ifndef CLIPBOARDMODEL_H
#define CLIPBOARDMODEL_H

#include "clipboarddata.h"
#include "clipboardmodelinterface.h"
#include <QAbstractListModel>

class ClipboardModel : public QAbstractListModel, public ClipboardModelInterface {
    Q_OBJECT

public:
    ClipboardModel(QObject *parent=0);
    ~ClipboardModel();

    enum ClipboardDataRoles {
        TextPreview = Qt::UserRole + 1,
        DataHash,
        DataOrigin,
        CopyTime,
        TransferProgress,
        Hovered,
        MouseOverAction
    };

    enum ClipboardDataAction {
        ToggleOnline = 1
    };

    const ClipboardData *clipboardData(int index) const;
    ClipboardData *clipboardData(int index);

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);

public Q_SLOTS:
    void clipboardDataInserted(int idx);
    void clipboardDataMoved(int fromidx,int toidx);
    void clipboardDataRemoved(int idx);
    void clipboardDataChanged(int idx);

protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QStringList m_clipboard_data_hashes;
    int m_current_mouseover;
    ClipboardDataAction m_action;
};
#endif // CLIPBOARDMODEL_H
