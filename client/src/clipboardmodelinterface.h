#ifndef CLIPBOARDMODELINTERFACE_H
#define CLIPBOARDMODELINTERFACE_H

#include "clipboarddata.h"

class ClipboardModelInterface {
public:
    virtual ClipboardData *clipboardData(int index) = 0;
    virtual const ClipboardData *clipboardData(int index) const = 0;
};

#endif // CLIPBOARDMODELINTERFACE_H

