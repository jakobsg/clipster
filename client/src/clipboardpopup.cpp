#include "clipboardpopup.h"
#include <QQuickWidget>
#include <QGridLayout>
#include <QApplication>
#include <QDesktopWidget>
#include <QQuickItem>

ClipboardPopup::ClipboardPopup(QWidget *parent) : QWidget(parent, Qt::Popup) {
    setWindowModality(Qt::WindowModal);
    m_layout = new QGridLayout(this);
    setSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::MinimumExpanding);
    m_layout->setMargin(0);
    m_clipboard_widget = new QQuickWidget(this);
    QUrl source("qrc:/main.qml");
    m_clipboard_widget->setSource(source);
    m_clipboard_widget->setResizeMode(QQuickWidget::SizeViewToRootObject);
    m_layout->addWidget(m_clipboard_widget);
    connect((QObject *) m_clipboard_widget->rootObject(), SIGNAL(qqHeightChanged(int)), this, SLOT(qqHeightChanged(int)));
    move(QApplication::desktop()->availableGeometry().center() - this->rect().center());
    popupAndFocus();
}

ClipboardPopup::~ClipboardPopup() {
}

void ClipboardPopup::popupAndFocus() {
    QMetaObject::invokeMethod((QObject *) m_clipboard_widget->rootObject(), "focusSearchInput");
    m_clipboard_widget->setFocus();
    show();
    resize(QSize(width(),m_clipboard_widget->sizeHint().height()));
}

void ClipboardPopup::showEvent(QShowEvent *se) {
    qDebug("Sizehint: %d", m_clipboard_widget->sizeHint().height());
}

void ClipboardPopup::qqHeightChanged(int height) {
    resize(QSize(width(),height));
}
