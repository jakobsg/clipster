#ifndef CLIPBOARDPOPUP_H
#define CLIPBOARDPOPUP_H

#include <QWidget>

class QQuickWidget;
class QGridLayout;

class ClipboardPopup : public QWidget {

    Q_OBJECT

public:
    explicit ClipboardPopup(QWidget *parent = 0);
    ~ClipboardPopup();

public Q_SLOTS:
    void popupAndFocus();
    void qqHeightChanged(int height);

protected:
    void showEvent(QShowEvent *se);

private:
    QQuickWidget *m_clipboard_widget;
    QGridLayout *m_layout;
};

#endif // CLIPBOARDPOPUP_H
