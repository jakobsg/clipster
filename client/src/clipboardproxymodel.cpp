#include "clipboardproxymodel.h"
#include "clipboardmodel.h"
#include <crashlib.h>

ClipboardProxyModel::ClipboardProxyModel(QObject *parent) :
QSortFilterProxyModel(parent) {
    CALL_LOGGER();
    setFilterCaseSensitivity(Qt::CaseInsensitive);
}

ClipboardProxyModel::~ClipboardProxyModel() {
    CALL_LOGGER();
}

void ClipboardProxyModel::setSourceClipboardModel(ClipboardModel *sourcemodel) {
    CALL_LOGGER();
    m_sourcemodel = sourcemodel;
    setSourceModel(sourcemodel);
    setFilterRole(ClipboardModel::TextPreview);
}

ClipboardData *ClipboardProxyModel::clipboardData(int rowindex) {
    CALL_LOGGER();
    QModelIndex src_index = mapToSource(index(rowindex,0));
    return m_sourcemodel->clipboardData(src_index.row());
}

const ClipboardData *ClipboardProxyModel::clipboardData(int rowindex) const {
    CALL_LOGGER();
    QModelIndex src_index = mapToSource(index(rowindex,0));
    return m_sourcemodel->clipboardData(src_index.row());
}
