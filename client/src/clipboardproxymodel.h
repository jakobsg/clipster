#ifndef CLIPBOARDPROXYMODEL_H
#define CLIPBOARDPROXYMODEL_H

#include "clipboardmodelinterface.h"

#include <QSortFilterProxyModel>

class ClipboardModel;
class ClipboardData;

class ClipboardProxyModel : public QSortFilterProxyModel, public ClipboardModelInterface {
    Q_OBJECT

public:
    ClipboardProxyModel(QObject *parent=0);
    ~ClipboardProxyModel();

    virtual const ClipboardData *clipboardData(int index) const;

    Q_INVOKABLE void setSourceClipboardModel(ClipboardModel *sourcemodel);
    Q_INVOKABLE ClipboardData *clipboardData(int index);

private:
	ClipboardModel *m_sourcemodel;
};

#endif // CLIPBOARDPROXYMODEL_H
