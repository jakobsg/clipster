#include "clipsterconfig.h"

#include <QStandardPaths>
#include <QDir>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <crashlib.h>

QString ClipsterConfig::configPath() {
    CALL_LOGGER();
    // User data location
    QString app_data_location = QStandardPaths::writableLocation(QStandardPaths::DataLocation);
    // Create if not exists
    QDir adl(app_data_location);
    adl.mkdir(app_data_location);
    // Clipster data dir
    QString configfile = adl.filePath("config.json");
    if (!QFile::exists(configfile)) {
        QJsonDocument doc;
        QFile cf_file(configfile);
        cf_file.open(QFile::WriteOnly);
        cf_file.write(doc.toJson());
        cf_file.close();
    }
    return configfile;
}

QJsonValue ClipsterConfig::configValue(const QString &key, QJsonValue defval, bool *ok) {
    CALL_LOGGER();
    QFile configfile(configPath());
    if (ok) {
        *ok = true;
    }
    if (!configfile.open(QFile::ReadOnly)) {
        if (ok) {
            *ok = false;
        }
        return defval;
    }
    QJsonDocument doc = QJsonDocument::fromJson(configfile.readAll());
    configfile.close();
    QJsonObject configobj = doc.object();
    if (!configobj.contains(key)) {
        return defval;
    }
    return configobj[key];
}

bool ClipsterConfig::setConfigValue(const QString &key, const QJsonValue &val) {
    CALL_LOGGER();
    QFile configfile(configPath());
    if (!configfile.open(QFile::ReadOnly)) {
        return false;
    }
    QJsonDocument doc = QJsonDocument::fromJson(configfile.readAll());
    configfile.close();
    QJsonObject configobj = doc.object();
    configobj[key] = val;
    QJsonDocument newdoc(configobj);
    if (!configfile.open(QFile::WriteOnly)) {
        return false;
    }
    configfile.write(newdoc.toJson());
    configfile.close();
    return true;
}

bool ClipsterConfig::removeConfigValue(const QString &key) {
    CALL_LOGGER();
    QFile configfile(configPath());
    if (!configfile.open(QFile::ReadOnly)) {
        return false;
    }
    QJsonDocument doc = QJsonDocument::fromJson(configfile.readAll());
    configfile.close();
    QJsonObject configobj = doc.object();
    configobj.remove(key);
    QJsonDocument newdoc(configobj);
    if (!configfile.open(QFile::WriteOnly)) {
        return false;
    }
    configfile.write(newdoc.toJson());
    configfile.close();
    return true;
}
