#ifndef CLIPSTERHOST_H
#define CLIPSTERHOST_H

#ifdef USE_CLIPSTER_DEV_SERVER
#define CLIPSTER_HTTP_SCHEMA "http"
#define CLIPSTER_HOST "dev.clipster.info"
#else
#define CLIPSTER_HTTP_SCHEMA "http"
#define CLIPSTER_HOST "clipster.info"
#endif

#endif // CLIPSTERHOST_H
