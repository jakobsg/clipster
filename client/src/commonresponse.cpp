#include "commonresponse.h"
#include <crashlib.h>

CommonResponse::CommonResponse() {
    CALL_LOGGER();
    m_serverfault = false;
    m_servicefault = false;
    m_networkerror = QNetworkReply::NoError;
    m_http_status_code = 0;
    m_res_code = -1;
    m_res_msg = "Bad response";
}

CommonResponse::CommonResponse(const CommonResponse &other) {
    CALL_LOGGER();
    m_json_response = other.m_json_response;
    m_networkerror = other.m_networkerror;
    m_serverfault = other.m_serverfault;
    m_servicefault = other.m_servicefault;
    m_res_code = other.m_res_code;
    m_res_msg = other.m_res_msg;
    m_http_reason = other.m_http_reason;
    m_http_status_code = other.m_http_status_code;
}

CommonResponse::~CommonResponse() {
    CALL_LOGGER();
}

