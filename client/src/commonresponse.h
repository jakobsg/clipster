#ifndef COMMONRESPONSE_H
#define COMMONRESPONSE_H

#include <QString>
#include <QJsonObject>
#include <QNetworkReply>

class CommonResponse {
public:
    CommonResponse();
    CommonResponse(const CommonResponse &other);
    ~CommonResponse();

    int m_res_code;
    QString m_res_msg;
    QJsonObject m_json_response;

    bool m_serverfault;
    bool m_servicefault;
    QNetworkReply::NetworkError m_networkerror;
    int m_http_status_code;
    QString m_http_reason;
};

#endif // COMMONRESPONSE_H
