#include "dataentrylistresponse.h"
#include <crashlib.h>

DataEntryListResponse::DataEntryListResponse()
: CommonResponse() {
    CALL_LOGGER();
}

DataEntryListResponse::DataEntryListResponse(const CommonResponse &resp)
: CommonResponse(resp) {
    CALL_LOGGER();
}

DataEntryListResponse::~DataEntryListResponse() {
    CALL_LOGGER();
}

