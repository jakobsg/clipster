#include "deviceinfo.h"

#include <QString>
#include <QCryptographicHash>
#include <crashlib.h>

QString hardwareSignature(const QString &username) {
    CALL_LOGGER();
    QString hardware_sig_unhashed = QString::fromStdString(getPhysicalDiskSerialNumber());
    if (hardware_sig_unhashed.isEmpty()) {
        return "";
    }
    hardware_sig_unhashed += username;
    QString intermediate = QString(
        QCryptographicHash::hash(
            hardware_sig_unhashed.toUtf8(),
            QCryptographicHash::Md5).toHex());
    return intermediate;
}
