#ifndef DEVICEINFO_H
#define DEVICEINFO_H

#include <QString>

std::string getPhysicalDiskSerialNumber(const char *path=0);
QString hardwareSignature(const QString &username="");

#endif // DEVICEINFO_H

