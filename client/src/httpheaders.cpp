#include "httpheaders.h"
#include "stringtools.h"
#include <algorithm>
#include <vector>
#include <string.h>
#include <crashlib.h>

HttpHeaders::HttpHeaders() {
    CALL_LOGGER();
    m_multipart = false;
    m_content_length = 0;
}

HttpHeaders::HttpHeaders(const HttpHeaders &other) {
    CALL_LOGGER();

    m_encoding = other.m_encoding;
    m_content_type = other.m_content_type;
    m_boundary = other.m_boundary;
    m_content_length = other.m_content_length;
    m_header_map = other.m_header_map;
    m_multipart = other.m_multipart;
}

void HttpHeaders::add_header_line(const char *data) {
    CALL_LOGGER();
    const char *p = strstr(data,":");
    if (p) {
        int idx = p-data;
        char *tmp = new char[idx+1];
        strncpy(tmp,data,idx);
        tmp[idx] = 0;
        std::string hkey = tmp;
        delete [] tmp;
        std::transform(hkey.begin(), hkey.end(), hkey.begin(), ::toupper);
        std::string hval = p+1;
        m_header_map[hkey] = trim(hval);
        if (hkey=="CONTENT-TYPE") {
            // Test multipart
            // hval = "multipart/related; boundary=\"2676ff6efebdb664f8f7ccb34f864e25\"";
            std::vector<std::string> ct_parts = split(hval,';');
            m_content_type = trim(ct_parts[0]);
            std::transform(m_content_type.begin(), m_content_type.end(), m_content_type.begin(), ::tolower);
            std::vector<std::string>::iterator ct_it = ct_parts.begin();
            for (; ct_it!=ct_parts.end(); ++ct_it) {
                trim(*ct_it);
                std::string lc_ct = *ct_it;
                std::transform(lc_ct.begin(), lc_ct.end(), lc_ct.begin(), ::tolower);
                if (lc_ct.find("charset=")==0) {
                    m_encoding = (*ct_it).substr(lc_ct.find("=")+1);
                    trim(m_encoding);
                }
                if (lc_ct.find("boundary=")==0) {
                    m_boundary = (*ct_it).substr(lc_ct.find("=")+1);
                    trim(m_boundary);
                    remove(m_boundary,'\"');
                }
            }
            if ((int)m_content_type.find("multipart")>-1 && !m_boundary.empty()) {
                m_multipart = true;
            }
        }
        if (hkey=="CONTENT-LENGTH") {
            m_content_length = atoi(hval.c_str());
        }
    }
}
const HttpHeaders::HeaderMap &HttpHeaders::header_map() const {
    CALL_LOGGER();
    return m_header_map;
}
const std::string &HttpHeaders::encoding() const {
    CALL_LOGGER();
    return m_encoding;
}
const std::string &HttpHeaders::boundary() const {
    CALL_LOGGER();
    return m_boundary;
}
const std::string &HttpHeaders::content_type() const {
    CALL_LOGGER();
    return m_content_type;
}
unsigned int HttpHeaders::content_length() const {
    CALL_LOGGER();
    return m_content_length;
}
bool HttpHeaders::is_multipart() const {
    CALL_LOGGER();
    return m_multipart;
}
std::string HttpHeaders::header_value(const std::string &header_name) const {
    CALL_LOGGER();
    HeaderMap::const_iterator it = m_header_map.find(header_name);
    if (it!=m_header_map.end()) {
        return it->second;
    }
    return "";
}
