#ifndef HTTPHEADERS_H
#define HTTPHEADERS_H

#include <map>
#include <string>

class HttpHeaders {
public:
    typedef std::map<std::string,std::string> HeaderMap;
    HttpHeaders();
    HttpHeaders(const HttpHeaders &other);
    void add_header_line(const char *data);
    const HeaderMap &header_map() const;
    const std::string &encoding() const;
    const std::string &content_type() const;
    const std::string &boundary() const;
    unsigned int content_length() const;
    bool is_multipart() const;
    std::string header_value(const std::string &header_name) const;

private:
    std::string m_encoding;
    std::string m_content_type;
    std::string m_boundary;
    unsigned int m_content_length;
    HeaderMap m_header_map;
    bool m_multipart;
};


#endif
