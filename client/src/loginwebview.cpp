﻿#include "loginwebview.h"
#include "clipsterconfig.h"
#include "webserviceclient.h"
#include "clipsterhost.h"

#include <crashlib.h>

#include <QApplication>
#include <QNetworkCookieJar>
#include <QNetworkCookie>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QTimer>

#define TIMEOUT_DURATION 5000

LoginWebView::LoginWebView(QWidget *parent) : QWebView(parent) {
    CALL_LOGGER();
    setMaximumWidth(769);
    setMaximumHeight(400);
    setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint);
    setStyleSheet("background:transparent");
    setAttribute(Qt::WA_TranslucentBackground);
    m_poll_timer = new QTimer(this);
    m_poll_timer->setInterval(200);
    m_poll_timer->setSingleShot(false);
    connect(m_poll_timer,SIGNAL(timeout()),this,SLOT(poll()));
    m_timeout_timer = new QTimer(this);
    m_timeout_timer->setInterval(TIMEOUT_DURATION);
    m_timeout_timer->stop();
    m_timeout_timer->setSingleShot(true);
    connect(m_timeout_timer,SIGNAL(timeout()),this,SLOT(timeoutCheck()));
    connect(page()->networkAccessManager(),SIGNAL(finished(QNetworkReply*)),this,SLOT(checkNetwork(QNetworkReply*)));
}

LoginWebView::~LoginWebView() {
    CALL_LOGGER();
}

void LoginWebView::startLogin() {
    CALL_LOGGER();
    m_is_online = false;
    page()->networkAccessManager()->setCookieJar(new QNetworkCookieJar());
    setUrl(QUrl(QString("%1://%2%3").arg(CLIPSTER_HTTP_SCHEMA).arg(CLIPSTER_HOST).arg("/login")));
    reload();
    m_timeout_timer->start();
    m_poll_timer->start();
}

void LoginWebView::timeoutCheck() {
    CALL_LOGGER();
    if (!m_is_online) {
        qDebug("timeout");
        hide();
        m_poll_timer->stop();
    }
}

void LoginWebView::poll() {
    CALL_LOGGER();
    QNetworkCookieJar *jar = page()->networkAccessManager()->cookieJar();
    QList<QNetworkCookie> cookies = jar->cookiesForUrl(url());
    foreach (QNetworkCookie cookie, cookies) {
        if (QString(cookie.name().data())=="login-token") {
            qDebug() << "Login successful: " << cookie.value();
            close();
            bool res = WebServiceClient::instance()->registerDevice(cookie.value().data());
            m_poll_timer->stop();
            if (!res) {
                qDebug("Failed to register device");
                qApp->exit();
            }
        }
        if (QString(cookie.name().data())=="cancelled") {
            qDebug("Login was cancelled by user");
            hide();
            m_poll_timer->stop();
        }
    }
}

void LoginWebView::checkNetwork(QNetworkReply *reply) {
    CALL_LOGGER();
    if (reply->error()==QNetworkReply::NoError) {
        m_is_online = true;
        show();
    }
}

