#ifndef LOGINWEBVIEW_H
#define LOGINWEBVIEW_H

#include <QWebView>

class QNetworkReply;
class WebServiceClient;
class QTimer;

class LoginWebView : public QWebView {

    Q_OBJECT

public:
    explicit LoginWebView(QWidget *parent=0);
    ~LoginWebView();
    void startLogin();

private slots:
    void poll();
    void checkNetwork(QNetworkReply *reply);
    void timeoutCheck();

private:
    QTimer *m_poll_timer;
    QTimer *m_timeout_timer;
    bool m_is_online;
};

#endif // LOGINWEBVIEW_H
