#include <IOKit/IOKitLib.h>
#include <stdlib.h>
#include <syslog.h>
#include <mach-o/dyld.h>

#include <string>
#define USERNAME_ENV "USER"

std::string toStdString(CFStringRef str,CFStringEncoding encoding) {
    // Convert CFStringRef to std::string
    // default encoding set to latin1 (kCFStringEncodingWindowsLatin1)
    if(!str) {
        return std::string();
    }
    CFIndex length = CFStringGetLength(str);
    const char *chars = CFStringGetCStringPtr(str,encoding);
    if (chars) {
        return std::string(chars,0,length);
    }
    char *buffer = (char *) malloc(length+1);
    CFStringGetCString(str, buffer, length+1, encoding);
    std::string out = buffer;
    free(buffer);
    return out;
}

std::string getPhysicalDiskSerialNumber(const char *indrive=0) {
    io_registry_entry_t ioRegistryRoot = IORegistryEntryFromPath(kIOMasterPortDefault, "IOService:/");
    CFStringRef uuidCf = (CFStringRef) IORegistryEntryCreateCFProperty(ioRegistryRoot, CFSTR(kIOPlatformUUIDKey), kCFAllocatorDefault, 0);
    IOObjectRelease(ioRegistryRoot);
    std::string uuid = toStdString(uuidCf,kCFStringEncodingISOLatin1);
    CFRelease(uuidCf);
    return uuid;
}
