#include <Cocoa/Cocoa.h>
#include <CoreFoundation/CoreFoundation.h>

int getChangeCount()
{
    return [[NSPasteboard generalPasteboard] changeCount];
}
