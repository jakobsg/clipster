#include <CoreFoundation/CoreFoundation.h>
#include <Carbon/Carbon.h>
#include <QApplication>

int sendCtrlC() {
    CGEventSourceRef source = CGEventSourceCreate(kCGEventSourceStateHIDSystemState);
    CGEventRef saveCommandDown = CGEventCreateKeyboardEvent(source, (CGKeyCode)kVK_ANSI_C, true);
    CGEventSetFlags(saveCommandDown, kCGEventFlagMaskCommand);
    CGEventRef saveCommandUp = CGEventCreateKeyboardEvent(source, (CGKeyCode)kVK_ANSI_C, false);
    CGEventSetFlags(saveCommandUp, kCGEventFlagMaskCommand);

    CGEventTapLocation loc = kCGSessionEventTap; // kCGSessionEventTap also works
    CGEventPost(loc, saveCommandDown);
    CGEventPost(loc, saveCommandUp);

    CFRelease(saveCommandUp);
    CFRelease(saveCommandDown);
    CFRelease(source);
    return 1;
}
