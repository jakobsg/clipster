#include <QApplication>
#include "clipboard.h"
#include "trayicon.h"
#include "clipboardapplicationmanager.h"
#include "clipboardmodel.h"
#include "clipboardproxymodel.h"

#include "clipboardlocaldb.h"
#include "mimedatatools.h"
#include "webserviceclient.h"
#include "deviceinfo.h"
#include "clipsterconfig.h"
#include <QImage>
#include "singleapplication.h"
#include <signal.h>
#include <crashlib.h>
#include "wrapper.h"
#include "deviceinfo.h"

int main(int argc, char *argv[]) {
    CALL_LOGGER();
	LOG_FATAL("Fatal");
	LOG_ERROR("Error");
	LOG_WARNING("Warning");
	LOG_INFO("Info");
	LOG_DEBUG("Debug");
	LOG_TRACE("Trace");
    store_git_version(GIT_VERSION);
#ifdef WINDOWS_OS
	::SetUnhandledExceptionFilter(UnhandledCrashHandler);
	signal(SIGILL, tmpfile_seg_handler);
    signal(SIGSEGV, tmpfile_seg_handler);
    signal(SIGFPE, tmpfile_seg_handler);
	signal(SIGABRT, tmpfile_seg_handler);
	signal(SIGABRT_COMPAT, tmpfile_seg_handler);
    signal(SIGINT, tmpfile_seg_handler);
#else
    struct sigaction sa;
    sa.sa_handler = tmpfile_seg_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    sigaction(SIGILL, &sa, NULL);
    sigaction(SIGSEGV, &sa, NULL);
    sigaction(SIGFPE, &sa, NULL);
#endif
    std::set_terminate( tmpfile_std_handler );
    return runapp(argc, argv);
}
