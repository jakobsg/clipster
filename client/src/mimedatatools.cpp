#include "mimedatatools.h"
#include <mediatype.h>
#include <QCryptographicHash>
#include <QImage>
#include <QDataStream>
#include <QDir>
#include <QFile>
#include <QStandardPaths>
#include <QBuffer>
#include <QJsonDocument>
#include <QJsonArray>
#include <QMessageBox>
#include <crashlib.h>

MimeDataTools *MimeDataTools::s_instance = 0;

MimeDataTools *MimeDataTools::instance() {
    CALL_LOGGER();
    if (s_instance) {
        return s_instance;
    }
    return s_instance = new MimeDataTools();
}

MimeDataTools::MimeDataTools() {
    CALL_LOGGER();
    if (m_accept_types.length()==0) {
        m_accept_types <<
            "text/plain" <<
            "text/html" <<
            "text/richtext";
    }
    QFile jsonFile(":/mt-matchrules.json");
    if (jsonFile.open(QFile::ReadOnly)) {
        QJsonDocument json_rulesets = QJsonDocument::fromJson(jsonFile.readAll());
        foreach (QJsonValue val, json_rulesets.array()) {
            ClipsterCore::MediaTypeMatchRuleSet *mrs = new ClipsterCore::MediaTypeMatchRuleSet();
            mrs->fromJson(val.toObject());
            m_rulesets[mrs->mediaTypeName()] = mrs;
        }
    }
}

const ClipsterCore::MediaTypeMatchRuleSet *MimeDataTools::detectMediaType(const ClipsterCore::MediaTypeSet &mts) {
    CALL_LOGGER();
    QList<const ClipsterCore::MediaTypeMatchRuleSet *> matches;
    foreach (ClipsterCore::MediaTypeMatchRuleSet *mrs, m_rulesets) {
        bool match = mts.checkMatchRules(mrs->detectionRules());
        if (match) {
            matches.append(mrs);
        }
    }
    float score = 0.0;
    const ClipsterCore::MediaTypeMatchRuleSet *selected_msr = 0;
    foreach (const ClipsterCore::MediaTypeMatchRuleSet *mrs, matches) {
        if (mrs->score()>score) {
            score = mrs->score();
            selected_msr = mrs;
        }
    }
    return selected_msr;
}

QString MimeDataTools::copyMimeData(const QMimeData &mimedata, QMimeData *copy) {
    CALL_LOGGER();
    copy->clear();
    ClipsterCore::MediaTypeSet mts(mimedata.formats());
    const ClipsterCore::MediaTypeMatchRuleSet *mrs = detectMediaType(mts);
    if (mrs==0) {
        QStringList mtypes;
        foreach (ClipsterCore::MediaType *mt, mts.mediaTypes().values()) {
            mtypes << mt->mediaType();
        }
        LOG_ERROR(QString("Clipboard data formats did not match any media type rule sets. formats: %1")
                  .arg(mimedata.formats().join(" , ")));
        return "";
    }
    QCryptographicHash h(QCryptographicHash::Sha1);
    h.reset();
    if (mrs->isPureImage()) {
        QImage image = qvariant_cast<QImage>(mimedata.imageData());
        if (!image.isNull()) {
            copy->setImageData(image);
            QBuffer buf;
            if (image.hasAlphaChannel()) {
                image.save(&buf,"png");
                h.addData(buf.data());
                copy->setData("image/png",buf.data());
            }
            else {
                image.save(&buf,"jpg");
                h.addData(buf.data());
                copy->setData("image/jpeg",buf.data());
            }
        }
    }
    else {
        foreach(QString format, mimedata.formats()) {
            foreach (const ClipsterCore::MediaTypeMatchRule *mtmr, mrs->copyRules()) {
                if (mtmr->matchRuleRegExp().indexIn(format) == 0) {
                    // Retrieving data
                    h.addData(mimedata.data(format));
                    // Checking for custom MIME types
                    copy->setData(format, mimedata.data(format));
                    break;
                }
            }
        }
    }
    return h.result().toHex().data();
}

bool first = true;
QString MimeDataTools::calcMimeDataHash(const QMimeData &mimedata) {
    CALL_LOGGER();
    QCryptographicHash h(QCryptographicHash::Sha1);
    h.reset();
    ClipsterCore::MediaTypeSet mts(mimedata.formats());
    const ClipsterCore::MediaTypeMatchRuleSet *mrs = detectMediaType(mts);
    if (mrs==0) {
        LOG_ERROR(QString("Clipboard data formats did not match any media type rule sets. formats: %1")
                  .arg(mimedata.formats().join(" , ")));
        return "";
    }
    if (mrs->isPureImage()) {
        QImage image = qvariant_cast<QImage>(mimedata.imageData());
        if (!image.isNull()) {
            QBuffer buf;
            if (image.hasAlphaChannel()) {
                image.save(&buf,"png");
                h.addData(buf.data());
            }
            else {
                image.save(&buf,"jpg");
                h.addData(buf.data());
            }
        }
    }
    else {
        foreach(QString format, mimedata.formats()) {
            foreach (const ClipsterCore::MediaTypeMatchRule *mtmr, mrs->copyRules()) {
                if (mtmr->matchRuleRegExp().indexIn(format) == 0) {
                    // Retrieving data
                    h.addData(mimedata.data(format));
                    break;
                }
            }
        }
    }
    return h.result().toHex().data();
}

bool MimeDataTools::deleteDataDirectory() {
    CALL_LOGGER();
    QDir datadir(MimeDataTools::dataDirectoryName());
	return datadir.removeRecursively();
}

QString MimeDataTools::dataDirectoryName() {
    CALL_LOGGER();
    // User data location
	QString app_data_location = QStandardPaths::writableLocation(QStandardPaths::DataLocation);
	// Create if not exists
	QDir adl(app_data_location);
	adl.mkdir(app_data_location);
	// Clipster data dir
	QString data_dir = adl.filePath("data");
	// Create if not exists
	adl.mkdir(data_dir);
	return data_dir;
}

bool MimeDataTools::serializeMimeData(const QString &data_hash,const QMimeData &mimedata) {
    CALL_LOGGER();
    QString data_dir = MimeDataTools::dataDirectoryName();

    QFile file(QDir(data_dir).filePath(data_hash));
    if (file.exists()) {
        return false;
    }
    if (!file.open(QIODevice::WriteOnly)) {
        return false;
    }
    QDataStream out(&file);
    foreach (QString typ, mimedata.formats()) {
        if (typ=="application/x-qt-image") {
            continue;
        }
        qDebug("Serializing %s",typ.toUtf8().data());
        out << typ;
        out << mimedata.data(typ);
    }
//    if (mimedata.hasImage()) {
//        QImage image = qvariant_cast<QImage>(mimedata.imageData());
//        if (!image.isNull()) {
//            out << QString("image");
//            out << image;
//        }
//    }
//    else {
//        foreach(QString typ, m_accept_types) {
//            if (mimedata.hasFormat(typ)) {
//                out << typ;
//                out << mimedata.data(typ);
//            }
//        }
//    }
    return true;
}

bool MimeDataTools::deserializeMimeData(const QString &data_hash, QMimeData &mimedata) {
    CALL_LOGGER();
    QString data_dir = MimeDataTools::dataDirectoryName();

    QFile file(QDir(data_dir).filePath(data_hash));
    if (!file.open(QIODevice::ReadOnly)) {
        return false;
    }
    QDataStream in(&file);
    while (!in.atEnd()) {
        QString format;
        QByteArray data;
        in >> format;
        if (format.startsWith("image")) {
            QImage image;
            in >> image;
            mimedata.setImageData(image);
            return true;
        }
        in >> data;
        mimedata.setData(format,data);
    }
    return true;
}
