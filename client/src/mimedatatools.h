#ifndef MIMEDATATOOLS_H
#define MIMEDATATOOLS_H

#include <QMimeData>
#include <mediatype.h>

class MimeDataTools
{
public:
    static MimeDataTools *instance();
    QString copyMimeData(const QMimeData &mimedata, QMimeData *copy);
    QString calcMimeDataHash(const QMimeData &mimedata);
    QString dataDirectoryName();
    bool deleteDataDirectory();
    bool serializeMimeData(const QString &data_hash,const QMimeData &mimedata);
    bool deserializeMimeData(const QString &data_hash, QMimeData &mimedata);
    const ClipsterCore::MediaTypeMatchRuleSet *detectMediaType(const ClipsterCore::MediaTypeSet &mts);

private:
    MimeDataTools();
    QStringList m_accept_types;
    QMap<QString, ClipsterCore::MediaTypeMatchRuleSet *> m_rulesets;
    static MimeDataTools *s_instance;
};

#endif // MIMEDATATOOLS_H
