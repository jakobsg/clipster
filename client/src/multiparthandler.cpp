#include "multiparthandler.h"
#include "stringtools.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <qdebug.h>
#include <crashlib.h>
#ifdef WINDOWS_OS
#include <windows.h>
#endif

AttachmentInfo::AttachmentInfo() {
    m_size = 0;
    CALL_LOGGER();
}

AttachmentInfo::AttachmentInfo(const AttachmentInfo &other) {
    CALL_LOGGER();
    m_size = other.m_size;
    m_headers = other.m_headers;
    m_path = other.m_path;
}

unsigned int AttachmentInfo::size() const {
    CALL_LOGGER();
    return m_size;
}

const HttpHeaders &AttachmentInfo::headers() const {
    CALL_LOGGER();
    return m_headers;
}

const std::string &AttachmentInfo::path() const {
    CALL_LOGGER();
    return m_path;
}

MultiPartReader::MultiPartReader(const std::string &boundary,unsigned int content_length) {
    CALL_LOGGER();
    m_inheader = true;
    m_cur_attachment = NULL;
    m_content_length = content_length;
    m_boundary = "--"+boundary;
    m_min_chunk_rest_size = m_boundary.length()+8;
    m_chunk_rest = NULL;
    m_chunk_rest_size = 0;
    m_request_bytes_read = 0;
    m_attachment_count = 0;
    m_fd = NULL;
}

MultiPartReader::MultiPartReader(const MultiPartReader &other) {
    CALL_LOGGER();
    m_inheader = false;
    m_fd = NULL;
    m_chunk_rest = NULL;
    m_chunk_rest_size = 0;
    m_cur_attachment = NULL;
    m_content_length = other.m_content_length;
    m_boundary = other.m_boundary;
    m_jsonwsp_part << other.m_jsonwsp_part.str();
    m_min_chunk_rest_size = other.m_min_chunk_rest_size;
    m_attachment_count = other.m_attachment_count;
    AttachmentById::const_iterator it = other.m_attachments_by_id.begin();
    for (; it!=other.m_attachments_by_id.end(); ++it) {
        AttachmentInfo *new_ainfo = new AttachmentInfo(*(it->second));
        m_attachments.push_back(new_ainfo);
        m_attachments_by_id[it->first] = new_ainfo;
    }
}

MultiPartReader::~MultiPartReader() {
    CALL_LOGGER();
    if (m_chunk_rest) {
        free(m_chunk_rest);
    }
    if (m_fd) {
        fclose(m_fd);
    }
    AttachmentVector::iterator it = m_attachments.begin();
    for (; it!=m_attachments.end(); ++it) {
        delete *it;
    }
}

#ifdef WINDOWS_OS
#define BUFSIZE 1024
int w_tmpnam(char *buf) {
    CALL_LOGGER();
    BOOL fSuccess  = FALSE;
	DWORD dwRetVal = 0;
	UINT uRetVal   = 0;

	char szTempFileName[MAX_PATH];
	char lpTempPathBuffer[MAX_PATH];
	char  chBuffer[BUFSIZE];
	buf[0] = '\0';
	dwRetVal = GetTempPathA(MAX_PATH,          // length of the buffer
						   lpTempPathBuffer); // buffer for path
	if (dwRetVal > MAX_PATH || (dwRetVal == 0)) {
		return 1;
	}
	uRetVal = GetTempFileNameA(lpTempPathBuffer, // directory for tmp files
							  "clp",     // temp file name prefix
							  0,                // create unique name
							  szTempFileName);  // buffer for name
	if (uRetVal == 0) {
		return 2;
	}
	memcpy(buf,(const char *) szTempFileName,strlen(szTempFileName));
	return 0;
}
#endif

void MultiPartReader::flush_data(char *data, unsigned int size, bool eop) {
    CALL_LOGGER();
    if (size==0) {
        return;
    }
    if (m_fd==NULL && m_attachment_count>0) {
#ifdef WINDOWS_OS
		char tmpname[MAX_PATH];
		w_tmpnam(tmpname);
#else
        char *tmpname;
        tmpname = tmpnam(NULL);
#endif
		m_fd = fopen(tmpname,"w+b");
        m_cur_attachment = new AttachmentInfo();
        m_attachments.push_back(m_cur_attachment);
        m_cur_attachment->m_path = tmpname;
    }
    unsigned int header_offset = 0;
    unsigned int header_end_offset = 0;
    if (m_inheader) {
        char *match_ptr1,*match_ptr2;
        match_ptr1 = strstr(data,"\n\n");
        match_ptr2 = strstr(data,"\r\n\r\n");
        if (match_ptr1) {
            m_inheader = false;
            header_offset = match_ptr1+2-data;
            header_end_offset = header_offset-2;
            if (match_ptr2 && match_ptr2<match_ptr1) {
                header_offset = match_ptr2+4-data;
                header_end_offset = header_offset-4;
            }
        }
        if (match_ptr2) {
            m_inheader = false;
            header_offset = match_ptr2+4-data;
            header_end_offset = header_offset-4;
            if (match_ptr1 && match_ptr1<match_ptr2) {
                header_offset = match_ptr1+2-data;
                header_end_offset = header_offset-2;
            }
        }

        if (!m_inheader) {
            data[header_end_offset] = '\0';
            m_attachment_headers += data;
            std::vector<std::string> header_lines = split(m_attachment_headers,'\n');
            std::vector<std::string>::iterator it = header_lines.begin();
            for (;it!=header_lines.end(); ++it) {
                if (m_attachment_count>0) {
                    m_cur_attachment->m_headers.add_header_line(trim(*it).c_str());
                }
            }
        }
        else {
            m_attachment_headers += data;
        }
    }
    if (!m_inheader) {
        if (m_attachment_count==0) {
            m_jsonwsp_part << std::string(data+header_offset,0,size-header_offset);
        }
        else {
            m_cur_attachment->m_size += size-header_offset;
            fwrite(data+header_offset,1,size-header_offset,m_fd);
        }
    }
    if (eop) {
// 			std::cout << "Content-Id: " << m_cur_attachment->headers().header_value("CONTENT-ID") << "\n";
// 			std::cout << "Size:       " << m_cur_attachment->size() << "\n";
// 			std::cout << "Path:       " << m_cur_attachment->path() << "\n";
        if (m_attachment_count>0) {
            std::string cid = m_cur_attachment->headers().header_value("CONTENT-ID");
            if (!cid.empty()) {
                m_attachments_by_id[cid] = m_cur_attachment;
            }
            fclose(m_fd);
            m_fd = NULL;
        }
        m_inheader = true;
        m_attachment_headers = "";
        m_attachment_count++;
    }
}

void MultiPartReader::write_data(char *data, size_t size, size_t nmemb) {
    CALL_LOGGER();
    bool done = false;
    unsigned int read_size = size*nmemb;
    if (m_content_length && m_content_length <= m_request_bytes_read + read_size) {
        read_size = m_content_length - m_request_bytes_read;
        done = true;
    }
    m_request_bytes_read += read_size;
    // Allocate memory for the previous chunk_rest and the next new chunk of data
    unsigned int chunk_size = m_chunk_rest_size+read_size;
    char *chunk = (char*) malloc(chunk_size);
    // Insert the rest of the previous chunk
    if (m_chunk_rest_size) {
        memcpy(chunk,m_chunk_rest,m_chunk_rest_size);
    }
    // concatenate the new chunk
    memcpy(chunk+m_chunk_rest_size,data,read_size);

    // Start search for part-cutting boundaries
    unsigned int match_size;
    unsigned int prev_part_start = 0;
    bool boundary_end = false;
    // Search for first boundary
    int match_idx = boundary_search(
        chunk,
        chunk_size,
        m_boundary.c_str(),
        m_boundary.length(),
        &match_size,
        &boundary_end,
        0);
    while (match_idx>-1) {
        // flush data till part end
        flush_data(chunk+prev_part_start,match_idx-prev_part_start,true);
// 			std::cout << "Boundary Match: " << match_idx;
// 			std::cout << " Length: " << match_size ;
// 			printf(" %20.20s\n",data+match_idx+match_size);
        if (match_idx>-1) {
            prev_part_start = match_idx+match_size;
        }
        if (boundary_end) {
            break;
        }
        match_idx = boundary_search(
            chunk,
            chunk_size,
            m_boundary.c_str(),
            m_boundary.length(),
            &match_size,
            &boundary_end,
            match_idx+match_size);
    }
    // Flush until m_min_chunk_rest_size
    unsigned int rest = chunk_size-prev_part_start;
    unsigned int flush_size = 0;
    if (!done && rest>m_min_chunk_rest_size) {
        flush_size = rest-m_min_chunk_rest_size;
    }
    if (done) {
        flush_data(chunk+prev_part_start,rest,false);
    }
    else {
        flush_data(chunk+prev_part_start,flush_size,false);
    }
    if (m_chunk_rest) {
        free(m_chunk_rest);
        m_chunk_rest = NULL;
    }
    if (!done) {
        m_chunk_rest_size = rest-flush_size;
        m_chunk_rest = (char *) malloc(m_chunk_rest_size);
        memcpy(m_chunk_rest,chunk+(chunk_size-m_chunk_rest_size),m_chunk_rest_size);
    }
    free(chunk);
    chunk = NULL;
}

const AttachmentInfo *MultiPartReader::attachment_by_sequence(int seq) const {
    CALL_LOGGER();
    if (seq<(int)m_attachments.size()) {
        return m_attachments[seq];
    }
    return NULL;
}

int MultiPartReader::attachment_count() const {
    CALL_LOGGER();
    return (int)m_attachments.size();
}

const AttachmentInfo *MultiPartReader::attachment_by_id(const std::string &cid) const {
    CALL_LOGGER();
    AttachmentById::const_iterator it = m_attachments_by_id.find(cid);
    if (it!=m_attachments_by_id.end()) {
        return it->second;
    }
    return NULL;
}

std::string MultiPartReader::jsonwsp() const {
    CALL_LOGGER();
    return m_jsonwsp_part.str();
}

std::stringstream &MultiPartReader::jsonwsp_stream() {
    CALL_LOGGER();
    return m_jsonwsp_part;
}

