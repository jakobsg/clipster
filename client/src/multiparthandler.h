#ifndef MULTIPARTHANDLER_H
#define MULTIPARTHANDLER_H

#include "httpheaders.h"
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>


class MultiPartReader;

class AttachmentInfo {
    friend class MultiPartReader;
public:
    AttachmentInfo();
    AttachmentInfo(const AttachmentInfo &other);
    unsigned int size() const;
    const HttpHeaders &headers() const;
    const std::string &path() const;
private:
    unsigned int m_size;
    HttpHeaders m_headers;
    std::string m_path;
};

class MultiPartReader {
public:
    typedef std::vector<AttachmentInfo *> AttachmentVector;
    typedef std::map<std::string,AttachmentInfo *> AttachmentById;
    MultiPartReader(const std::string &boundary,unsigned int content_length);
    MultiPartReader(const MultiPartReader &other);
    ~MultiPartReader();
    void write_data(char *data, size_t size, size_t nmemb);
    const AttachmentInfo *attachment_by_sequence(int seq) const;
    const AttachmentInfo *attachment_by_id(const std::string &cid) const;
    int attachment_count() const;
    std::string jsonwsp() const;
    std::stringstream &jsonwsp_stream();

private:
    void flush_data(char *data, unsigned int size, bool eop);
    unsigned int m_content_length;
    std::string m_boundary;
    std::stringstream m_jsonwsp_part;
    char *m_chunk_rest;
    unsigned int m_chunk_rest_size;
    unsigned int m_min_chunk_rest_size;
    AttachmentInfo *m_cur_attachment;
    AttachmentVector m_attachments;
    AttachmentById m_attachments_by_id;
    std::string m_attachment_headers;
    FILE *m_fd;
    unsigned int m_request_bytes_read;
    unsigned int m_attachment_count;
    bool m_inheader;
};

#endif
