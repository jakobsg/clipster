#include "networkreplyparser.h"

#include <QNetworkReply>
#include <QObject>
#include <QRegExp>
#include <QString>
#include <QStringList>

NetworkReplyParser::NetworkReplyParser( QNetworkReply *reply )
    : mReply( reply )
    , mValid( false ) {
  if ( !mReply ) return;

  // Content type examples:
  //   multipart/mixed; boundary=wcs
  //   multipart/mixed; boundary="wcs"\n
  if ( !isMultipart( mReply ) )
  {
    // reply is not multipart, copy body and headers
    QMap<QByteArray, QByteArray> headers;
    foreach ( QByteArray h, mReply->rawHeaderList() )
    {
        headers.insert( h, mReply->rawHeader( h ) );
    }
    mHeaders.append( headers );
    mBodies.append( mReply->readAll() );
  }
  else // multipart
  {
    QString contentType = mReply->header( QNetworkRequest::ContentTypeHeader ).toString();
    QRegExp re( ".*boundary=\"?([^;\"]+)\"?\\s?", Qt::CaseInsensitive );

    if ( !( re.indexIn( contentType ) == 0 ) )
    {
      mError = tr( "Cannot find boundary in multipart content type" );
      return;
    }

    QString boundary = re.cap( 1 );
    boundary = "--" + boundary;
    qDebug() << "Boundary" << boundary;

    // Lines should be terminated by CRLF ("\r\n") but any new line combination may appear
    QByteArray data = mReply->readAll();
    int from, to;
    from = data.indexOf( boundary.toLatin1(), 0 ) + boundary.length() + 1;
    //QVector<QByteArray> partHeaders;
    //QVector<QByteArray> partBodies;
    while ( true )
    {
      // 'to' is not really 'to', but index of the next byte after the end of part
      to = data.indexOf( boundary.toLatin1(), from );
      if ( to < 0 )
      {
        // It may be end, last boundary is followed by '--'
        if ( data.size() - from - 1 == 2 && QString( data.mid( from, 2 ) ) == "--" ) // end
        {
          break;
        }

        // It may happen that boundary is missing at the end (GeoServer)
        // in that case, take everything to the end
        if ( data.size() - from > 1 )
        {
          to = data.size(); // out of range OK
        }
        else
        {
          break;
        }
      }
      QByteArray part = data.mid( from, to - from );
      // Remove possible new line from beginning
      while ( part.size() > 0 && ( part.at( 0 ) == '\r' || part.at( 0 ) == '\n' ) )
      {
        part.remove( 0, 1 );
      }
      // Split header and data (find empty new line)
      // New lines should be CRLF, but we support also CRLFCRLF, LFLF to find empty line
      int pos = 0; // body start
      while ( pos < part.size() - 1 )
      {
        if ( part.at( pos ) == '\n' && ( part.at( pos + 1 ) == '\n' || part.at( pos + 1 ) == '\r' ) )
        {
          if ( part.at( pos + 1 ) == '\r' ) pos++;
          pos += 2;
          break;
        }
        pos++;
      }
      // parse headers
      RawHeaderMap headersMap;
      QByteArray headers = part.left( pos );

      QStringList headerRows = QString( headers ).split( QRegExp( "[\n\r]+" ) );
      foreach ( QString row, headerRows )
      {
        QStringList kv = row.split( ": " );
        headersMap.insert( kv.value( 0 ).toLatin1(), kv.value( 1 ).toLatin1() );
      }
      mHeaders.append( headersMap );

      mBodies.append( part.mid( pos ) );

      from = to + boundary.length();
    }
  }
  mValid = true;
}

bool NetworkReplyParser::isMultipart( QNetworkReply *reply )
{
  if ( !reply ) return false;

  QString contentType = reply->header( QNetworkRequest::ContentTypeHeader ).toString();

  // Multipart content type examples:
  //   multipart/mixed; boundary=wcs
  //   multipart/mixed; boundary="wcs"\n
  return contentType.startsWith( "multipart/", Qt::CaseInsensitive );
}
