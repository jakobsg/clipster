#include "onlineclipboard.h"
#include "clipboarddatamanager.h"
#include "webserviceclient.h"
#include "mimedatatools.h"
#include <mediatype.h>
#include <crashlib.h>

#include <QJsonDocument>
#include <QImage>

OnlineClipboard::OnlineClipboard(QObject *parent) : ClipboardDataProvider(parent) {
    CALL_LOGGER();
    connect(WebServiceClient::instance(),SIGNAL(whoamiResponseReady(const WhoamiResponse&)),this,SLOT(processWhoamiResponse(const WhoamiResponse &)));
    connect(WebServiceClient::instance(),SIGNAL(dataEntryListResponseReady(const DataEntryListResponse&)),this,SLOT(processDataEntries(const DataEntryListResponse&)));
}

OnlineClipboard::~OnlineClipboard() {
    CALL_LOGGER();
}

void OnlineClipboard::processWhoamiResponse(const WhoamiResponse &response) {
    CALL_LOGGER();
    if (response.m_res_code==0) {
        WebServiceClient::instance()->listDataEntries();
    }
}

void OnlineClipboard::processDataEntries(const DataEntryListResponse &response) {
    CALL_LOGGER();
    foreach (QJsonValue v, response.m_data_entries) {
        QJsonObject entry = v.toObject();
        QString data_hash = entry["data_hash"].toString();
        ClipboardData *cb_data = 0;
        cb_data = ClipboardDataManager::instance()->clipboardDataByDataHash(data_hash);
        if (cb_data==0) {
            cb_data = new ClipboardData(data_hash);
        }
        cb_data->setCopyTime(QDateTime::fromMSecsSinceEpoch(entry["copytime"].toDouble()));
        cb_data->setDataOrigin(ClipboardData::RemoteDistributedCopy);
        if (!entry["text_preview"].isNull()) {
            cb_data->setTextPreview(entry["text_preview"].toString());
        }
        if (!entry["image_preview"].isNull()) {
            QImage img = QImage::fromData(QByteArray::fromBase64(entry["image_preview"].toString().toLatin1()),"png");
            cb_data->setImagePreview(img);
        } else {
            QStringList formats;
            foreach (QJsonValue val, entry["mime_types"].toArray()) {
                formats.append(val.toString());
            }
            ClipsterCore::MediaTypeSet mts(formats);
            const ClipsterCore::MediaTypeMatchRuleSet *mrs = MimeDataTools::instance()->detectMediaType(mts);
            if (mrs) {
                cb_data->setImagePreview(QImage(mrs->iconResourceUrl()));
            }
        }
        addClipboardData(cb_data);
        ClipboardDataManager::instance()->processClipboardData(cb_data);
    }
}
