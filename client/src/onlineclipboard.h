#ifndef ONLINECLIPBOARD_H
#define ONLINECLIPBOARD_H

#include "clipboarddataprovider.h"
#include "dataentrylistresponse.h"
#include "whoamiresponse.h"

class OnlineClipboard : public ClipboardDataProvider
{
    Q_OBJECT
public:
    explicit OnlineClipboard(QObject *parent = 0);
    ~OnlineClipboard();

signals:

public slots:
private slots:
    void processWhoamiResponse(const WhoamiResponse &response);
    void processDataEntries(const DataEntryListResponse &response);
};

#endif // ONLINECLIPBOARD_H
