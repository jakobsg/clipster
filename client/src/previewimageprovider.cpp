#include "previewimageprovider.h"
#include "clipboarddatamanager.h"
#include "clipboarddata.h"
#include <crashlib.h>

#define PREVIEW_MAX_WIDTH 32
#define PREVIEW_MAX_HEIGHT 24

PreviewImageProvider::PreviewImageProvider() :
QQuickImageProvider(QQuickImageProvider::Image) {
    CALL_LOGGER();
}

PreviewImageProvider::~PreviewImageProvider() {
    CALL_LOGGER();
}

QImage PreviewImageProvider::requestImage(const QString & id, QSize * size, const QSize & requestedSize) {
    Q_UNUSED(requestedSize);
    CALL_LOGGER();
    ClipboardData *cb_data = ClipboardDataManager::instance()->clipboardDataByDataHash(id);
    if (cb_data && cb_data->imagePreview().isNull()==false) {
        QImage prev_img;
        if ((float)cb_data->imagePreview().height()/cb_data->imagePreview().width() > 0.75) {
            prev_img = cb_data->imagePreview().scaledToHeight(PREVIEW_MAX_HEIGHT,Qt::SmoothTransformation);
        }
        else {
            prev_img = cb_data->imagePreview().scaledToWidth(PREVIEW_MAX_WIDTH,Qt::SmoothTransformation);
        }
        if (size) {
            *size = prev_img.size();
        }
        return prev_img;
    }

    QImage defimg(":/images/doc.png");
    if (size) {
        *size = defimg.size();
    }
    return defimg;
}
