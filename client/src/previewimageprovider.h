#ifndef PREVIEWIMAGEPROVIDER_H
#define PREVIEWIMAGEPROVIDER_H

#include <QQuickImageProvider>

class PreviewImageProvider : public QQuickImageProvider {
public:
    PreviewImageProvider();
    ~PreviewImageProvider();
    virtual QImage requestImage(const QString & id, QSize * size, const QSize & requestedSize);

private:
    QHash<QString,QImage> m_previews;
};

#endif // PREVIEWIMAGEPROVIDER_H
