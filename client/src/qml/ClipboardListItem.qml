import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0
import Limbosoft.Clipster 1.0

Rectangle {
    id: root
    height: 40
    width: parent.width - 2
    color: "transparent"
    signal selected(int index);
    signal networkDistribute(int index);
    signal callbackNetworkDistribution(int index);
    signal itemEntered(int index);
    signal itemExited(int index);

    radius: 4
    LinearGradient {
        start: Qt.point(0,0)
        end: Qt.point(width,0)
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#0040E040" }
            GradientStop { position: transferProgress; color: "#5040E040" }
            GradientStop { position: transferProgress+0.001; color: "#00000000" }
        }
        anchors.fill: parent

        RowLayout {
            y: 6
            x: 12
            Image {
                source: "image://preview/" + dataHash
            }
            ColumnLayout {
                spacing: 0
                Text {
                    id: dataPreview
                    text: textPreview
                    font.family: "Arial"
                    elide: Text.ElideMiddle
                    textFormat: Text.PlainText
                    maximumLineCount: 1
                    Layout.fillWidth: true
                    color: "grey"
                }
                Text {
                    id: dataType
                    text: copyTime
                    font.family: "Arial"
                }
                Layout.maximumWidth: 550
                Layout.minimumWidth: 550

                MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: {
                        root.itemEntered(index);
                    }
                    onExited: {
                        root.itemExited(index);
                    }
                    onClicked: {
                        root.forceActiveFocus();
                        root.selected(index);
//                        root.state = "nonhovered";
                    }
                }

            }
            Image {
                id: dataOriginImage
                source: "images/cloud-in.png"
                Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
                opacity: 0.5
                property string org: "local";

                MouseArea {
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: {
                        dataOriginImage.opacity = 1.0;
                    }
                    onExited: {
                        dataOriginImage.opacity = 0.5;
                    }

                    onClicked: {
                        console.debug(dataOriginImage.org);
                        if (dataOriginImage.org==="local") {
                            dataOriginImage.org = "dist";
                            dataOriginImage.source = "qrc:/images/cloud-closed.png";
                            networkDistribute(index);
                        }
                        else {
                            dataOriginImage.org = "local";
                            dataOriginImage.source="qrc:/images/cloud-in.png"
                            callbackNetworkDistribution(index);
                        }
                    }
                }
            }
            Layout.fillWidth: true
        }
    }
//    states: [
//        State {
//            name: "nonhovered"
//            PropertyChanges { target: root; color: "#eee" }
//        },
//        State {
//            name: "hovered"
//            PropertyChanges { target: root; color: "lightblue" }
//        }
//    ]
    transitions: Transition {
        to: "nonhovered"
        ColorAnimation { duration:250; easing.type: Easing.InOutQuad }
    }

    Component.onCompleted: {
        if (dataOrigin === ClipboardData.LocalDistributedCopy) {
            dataOriginImage.source = "images/cloud-closed.png";
            dataOriginImage.org = "dist";
        }
        else if (dataOrigin === ClipboardData.LocalCopy) {
            dataOriginImage.source = "images/cloud-in.png";
            dataOriginImage.org = "local";
        }
        else if (dataOrigin === ClipboardData.RemoteDistributedCopy) {
            dataOriginImage.source = "images/cloud-out.png";
        }
    }

}
