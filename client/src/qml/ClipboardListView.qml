import QtQuick 2.0

import Limbosoft.Clipster 1.0

Rectangle {
    id: root
    height: childrenRect.height
    clip: true
    color: "transparent"
    property alias model: listview.model
    property alias listView: listview

    signal dataSelected(ClipboardData data)

    function selectCurrent() {
        root.dataSelected(clipboard_proxy_model.clipboardData(listview.currentIndex));
    }

    ListView {
        id: listview
        width: parent.width
        property ClipboardListItem latestEntered

        height: {
            if (count>0) {
                40 * (count<10 ? count : 10);
            }
        }
        highlightMoveVelocity: -1
        highlightMoveDuration: -1
        highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
        delegate: ClipboardListItem {
            onSelected: {
                root.dataSelected(clipboard_proxy_model.clipboardData(index));
            }
            onNetworkDistribute: {
                clipboard_proxy_model.clipboardData(index).upload();
            }

            onItemEntered: {
//                if (listview.latestEntered!=null) {
//                    listview.latestEntered.state = "nonhovered"
//                }
                listview.currentIndex = index;
//                listview.currentItem.state = "hovered";
//                listview.latestEntered = listview.currentItem;
            }
            onItemExited: {
//                listview.currentIndex = index;
//                listview.currentItem.state = "nonhovered";
            }
        }

    }
    function moveUp() {
        if (listview.currentIndex>0) {
            listview.currentIndex--;
        }
//        if (listview.currentIndex>0) {
//            listview.currentIndex--;
//            listview.currentItem.state = "hovered";
//            if (listview.latestEntered!=null) {
//                listview.latestEntered.state = "nonhovered";
//            }
//            listview.latestEntered = listview.currentItem;
//        }
    }

    function moveDown() {
        if (listview.currentIndex<listview.count-1) {
            listview.currentIndex++;
        }
//        if (listview.currentIndex==0 && listview.currentItem.state !== "hovered") {
//            listview.currentItem.state = "hovered";
//            listview.latestEntered = listview.currentItem;
//            return;
//        }

//        if (listview.currentIndex<listview.count-1) {
//            listview.currentIndex++;
//            if (listview.latestEntered!=null) {
//                listview.latestEntered.state = "nonhovered";
//            }
//            listview.currentItem.state = "hovered";
//            listview.latestEntered = listview.currentItem;
//        }
    }
}
