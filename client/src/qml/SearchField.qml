import QtQuick 2.0
import QtQuick.Layouts 1.1

RowLayout {
    id: root

    signal magnifyingGlassClicked()
    signal closeClicked()
    signal searchChanged(string text)
    signal keyUpPressed()
    signal keyDownPressed()
    signal keyEnterPressed()

    function searchInputForceFocus() {
        searchTextEdit.forceActiveFocus();
        searchTextEdit.text = "";
        root.searchChanged(searchTextEdit.text);
    }

    height: 56

    Timer {
        id: deferSearchSignal
        interval: 500
        running: false
        repeat: false
        onTriggered: {
            root.searchChanged(searchTextEdit.text);
        }
    }

    Image {
        source: "images/magnifying-glass.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                root.magnifyingGlassClicked();
            }
        }
    }
    TextInput {
        id: searchTextEdit
        Layout.fillWidth: true
        color: "black"
        font.pixelSize: 24
        clip: true

        Text {
            id: defaultText
            text: "Clipboard Search"
            font.pixelSize: parent.font.pixelSize
            color: "grey"
        }
        onTextChanged: {
            deferSearchSignal.restart()
            if (text==="") {
                defaultText.text = "Clipboard Search";
            }
            else {
                defaultText.text = "";
            }
        }
        Keys.onPressed: {
            if (event.key === Qt.Key_Down) {
                root.keyDownPressed();
            }
            if (event.key === Qt.Key_Up) {
                root.keyUpPressed();
            }
            if (event.key === Qt.Key_Escape) {
                root.closeClicked();
            }
        }
        onAccepted: {
            root.keyEnterPressed();
        }
    }
    Image {
        source: "images/close.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                root.forceActiveFocus();
                root.closeClicked();
            }
        }
    }
}
