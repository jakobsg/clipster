import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import Limbosoft.Clipster 1.0

ApplicationWindow {
    id: mainWindow
    width: 640
    visible: true
    color: "transparent"
    flags: mainWindow.flags + Qt.FramelessWindowHint + Qt.WindowStaysOnTopHint
    property int clipidx: 0;
    x: (Screen.desktopAvailableWidth/2) - 320
    y: (Screen.desktopAvailableHeight/2) - 200

    ClipboardModel {
        id: clipboard_model
    }
    Clipboard {
        id: clipboard
        onClipboardEntryReady: {
        }
    }

    Rectangle {
        id: mainRect
        color: "#eee"
        anchors.left: parent.left
        anchors.right: parent.right
        height: childrenRect.height + 6
        border.color: "grey"
        border.width: 1
        clip: true
        radius: 6
        SearchField {
            id: searchfield
            width: parent.width - 24
            x: 12
            y: 4
            onMagnifyingGlassClicked: {
                //clipholder.children[0].destroy();
            }
            onCloseClicked: {
                mainWindow.visible = false;
            }
            onSearchChanged: {
                clipboard_proxy_model.setFilterWildcard(text);
            }
            onKeyDownPressed: {
                clipboard_list.moveDown();
            }
            onKeyUpPressed: {
                clipboard_list.moveUp();
            }
            onKeyEnterPressed: {
                clipboard_list.selectCurrent();
            }
        }
        ClipboardListView {
            id: clipboard_list
            width: parent.width
            anchors.top: searchfield.bottom
            x: 1

            model: ClipboardProxyModel {
                id: clipboard_proxy_model
                Component.onCompleted: {
                    setSourceClipboardModel(clipboard_model);
                }
            }

            onDataSelected: {
                clipboard.activateClipboardData(data);
                //mainWindow.visible = false;
            }
        }


//        PropertyAnimation {
//            id: heightAnimation
//            duration: 750
//            target: mainRect
//            properties: "height"
//            easing.type: Easing.OutExpo
//        }
//        onChildrenRectChanged: {
//            mainWindow.height = childrenRect.height + (childrenRect.height==0 ? 0 : 12);
//            heightAnimation.stop();
//            heightAnimation.to = childrenRect.height + (childrenRect.height==0 ? 0 : 12);
//            heightAnimation.start();
//        }
//        Component.onCompleted: {
//            mainWindow.height = childrenRect.height + (childrenRect.height==0 ? 0 : 12);
//            heightAnimation.stop();
//            heightAnimation.to = childrenRect.height + (childrenRect.height==0 ? 0 : 12);
//            heightAnimation.start();
//        }
    }
    onVisibilityChanged: {
        if (visible==true) {
            searchfield.searchInputForceFocus();
        }
        clipboard_list.listView.currentIndex = 0;
//        if (clipboard_list.listView.latestEntered!=null) {
//            clipboard_list.listView.latestEntered.state = "nonhovered";
//        }
    }
}
