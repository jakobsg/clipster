#include "roundedpopup.h"
#include "roundedpopupbutton.h"
#include <QLayout>
#include <QIcon>
#include <QActionEvent>
#include <QPropertyAnimation>
#include <QStateMachine>
#include <QState>
#include <QSignalTransition>
#include <QTimer>

RoundedPopup::RoundedPopup(QWidget *parent)
: QWidget(parent) {
    //setWindowFlags(windowFlags()|Qt::WindowStaysOnTopHint|Qt::FramelessWindowHint);
    setWindowFlags(windowFlags()|Qt::WindowStaysOnTopHint|Qt::FramelessWindowHint|Qt::Tool);
    //this->re(500,500);
    setAttribute(Qt::WA_TranslucentBackground);
    QHBoxLayout *ml = new QHBoxLayout(this);
    setLayout(ml);
    ml->setContentsMargins(QMargins(0,9,9,9));
    m_baseframe = new QFrame(this);
    ml->addWidget(m_baseframe);
    m_baseframe->setStyleSheet("QFrame{border: 1px solid #555; border-radius: 4px; background-color: #fff; }"
                               "QPushButton { text-align: left; padding: 10px; }");
    m_buttonLayout = new QVBoxLayout(m_baseframe);
    m_baseframe->setLayout(m_buttonLayout);

    m_closeTimer = new QTimer(this);
    m_closeTimer->setInterval(3000);
    m_closeTimer->setSingleShot(true);
    connect(m_closeTimer, SIGNAL(timeout()), this, SLOT(startClose()));

    m_stateshown = new QState();
    m_statehidden = new QState();
    m_stateshown->assignProperty(this, "windowOpacity", 1.0);
    m_stateshown->assignProperty(this,"visible",true);
    m_statehidden->assignProperty(this, "windowOpacity", 0.0);

    m_fadein = new QPropertyAnimation(this,"windowOpacity");
    m_fadein->setDuration(250);
    m_fadeout = new QPropertyAnimation(this,"windowOpacity");
    m_fadeout->setDuration(1500);
    QSignalTransition *t1 = m_statehidden->addTransition(this,SIGNAL(fadeIn()),m_stateshown);
    connect(m_fadein,SIGNAL(finished()), m_closeTimer, SLOT(start()));
    t1->addAnimation(m_fadein);
    QSignalTransition *t2 = m_stateshown->addTransition(this,SIGNAL(fadeOut()),m_statehidden);
    t2->addAnimation(m_fadeout);
    m_stateshown->addTransition(this,SIGNAL(fastFadeOut()),m_statehidden);
    connect(m_fadeout,SIGNAL(finished()),this,SLOT(fadeOutFinished()));
    m_statemachine = new QStateMachine(this);
    m_statemachine->addState(m_stateshown);
    m_statemachine->addState(m_statehidden);
    m_statemachine->setInitialState(m_statehidden);
    m_statemachine->start();
    setMouseTracking(true);
}

RoundedPopup::~RoundedPopup() {
}

void RoundedPopup::popup(const QPoint &pos) {
    qDebug("popup");
    QPoint final_pos = pos;
    if (pos.isNull()) {
        final_pos = QCursor::pos()-QPoint(32,0);
    }

    move(final_pos);
    if (!geometry().contains(QCursor::pos())) {
        m_closeTimer->start();
    }
    raise();
    show();
    emit fadeIn();
}

void RoundedPopup::fixSize() {
    resize(sizeHint());
}

void RoundedPopup::startClose() {
    emit fadeOut();
}

void RoundedPopup::fastClose() {
    emit fadeIn();
    emit fastFadeOut();
    m_closeTimer->stop();
}

void RoundedPopup::fadeOutFinished() {
    this->close();
}

void RoundedPopup::buttonClick(RoundedPopupButton *obj, bool checked) {
    if (m_button_map_action.contains(obj)) {
        emit actionTriggered(m_button_map_action[obj]);
    }
}

void RoundedPopup::actionEvent(QActionEvent *e) {
    if (e->type() == QEvent::ActionAdded) {
        QString hover_effect =
           "QPushButton {"
           "  text-align: left;"
           "  padding: 6px;"
           "  border: 0px;"
           "  background-color: #fff;"
           "}"
           "QPushButton:hover:!pressed {"
           "  background-color: #ddd;"
           "  border-radius: 4px;"
           "}"
           "QPushButton:hover:pressed {"
           "  background-color: #ccc;"
           "  border-radius: 4px;"
           "}";
        RoundedPopupButton *b = new RoundedPopupButton(e->action()->icon(),e->action()->text(),this);
        b->setIconSize(QSize(32,32));
        b->setStyleSheet(hover_effect);
        m_buttonLayout->addWidget(b);
        connect(b, SIGNAL(clicked()), e->action(), SLOT(trigger()));
        connect(b, SIGNAL(clicked()), this, SLOT(close()));
        connect(b, SIGNAL(clicked(RoundedPopupButton*,bool)), this, SLOT(buttonClick(RoundedPopupButton*,bool)));
        m_action_map_button[e->action()] = b;
        m_button_map_action[b] = e->action();
    }
    if (e->type() == QEvent::ActionRemoved) {
        if (m_action_map_button.contains(e->action())) {
            RoundedPopupButton *b = m_action_map_button.take(e->action());
            m_button_map_action.take(b);
            m_buttonLayout->removeWidget(b);
            delete b;
        }
    }
}

void RoundedPopup::enterEvent(QEvent *e) {
    emit fadeIn();
    disconnect(m_fadein,SIGNAL(finished()), m_closeTimer, SLOT(start()));
    m_closeTimer->stop();
    QWidget::enterEvent(e);
}

void RoundedPopup::showEvent(QShowEvent *se) {
    Q_UNUSED(se);
    qDebug("show");
    QTimer::singleShot(0,this,SLOT(fixSize()));
}

void RoundedPopup::leaveEvent(QEvent *e) {
    m_closeTimer->start();
    QWidget::leaveEvent(e);
}


