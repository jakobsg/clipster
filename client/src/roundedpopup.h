#ifndef ROUNDEDPOPUP_H
#define ROUNDEDPOPUP_H

#include "roundedpopupbutton.h"

#include <QWidget>
#include <QFrame>
#include <QMenu>

class QVBoxLayout;
class QPropertyAnimation;
class QStateMachine;
class QState;
class QTimer;

typedef QMap<QAction *, RoundedPopupButton *> ActionMapButton;
typedef QMap<RoundedPopupButton *, QAction *> ButtonMapAction;

class RoundedPopup : public QWidget {

    Q_OBJECT

public:

    explicit RoundedPopup(QWidget *parent = 0);
    ~RoundedPopup();

protected:
    void actionEvent(QActionEvent *);
    void leaveEvent(QEvent *e);
    void enterEvent(QEvent *e);
    void showEvent(QShowEvent *se);

signals:
    void fadeIn();
    void fadeOut();
    void fastFadeOut();
    void actionTriggered(QAction *triggered);

public slots:
    void popup(const QPoint &pos = QPoint());
    void startClose();
    void fastClose();
    void fixSize();
    void fadeOutFinished();
    void buttonClick(RoundedPopupButton *obj, bool checked);

private:
    QFrame *m_baseframe;
    QVBoxLayout *m_buttonLayout;
    QStateMachine *m_statemachine;
    QState *m_statehidden;
    QState *m_stateshown;
    QPropertyAnimation *m_fadein;
    QPropertyAnimation *m_fadeout;
    QTimer *m_closeTimer;
    ActionMapButton m_action_map_button;
    ButtonMapAction m_button_map_action;
};

#endif // ROUNDEDPOPUP_H
