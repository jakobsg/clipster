#include "roundedpopupbutton.h"

RoundedPopupButton::RoundedPopupButton(QWidget *parent)
: QPushButton(parent){
    connect(this, SIGNAL(clicked(bool)),this,SLOT(selfClicked(bool)));
}

RoundedPopupButton::RoundedPopupButton(const QIcon &icon, const QString &text, QWidget *parent)
: QPushButton(icon,text,parent) {
    connect(this, SIGNAL(clicked(bool)),this,SLOT(selfClicked(bool)));
}

void RoundedPopupButton::selfClicked(bool checked) {
    emit clicked(this, checked);
}

