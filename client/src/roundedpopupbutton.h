#ifndef ROUNDEDPOPUPBUTTON_H
#define ROUNDEDPOPUPBUTTON_H

#include <QPushButton>
#include <QIcon>

class RoundedPopupButton : public QPushButton {

    Q_OBJECT

public:
    RoundedPopupButton(QWidget *Parent=0);
    RoundedPopupButton(const QIcon &icon, const QString &text, QWidget *parent = 0);

signals:
    void clicked(RoundedPopupButton *obj, bool checked);

public slots:

private slots:
    void selfClicked(bool checked);
};

#endif // ROUNDEDPOPUPBUTTON_H
