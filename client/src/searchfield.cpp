#include "searchfield.h"
#include <QKeyEvent>

SearchField::SearchField(QWidget *parent)
: QLineEdit(parent) {

}

SearchField::~SearchField() {

}

void SearchField::keyPressEvent(QKeyEvent *event) {
    QLineEdit::keyPressEvent(event);
    if (event->key()==Qt::Key_Up) {
        emit upPressed();
    }
    else if (event->key()==Qt::Key_Down) {
        emit downPressed();
    }
    else if (event->key()==Qt::Key_Return) {
        emit returnPressed();
    }
    else if (event->key()==Qt::Key_Escape) {
        emit escapePressed();
    }
}
