#ifndef SEARCHFIELD_H
#define SEARCHFIELD_H

#include <QLineEdit>

class SearchField : public QLineEdit
{

    Q_OBJECT

public:
    SearchField(QWidget *parent);
    ~SearchField();

signals:
    void upPressed();
    void downPressed();
    void returnPressed();
    void escapePressed();

protected:
    void keyPressEvent(QKeyEvent * event);

};

#endif // SEARCHFIELD_H
