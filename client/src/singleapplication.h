#ifndef SINGLEAPPLICATION_H
#define SINGLEAPPLICATION_H

#include <QSharedMemory>

class SingleApplication
{
public:
    SingleApplication(bool unique_by_path=false);
    ~SingleApplication();
    bool alreadyRunning();
private:
    QSharedMemory m_singleapp_pid;
    bool m_already_running;
};

#endif // SINGLEAPPLICATION_H
