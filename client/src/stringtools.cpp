#include "stringtools.h"
#include <sstream>
#include <iostream>
#include <algorithm>
#include <functional>
#include <cctype>
#include <crashlib.h>

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    CALL_LOGGER();
    std::stringstream ss(s);
	std::string item;
	while(std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
    CALL_LOGGER();
    std::vector<std::string> elems;
	return split(s, delim, elems);
}


// trim from both ends
std::string &trim(std::string &s) {
    CALL_LOGGER();
    return ltrim(rtrim(s));
}

// trim from start
std::string &ltrim(std::string &s) {
    CALL_LOGGER();
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
	return s;
}

// trim from end
std::string &rtrim(std::string &s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
	return s;
}

std::string &remove(std::string &s, char remove_ch) {
	s.erase(
		remove( s.begin(), s.end(), remove_ch ), s.end() );
	return s;
}


int boundary_search(const char *data, int data_size, const char *needle, int needle_size, unsigned int *match_size,bool *end_boundary,unsigned int start_idx) {
    int match_idx = -1;
	int midx;

	for (int bidx=start_idx; bidx<data_size-needle_size; bidx++) {
		match_idx = bidx;
		for (midx=0; midx<needle_size; midx++) {
			char nxt = data[bidx+midx];
			char *uchr = (char *)(&nxt);
			if ((*uchr) != needle[midx]) {
				match_idx = -1;
				break;
			}
		}
		if (match_idx>-1) {
			if (data[match_idx+needle_size]=='\n') {
				if (match_size) {
					*match_size = needle_size+1;
				}
				break;
			}
			else if (data[match_idx+needle_size]=='\r' && data[match_idx+needle_size+1]=='\n') {
				if (match_size) {
					*match_size = needle_size+2;
				}
				break;
			}
			else if (data[match_idx+needle_size]=='-' && data[match_idx+needle_size+1]=='-') {
				if (match_size) {
					*match_size = needle_size+2;
				}
				if (end_boundary) {
					*end_boundary = true;
				}
				break;
			}
			match_idx = -1;
		}
	}
	if (match_idx==-1 && match_size) {
		*match_size = 0;
	}
	return match_idx;
}
