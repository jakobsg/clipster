#ifndef STRINGTOOLS_H
#define STRINGTOOLS_H

#include <vector>
#include <string>

std::vector<std::string> &split(
	const std::string &s,
	char delim,
	std::vector<std::string> &elems);

std::vector<std::string> split(
	const std::string &s,
	char delim);

std::string &trim(std::string &s);
std::string &ltrim(std::string &s);
std::string &rtrim(std::string &s);
std::string &remove(std::string &s, char remove_ch);

int boundary_search(
	const char *data,
	int data_size,
	const char *needle,
	int needle_size,
	unsigned int *match_size=NULL,
	bool *end_boundary=NULL,
	unsigned int start_idx=0);
#endif