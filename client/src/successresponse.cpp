#include "successresponse.h"
#include <crashlib.h>

SuccessResponse::SuccessResponse()
: CommonResponse() {
    CALL_LOGGER();
    m_success = false;
}

SuccessResponse::SuccessResponse(const CommonResponse &resp)
: CommonResponse(resp) {
    CALL_LOGGER();
    m_success = false;
}

SuccessResponse::~SuccessResponse() {
    CALL_LOGGER();
}
