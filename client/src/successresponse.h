#ifndef SUCCESSRESPONSE_H
#define SUCCESSRESPONSE_H

#include "commonresponse.h"

class SuccessResponse : public CommonResponse {
public:
    SuccessResponse();
    SuccessResponse(const CommonResponse &resp);
    ~SuccessResponse();

    bool m_success;

};

#endif
