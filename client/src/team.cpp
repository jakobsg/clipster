#include "team.h"

Team::Team(const QString &teamname,
    int pending_owner_accepts,
    bool searchable,
    bool is_private,
    bool member_accepted,
    bool pending_accept,
    bool owner_accepted,
    const QString &owner,
    const QImage &icon) {
    m_teamname = teamname;
    m_pending_owner_accepts = pending_owner_accepts;
    m_searchable = searchable;
    m_is_private = is_private;
    m_member_accepted = member_accepted;
    m_pending_accept = pending_accept;
    m_owner_accepted = owner_accepted;
    m_owner = owner;
    m_icon = icon;
}

const QString &Team::teamname() const {
    return m_teamname;
}

int Team::pendingOwnerAccepts() const {
    return m_pending_owner_accepts;
}

bool Team::searchable() const {
    return m_searchable;
}

bool Team::memberAccepted() const {
    return m_member_accepted;
}

bool Team::isPrivate() const {
    return m_is_private;
}

bool Team::pendingAccept() const {
    return m_pending_accept;
}

bool Team::ownerAccepted() const {
    return m_owner_accepted;
}

const QString &Team::owner() const {
    return m_owner;
}

QImage Team::icon() const {
    return m_icon;
}
