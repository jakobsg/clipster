#ifndef TEAM_H
#define TEAM_H

#include <QString>
#include <QImage>
#include <QList>

class Team {
public:
    Team( const QString &teamname,
        int pending_owner_accepts,
        bool searchable,
        bool is_private,
        bool member_accepted,
        bool pending_accept,
        bool owner_accepted,
        const QString &owner,
        const QImage &icon);

    const QString &teamname() const;
    int pendingOwnerAccepts() const;
    bool searchable() const;
    bool memberAccepted() const;
    bool isPrivate() const;
    bool pendingAccept() const;
    bool ownerAccepted() const;
    const QString &owner() const;
    QImage icon() const;

private:
    QString m_teamname;
    int m_pending_owner_accepts;
    bool m_searchable;
    bool m_is_private;
    bool m_member_accepted;
    bool m_pending_accept;
    bool m_owner_accepted;
    QString m_owner;
    QImage m_icon;
};

typedef QList<Team> TeamList;

#endif // TEAM_H
