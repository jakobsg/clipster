#include "teamlistresponse.h"
#include <crashlib.h>

TeamListResponse::TeamListResponse()
: CommonResponse() {
    CALL_LOGGER();
}

TeamListResponse::TeamListResponse(const CommonResponse &resp)
: CommonResponse(resp) {
    CALL_LOGGER();
}

TeamListResponse::~TeamListResponse() {
    CALL_LOGGER();
}

