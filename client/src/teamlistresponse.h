#ifndef TEAMLISTRESPONSE_H
#define TEAMLISTRESPONSE_H

#include "clipboarddata.h"
#include "commonresponse.h"
#include <QJsonArray>

class TeamListResponse : public CommonResponse
{
public:
    TeamListResponse();
    TeamListResponse(const CommonResponse &resp);

    ~TeamListResponse();
    QJsonArray m_teams;
};

#endif // TEAMENTRYLISTRESPONSE_H
