#include "teammanager.h"
#include "webserviceclient.h"
#include <crashlib.h>
#include <QApplication>
#include <QImage>

TeamManager *TeamManager::s_instance = 0;

TeamManager::TeamManager(QObject *parent) : QObject(parent) {
    connect(WebServiceClient::instance(),SIGNAL(teamListResponseReady(const TeamListResponse &)),this,SLOT(setTeamList(const TeamListResponse &)));
}

TeamManager::~TeamManager() {
    clearTeamList();
}

void TeamManager::clearTeamList() {
    m_teams.clear();
}

TeamManager *TeamManager::instance() {
    CALL_LOGGER();
    if (s_instance==0) {
        s_instance = new TeamManager(QApplication::instance());
    }
    return s_instance;
}

TeamList TeamManager::teamList(bool show_unaccepted) {
    if (show_unaccepted==false) {
        TeamList tl;
        foreach (Team t, m_teams) {
            if (!t.pendingAccept())
            tl.append(t);
        }
        return tl;
    }
    return m_teams;
}

void TeamManager::updateTeamList() {
    WebServiceClient::instance()->listTeams();
}

void TeamManager::setTeamList(const TeamListResponse &teamlist) {
    clearTeamList();
    foreach (QJsonValue t, teamlist.m_teams) {
        QJsonObject teamobj = t.toObject();
        Team team(
            teamobj["teamname"].toString(),
            teamobj["pending_owner_accepts"].toInt(),
            teamobj["searchable"].toBool(),
            teamobj["private"].toBool(),
            teamobj["member_accepted"].toBool(),
            teamobj["pending_accept"].toBool(),
            teamobj["member_accepted"].toBool(),
            teamobj["owner"].toString(),
            QImage() );
        m_teams.append(team);
    }
    emit teamsUpdated();
}
