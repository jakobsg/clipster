#ifndef TEAMMANAGER_H
#define TEAMMANAGER_H

#include "team.h"
#include "teamlistresponse.h"

#include <QObject>

class TeamManager : public QObject
{
    Q_OBJECT
public:
    ~TeamManager();
    static TeamManager *instance();
    TeamList teamList(bool show_unaccepted=false);
    void updateTeamList();

signals:
    void teamsUpdated();

public slots:
    void setTeamList(const TeamListResponse &teamlist);

private:
    void clearTeamList();
    explicit TeamManager(QObject *parent = 0);
    static TeamManager *s_instance;
    TeamList m_teams;
};

#endif // TEAMMANAGER_H
