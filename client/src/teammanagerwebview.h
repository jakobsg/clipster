#ifndef TEAMMANAGEMENTWEBVIEW_H
#define TEAMMANAGEMENTWEBVIEW_H

#include <QWebView>

class QNetworkReply;
class QTimer;

class TeamManagementWebView : public QWebView {

    Q_OBJECT

public:
    explicit TeamManagementWebView(QWidget *parent=0);
    ~TeamManagementWebView();
    void startManagement();

private slots:
//    void poll();
    void checkNetwork(QNetworkReply *reply);
    void timeoutCheck();

private:
//    QTimer *m_poll_timer;
    QTimer *m_timeout_timer;
    bool m_is_online;
};

#endif
