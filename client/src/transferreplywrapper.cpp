#include "transferreplywrapper.h"
#include <crashlib.h>

#define TIMEOUT_MSECS 10000

TransferReplyWrapper::TransferReplyWrapper(QNetworkReply *rep, const QString &data_hash) : QObject(rep) {
    CALL_LOGGER();
    m_data_hash = data_hash;
    m_reply = rep;
    connect(rep,SIGNAL(downloadProgress(qint64,qint64)),this,SLOT(passOnTransferProgress(qint64,qint64)));
    connect(rep,SIGNAL(uploadProgress(qint64,qint64)),this,SLOT(passOnTransferProgress(qint64,qint64)));
    connect(rep,SIGNAL(finished()),this,SLOT(passOnFinished()));
    m_timeout_timer = new QTimer(this);
    m_timeout_timer->setSingleShot(true);
    connect(m_timeout_timer,SIGNAL(timeout()),this,SLOT(requestTimeout()));
    m_timeout_timer->start(TIMEOUT_MSECS);
}

TransferReplyWrapper::~TransferReplyWrapper() {
    CALL_LOGGER();
}


void TransferReplyWrapper::passOnTransferProgress(qint64 progress, qint64 total) {
    CALL_LOGGER();
    m_timeout_timer->stop();
    m_timeout_timer->start(TIMEOUT_MSECS);
    emit transferProgress(m_data_hash, progress, total);
}

void TransferReplyWrapper::passOnFinished() {
    CALL_LOGGER();
    m_timeout_timer->stop();
    emit finished(m_data_hash);
}

void TransferReplyWrapper::requestTimeout() {
    CALL_LOGGER();
    qDebug("Timeout occured!");
    m_reply->abort();
}
