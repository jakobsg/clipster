#ifndef TRANSFERREPLYHANDLER_H
#define TRANSFERREPLYHANDLER_H

#include <QNetworkReply>
#include <QTimer>

class TransferReplyWrapper : public QObject
{
    Q_OBJECT
public:
    explicit TransferReplyWrapper(QNetworkReply *rep, const QString &data_hash);
    ~TransferReplyWrapper();

    QString m_data_hash;
    QNetworkReply *m_reply;

private slots:
    void passOnTransferProgress(qint64 progress, qint64 total);
    void passOnFinished();
    void requestTimeout();

Q_SIGNALS:
    void transferProgress(const QString &data_hash, qint64 progress, qint64 total);
    void finished(const QString &data_hash);
private:
    QTimer *m_timeout_timer;
};

#endif // TRANSFERREPLYHANDLER_H
