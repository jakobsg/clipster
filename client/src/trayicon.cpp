#include "trayicon.h"
#include "qxtglobalshortcut.h"
#include "clipboard.h"
#include "clipboardapplicationmanager.h"
#include "clipsterconfig.h"
#include <QApplication>
#include <QSystemTrayIcon>
#include <QIcon>
#include <QDialog>
#include <QLineEdit>
#include <QTimer>
#include <QMenu>
#include <crashlib.h>
#include <logview.h>

extern int sendCtrlC();

TrayIcon::TrayIcon(QObject *parent) : QObject(parent) {
    CALL_LOGGER();
    m_systray = new QSystemTrayIcon(this);
    m_icon_normal.addPixmap(QPixmap(":/images/clipboard-icon_x11_16"));
    m_icon_normal.addPixmap(QPixmap(":/images/clipboard-icon_x11_24"));
    m_icon_normal.addPixmap(QPixmap(":/images/clipboard-icon_x11_32"));
    m_icon_clipdata_added.addPixmap(QPixmap(":/images/clipboard-icon-data_x11_16"));
    m_icon_clipdata_added.addPixmap(QPixmap(":/images/clipboard-icon-data_x11_24"));
    m_icon_clipdata_added.addPixmap(QPixmap(":/images/clipboard-icon-data_x11_32"));
    m_systray->setIcon(m_icon_normal);
    m_systray->setVisible(true);
    QxtGlobalShortcut* shortcut = 0;
    //shortcut = new QxtGlobalShortcut(this);
    //connect(shortcut, SIGNAL(activated()), this, SLOT(startDistributedCopy()));
    //shortcut->setShortcut(QKeySequence("Ctrl+Shift+c"));
    shortcut = new QxtGlobalShortcut(this);
    connect(shortcut, SIGNAL(activated()), this, SLOT(toggleLogView()));
    shortcut->setShortcut(QKeySequence("Ctrl+Shift+L"));
    shortcut = new QxtGlobalShortcut(this);
    connect(shortcut, SIGNAL(activated()), ClipboardApplicationManager::instance(), SLOT(toggleVisible()));
    shortcut->setShortcut(QKeySequence("Ctrl+Shift+M"));
    ClipboardApplicationManager::instance()->toggleVisible();
    m_icon_timer = new QTimer(this);
    m_icon_timer->setSingleShot(true);
    connect(m_icon_timer,SIGNAL(timeout()),this,SLOT(normalizeIcon()));
    ClipboardList cl = Clipboard::getClipboardInstances();
    if (cl.length()) {
        Clipboard *cb = cl.at(0);
        connect(cb,SIGNAL(clipboardEntryReady()),this,SLOT(signalLocalCopy()));
    }
    initActions();
}

void TrayIcon::startDistributedCopy() {
    CALL_LOGGER();
    QList<Clipboard *> &instances = Clipboard::getClipboardInstances();
    for (int i = 0; i < instances.size(); ++i) {
        instances.at(i)->setNextDataOrigin(ClipboardData::LocalDistributedCopy);
    }
    sendCtrlC();
}

void TrayIcon::toggleLogView() {
    LogView::toggleVisible();
}



void TrayIcon::normalizeIcon() {
    CALL_LOGGER();
    m_systray->setIcon(m_icon_normal);
}

void TrayIcon::signalLocalCopy() {
    CALL_LOGGER();
    m_systray->setIcon(m_icon_clipdata_added);
    m_icon_timer->start(1000);
}

TrayIcon::~TrayIcon() {
}

void TrayIcon::login() {
    CALL_LOGGER();
    ClipboardApplicationManager::instance()->login();
}

void TrayIcon::initActions() {
    CALL_LOGGER();
    QMenu *menu = new QMenu();
    QAction *login_action = new QAction(tr("Login..."),menu);
    QAction *show_manager_action = new QAction(tr("Show Clipboard Manager..."),menu);
    connect(login_action,SIGNAL(triggered()),this,SLOT(login()));
    connect(show_manager_action,SIGNAL(triggered()),ClipboardApplicationManager::instance(),SLOT(toggleVisible()));
    menu->addAction(login_action);
    menu->addAction(show_manager_action);
    menu->addSeparator();
    QAction *clear_cache_action = new QAction(tr("Clear cache"),menu);
    connect(clear_cache_action,SIGNAL(triggered()),ClipboardApplicationManager::instance(),SLOT(clearLocalClipboardEntries()));
    menu->addAction(clear_cache_action);
    menu->addSeparator();
    QAction *std_crash_action = new QAction(tr("Simulate std::exception crash"),menu);
    connect(std_crash_action,SIGNAL(triggered()),ClipboardApplicationManager::instance(),SLOT(simulateSECrash()));
    menu->addAction(std_crash_action);

    QAction *segv_crash_action = new QAction(tr("Simulate SEGV crash"),menu);
    connect(segv_crash_action,SIGNAL(triggered()),ClipboardApplicationManager::instance(),SLOT(simulateSEGVCrash()));
    menu->addAction(segv_crash_action);

    menu->addSeparator();
    QAction *quit_action = new QAction(tr("Quit"),menu);
    connect(quit_action,SIGNAL(triggered()),QApplication::instance(),SLOT(quit()));
    menu->addAction(quit_action);
    m_systray->setContextMenu(menu);
}

QSystemTrayIcon *TrayIcon::trayIcon() {
    CALL_LOGGER();
    return m_systray;
}
