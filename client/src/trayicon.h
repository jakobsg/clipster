#ifndef TRAYICON_H
#define TRAYICON_H

#include <QObject>
#include <QIcon>

class ClipboardApplicationManager;
class QTimer;
class QSystemTrayIcon;

class TrayIcon : public QObject
{
    Q_OBJECT
public:
    explicit TrayIcon(QObject *parent = 0);
    ~TrayIcon();
    void initActions();
    QSystemTrayIcon *trayIcon();

public Q_SLOTS:
    void startDistributedCopy();
    void toggleLogView();
    void signalLocalCopy();
    void login();

private slots:
    void normalizeIcon();

private:
    QIcon m_icon_normal;
    QIcon m_icon_clipdata_added;
    QTimer *m_icon_timer;
    QSystemTrayIcon *m_systray;

Q_SIGNALS:
};

#endif // TRAYICON_H
