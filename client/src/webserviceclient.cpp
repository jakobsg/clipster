#include "webserviceclient.h"
#include "commonresponse.h"
#include "deviceinfo.h"
#include "clipsterconfig.h"
#include "networkreplyparser.h"
#include "mimedatatools.h"
#include "transferreplywrapper.h"
#include "multiparthandler.h"
#include "successresponse.h"
#include "clipsterhost.h"

#include <crashlib.h>
#include <QStandardPaths>
#include <QDir>
#include <QHttpMultiPart>
#include <QHttpPart>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QFile>
#include <QImage>
#include <QBuffer>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonParseError>
#include <QHostInfo>
#include <QSysInfo>
#include <QApplication>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <crashlib.h>

#ifdef WINDOWS_OS
#include <windows.h>
#define snprintf _sprintf_p
#endif

WebServiceClient *WebServiceClient::s_instance = 0;

WebServiceClient::WebServiceClient(QObject *parent) : QObject(parent) {
    CALL_LOGGER();
    m_device_hash = ClipsterConfig::configValue("device_hash").toString();
    qDebug() << "Device Hash: " << m_device_hash;
    if (!m_device_hash.isEmpty()) {
        store_device_hash(m_device_hash);
    }
}

WebServiceClient *WebServiceClient::instance() {
    CALL_LOGGER();
    if (s_instance==0) {
        s_instance = new WebServiceClient(QApplication::instance());
    }
    return s_instance;
}

WebServiceClient::~WebServiceClient() {
    CALL_LOGGER();

}

void WebServiceClient::uploadReply(QNetworkReply *reply) {
    CALL_LOGGER();
    CommonResponse resp;
    commonReplyHandler(reply,&resp);
    if (resp.m_res_code==0) {
        QJsonObject result = resp.m_json_response["result"].toObject();
        bool success = result["success"].toBool();
        if (success) {
            QString data_hash = result["data_hash"].toString();
            emit dataUploadDone(data_hash);
        }

    }
    reply->manager()->deleteLater();
}

void WebServiceClient::downloadReply(QNetworkReply *reply) {
    CALL_LOGGER();
    NetworkReplyParser rep(reply);
    bool image_found = false;
    bool image_processing_done = false;
    if (rep.parts()>1) {
        QMimeData md;
        bool data_present = false;
        // First part is the json-response body
        QJsonParseError parse_error;
        QJsonDocument resp_doc = QJsonDocument::fromJson(rep.body(0),&parse_error);
        if (parse_error.error != QJsonParseError::NoError) {
            qDebug("Response JSON parse error: %s", parse_error.errorString().toUtf8().data());
            return;
        }
        QJsonObject resp = resp_doc.object();
        QString data_hash = resp["reflection"].toString();
        emit dataTransferProgressChanged(data_hash,0.0);
        for (int i=0; i<rep.parts(); i++) {
            QByteArray mimetype = rep.rawHeader(i,QString("Content-Type").toLatin1());
            if (!mimetype.isNull()) {
                data_present = true;
                if (mimetype.startsWith("image/")) {
                    if (!image_processing_done) {
                        QImage img = QImage::fromData(rep.body(i),mimetype.mid(6));
                        if (!img.isNull()) {
                            if (img.hasAlphaChannel()) {
                                md.setImageData(img);
                                image_processing_done = true;
                            }
                            else if (!image_found) {
                                md.setImageData(img);
                                image_found = true;
                            }
                        }
                    }
                }
                md.setData(QString::fromLatin1(mimetype),rep.body(i));
            }
        }
        if (data_present) {
            MimeDataTools::instance()->serializeMimeData(data_hash, md);
            emit dataDownloadDone(data_hash);
        }
    }
    return;

//    HttpHeaders headers;
//    foreach (QNetworkReply::RawHeaderPair hpair, reply->rawHeaderPairs()) {
//        static char rawheader_line[1024];
//        snprintf(rawheader_line, 512, "%s: %s", hpair.first.data(), hpair.second.data());
//        headers.add_header_line(rawheader_line);
//    }

//    MultiPartReader mpr(headers.boundary(), headers.content_length());
//    qDebug("Boundary: %s", headers.boundary().c_str());
//	qint64 size = reply->size();
//	mpr.write_data(reply->readAll().data(), size, 1);
//	if (mpr.attachment_count()) {
//        QMimeData md;
//        bool data_present = false;
//        // First part is the json-response body
//        QJsonParseError parse_error;
//        QJsonDocument resp_doc = QJsonDocument::fromJson(QByteArray::fromStdString(mpr.jsonwsp()),&parse_error);
//        if (parse_error.error != QJsonParseError::NoError) {
//            qDebug("Response JSON parse error: %s", parse_error.errorString().toUtf8().data());
//            return;
//        }
//        QJsonObject resp = resp_doc.object();
//        QString data_hash = resp["reflection"].toString();
//        emit dataTransferProgressChanged(data_hash,0.0);
//        bool image_found = false;
//        bool image_processing_done = false;
//        for (int i=0; i<mpr.attachment_count(); i++) {
//            const AttachmentInfo *ainfo = mpr.attachment_by_sequence(i);
//            qDebug() <<
//                "Type:" << ainfo->headers().content_type().c_str() <<
//                "Size:" << ainfo->size() <<
//                "Path:" << ainfo->path().c_str();

//            QString mimetype = QString::fromStdString(ainfo->headers().content_type());
//            if (!mimetype.isNull()) {
//                data_present = true;
//                if (mimetype.startsWith("image/")) {
//                    if (!image_processing_done) {
//                        QImage img(ainfo->path().c_str());
//                        if (!img.isNull()) {
//                            if (img.hasAlphaChannel()) {
//                                md.setImageData(img);
//                                image_processing_done = true;
//                            }
//                            else if (!image_found) {
//                                md.setImageData(img);
//                                image_found = true;
//                            }
//                        }
//                    }
//                }
//                QFile file(ainfo->path().c_str());
//                if (!file.open(QIODevice::ReadOnly)) {
//                    continue;
//                }
//                md.setData(mimetype,file.readAll());
//                file.close();
//                continue;
//            }
//        }
//        for (int i=0; i<mpr.attachment_count(); i++) {
//            const AttachmentInfo *ainfo = mpr.attachment_by_sequence(i);
//            QFile::remove(QString::fromStdString(ainfo->path()));
//        }

//        if (data_present) {
//            MimeDataTools::instance()->serializeMimeData(data_hash, md);
//            emit dataDownloadDone(data_hash);
//        }
//    }
//    reply->manager()->deleteLater();
}

void WebServiceClient::transferProgress(const QString &data_hash, qint64 received, qint64 total) {
    CALL_LOGGER();
    emit dataTransferProgressChanged(data_hash,float(received)/total);
}

void WebServiceClient::listDataEntriesReply(QNetworkReply *reply) {
    CALL_LOGGER();
    DataEntryListResponse resp;
    commonReplyHandler(reply,&resp);
    if (resp.m_res_code==0) {
        QJsonObject result = resp.m_json_response["result"].toObject();
        resp.m_data_entries = result["data_entries"].toArray();
        if (resp.m_data_entries.count()) {
            resp.m_latest_timestamp = result["latest_timestamp"].toDouble();
            ClipsterConfig::setConfigValue("data_timestamp", resp.m_latest_timestamp);
        }
        emit dataEntryListResponseReady(resp);
    }
    reply->manager()->deleteLater();
}

void WebServiceClient::listTeamDataEntriesReply(QNetworkReply *reply) {
    CALL_LOGGER();
    DataEntryListResponse resp;
    commonReplyHandler(reply,&resp);
    if (resp.m_res_code==0) {
        QJsonObject result = resp.m_json_response["result"].toObject();
        resp.m_data_entries = result["data_entries"].toArray();
        if (resp.m_data_entries.count()) {
            resp.m_latest_timestamp = result["latest_timestamp"].toDouble();
            ClipsterConfig::setConfigValue("team_data_timestamp", resp.m_latest_timestamp);
        }
        emit dataEntryListResponseReady(resp);
    }
    reply->manager()->deleteLater();
}

void WebServiceClient::loginReply(QNetworkReply *reply) {
    CALL_LOGGER();
    CommonResponse resp;
    commonReplyHandler(reply,&resp);
    if (resp.m_res_code==0) {
        QJsonObject result = resp.m_json_response["result"].toObject();
        m_login_token = result["login_token"].toString();
        qDebug("LOGIN TOKEN: %s", m_login_token.toUtf8().data());
        registerDevice();
    }
    reply->manager()->deleteLater();
}

void WebServiceClient::registerDeviceReply(QNetworkReply *reply) {
    CALL_LOGGER();
    SuccessResponse sresp;
    commonReplyHandler(reply,&sresp);
    if (sresp.m_res_code==0) {
        QJsonObject result = sresp.m_json_response["result"].toObject();
        m_device_hash = result["device_hash"].toString();
        if (!m_device_hash.isEmpty()) {
            store_device_hash(m_device_hash);
        }
        qDebug("DEVICE HASH: %s", m_device_hash.toUtf8().data());
        ClipsterConfig::setConfigValue("device_hash",m_device_hash);
        sresp.m_success = true;
    }
    emit registerDeviceResponseReady(sresp);
    reply->manager()->deleteLater();
}

void WebServiceClient::whoamiReply(QNetworkReply *reply) {
    CALL_LOGGER();
    WhoamiResponse resp;
    commonReplyHandler(reply,&resp);
    if (resp.m_res_code==0) {
        QJsonObject result = resp.m_json_response["result"].toObject();
        QJsonObject user_info = result["user_info"].toObject();
        if (!user_info.isEmpty()) {
            resp.m_username = user_info["username"].toString();
            store_username(resp.m_username);
        }
    }
    else if (resp.m_res_code==10301) {
        // Invalid device hash
        ClipsterConfig::removeConfigValue("device_hash");
    }

    emit whoamiResponseReady(resp);
    reply->manager()->deleteLater();
}

void WebServiceClient::commonReplyHandler(QNetworkReply *reply, CommonResponse *resp) {
    CALL_LOGGER();
    QJsonParseError parse_error;
    if (reply->error()>0) {
        LOG_ERROR(QString("Network Error: %1").arg(reply->errorString()));
        resp->m_networkerror = reply->error();
        return;
    }
    int http_status_code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    resp->m_http_reason = reply->attribute( QNetworkRequest::HttpReasonPhraseAttribute ).toString();
    resp->m_http_status_code = http_status_code;
    if (http_status_code==501) {
        LOG_ERROR(QString("Server Error: 501"));
        resp->m_serverfault = true;
        return;
    }
    QByteArray json_response = reply->readAll();
    QJsonDocument resp_doc = QJsonDocument::fromJson(json_response,&parse_error);
    if (parse_error.error != QJsonParseError::NoError) {
        LOG_ERROR(QString("Response JSON parse error: %1").arg(parse_error.errorString()));
        resp->m_serverfault = true;
        return;
    }
    resp->m_json_response = resp_doc.object();
    if (resp->m_json_response["fault"].isUndefined()) {
        LOG_ERROR(QString("Server Fault: %1").arg(json_response.data()));
        // if we reach to here with no exception, then we have a service fault
        resp->m_servicefault = true;
        return;
    }
    LOG_TRACE(QString("Response: %1").arg(json_response.data()));
    QJsonObject result_obj = resp->m_json_response["result"].toObject();
    if (result_obj["method_result"].isUndefined()) {
        resp->m_servicefault = true;
        return;
    }
    QJsonObject method_result = result_obj["method_result"].toObject();
    int res_code = method_result["res_code"].toInt();
    QString res_msg = method_result["res_msg"].toString();
    resp->m_res_code = res_code;
    resp->m_res_msg = res_msg;
}

bool WebServiceClient::upload(const QString &data_hash) {
    CALL_LOGGER();
    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::RelatedType);
    qDebug() << "Local Data Hash: " << data_hash;
    multiPart->setBoundary(QByteArray("CLIPSTER_BOUNDARY_987654321"));

    QHttpPart textPart;
    textPart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json, charset=UTF-8"));
    QJsonDocument jdoc;
    QJsonObject jobj;
    jobj["type"] = "jsonwsp/request";
    jobj["version"] = "1.0";
    jobj["methodname"] = "upload";
    QJsonObject jargs;
    jargs["device_hash"] = m_device_hash;

    jobj["args"] = jargs;
    jdoc.setObject(jobj);
    QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);
    LOG_TRACE(QString("Request: %1").arg(utf8json.data()));
    textPart.setBody(utf8json);

    multiPart->append(textPart);

    // User data location
    QString app_data_location = QStandardPaths::writableLocation(QStandardPaths::DataLocation);
    // Create if not exists
    QDir adl(app_data_location);
    adl.mkdir(app_data_location);
    // Clipster data dir
    QString data_dir = adl.filePath("data");
    // Create if not exists
    adl.mkdir(data_dir);

    QFile file(QDir(data_dir).filePath(data_hash));
    if (!file.open(QIODevice::ReadOnly)) {
        return false;
    }
    QDataStream in(&file);
    while (!in.atEnd()) {
        QString format;
        QByteArray data;
        QHttpPart part;
        in >> format;
        if (format=="image") {
            QImage image;
            in >> image;
            part.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/png"));
            QBuffer buf;
            image.save(&buf,"png");
            part.setBody(buf.data());
            multiPart->append(part);
            break;
        }
        in >> data;
        part.setHeader(QNetworkRequest::ContentTypeHeader, format);
        part.setBody(data);
        multiPart->append(part);
    }

    QUrl url(QString("%1://%2/service/v1/TransferService/jsonwsp").arg(CLIPSTER_HTTP_SCHEMA).arg(CLIPSTER_HOST));
    QNetworkRequest request(url);

    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    connect(manager,SIGNAL(finished(QNetworkReply*)),this,SLOT(uploadReply(QNetworkReply*)));
    QNetworkReply *reply = manager->post(request, multiPart);
    new TransferReplyWrapper(reply, data_hash); // Handle timeout and transfer progress
    multiPart->setParent(reply); // delete the multiPart with the reply
    return true;
    // here connect signals etc.
}


bool WebServiceClient::download(const QString &data_hash) {
    CALL_LOGGER();
    QJsonDocument jdoc;
    QJsonObject jobj;
    jobj["type"] = "jsonwsp/request";
    jobj["version"] = "1.0";
    jobj["methodname"] = "download";
    jobj["mirror"] = data_hash;
    QJsonObject jargs;
    jargs["device_hash"] = m_device_hash;
    jargs["data_hash"] = data_hash;
    jobj["args"] = jargs;
    jdoc.setObject(jobj);
    QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);
    LOG_TRACE(QString("Request: %1").arg(utf8json.data()));
//    qDebug("Posting json: %s", utf8json.data());

    QUrl url(QString("%1://%2/service/v1/TransferService/jsonwsp").arg(CLIPSTER_HTTP_SCHEMA).arg(CLIPSTER_HOST));
    QNetworkRequest req(url);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(downloadReply(QNetworkReply*)));
    QNetworkReply *reply = manager->post(req,utf8json);
    TransferReplyWrapper *trw = new TransferReplyWrapper(reply, data_hash);
    connect(trw,SIGNAL(transferProgress(const QString &,qint64,qint64)),this,SLOT(transferProgress(const QString &,qint64,qint64)));
    return true;
}


bool WebServiceClient::login(const QString &username, const QString &password) {
    CALL_LOGGER();
    QJsonDocument jdoc;
    QJsonObject jobj;
    jobj["type"] = "jsonwsp/request";
    jobj["version"] = "1.0";
    jobj["methodname"] = "login";
    QJsonObject jargs;
    jargs["username"] = username;
    jargs["password"] = password;
    jobj["args"] = jargs;
    jdoc.setObject(jobj);
    QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);
    LOG_TRACE(QString("Request: %1").arg(utf8json.data()));

//    qDebug("Posting json: %s", utf8json.data());
    QUrl url(QString("%1://%2/service/v1/UserService/jsonwsp").arg(CLIPSTER_HTTP_SCHEMA).arg(CLIPSTER_HOST));
    QNetworkRequest req(url);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(loginReply(QNetworkReply*)));
    QNetworkReply *reply = manager->post(req,utf8json);
    new TransferReplyWrapper(reply, ""); // Handle timeout and transfer progress
    return true;
}

bool WebServiceClient::registerDevice(const QString &login_token) {
    CALL_LOGGER();
    if (!login_token.isEmpty()) {
        m_login_token = login_token;
    }
    QJsonDocument jdoc;
    QJsonObject jobj;
    jobj["type"] = "jsonwsp/request";
    jobj["version"] = "1.0";
    jobj["methodname"] = "registerDevice";
    QJsonObject jargs;
    jargs["login_token"] = m_login_token;
    QString hwsig = hardwareSignature();
    if (hwsig.isEmpty()) {
        qDebug("Unable to construct a hardware signature");
        return false;
    }
    jargs["hardware_signature"] = hwsig;
    jargs["hostname"] = QHostInfo::localHostName();
    jargs["os_type"] = QSysInfo::productType();
    jargs["os_version"] = QSysInfo::productVersion();

    jobj["args"] = jargs;
    jdoc.setObject(jobj);
    QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);
    LOG_TRACE(QString("Request: %1").arg(utf8json.data()));

//    qDebug("Posting json: %s", utf8json.data());
    QUrl url(QString("%1://%2/service/v1/DeviceService/jsonwsp").arg(CLIPSTER_HTTP_SCHEMA).arg(CLIPSTER_HOST));
    QNetworkRequest req(url);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(registerDeviceReply(QNetworkReply*)));
    QNetworkReply *reply = manager->post(req,utf8json);
    new TransferReplyWrapper(reply, ""); // Handle timeout and transfer progress
    return true;
}

bool WebServiceClient::whoami() {
    CALL_LOGGER();
    QJsonDocument jdoc;
    QJsonObject jobj;
    jobj["type"] = "jsonwsp/request";
    jobj["version"] = "1.0";
    jobj["methodname"] = "whoami";
    QJsonObject jargs;
    jargs["device_hash"] = m_device_hash;

    jobj["args"] = jargs;
    jdoc.setObject(jobj);
    QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);
    LOG_TRACE(QString("Request: %1").arg(utf8json.data()));

//    qDebug("Posting json: %s", utf8json.data());
    QUrl url(QString("%1://%2/service/v1/UserService/jsonwsp").arg(CLIPSTER_HTTP_SCHEMA).arg(CLIPSTER_HOST));
    QNetworkRequest req(url);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(whoamiReply(QNetworkReply*)));
    QNetworkReply *reply = manager->post(req,utf8json);
    new TransferReplyWrapper(reply, ""); // Handle timeout and transfer progress
    return true;
}

bool WebServiceClient::listDataEntries() {
    CALL_LOGGER();
    QJsonDocument jdoc;
    QJsonObject jobj;
    jobj["type"] = "jsonwsp/request";
    jobj["version"] = "1.0";
    jobj["methodname"] = "listDataEntries";
    QJsonObject jargs;
    jargs["device_hash"] = m_device_hash;
    //qDebug() << "Since Timestamp: " << ClipsterConfig::configValue("data_timestamp",0).toDouble();
    jargs["since_timestamp"] = ClipsterConfig::configValue("data_timestamp",0).toDouble();
    jobj["args"] = jargs;
    jdoc.setObject(jobj);
    QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);
    LOG_TRACE(QString("Request: %1").arg(utf8json.data()));

//    qDebug("Posting json: %s", utf8json.data());
    QUrl url(QString("%1://%2/service/v1/TransferService/jsonwsp").arg(CLIPSTER_HTTP_SCHEMA).arg(CLIPSTER_HOST));
    qDebug(url.toString().toUtf8().data());
    QNetworkRequest req(url);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(listDataEntriesReply(QNetworkReply*)));
    QNetworkReply *reply = manager->post(req,utf8json);
    new TransferReplyWrapper(reply, ""); // Handle timeout and transfer progress
    return true;
}

bool WebServiceClient::listTeamDataEntries() {
    CALL_LOGGER();
    QJsonDocument jdoc;
    QJsonObject jobj;
    jobj["type"] = "jsonwsp/request";
    jobj["version"] = "1.0";
    jobj["methodname"] = "listTeamDataEntries";
    QJsonObject jargs;
    jargs["device_hash"] = m_device_hash;
    //qDebug() << "Since Timestamp: " << ClipsterConfig::configValue("data_timestamp",0).toDouble();
    jargs["since_timestamp"] = ClipsterConfig::configValue("team_data_timestamp",0).toDouble();
    jobj["args"] = jargs;
    jdoc.setObject(jobj);
    QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);
    LOG_TRACE(QString("Request: %1").arg(utf8json.data()));

//    qDebug("Posting json: %s", utf8json.data());
    QUrl url(QString("%1://%2/service/v1/TransferService/jsonwsp").arg(CLIPSTER_HTTP_SCHEMA).arg(CLIPSTER_HOST));
    QNetworkRequest req(url);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(listTeamDataEntriesReply(QNetworkReply*)));
    QNetworkReply *reply = manager->post(req,utf8json);
    new TransferReplyWrapper(reply, ""); // Handle timeout and transfer progress
    return true;
}

bool WebServiceClient::listTeams() {
    CALL_LOGGER();
    QJsonDocument jdoc;
    QJsonObject jobj;
    jobj["type"] = "jsonwsp/request";
    jobj["version"] = "1.0";
    jobj["methodname"] = "listOwnMemberships";
    QJsonObject jargs;
    jargs["device_hash"] = m_device_hash;
    jobj["args"] = jargs;
    jdoc.setObject(jobj);
    QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);
    LOG_TRACE(QString("Request: %1").arg(utf8json.data()));

//    qDebug("Posting json: %s", utf8json.data());
    QUrl url(QString("%1://%2/service/v1/TeamService/jsonwsp").arg(CLIPSTER_HTTP_SCHEMA).arg(CLIPSTER_HOST));
    QNetworkRequest req(url);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(listTeamsReply(QNetworkReply*)));
    QNetworkReply *reply = manager->post(req,utf8json);
    new TransferReplyWrapper(reply, ""); // Handle timeout and transfer progress
    return true;
}

bool WebServiceClient::pendingTeamInvitations() {
    CALL_LOGGER();
    QJsonDocument jdoc;
    QJsonObject jobj;
    jobj["type"] = "jsonwsp/request";
    jobj["version"] = "1.0";
    jobj["methodname"] = "pendingInvitations";
    QJsonObject jargs;
    jargs["device_hash"] = m_device_hash;
    jobj["args"] = jargs;
    jdoc.setObject(jobj);
    QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);
    LOG_TRACE(QString("Request: %1").arg(utf8json.data()));

//    qDebug("Posting json: %s", utf8json.data());
    QUrl url(QString("%1://%2/service/v1/TeamService/jsonwsp").arg(CLIPSTER_HTTP_SCHEMA).arg(CLIPSTER_HOST));
    QNetworkRequest req(url);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(pendingTeamInvitationsReply(QNetworkReply*)));
    QNetworkReply *reply = manager->post(req,utf8json);
    new TransferReplyWrapper(reply, ""); // Handle timeout and transfer progress
    return true;
}

bool WebServiceClient::pendingTeamMemberAccepts() {
    CALL_LOGGER();
    QJsonDocument jdoc;
    QJsonObject jobj;
    jobj["type"] = "jsonwsp/request";
    jobj["version"] = "1.0";
    jobj["methodname"] = "pendingMemberAccepts";
    QJsonObject jargs;
    jargs["device_hash"] = m_device_hash;
    jobj["args"] = jargs;
    jdoc.setObject(jobj);
    QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);
    LOG_TRACE(QString("Request: %1").arg(utf8json.data()));

//    qDebug("Posting json: %s", utf8json.data());
    QUrl url(QString("%1://%2/service/v1/TeamService/jsonwsp").arg(CLIPSTER_HTTP_SCHEMA).arg(CLIPSTER_HOST));
    QNetworkRequest req(url);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(pendingTeamMemberAcceptsReply(QNetworkReply*)));
    QNetworkReply *reply = manager->post(req,utf8json);
    new TransferReplyWrapper(reply, ""); // Handle timeout and transfer progress
    return true;
}

bool WebServiceClient::teamShareDataEntry(const QString &team_owner, const QString &teamname, const QString &data_hash) {
    CALL_LOGGER();
    QJsonDocument jdoc;
    QJsonObject jobj;
    jobj["type"] = "jsonwsp/request";
    jobj["version"] = "1.0";
    jobj["methodname"] = "shareDataEntry";
    QJsonObject jargs;
    jargs["device_hash"] = m_device_hash;
    jargs["team_owner"] = team_owner;
    jargs["teamname"] = teamname;
    jargs["data_hash"] = data_hash;
    jobj["args"] = jargs;
    jdoc.setObject(jobj);
    QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);
    LOG_TRACE(QString("Request: %1").arg(utf8json.data()));

//    qDebug("Posting json: %s", utf8json.data());
    QUrl url(QString("%1://%2/service/v1/TeamService/jsonwsp").arg(CLIPSTER_HTTP_SCHEMA).arg(CLIPSTER_HOST));
    QNetworkRequest req(url);
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
    req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(teamShareDataEntryReply(QNetworkReply*)));
    QNetworkReply *reply = manager->post(req,utf8json);
    new TransferReplyWrapper(reply, ""); // Handle timeout and transfer progress
    return true;
}

bool WebServiceClient::hasDeviceHash() {
    CALL_LOGGER();
    return m_device_hash.isEmpty()==false;
}

void WebServiceClient::listTeamsReply(QNetworkReply *reply) {
    CALL_LOGGER();
    TeamListResponse resp;
    commonReplyHandler(reply,&resp);
    if (resp.m_res_code==0) {
        QJsonObject result = resp.m_json_response["result"].toObject();
        resp.m_teams = result["teams"].toArray();
        emit teamListResponseReady(resp);
    }
    reply->manager()->deleteLater();
}

void WebServiceClient::pendingTeamInvitationsReply(QNetworkReply *reply) {
    CALL_LOGGER();
    CommonResponse resp;
    commonReplyHandler(reply,&resp);
    if (resp.m_res_code==0) {
        QJsonObject result = resp.m_json_response["result"].toObject();
        int number = result["number"].toInt();
        emit pendingTeamInvitationsResponseReady(number);
    }
    reply->manager()->deleteLater();
}

void WebServiceClient::pendingTeamMemberAcceptsReply(QNetworkReply *reply) {
    CALL_LOGGER();
    CommonResponse resp;
    commonReplyHandler(reply,&resp);
    if (resp.m_res_code==0) {
        QJsonObject result = resp.m_json_response["result"].toObject();
        int number = result["number"].toInt();
        emit pendingTeamMemberAcceptsResponseReady(number);
    }
    reply->manager()->deleteLater();
}

void WebServiceClient::teamShareDataEntryReply(QNetworkReply *reply) {
    CALL_LOGGER();
    CommonResponse resp;
    commonReplyHandler(reply,&resp);
    if (resp.m_res_code==0) {
        QJsonObject result = resp.m_json_response["result"].toObject();
        if (result["success"].toBool()==true) {
            qDebug("team share successful!");
        }
    }
    reply->manager()->deleteLater();
}
