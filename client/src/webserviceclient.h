#ifndef WEBSERVICECLIENT_H
#define WEBSERVICECLIENT_H

#include <QObject>
#include "whoamiresponse.h"
#include "dataentrylistresponse.h"
#include "successresponse.h"
#include "teamlistresponse.h"

class QNetworkAccessManager;
class QNetworkReply;
class CommonResponse;

class WebServiceClient : public QObject
{
    Q_OBJECT

public:
    static WebServiceClient *instance();
    ~WebServiceClient();

    bool upload(const QString &data_hash);
    bool download(const QString &data_hash);
    bool login(const QString &username, const QString &password);
    bool registerDevice(const QString &login_token=QString());
    bool whoami();
    bool listDataEntries();
    bool listTeamDataEntries();
    bool listTeams();
    bool pendingTeamInvitations();
    bool pendingTeamMemberAccepts();
    bool teamShareDataEntry(const QString &team_owner, const QString &teamname, const QString &data_hash);

signals:
    void whoamiResponseReady(const WhoamiResponse &response);
    void registerDeviceResponseReady(const SuccessResponse &response);
    void dataEntryListResponseReady(const DataEntryListResponse &response);
    void dataDownloadDone(const QString &data_hash);
    void dataTransferProgressChanged(const QString &data_hash, float progress);
    void dataUploadDone(const QString &data_hash);
    void pendingTeamInvitationsResponseReady(int count);
    void pendingTeamMemberAcceptsResponseReady(int count);
    void teamListResponseReady(const TeamListResponse &responset);

public slots:

private slots:
    void uploadReply(QNetworkReply *reply);
    void downloadReply(QNetworkReply *reply);
    void transferProgress(const QString &data_hash, qint64 received, qint64 total);
    void loginReply(QNetworkReply *reply);
    void registerDeviceReply(QNetworkReply *reply);
    void whoamiReply(QNetworkReply *reply);
    void commonReplyHandler(QNetworkReply *reply, CommonResponse *resp);
    void listDataEntriesReply(QNetworkReply *reply);
    void listTeamDataEntriesReply(QNetworkReply *reply);
    void listTeamsReply(QNetworkReply *reply);
    void pendingTeamInvitationsReply(QNetworkReply *reply);
    void pendingTeamMemberAcceptsReply(QNetworkReply *reply);
    void teamShareDataEntryReply(QNetworkReply *reply);
    bool hasDeviceHash();

private:
    WebServiceClient(QObject *parent = 0);
    static WebServiceClient *s_instance;
    QString m_device_hash;
    QString m_new_device_hash;
    QString m_login_token;
};

#endif // DATATRANSFER_H
