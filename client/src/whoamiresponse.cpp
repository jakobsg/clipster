#include "whoamiresponse.h"
#include <crashlib.h>

WhoamiResponse::WhoamiResponse()
: CommonResponse() {
    CALL_LOGGER();
}

WhoamiResponse::WhoamiResponse(const CommonResponse &resp)
: CommonResponse(resp) {
    CALL_LOGGER();
}

WhoamiResponse::~WhoamiResponse() {
    CALL_LOGGER();
}
