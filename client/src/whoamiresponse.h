#ifndef WHOAMIRESPONSE_H
#define WHOAMIRESPONSE_H

#include "commonresponse.h"

class WhoamiResponse : public CommonResponse {
public:
    WhoamiResponse();
    WhoamiResponse(const CommonResponse &resp);
    ~WhoamiResponse();

    QString m_username;
    QString m_firstname;
    QString m_lastname;

};

#endif // WHOAMIRESPONSE_H
