#include "wrapper.h"
#include <QApplication>
#include "singleapplication.h"
#include "clipboard.h"
#include "trayicon.h"
#include "clipboardapplicationmanager.h"
#include "clipboardmodel.h"
#include "clipboardproxymodel.h"
#include "clipboardbrowser.h"
#include <crashlib.h>
#include <QWidget>
#include <QLayout>

int runapp(int argc, char **argv) {
    CALL_LOGGER();
    SingleApplication singleapp;

    if (singleapp.alreadyRunning()) {
        return 0;
    }
    QApplication app(argc, argv);
    app.setQuitOnLastWindowClosed(false);
    app.setApplicationName("Clipster");
    Clipboard c;
//    qmlRegisterType<Clipboard>("Limbosoft.Clipster", 1,0, "Clipboard");
//    qmlRegisterType<ClipboardData>("Limbosoft.Clipster", 1,0, "ClipboardData");
//    qmlRegisterType<ClipboardModel>("Limbosoft.Clipster", 1,0, "ClipboardModel");
//    qmlRegisterType<ClipboardProxyModel>("Limbosoft.Clipster", 1,0, "ClipboardProxyModel");
    ClipboardApplicationManager::instance();
    return app.exec();
}
