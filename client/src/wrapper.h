#ifndef WRAPPER_H
#define WRAPPER_H
#include <QThread>
#include <crashlib.h>
#include "webserviceclient.h"

int runapp(int argc, char **argv);


class MyThread : public QThread
{
public:
    MyThread(QObject *parent) : QThread(parent) {}
private:
    void run() {
        CALL_LOGGER();
        x();
    }

    void x() {
        CALL_LOGGER();
    }
};

#endif // WRAPPER_H

