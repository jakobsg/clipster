#include "crashinfo.h"
#include "ui_crashinfo.h"
#include "sendreport.h"
#include <QFile>
#include <QTextStream>
#include <QIcon>
#include <QJsonObject>

CrashInfo::CrashInfo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CrashInfo) {
    ui->setupUi(this);
    setWindowIcon(QIcon(":/clipster-crash.png"));
    QFont f = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    f.setPixelSize(10);
    ui->m_crashinfo->setFont(f);
    ui->m_send_crashinfo->setDisabled(true);
    if (QApplication::arguments().length()>1) {
        ui->m_send_crashinfo->setDisabled(false);
        QFile jsonFile(QApplication::arguments().at(1));
        jsonFile.open(QFile::ReadOnly);
        m_crash_doc = QJsonDocument().fromJson(jsonFile.readAll());
        ui->m_crashinfo->appendPlainText(m_crash_doc.toJson(QJsonDocument::Indented));
    }
    ui->m_crashinfo->moveCursor (QTextCursor::Start) ;
    ui->m_crashinfo->ensureCursorVisible();
    connect(ui->m_send_crashinfo, SIGNAL(clicked()), this, SLOT(sendCrashInfo()));
}

CrashInfo::~CrashInfo() {
    delete ui;
}

void CrashInfo::sendCrashInfo() {
    SendReport sr(this, m_crash_doc);
    int res = sr.exec();
    if (res==QDialog::Accepted) {
        accept();
    }
}
