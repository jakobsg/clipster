#ifndef CRASHINFO_H
#define CRASHINFO_H

#include <QDialog>
#include <QJsonDocument>

namespace Ui {
class CrashInfo;
}

class QNetworkReply;

class CrashInfo : public QDialog
{
    Q_OBJECT

public:
    explicit CrashInfo(QWidget *parent = 0);
    ~CrashInfo();

public slots:
    void sendCrashInfo();

private:
    Ui::CrashInfo *ui;
    QJsonDocument m_crash_doc;
    QString m_username;
};

#endif // CRASHINFO_H
