#-------------------------------------------------
#
# Project created by QtCreator 2015-08-04T21:24:38
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = crashinfo
TEMPLATE = app
DESTDIR = bin

SOURCES += main.cpp\
        crashinfo.cpp \
    sendreport.cpp

HEADERS  += crashinfo.h \
    sendreport.h

FORMS    += crashinfo.ui \
    sendreport.ui

RESOURCES += \
    resources/images/images.qrc
