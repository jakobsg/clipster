#include "sendreport.h"
#include "ui_sendreport.h"

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QMessageBox>
#include <QJsonObject>

SendReport::SendReport(QWidget *parent, const QJsonDocument &crash_doc) :
    QDialog(parent),
    ui(new Ui::SendReport) {
    m_crash_doc = crash_doc;
    ui->setupUi(this);
    QString username = m_crash_doc.object()["username"].toString();
    if (!username.isEmpty()) {
        ui->m_optional_email->setText(username);
        ui->m_contact_ok->setChecked(true);
    }
    else {
        ui->m_contact_ok->setChecked(false);
    }
    connect(ui->m_send, SIGNAL(clicked()), this, SLOT(sendCrashInfo()));
}

SendReport::~SendReport() {
    delete ui;
}

void SendReport::sendCrashInfoFinished(QNetworkReply *rep) {
    QMessageBox::information(this,tr("Crash Info Transfer Result"),QString::fromUtf8(rep->readAll().data()));
    accept();
}

void SendReport::sendCrashInfo() {
    QNetworkAccessManager *am = new QNetworkAccessManager(this);
    QNetworkRequest req(QUrl("http://clipster-dev.limbosoft.com/upload-crashinfo"));
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    QJsonObject alter_obj = m_crash_doc.object();
    alter_obj["contact_ok"] = ui->m_contact_ok->isChecked();
    alter_obj["contact_mail"] = ui->m_optional_email->text();
    alter_obj["user_details"] = ui->m_optional_details->toPlainText();
    m_crash_doc.setObject(alter_obj);
    am->post(req,m_crash_doc.toJson(QJsonDocument::Indented));
    connect(am, SIGNAL(finished(QNetworkReply*)), this, SLOT(sendCrashInfoFinished(QNetworkReply*)));
}
