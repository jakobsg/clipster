#ifndef SENDREPORT_H
#define SENDREPORT_H

#include <QDialog>
#include <QNetworkReply>
#include <QJsonDocument>

namespace Ui {
class SendReport;
}

class SendReport : public QDialog
{
    Q_OBJECT

public:
    explicit SendReport(QWidget *parent, const QJsonDocument &crash_doc);
    ~SendReport();

public slots:
    void sendCrashInfo();
    void sendCrashInfoFinished(QNetworkReply *);

private:
    QJsonDocument m_crash_doc;
    Ui::SendReport *ui;
};

#endif // SENDREPORT_H
