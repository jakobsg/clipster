set EXTRA_COMPILER_FLAGS=
set EXTRA_LINKER_FLAGS=
set DEBUG=true

cd ..\lib\crashlib
rm debug release Makefile* bin *.pdb -Rf
qmake "CONFIG+=debug"
nmake clean
nmake

cd ..\cliplib
rm debug release Makefile* bin *.pdb -Rf
qmake "CONFIG+=debug"
nmake clean
nmake

cd ..\logview
rm debug release Makefile* bin *.pdb -Rf
qmake "CONFIG+=debug"
nmake clean
nmake

cd ..\..\crashinfo
rm debug release Makefile* bin *.pdb -Rf
qmake "CONFIG+=debug"
nmake clean
nmake

cd ..\desktop-client
rm debug release Makefile* bin dist/debug build *.pdb -Rf
qmake "CONFIG+=debug"
nmake clean
nmake

copy ..\lib\cliplib\bin\cliplib.dll dist\debug
copy ..\lib\logview\bin\logview.dll dist\debug
copy ..\lib\crashlib\bin\crashlib.dll dist\debug
copy ..\crashinfo\bin\crashinfo.exe dist\debug

windeployqt dist\debug\Clipster.exe --debug

copy ..\client\vcdist\vcredist_%BUILD_PLATFORM%.exe dist\debug

rem iscc win-deploy-release.iss
rem scp -i .ssh\id_rsa release-msi/setup-%BUILD_PLATFORM%.exe jakob@clipster-dev.limbosoft.com:downloads/clipster-dev/setup-%BUILD_PLATFORM%.exe
