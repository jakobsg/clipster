#-------------------------------------------------
#
# Project created by QtCreator 2015-10-14T21:53:01
#
#-------------------------------------------------

OUTPUT_EXT =
!isEmpty(QMAKE_TARGET.arch) {
  #OUTPUT_EXT = -$$QMAKE_TARGET.arch
}

CONFIG(debug, debug|release) {
    DESTDIR = dist/debug$$OUTPUT_EXT
    OBJECTS_DIR = build/$$OUTPUT_EXT/obj
    MOC_DIR = build/debug$$OUTPUT_EXT/moc
    RCC_DIR = build/debug$$OUTPUT_EXT/rcc
    UI_DIR = build/debug$$OUTPUT_EXT/ui
} else {
    DESTDIR = dist/release$$OUTPUT_EXT
    OBJECTS_DIR = build/release$$OUTPUT_EXT/obj
    MOC_DIR = build/release$$OUTPUT_EXT/moc
    RCC_DIR = build/release$$OUTPUT_EXT/rcc
    UI_DIR = build/release$$OUTPUT_EXT/ui
}

GIT_VERSION = $$system(git describe  --always --tags)

DEFINES += GIT_VERSION=\\\"$$GIT_VERSION\\\"

CONFIG += $$(DEBUG)

QT += core gui network concurrent webenginewidgets webchannel websockets

TARGET = Clipster
TEMPLATE = app


SOURCES += \
	src/main.cpp\
        src/applicationmanager.cpp \
    src/logindialog.cpp \
    src/maincontrol.cpp

HEADERS  += \
	src/applicationmanager.h \
    src/logindialog.h \
    src/maincontrol.h

LIBS += \
    -L../lib/cliplib/bin -lcliplib -L../lib/crashlib/bin -lcrashlib -L../lib/logview/bin -llogview


unix:!macx {
    DEFINES += LINUX_OS
    QMAKE_LFLAGS += -g -rdynamic "-Wl,-rpath,\'\$$ORIGIN\'"
    LIBS += -lX11 -lXtst -lXinerama -ludev -lbfd
}
macx {
    DEFINES += DARWIN_OS
}
win32 {
    DEFINES += WINDOWS_OS
}


INCLUDEPATH += \
    ../lib/cliplib/include \
    ../lib/crashlib/include \
    ../lib/logview/include

RESOURCES += \
    common.qrc

DISTFILES += \
    todo.txt

include(../lib/qtawesome/QtAwesome.pri)
