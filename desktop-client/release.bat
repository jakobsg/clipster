set EXTRA_COMPILER_FLAGS=
set EXTRA_LINKER_FLAGS=
set DEBUG=

cd ..\lib\crashlib
rm debug release Makefile* bin *.pdb -Rf
qmake
jom clean
jom -j4

cd ..\cliplib
rm debug release Makefile* bin *.pdb -Rf
qmake
jom clean
jom -j4

cd ..\logview
rm debug release Makefile* bin *.pdb -Rf
qmake
jom clean
jom -j4

cd ..\..\crashinfo
rm debug release Makefile* bin *.pdb -Rf
qmake
jom clean
jom -j4

cd ..\desktop-client
rm debug release Makefile* bin dist/release build *.pdb -Rf
qmake
jom clean
jom -j4

copy ..\lib\cliplib\bin\cliplib.dll dist\release
copy ..\lib\logview\bin\logview.dll dist\release
copy ..\lib\crashlib\bin\crashlib.dll dist\release
copy ..\crashinfo\bin\crashinfo.exe dist\release

windeployqt dist\release\Clipster.exe --release

copy ..\client\vcdist\vcredist_%BUILD_PLATFORM%.exe dist\release

rem iscc win-deploy-release.iss
rem scp -i .ssh\id_rsa release-msi/setup-%BUILD_PLATFORM%.exe jakob@clipster-dev.limbosoft.com:downloads/clipster-dev/setup-%BUILD_PLATFORM%.exe
