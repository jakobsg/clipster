#include "applicationmanager.h"

#include <Clipster/Net/webserviceclient.h>
#include <Clipster/Net/dataentrylistresponse.h>
#include <Clipster/Core/mediatype.h>
#include <Clipster/Core/clipboardtools.h>
#include <Clipster/Core/mediatypematcheddata.h>
#include <Clipster/Core/pathentrymanager.h>
#include <Clipster/Core/pathtools.h>
#include <Clipster/Core/configmanager.h>
#include <Clipster/Net/clipsterhost.h>
#include <QPushButton>
#include <QLayout>
#include <QStringList>
#include <QtDebug>
#include <QTimer>
#include <QApplication>
#include <QClipboard>
#include <QFutureWatcher>
#include <QTableWidget>
#include <QWebEngineView>
#include <QWebEnginePage>
#include <QWebChannel>
#include <QWebEngineSettings>
#include <QDesktopWidget>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QDir>
#include <QTimer>
#include <QMessageBox>
#include <QSlider>

ApplicationManager *ApplicationManager::s_instance = 0;

ApplicationManager::ApplicationManager(ClipsterNet::WebServiceClient *wsclient, QWidget *parent)
    : QWidget(parent) {
    connect(ClipsterCore::ClipboardTools::instance(), SIGNAL(clipDetailsAvaiable(const ClipsterCore::ClipDetails &)), this, SLOT(clipDetailsAvailable(const ClipsterCore::ClipDetails &)));
    m_check_online_timer = new QTimer(this);
    m_check_online_timer->setInterval(7000);
    m_check_online_timer->setSingleShot(false);
    connect(m_check_online_timer, SIGNAL(timeout()), this, SLOT(listEntries()));

    m_check_team_online_timer = new QTimer(this);
    m_check_team_online_timer->setInterval(7300);
    m_check_team_online_timer->setSingleShot(false);
    connect(m_check_team_online_timer, SIGNAL(timeout()), this, SLOT(listTeamEntries()));

    m_check_pending_team_actions = new QTimer(this);
    m_check_pending_team_actions->setInterval(15000);
    m_check_pending_team_actions->setSingleShot(false);
    connect(m_check_pending_team_actions, SIGNAL(timeout()), this, SLOT(updatePendingTeamActions()));

    m_data_load_defer = new QTimer(this);
    m_data_load_defer->setInterval(1000);
    m_data_load_defer->setSingleShot(true);
    connect(m_data_load_defer, SIGNAL(timeout()), this, SLOT(deferredDataLoad()));

    m_wsclient = wsclient;
    connect(m_wsclient, SIGNAL(dataEntryRemoved(QString,ClipsterNet::SuccessResponse)), this, SLOT(dataEntryRemoveCompleted(QString,ClipsterNet::SuccessResponse)));
    connect(m_wsclient, SIGNAL(dataEntryListResponseReady(const ClipsterNet::DataEntryListResponse &)), this, SLOT( processDataEntries(const ClipsterNet::DataEntryListResponse &) ));
    connect(m_wsclient, SIGNAL(uploadError(const QString &)), this, SLOT(showUploadError(const QString &)));
    connect(m_wsclient, SIGNAL(teamListResponseReady(ClipsterNet::TeamListResponse)), this, SLOT(teamsResponse(ClipsterNet::TeamListResponse)));
    connect(m_wsclient, SIGNAL(teamShareDataEntryResponseReady(ClipsterNet::SuccessResponse)), this, SLOT(teamShareResponse(ClipsterNet::SuccessResponse)));
    connect(m_wsclient, SIGNAL(pendingTeamInvitationsResponseReady(int)),this,SLOT(setPendingTeamInvitations(int)));
    connect(m_wsclient, SIGNAL(pendingTeamMemberAcceptsResponseReady(int)),this,SLOT(setPendingTeamMemberAccepts(int)));
    QVBoxLayout *l = new QVBoxLayout(this);
    m_web_engine_view = new QWebEngineView(this);
    m_web_engine_view->settings()->setAttribute(QWebEngineSettings::LocalContentCanAccessFileUrls,true);
    l->addWidget(m_web_engine_view);
    QWebChannel *channel = new QWebChannel(m_web_engine_view);
    channel->registerObject("app",this);
    m_web_engine_view->page()->setWebChannel(channel);
    QDir appdir(QApplication::applicationDirPath());
    appdir.cdUp();
    appdir.cdUp();
    appdir.cd(QString("web%1clipster%1pages").arg(appdir.separator()));
    m_web_engine_view->load(QUrl::fromLocalFile(appdir.filePath("cliplist.html")));
    connect(m_web_engine_view, SIGNAL(loadFinished(bool)), this, SLOT(viewLoaded(bool)));
    //connect(web_engine_view->page(), SIGNAL(linkHovered(QString)), this, SLOT(whichLink(QString)));
    QHBoxLayout *l2 = new QHBoxLayout(this);
    m_zoom_slider = new QSlider(Qt::Horizontal,this);
    m_zoom_slider->setMaximum(100);
    m_zoom_slider->setMinimum(60);
    m_zoom_slider->setMinimumWidth(200);
    m_zoom_slider->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Preferred);
    QSpacerItem *s = new QSpacerItem(20,10,QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    l2->addSpacerItem(s);
    l2->addWidget(m_zoom_slider);
    l->addLayout(l2);
    connect(m_zoom_slider, SIGNAL(valueChanged(int)), this, SLOT(zoomSliderChanged(int)));
    setLayout(l);
}

ApplicationManager::~ApplicationManager() {
}

ApplicationManager *ApplicationManager::instance(ClipsterNet::WebServiceClient *wsclient, QWidget *parent) {
    if (s_instance==0) {
        s_instance = new ApplicationManager(wsclient, parent);
    }
    return s_instance;
}

void ApplicationManager::download(const QString &data_hash) {
    ClipsterNet::TransferReplyWrapper *trw = m_wsclient->download(data_hash);
    if (trw) {
        connect(trw, SIGNAL(downloadProgress(ClipsterNet::TransferReplyWrapper*,qint64,qint64)), this, SLOT(progress(ClipsterNet::TransferReplyWrapper*,qint64,qint64)));
        connect(trw, SIGNAL(finished(ClipsterNet::TransferReplyWrapper*)), this, SLOT(downloadComplete(ClipsterNet::TransferReplyWrapper*)));
    }
    else {
        copyIt(data_hash);
    }
}

void ApplicationManager::freezeAndUpload(const QString &data_hash) {
    FutureFreezeWatcher *freezewatcher = new FutureFreezeWatcher(this);
    connect(freezewatcher, SIGNAL(finished(FutureFreezeWatcher*)), this, SLOT(freezeReadyAt(FutureFreezeWatcher*)));
    QFuture< DataHashFeezeResult > future1 = ClipsterCore::PathTools::backgroundFreeze(data_hash);
    freezewatcher->setFuture(future1);
}

void ApplicationManager::dataEntryRemoveCompleted(const QString &data_hash, const ClipsterNet::SuccessResponse &response) {
    if (response.m_success) {
        bool data_avail = ClipsterCore::PathTools::filesDirExists(data_hash)||ClipsterCore::PathTools::mimeDataDirExists(data_hash);
        emit dataEntryRemoved(data_hash,data_avail);
    }
}

void ApplicationManager::removeDataEntry(const QString &data_hash) {
    m_wsclient->removeDataEntry(data_hash);
}

void ApplicationManager::showUploadError(const QString &error_string) {
    QMessageBox::warning(this, tr("Upload Error"), error_string);
}

void ApplicationManager::clipDetailsAvailable(const ClipsterCore::ClipDetails &cd) {
    QJsonDocument doc;
    QJsonArray arr;
    arr.append(cd.jsonObject());
    doc.setArray(arr);
    emit newData(doc.toJson().data());
}

void ApplicationManager::dataToView() {
    QJsonDocument doc;
    doc.setArray(ClipsterCore::ClipDetails::clipDataEntries());
    emit newData(doc.toJson().data());
}

void ApplicationManager::progress(ClipsterNet::TransferReplyWrapper *reply, qint64 progress, qint64 total) {
    if (total==0) {
        emit downloadProgress(reply->dataHash(),0);
        return;
    }
    emit downloadProgress(reply->dataHash(),(progress*100)/total);
}

void ApplicationManager::downloadComplete(ClipsterNet::TransferReplyWrapper *trw) {
    copyIt(trw->dataHash());
    emit downloadProgress(trw->dataHash(),0);
}

void ApplicationManager::uploadComplete(ClipsterNet::TransferReplyWrapper *trw) {
    emit dataUploaded(trw->dataHash());
}

void ApplicationManager::freezeReadyAt(FutureFreezeWatcher *freezewatcher) {
    DataHashFeezeResult freeze_result = freezewatcher->result();
    if (freeze_result.second == ClipsterCore::PathEntryManager::Success ||
        freeze_result.second == ClipsterCore::PathEntryManager::FrozenAlready) {
        // Freeze success, put into cloud:
        //ClipsterCore::MediaTypeMatchedData *data = ClipsterCore::MediaTypeMatchedData::createFromCache(freeze_result.first, this);
        ClipsterNet::TransferReplyWrapper *trw = m_wsclient->upload(freeze_result.first);
        if (trw) {
            connect(trw, SIGNAL(uploadProgress(ClipsterNet::TransferReplyWrapper*,qint64,qint64)), this, SLOT(progress(ClipsterNet::TransferReplyWrapper*,qint64,qint64)));
            connect(trw, SIGNAL(finished(ClipsterNet::TransferReplyWrapper*)), this, SLOT(uploadComplete(ClipsterNet::TransferReplyWrapper*)));
        }
    }
    else if (freeze_result.second == ClipsterCore::PathEntryManager::FrozenAlready) {
        // Already frozen, means that it has already been put into the cloud.
    }
    qDebug("Frozen: %s, result: %d",freeze_result.first.toUtf8().data(), (int) freeze_result.second);
    freezewatcher->deleteLater();
}

void ApplicationManager::whichLink(QString link) {
    qDebug() << link;
}


void ApplicationManager::listEntries() {
    m_wsclient->listDataEntries();
}

void ApplicationManager::listTeamEntries() {
    m_wsclient->listTeamDataEntries();
}

void ApplicationManager::teamsResponse(const ClipsterNet::TeamListResponse &teams) {
    QJsonDocument doc(teams.m_teams);
    emit updateTeams(doc.toJson());
}

void ApplicationManager::teamShareResponse(const ClipsterNet::SuccessResponse &response) {
    QJsonObject data = response.data();
    if (response.m_success) {
        emit dataTeamShared(data["data_hash"].toString(), data["team_owner"].toString(), data["teamname"].toString());
    }
}

void ApplicationManager::processDataEntries(const ClipsterNet::DataEntryListResponse &response) {
    QJsonDocument doc;
    QJsonArray arr;
    foreach (QJsonValue v, response.m_data_entries) {
        QVariantMap map = v.toObject().toVariantMap();
        ClipsterCore::ClipDetails cd(map["data_hash"].toString());
        QJsonObject json = cd.jsonObject();
        json["deleted"] = false;
        if (map["removed"]==true && !cd.dataAvaiable()) {
            json["data_hash"] = map["data_hash"].toString();
            json["deleted"] = true;
        }
        arr.append(json);
    }
    doc.setArray(arr);
    emit newData(doc.toJson().data());
}

void ApplicationManager::viewLoaded(bool yes) {
    if (yes) {
        m_data_load_defer->start();
        m_wsclient->listTeams();
        m_check_online_timer->start();
        m_check_team_online_timer->start();
        m_check_pending_team_actions->start();
    }
}

void ApplicationManager::zoomSliderChanged(int sliderval) {
    ClipsterCore::ConfigManager::setConfigValue("zoom_factor",sliderval);
    emit zoomValueChanged((float) sliderval/100.0);
}

void ApplicationManager::setPendingTeamInvitations(int count) {
    m_pending_team_invitations = count;
    int old_count = m_pending_team_actions;
    m_pending_team_actions = m_pending_team_invitations + m_pending_team_member_accepts;
    if (old_count!=m_pending_team_actions) {
        pendingTeamActions(m_pending_team_actions);
    }
}

void ApplicationManager::setPendingTeamMemberAccepts(int count) {
    m_pending_team_member_accepts = count;
    int old_count = m_pending_team_actions;
    m_pending_team_actions = m_pending_team_invitations + m_pending_team_member_accepts;
    if (old_count!=m_pending_team_actions) {
        pendingTeamActions(m_pending_team_actions);
    }

}

void ApplicationManager::updatePendingTeamActions() {
    m_wsclient->pendingTeamInvitations();
    m_wsclient->pendingTeamMemberAccepts();
}

void ApplicationManager::deferredDataLoad() {
    m_zoom_slider->setValue(ClipsterCore::ConfigManager::configValue("zoom_factor").toInt(100));
    dataToView();
    listEntries();
}

void ApplicationManager::hideEvent(QHideEvent *he) {
    emit visibilityToggled(false);
    QWidget::hideEvent(he);
}

void ApplicationManager::showEvent(QShowEvent *se) {
    emit visibilityToggled(true);
    QWidget::showEvent(se);
}



void ApplicationManager::copyIt(const QString &data_hash) {
    ClipsterCore::ClipDetails cd(data_hash);
    if (!cd.dataAvaiable()) {
        download(data_hash);
    }
    ClipsterCore::MediaTypeMatchedData *mtmd = ClipsterCore::MediaTypeMatchedData::createFromCache(data_hash);
    if (mtmd) {
        QApplication::clipboard()->blockSignals(true);
        mtmd->mimeData()->setData("application/x-clipster-copy",QByteArray()); // Ensure no re-copy
        QApplication::clipboard()->setMimeData(mtmd->mimeData());
        QApplication::clipboard()->blockSignals(false);
        qDebug("Data put on clipboard: %s", mtmd->calculateDataHash().toUtf8().data());
    }
}

void ApplicationManager::teamShare(const QString &data_hash, const QString &team_owner, const QString &teamname) {
    m_wsclient->teamShareDataEntry(team_owner, teamname, data_hash);
}

void ApplicationManager::teamManage() {
    m_web_engine_view->load(ClipsterNet::ClipsterHost::teamsPageUrl());
}

