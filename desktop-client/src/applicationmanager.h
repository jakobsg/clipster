#ifndef APPLICATIONMANAGER_H
#define APPLICATIONMANAGER_H

#include <Clipster/Net/webserviceclient.h>
#include <Clipster/Core/mediatype.h>
#include <Clipster/Net/transferreplywrapper.h>
#include <Clipster/Core/pathtools.h>
#include <Clipster/Core/clipdetails.h>

#include <QWidget>
#include <QFutureWatcher>

class QPushButton;
class QTableWidget;
class QTimer;
class QWebEngineView;
class QSlider;

class FutureFreezeWatcher : public QFutureWatcher<DataHashFeezeResult> {
    Q_OBJECT
public:
    FutureFreezeWatcher(QObject *parent=0) : QFutureWatcher<DataHashFeezeResult>(parent) {
        connect(this,SIGNAL(finished()),this,SLOT(signalFinishedWithObject()));
    }

signals:
    void finished(FutureFreezeWatcher *obj);

public slots:
    void signalFinishedWithObject() {
        emit finished(this);
    }
};

class ApplicationManager : public QWidget {

    Q_OBJECT

public:
    ~ApplicationManager();
    static ApplicationManager *instance(ClipsterNet::WebServiceClient *wsclient, QWidget *parent=0);
    Q_INVOKABLE void copyIt(const QString &data_hash);
    Q_INVOKABLE void teamShare(const QString &data_hash, const QString &team_owner, const QString &teamname);
    Q_INVOKABLE void teamManage();

signals:
    void newData(const QString &data);
    void updateTeams(const QString &data);
    void downloadProgress(const QString &data_hash, int pct);
    void dataUploaded(const QString &data_hash);
    void dataEntryRemoved(const QString &data_hash, bool data_available);
    void dataTeamShared(const QString &data_hash, const QString &team_owner, const QString &teamname);
    void zoomValueChanged(float scaleval);
    void pendingTeamActions(int action_count);
    void visibilityToggled(bool visible);

public slots:
    void download(const QString &data_hash);
    Q_INVOKABLE void freezeAndUpload(const QString &data_hash);
    Q_INVOKABLE void removeDataEntry(const QString &data_hash);
    void showUploadError(const QString &error_string);
    void clipDetailsAvailable(const ClipsterCore::ClipDetails &cd);
    void dataToView();
    void progress(ClipsterNet::TransferReplyWrapper *reply, qint64 progress, qint64 total);
    void downloadComplete(ClipsterNet::TransferReplyWrapper *trw);
    void dataEntryRemoveCompleted(const QString &data_hash, const ClipsterNet::SuccessResponse &response);
    void uploadComplete(ClipsterNet::TransferReplyWrapper *trw);
    void freezeReadyAt(FutureFreezeWatcher *freezewatcher);
    void whichLink(QString);
    void listEntries();
    void listTeamEntries();
    void teamsResponse(const ClipsterNet::TeamListResponse &teams);
    void teamShareResponse(const ClipsterNet::SuccessResponse &response);
    void processDataEntries(const ClipsterNet::DataEntryListResponse &response);
    void viewLoaded(bool yes);
    void zoomSliderChanged(int sliderval);
    void setPendingTeamInvitations(int count);
    void setPendingTeamMemberAccepts(int count);
    void updatePendingTeamActions();
    void deferredDataLoad();

protected:
    void hideEvent(QHideEvent *he);
    void showEvent(QShowEvent *se);

private:
    ApplicationManager(ClipsterNet::WebServiceClient *wsclient, QWidget *parent = 0);

    static ApplicationManager *s_instance;
    ClipsterNet::WebServiceClient *m_wsclient;

    QWebEngineView *m_web_engine_view;
    QTableWidget *m_cliplist;
    QTimer *m_check_online_timer;
    QTimer *m_check_team_online_timer;
    QTimer *m_check_pending_team_actions;
    QTimer *m_data_load_defer;
    QSlider *m_zoom_slider;
    int m_pending_team_invitations;
    int m_pending_team_member_accepts;
    int m_pending_team_actions;
//    QPushButton *m_download;
//    QPushButton *m_upload;
};

#endif // APPLICATIONMANAGER_H
