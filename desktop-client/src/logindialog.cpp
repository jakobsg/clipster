#include "logindialog.h"
#include "Clipster/Net/clipsterhost.h"

#include <QWebEngineCallback>
#include <QLayout>
#include <QDebug>

LoginDialog *cur_login_dlg = 0;

LoginDialog::LoginDialog(QWidget *parent) : QWidget(parent) {
    QGridLayout *layout = new QGridLayout(this);
    setLayout(layout);
    m_webview = new QWebEngineView(this);
    layout->addWidget(m_webview);
    layout->setMargin(0);
    setMaximumHeight(330);
    setMaximumWidth(500);
    m_webview->load(ClipsterNet::ClipsterHost::loginPageUrl());
    m_poll_timer = new QTimer(this);
    m_poll_timer->setInterval(500);
    m_poll_timer->setSingleShot(false);
    connect(m_poll_timer, SIGNAL(timeout()), this, SLOT(checkLoginToken()));
    m_poll_timer->start();
    cur_login_dlg = this;
}

void LoginDialog::emitLoginSuccess(const QString &login_token) {
    emit loginSuccess(login_token);
    close();
    deleteLater();
}

void test_login_token(const QVariant &res) {
    if (!res.toString().isEmpty()) {
        cur_login_dlg->emitLoginSuccess(res.toString());
    }
}

void LoginDialog::checkLoginToken() {
    m_webview->page()->runJavaScript("$.cookie('login-token')", test_login_token);
}
