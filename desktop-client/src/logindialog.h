#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QWidget>
#include <QWebEngineView>
#include <QWebEngineCallback>
#include <QTimer>

class LoginDialog : public QWidget
{
    Q_OBJECT
public:
    explicit LoginDialog(QWidget *parent = 0);
    void emitLoginSuccess(const QString &login_token);

signals:
    void loginSuccess(const QString &login_token);

public slots:
    void checkLoginToken();

private:
    QWebEngineView *m_webview;
    QTimer *m_poll_timer;
};

#endif // LOGINDIALOG_H
