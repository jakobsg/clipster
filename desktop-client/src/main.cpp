#include "maincontrol.h"

#include <Clipster/Core/singleapplication.h>

#include <QApplication>
#include <QDesktopWidget>

int main(int argc, char *argv[])
{
    ClipsterCore::SingleApplication singleapp;

    if (singleapp.alreadyRunning()) {
        return 0;
    }

    qputenv("QTWEBENGINE_REMOTE_DEBUGGING", "9000");
    QApplication a(argc, argv);
    a.setQuitOnLastWindowClosed(false);
    MainControl m;
    return a.exec();
}
