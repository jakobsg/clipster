#include "maincontrol.h"

#include <Clipster/Core/configmanager.h>
#include <Clipster/Net/webserviceclient.h>

#include <logview.h>

#include <QDesktopWidget>
#include <QMenu>
#include <QApplication>

MainControl::MainControl(QObject *obj)
: QObject(obj) {
    m_wsclient = new ClipsterNet::WebServiceClient(this);

    m_awesome = new QtAwesome(this);
    m_awesome->initFontAwesome();

    connect(m_wsclient, SIGNAL(whoamiResponseReady(const ClipsterNet::WhoamiResponse &)),
        this, SLOT(whoamiResponse(const ClipsterNet::WhoamiResponse &)));
    connect(m_wsclient, SIGNAL(registerDeviceResponseReady(const ClipsterNet::SuccessResponse &)),
        this, SLOT(registerDeviceResponse(const ClipsterNet::SuccessResponse &)));
    QString device_hash = ClipsterCore::ConfigManager::configValue("device_hash").toString();
    if (!device_hash.isEmpty()) {
        m_wsclient->whoami();
    }
    else {
        m_login_dialog = new LoginDialog();
        connect(m_login_dialog, SIGNAL(loginSuccess(const QString &)),
            this, SLOT(loginSucceeded(const QString &)));
        m_login_dialog->show();
        QRect position = m_login_dialog->frameGeometry();
        position.moveCenter(QDesktopWidget().availableGeometry().center());
        m_login_dialog->move(position.topLeft());
    }
    m_tray_icon = new QSystemTrayIcon(QIcon(":/resources/images/clipster-logo.ico"),this);
    m_tray_menu = new QMenu(tr("Account"));

    m_show_clipboard_action = m_tray_menu->addAction(QIcon(":/resources/images/clipster-logo.ico"), tr("Show &Clipboard"));
    m_show_clipboard_action->setEnabled(false);
    m_show_clipboard_action->setCheckable(true);

    m_show_logview_action = m_tray_menu->addAction(QIcon(m_awesome->icon( fa::listalt )), tr("Show &Logview"));
    m_show_logview_action->setCheckable(true);
    connect(m_show_logview_action, SIGNAL(triggered()), this, SLOT(toggleLogView()));

    QAction *quit_action = m_tray_menu->addAction(QIcon(m_awesome->icon( fa::close )),tr("&Quit"));
    connect(quit_action, SIGNAL(triggered()), QApplication::instance(), SLOT(quit()));

    m_tray_icon->setContextMenu(m_tray_menu);
    m_tray_icon->show();
    connect(m_tray_menu, SIGNAL(aboutToShow()), this, SLOT(updateMenuState()));
}

void MainControl::whoamiResponse(const ClipsterNet::WhoamiResponse &resp) {
    if (resp.m_res_code==0) {
        m_whoami = resp;
        m_app_manager = ApplicationManager::instance(m_wsclient);
        connect(m_show_clipboard_action, SIGNAL(toggled(bool)), m_app_manager, SLOT(setVisible(bool)));
        connect(m_app_manager, SIGNAL(visibilityToggled(bool)), m_show_clipboard_action, SLOT(setChecked(bool)));
        m_show_clipboard_action->setEnabled(true);
        m_show_clipboard_action->setChecked(true);
        m_app_manager->show();
        QRect position = m_app_manager->frameGeometry();
        position.moveCenter(QDesktopWidget().availableGeometry().center());
        m_app_manager->move(position.topLeft());
    }
}

void MainControl::loginSucceeded(const QString &login_token) {
    m_wsclient->registerDevice(login_token);
}

void MainControl::registerDeviceResponse(const ClipsterNet::SuccessResponse &resp) {
    if (resp.m_res_code==0 && resp.m_success) {
        m_wsclient->whoami();
    }
}

void MainControl::toggleLogView() {
    LogView::toggleVisible();
}

void MainControl::updateMenuState() {
    m_show_logview_action->setChecked(LogView::hasInstance());
}


