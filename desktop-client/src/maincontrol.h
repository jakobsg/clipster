#ifndef MAINCONTROL_H
#define MAINCONTROL_H

#include "logindialog.h"
#include "applicationmanager.h"
#include "QtAwesome.h"

#include <Clipster/Net/webserviceclient.h>
#include <Clipster/Net/whoamiresponse.h>

#include <QObject>
#include <QSystemTrayIcon>

class MainControl : public QObject {

    Q_OBJECT

public:
    MainControl(QObject *obj=0);

public slots:
    void whoamiResponse(const ClipsterNet::WhoamiResponse &resp);
    void loginSucceeded(const QString &login_token);
    void registerDeviceResponse(const ClipsterNet::SuccessResponse &resp);
    void toggleLogView();
    void updateMenuState();

private:
    ClipsterNet::WebServiceClient *m_wsclient;
    ApplicationManager *m_app_manager;
    LoginDialog *m_login_dialog;
    ClipsterNet::WhoamiResponse m_whoami;
    QSystemTrayIcon *m_tray_icon;
    QMenu *m_tray_menu;
    QAction *m_show_clipboard_action;
    QAction *m_show_logview_action;
    QtAwesome* m_awesome;
};

#endif // MAINCONTROL_H
