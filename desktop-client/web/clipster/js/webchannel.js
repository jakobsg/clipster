var gchannel = null;
var cur_data_item = null;
var cur_zoom_val = 1.0;
$(function() {
    var $mainContextMenu = $("#main-context-menu");
    var $teamContextMenu = $("#team-context-menu");

    $mainContextMenu.find('a[name="teams"]').click( function(event) {
        event.stopPropagation();
        var maintop = $mainContextMenu.css("top");
        $mainContextMenu.fadeOut("fast");
        $teamContextMenu.css({
            right: "1em",
            top: maintop,
            "z-index": 1000
        });
        $teamContextMenu.fadeIn("fast");
    });

    $("body").click( function(event) {
       $(".contextmenu").fadeOut("fast");
    });

    $(".context-tab").click( function() {
        $(".context-tab").addClass("btn-default");
        $(".context-tab").removeClass("btn-primary");
        $(this).addClass("btn-primary");
    });

    jQuery.expr[":"].Contains = jQuery.expr.createPseudo(function(arg) {
        return function( elem ) {
            return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
        };
    });

    function filter_items() {
        var search_val = $("input#search-input").val().trim();
        if (search_val.length>0) {
            $('div.preview-text:not(:Contains("' + search_val + '"))').closest("div.list-group-item").hide();
            $('div.preview-text:Contains("' + search_val + '")').closest("div.list-group-item").show();
        }
        else {
            $('div.preview-text').closest("div.list-group-item").show();
        }
    }

    function remove_item(data_hash) {
        var li = $("div.list-group-item[data_hash="+data_hash+"]");
        li.animate({height: 0}, {
            duration: 400,
            complete: function() {
                li.remove();
            }
        });
    }

    var filter_timer = null;

    $("input#search-input").keyup( function() {
        if (filter_timer!==null) {
            clearTimeout(filter_timer)
        }
        filter_timer = setTimeout(function(){filter_items()},300);
    });

    new QWebChannel(qt.webChannelTransport, function (channel) {
        gchannel = channel;
        gchannel.objects.app.downloadProgress.connect( function(data_hash, pct) {
            $("div.list-group-item[data_hash="+data_hash+"]").css("background-size",pct+"% 2px");
        });
        gchannel.objects.app.dataUploaded.connect( function(data_hash) {
            $("div.list-group-item[data_hash="+data_hash+"]").addClass("in-cloud");
        });
        gchannel.objects.app.dataTeamShared.connect( function(data_hash, team_owner, teamname) {
            var team_tags = $("div.list-group-item[data_hash="+data_hash+"]").find('div.team-tags');
            var badge = $('<span class="badge pull-right">'+teamname+'</span>');
            badge.css("display","none");
            team_tags.append(badge);
            badge.fadeIn();
        });
        gchannel.objects.app.pendingTeamActions.connect( function(action_count) {
            $("span#team-manage-badge").html(action_count);
            if (action_count==0) {
                $("span#team-manage-badge").fadeOut("slow");
            }
            else {
                $("span#team-manage-badge").fadeIn("slow");
            }
        });
        gchannel.objects.app.dataEntryRemoved.connect( function(data_hash, data_avail) {
            var li = $("div.list-group-item[data_hash="+data_hash+"]")
            li.removeClass("in-cloud");
            if (!data_avail) {
                remove_item(data_hash);
            }
        });
        gchannel.objects.app.zoomValueChanged.connect( function(val) {
            cur_zoom_val = val;
            $("div#cliplist").css("zoom",val);
        });
        gchannel.objects.app.updateTeams.connect( function(data) {
            $("#team-context-menu").find('ul').children().remove();
            var json = eval("("+data+")");
            for (var idx in json) {
                var team = json[idx];
                if (idx>0) {
                    $teamContextMenu.find('ul').append('<li role="separator" class="divider"></li>');
                }
                var team_li = $('<li team_owner="'+team.owner+'" teamname="'+team.teamname+'"><a href="#" class="clipster-group medium-text">&nbsp;&nbsp;'+team.teamname+'</a></li>');
                $teamContextMenu.find('ul').append(team_li);
                team_li.click( function() {
                    var list_elem = cur_data_item.closest("div.list-group-item");
                    var data_hash = list_elem.attr("data_hash");
                    var team_owner = $(this).attr("team_owner");
                    var teamname = $(this).attr("teamname");
                    gchannel.objects.app.teamShare(data_hash,team_owner,teamname);
                });
            }
        });
        gchannel.objects.app.newData.connect(function(data) {
            var first_item = $("div.list-group-item:visible").first();
            var item_height = 0;
            if (first_item.length>0) {
                item_height = $(first_item[0]).height();
            }

            var json = eval("("+data+")");
            for (var idx in json) {
                var clipitem = json[idx];
                var li = $("div.list-group-item[data_hash="+clipitem.data_hash+"]");
                if (clipitem.deleted) {
                    remove_item(clipitem.data_hash);
                    continue;
                }
                var team_tags = null;
                var text_preview = null;
                if (li.length===0) {
                    li = $('<div class="list-group-item highlight"></div>');
                    var thumb_span = $('<div class="preview-thumb"></div>');
                    var details_span = $('<div class="details"></div>');
//                    var remove_span = $('<div class="clipster-trash-empty glow blue"></div>');
                    var remove_span = $('<div class="glow blue"><input type="checkbox"></input></div>');
                    var cloud_span = $('<div class="clipster-cloud-marker glow blue"></div>');

                    var options_span = $('<div class="right-offset clipster-menu glow blue"></div>');
                    options_span.click( function(event) {
                        event.stopPropagation();
                        if ($(".contextmenu").is(":visible")) {
                            $(".contextmenu").fadeOut("fast");
                            return;
                        }
                        cur_data_item = $(this);
                        var button_pos = $(this).offset();
                        $mainContextMenu.css({
                            right: (1.0*cur_zoom_val)+"em",
                            top: (button_pos.top*cur_zoom_val)+$(this).height(),
                            "z-index": 1000
                        });
                        $mainContextMenu.fadeIn("fast");
                    });
                    cloud_span.click( function(event) {
                        event.stopPropagation();
                        var list_elem = $(this).closest("div.list-group-item");
                        var data_hash = list_elem.attr("data_hash");
                        if (list_elem.hasClass("in-cloud")) {
                            gchannel.objects.app.removeDataEntry(data_hash);
                        }
                        else {
                            gchannel.objects.app.freezeAndUpload(data_hash);
                        }
                    });

                    remove_span.click( function(event) {
                        event.stopPropagation();
                        var list_item = $(this).closest('.list-group-item');
                        var remove_question = $("<div style='display: none'/>").css({
                            position: "absolute",
                            width: "100%",
                            height: "100%",
                            left: 0,
                            top: 0,
                            zIndex: 1000000,  // to be on the safe side
                            "background-color": list_item.css("background-color")
                        }).appendTo(list_item.css("position", "relative"));
                        remove_question.fadeIn("fast");
                        var form_group = $('<div class="form-group question"></div>');
                        var label = $('<span>Do you want to delete this data?</span>');
                        var delete_btn = $('<button type="button" class="btn btn-default pull-right"><span class="glyphicon clipster-trash-empty" aria-hidden="true"></span> Delete</button>');
                        var cancel_btn = $('<button type="button" class="btn btn-default pull-right"><span class="glyphicon clipster-cancel" aria-hidden="true"></span> Cancel</button>');

                        remove_question.append(form_group);
                        form_group.append(label);
                        form_group.append(cancel_btn);
                        form_group.append(delete_btn);
                        cancel_btn.click(function(event) {
                            event.stopPropagation();
                            remove_question.fadeOut('fast', function() {
                               remove_question.remove();
                            });
                        });
                        remove_question.append()
                    });

                    thumb_span.css('background-image',"url( file://" + clipitem.image_preview_path + ")");
                    li.append(thumb_span);
                    li.append(options_span);
                    li.append(cloud_span);
                    li.append(remove_span);
                    li.append(details_span);
                    var clipitem_date = $('<div class="clipitem-date"></div>');
                    text_preview = $('<div class="preview-text"></div>');
                    team_tags = $('<div class="team-tags"></div>');
                    details_span.append(text_preview);
                    details_span.append(team_tags);
                    details_span.append(clipitem_date);
                    clipitem_date.append(clipitem.copytime_formatted);
                    li.attr('data_hash',clipitem.data_hash);
                    li.click( function() {
                        if ($(".contextmenu").is(":visible")) {
                            return;
                        }

                        gchannel.objects.app.copyIt($(this).attr('data_hash'));
                    });
                    if (json.length===1) {
                        li.animate({height: item_height}, {
                            duration: 400,
                        });
                    }

                    $("#cliplist").prepend(li);
                }
                else {
                    team_tags = li.find("div.team-tags");
                    text_preview = li.find("div.preview-text");
                }
                if (clipitem.in_cloud && clipitem.in_cloud===true) {
                    li.addClass("in-cloud");
                }
                else {
                    li.removeClass("in-cloud");
                }

                team_tags.children().remove();
                for (var tidx in clipitem.teams) {
                    var teaminfo = clipitem.teams[tidx];
                    var badge = $('<span class="badge pull-right">'+teaminfo.teamname+'</span>');
                    badge.css("display","none");
                    team_tags.append(badge);
                    badge.fadeIn();
                }
                text_preview.text((clipitem.text_preview===null || clipitem.text_preview.trim()==="") ? "No preview text" : clipitem.text_preview);
            }
        });
        $("#team-manage").click( function() {
            gchannel.objects.app.teamManage();
        });
    });
});
