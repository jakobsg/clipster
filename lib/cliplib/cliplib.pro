#-------------------------------------------------
#
# Project created by QtCreator 2015-07-03T13:57:41
#
#-------------------------------------------------

QT += network gui widgets concurrent

TARGET = cliplib
DESTDIR = bin
TEMPLATE = lib

DEFINES += CLIPLIB_LIBRARY

DEFINES += USE_CLIPSTER_DEV_SERVER
DEFINES += CLIPSTER_SERVICE_VERSION=1

HEADERS += \
    src/cliplib_global.h \
    src/core/mediatype.h \
    src/core/mediatypeset.h \
    src/core/mediatypematchrule.h \
    src/core/mediatypematchruleset.h \
    src/core/mediatypehelper.h \
    src/core/pathentrymanager.h \
    src/net/transferreplywrapper.h \
    src/net/webserviceclient.h \
    src/net/multipartreader.h \
    src/core/mediatypematcheddata.h \
    src/net/clipsterhost.h \
    src/net/commonresponse.h \
    src/core/pathtools.h \
    src/core/clipboardtools.h \
    src/core/platformtools.h \
    src/core/clipdetails.h \
    src/net/dataentrylistresponse.h \
    src/net/successresponse.h \
    src/net/teamlistresponse.h \
    src/net/whoamiresponse.h \
    src/core/deviceinfo.h \
    src/core/configmanager.h \
    src/core/singleapplication.h

SOURCES += \
    src/core/mediatype.cpp \
    src/core/mediatypeset.cpp \
    src/core/mediatypematchrule.cpp \
    src/core/mediatypematchruleset.cpp \
    src/main.cpp \
    src/core/mediatypehelper.cpp \
    src/core/pathentrymanager.cpp \
    src/net/transferreplywrapper.cpp \
    src/net/webserviceclient.cpp \
    src/net/multipartreader.cpp \
    src/core/mediatypematcheddata.cpp \
    src/net/commonresponse.cpp \
    src/core/pathtools.cpp \
    src/core/clipboardtools.cpp \
    src/core/clipdetails.cpp \
    src/net/dataentrylistresponse.cpp \
    src/net/successresponse.cpp \
    src/net/teamlistresponse.cpp \
    src/net/whoamiresponse.cpp \
    src/core/configmanager.cpp \
    src/core/deviceinfo.cpp \
    src/net/clipsterhost.cpp \
    src/core/singleapplication.cpp

unix:!macx {
SOURCES += \
    src/core/posix/hardlink.cpp \
    src/core/linux/deviceinfo_lin.cpp
    DEFINES += LINUX_OS
    QMAKE_LFLAGS += -g -rdynamic "-Wl,-rpath,\'\$$ORIGIN\'"
    LIBS += -lX11 -lXtst -lXinerama -ludev -lbfd
}
macx {
SOURCES += \
    src/core/posix/hardlink.cpp \
    src/core/mac/deviceinfo_mac.cpp
    DEFINES += DARWIN_OS
}
win32 {
    SOURCES += \
        src/core/windows/hardlink.cpp \
        src/core/windows/deviceinfo_win.cpp
    DEFINES += CLIPLIB_LIBRARY WINDOWS_OS
    DEFINES += WINDOWS_OS
    LIBS += User32.lib userenv.lib dbghelp.lib
    QMAKE_CXXFLAGS_RELEASE -= -Zc:strictStrings
    QMAKE_CFLAGS_RELEASE += $$(EXTRA_COMPILER_FLAGS)
    QMAKE_CXXFLAGS_RELEASE += $$(EXTRA_COMPILER_FLAGS)
    QMAKE_LFLAGS_RELEASE += $$(EXTRA_LINKER_FLAGS)
    QMAKE_CFLAGS_RELEASE += -Zi
    QMAKE_CXXFLAGS_RELEASE += -Zi
    QMAKE_LFLAGS_RELEASE += /DEBUG /OPT:REF
    QMAKE_CXXFLAGS_EXCEPTIONS_ON = /EHa
    QMAKE_CXXFLAGS_STL_ON = /EHa
}
unix {
    target.path = /usr/lib
    INSTALLS += target
}

RESOURCES += \
    resources/images/icons.qrc

LIBS += \
    -L../crashlib/bin -lcrashlib

INCLUDEPATH += \
    ../crashlib/include
