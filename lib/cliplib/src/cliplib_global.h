#ifndef CLIPLIB_GLOBAL_H
#define CLIPLIB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(CLIPLIB_LIBRARY)
#  define CLIPLIBSHARED_EXPORT Q_DECL_EXPORT
#else
#  define CLIPLIBSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // CLIPLIB_GLOBAL_H
