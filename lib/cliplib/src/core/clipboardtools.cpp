#include "clipboardtools.h"
#include "mediatypematcheddata.h"
#include "pathtools.h"
#include "mediatype.h"

#include <QApplication>
#include <QClipboard>
#include <QMimeData>
#include <QTimer>
#include <QDir>
#include <QUrl>

ClipsterCore::ClipboardTools *ClipsterCore::ClipboardTools::s_instance = 0;

namespace ClipsterCore {

    ClipboardTools::ClipboardTools(QObject *parent) : QObject(parent) {
        setParent(QApplication::instance());

        // Connect QClipboard::dataChanged() to Clipboard::dataChanged() slot in order to
        // recieve events about clipboard copies.
        QTimer *data_deferrence_timer = new QTimer(this);
        data_deferrence_timer->setInterval(250);
        data_deferrence_timer->setSingleShot(true);
        connect(data_deferrence_timer,SIGNAL(timeout()),this,SLOT(clipboardDataChanged()));
        connect(QApplication::clipboard(), SIGNAL(dataChanged()), data_deferrence_timer, SLOT(start()));
    }

    ClipboardTools *ClipboardTools::instance() {
        if (s_instance==0) {
            s_instance = new ClipboardTools;
        }
        return s_instance;
    }

    void ClipboardTools::clipboardDataChanged() {
        // As soon as data enters the Clipboard copy it in mime-data form to
        // local disk cache.
        if (QApplication::clipboard()->mimeData()->formats().contains("application/x-clipster-copy")) {
            // Copy is from clipster - ignore
            return;
        }
        MediaTypeMatchedData data(QApplication::clipboard()->mimeData());
        QString data_hash = data.saveToCache();
        ClipDetails cd(data_hash, &data);
        cd.save();
        emit clipDetailsAvaiable(cd);
        qDebug("Data available: %s",data.calculateDataHash().toUtf8().data());
    }

    void ClipboardTools::createFromCache(const QString &data_hash, QMimeData &mimedata) {
        QDir data_dir(PathTools::dataDir(data_hash));
        QString file_data_path = data_dir.filePath("files");
        QString mime_data_path = data_dir.filePath("mimedata");
        if (QFileInfo::exists(file_data_path)) {
            // Setup file copy
            QDir files_dir(file_data_path);
            QList<QUrl> urls;
            foreach (QFileInfo fi, files_dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files)) {
                urls << QUrl::fromLocalFile(fi.absoluteFilePath());
            }
            mimedata.setUrls(urls);
        }
        else if (QFileInfo::exists(mime_data_path)) {
            // Setup Mimedata copy
            QDir mimedata_dir(mime_data_path);
            foreach (QFileInfo top_fi, mimedata_dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files)) {
                if (top_fi.isDir()) {
                    QDir top_dir(top_fi.filePath());
                    foreach (QFileInfo sub_fi, top_dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files)) {
                        if (sub_fi.isFile()) {
                            QFile data(sub_fi.filePath());
                            if (data.open(QFile::ReadOnly)) {
                                QString format = QString("%1/%2").arg(top_fi.fileName()).arg(sub_fi.fileName());
                                mimedata.setData(format,data.readAll());
                            }
                        }
                    }
                }
            }
        }
    }

}
