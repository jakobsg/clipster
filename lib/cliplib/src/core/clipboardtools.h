#ifndef CLIPBOARDTOOLS_H
#define CLIPBOARDTOOLS_H

#include "../cliplib_global.h"
#include "clipdetails.h"

#include <QObject>

namespace ClipsterCore {

    class CLIPLIBSHARED_EXPORT ClipboardTools : public QObject
    {

        Q_OBJECT

    public:
        static ClipboardTools *instance();
        static void createFromCache(const QString &data_hash, QMimeData &mimedata);

    signals:
        void clipDetailsAvaiable(const ClipsterCore::ClipDetails &cd);

    public slots:
        void clipboardDataChanged();

    private:
        explicit ClipboardTools(QObject *parent = 0);
        static ClipboardTools *s_instance;
    };

}
#endif // CLIPBOARDTOOLS_H
