#include "clipdetails.h"
#include "pathtools.h"
#include "mediatypematchruleset.h"
#include "mediatypehelper.h"

#include <QDir>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QMimeData>
#include <QSysInfo>
#include <QHostInfo>

namespace ClipsterCore {

    ClipDetails::ClipDetails(const QString &data_hash, const MediaTypeMatchedData *mtmd) {
        init(data_hash, mtmd);
    }

    ClipDetails::ClipDetails(const QJsonObject &data_entry) {
        QString data_hash = data_entry["data_hash"].toString();
        m_details = data_entry;
        PathTools::dataDir(data_hash, true);
        init(data_hash);
        m_details["in_cloud"] = true;
        QImage img;
        QString image_preview = m_details["image_preview"].toString();
        if (!image_preview.isEmpty()) {
            img = QImage::fromData(QByteArray::fromBase64(image_preview.toLatin1()),"png");
            m_details.remove("image_preview");
        }
        else {
            QStringList formats;
            foreach (QJsonValue mt, m_details["mime_types"].toArray()) {
                formats << mt.toString();
            }
            const MediaTypeMatchRuleSet* mtrs = MediaTypeHelper::detectMediaType(formats);
            if (mtrs) {
                img = QImage(mtrs->iconResourceUrl());
            }
        }
        QString teamname = data_entry["teamname"].toString();
        QString team_owner = data_entry["team_owner"].toString();
        if (!teamname.isEmpty() && !team_owner.isEmpty()) {
            addTeam(team_owner, teamname);
        }
        img.save(m_preview_png_path,"png");
    }

    void ClipDetails::init(const QString &data_hash, const MediaTypeMatchedData *mtmd) {
        if (!data_hash.isEmpty()) {
            m_data_hash = data_hash;
            QDir datadir(PathTools::dataDir(data_hash, mtmd!=0));
            m_details_json_path = datadir.filePath("details.json");
            m_preview_png_path = datadir.filePath("preview.png");
            if (mtmd) {
                const QMimeData *md = mtmd->mimeData();
                const MediaTypeMatchRuleSet *mtrs = mtmd->mediaTypeMatchRuleSet();
                QImage img;
                if (mtrs->isPureImage()) {
                    img = qvariant_cast<QImage>(md->imageData());
                    img = img.scaledToHeight(128,Qt::SmoothTransformation);
                }
                else {
                    img = QImage(mtrs->iconResourceUrl());
                }
                img.save(datadir.filePath("preview.png"),"png");
                m_details["data_hash"] = data_hash;
                m_details["text_preview"] = md->text();
                m_details["copytime"] = QDateTime::currentMSecsSinceEpoch();
                m_details["hostname"] = QHostInfo::localHostName();
                m_details["os_type"] = QSysInfo::productType();
                m_details["os_version"] = QSysInfo::productVersion();
                m_details["mime_types"] = QJsonArray::fromStringList(mtmd->mimeData()->formats());
                m_details["in_cloud"] = false;
            }
            else {
                QFile details_json(m_details_json_path);
                if (details_json.exists()) {
                    if (details_json.open(QFile::ReadOnly)) {
                        QJsonDocument doc = QJsonDocument::fromJson(details_json.readAll());
                        m_details = doc.object();
                        details_json.close();
                    }
                }
            }
            m_details["image_preview_path"] = ClipsterCore::PathTools::previewPngFile(data_hash);
            if (m_details.contains("copytime")) {
                m_details["copytime_formatted"] = copyTime().toString();
            }
            if (!m_details.contains("teams")) {
                m_details["teams"] = QJsonArray();
            }
        }
    }

    bool ClipDetails::save() {
        if (!m_data_hash.isEmpty()) {
            QFile details_json(m_details_json_path);
            if (details_json.open(QFile::WriteOnly)) {
                QJsonDocument doc;
                doc.setObject(m_details);
                details_json.write(doc.toJson());
                details_json.close();
                return true;
            }
        }
        return false;
    }

    bool ClipDetails::remove() {
        return PathTools::removeDir(PathTools::dataDir(dataHash()));
    }

    const QJsonObject &ClipDetails::jsonObject() const {
        return m_details;
    }

    void ClipDetails::saveClipDataEntries(const QJsonArray &entries) {
        foreach (QJsonValue v, entries) {
            QJsonObject data_entry = v.toObject();
            bool removed = data_entry["removed"].toBool();
            ClipDetails cd(data_entry);
            cd.setIsInCloud(!removed);
            if (data_entry["removed"].toBool()==true && !cd.dataAvaiable()) {
                cd.remove();
                continue;
            }
            cd.save();
        }
    }

    QJsonArray ClipDetails::clipDataEntries() {
        QDir data_base(ClipsterCore::PathTools::dataBaseDir());
        QStringList data_hashes = data_base.entryList(QDir::AllDirs|QDir::NoDotAndDotDot);
        QMap<qint64,QJsonObject> clip_entry_map;
        foreach (QString data_hash, data_hashes) {
            ClipsterCore::ClipDetails cd(data_hash);
            clip_entry_map[cd.m_details["copytime"].toVariant().toLongLong()] = cd.m_details;
        }
        QList<qint64> copytimes = clip_entry_map.keys();
        qSort(copytimes);

        QJsonArray arr;
        foreach (qint64 copytime, copytimes) {
            arr.append(clip_entry_map[copytime]);
        }
        return arr;
    }

    QString ClipDetails::detailsJsonPath() const {
        return m_details_json_path;
    }

    QString ClipDetails::previewText() const {
        return m_details["text_preview"].toString();
    }

    void ClipDetails::setPreviewText(const QString &text_preview) {
        m_details["text_preview"] = text_preview.left(512);
    }

    QString ClipDetails::previewImagePath() const {
        QDir datadir(PathTools::dataDir(m_data_hash, false));
        return datadir.filePath("preview.png");
    }

    QString ClipDetails::dataHash() const {
        return m_data_hash;
    }

    QString ClipDetails::osType() const {
        return m_details["os_type"].toString();
    }

    void ClipDetails::setOsType(const QString &os_type) {
        m_details["os_type"] = os_type;
    }

    QString ClipDetails::osVersion() const {
        return m_details["os_version"].toString();
    }

    void ClipDetails::setOsVersion(const QString &os_version) {
        m_details["os_version"] = os_version;
    }

    QString ClipDetails::hostname() const {
        return m_details["hostname"].toString();
    }

    void ClipDetails::setHostname(const QString &hostname) {
        m_details["hostname"] = hostname;
    }

    QDateTime ClipDetails::copyTime() const {
        return QDateTime::fromMSecsSinceEpoch(m_details["copytime"].toVariant().toLongLong());
    }

    void ClipDetails::setCopyTime(const QDateTime &copy_time) {
        m_details["copytime"] = copy_time.toMSecsSinceEpoch();
    }

    bool ClipDetails::isFrozen() const {
        if (m_details.contains("frozen")) {
            return m_details["frozen"].toBool();
        }
        return false;
    }

    void ClipDetails::setIsFrozen(bool frozen) {
        m_details["frozen"] = frozen;
    }

    bool ClipDetails::isInCloud() const {
        return m_details["in_cloud"].toBool();
    }

    void ClipDetails::setIsInCloud(bool in_cloud) {
        m_details["in_cloud"] = in_cloud;
    }

    QJsonArray ClipDetails::teams() const {
        return m_details["teams"].toArray();
    }

    void ClipDetails::addTeam(const QString &team_owner, const QString &teamname) {
        bool already_added = false;
        QJsonArray teamsarray = m_details["teams"].toArray();
        foreach (QJsonValue teamval, teamsarray) {
            QJsonObject teamobj = teamval.toObject();
            if (teamobj["team_owner"].toString()==team_owner && teamobj["teamname"].toString()==teamname) {
                already_added = true;
                break;
            }
        }
        if (!already_added) {
            QJsonObject teamobj;
            teamobj["team_owner"] = team_owner;
            teamobj["teamname"] = teamname;
            teamsarray.append(teamobj);
        }
        m_details["teams"] = teamsarray;
    }

    bool ClipDetails::dataAvaiable() const {
        if (!m_data_hash.isEmpty()) {
            return PathTools::mimeDataDirExists(m_data_hash) || PathTools::filesDirExists(m_data_hash);
        }
        return false;
    }


    //QString PathTools::clipDetails(const QString &data_hash) {
    //    QDir datadir(PathTools::dataDir(data_hash, create));
    //    return datadir.filePath("details.json");
    //}

    //bool PathTools::clipDetailsExists(const QString &data_hash) {
    //    return QFileInfo::exists(PathTools::clipDetails(data_hash));
    //}
}
