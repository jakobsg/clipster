#ifndef CLIPDETAILS_H
#define CLIPDETAILS_H

#include "../cliplib_global.h"
#include "mediatypematcheddata.h"

#include <QStringList>
#include <QImage>
#include <QDateTime>
#include <QJsonObject>
#include <QJsonArray>

namespace ClipsterCore {

    class CLIPLIBSHARED_EXPORT ClipDetails {
    public:
        ClipDetails(const QString &data_hash, const MediaTypeMatchedData *mtmd=0);
        ClipDetails(const QJsonObject &data_entry);

        void init(const QString &data_hash, const MediaTypeMatchedData *mtmd=0);

        QString dataHash() const;
        QString detailsJsonPath() const;
        QString previewImagePath() const;

        QString previewText() const;
        void setPreviewText(const QString &text_preview);

        QString osType() const;
        void setOsType(const QString &os_type);

        QString osVersion() const;
        void setOsVersion(const QString &os_version);

        QString hostname() const;
        void setHostname(const QString &hostname);

        QDateTime copyTime() const;
        void setCopyTime(const QDateTime &copy_time);

        bool isFrozen() const;
        void setIsFrozen(bool frozen=true);

        bool isInCloud() const;
        void setIsInCloud(bool in_cloud=true);

        QJsonArray teams() const;
        void addTeam(const QString &team_owner, const QString &teamname);

        bool dataAvaiable() const;

        bool save();
        bool remove();

        const QJsonObject &jsonObject() const;

        static void saveClipDataEntries(const QJsonArray &entries);
        static QJsonArray clipDataEntries();

    private:
        QJsonObject m_details;
        QString m_data_hash;
        QString m_details_json_path;
        QString m_preview_png_path;
    };

}
#endif // CLIPDETAILS_H
