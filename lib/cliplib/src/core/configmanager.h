#ifndef CLIPSTERCONFIG_H
#define CLIPSTERCONFIG_H

#include "../cliplib_global.h"
#include <QString>
#include <QJsonValue>

namespace ClipsterCore {

    class CLIPLIBSHARED_EXPORT ConfigManager {
    public:
        static QJsonValue configValue(const QString &key, QJsonValue defval=QJsonValue(), bool *ok=0);
        static bool setConfigValue(const QString &key, const QJsonValue &value);
        static bool removeConfigValue(const QString &key);
    private:
        static QString configPath();
    };

}
#endif // CLIPSTERCONFIG_H
