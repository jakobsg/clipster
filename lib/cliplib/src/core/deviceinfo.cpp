#include "deviceinfo.h"
#include "configmanager.h"

#include <crashlib.h>

#include <QString>
#include <QDateTime>
#include <QCryptographicHash>
#include <QHostInfo>

#ifdef WINDOWS_OS
#define USERNAME_ENV "USERNAME"
#else DARWIN_OS
#define USERNAME_ENV "USER"
#endif

namespace ClipsterCore {

    QString hardwareSignature(const QString &username) {
        CALL_LOGGER();
        // TODO: Must add bi-directional encoding
        QString hwsig = ConfigManager::configValue("hwsig").toString();
        if (!hwsig.isEmpty()) {

        }
        else {
            QString username(getenv(USERNAME_ENV));
            QDateTime now = QDateTime::currentDateTime();
            hwsig = QString("%1;%2-MV-%3-ID-%4;%5")
                .arg(QHostInfo::localHostName())
                .arg(now.toString("yyyy-MM-dd"))
                .arg(now.toString("HH"))
                .arg(now.toString("mm-ss-zzz"))
                .arg(username);
            ConfigManager::setConfigValue("hwsig", hwsig);
        }
//        QString hardware_sig_unhashed = QString::fromStdString(getPhysicalDiskSerialNumber());
//        if (hardware_sig_unhashed.isEmpty()) {
//            return "";
//        }
//        hardware_sig_unhashed += username;
        QString intermediate = QString(
            QCryptographicHash::hash(
                hwsig.toUtf8(),
                QCryptographicHash::Md5).toHex());
        return intermediate;
    }

}
