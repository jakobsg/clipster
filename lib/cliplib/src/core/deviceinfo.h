#ifndef DEVICEINFO_H
#define DEVICEINFO_H

#include "../cliplib_global.h"
#include <QString>

namespace ClipsterCore {

    std::string CLIPLIBSHARED_EXPORT getPhysicalDiskSerialNumber(const char *path=0);
    QString CLIPLIBSHARED_EXPORT hardwareSignature(const QString &username="");

}

#endif // DEVICEINFO_H
