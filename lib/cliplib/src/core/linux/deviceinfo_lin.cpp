#include <libudev.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string>
#include <qdebug.h>

#define USERNAME_ENV "USER"

namespace ClipsterCore {

    std::string getPhysicalDiskSerialNumber(const char *path) {
        struct stat s;
        std::string serial;
        char syspath[256];
        char final_path[256];
        if (path) {
            strncpy(final_path,path,255);
        }
        strncpy(final_path,"/",255);
        syspath[0] = '\0';
        stat(final_path,&s);
        sprintf(syspath,"/sys/dev/block/%d:%d",major(s.st_dev),minor(s.st_dev));

        struct udev *context = udev_new();
        struct udev_device *device = udev_device_new_from_syspath(context, syspath);
        const char *id = udev_device_get_property_value(device, "ID_SERIAL");
        if (id==NULL) {
            id = udev_device_get_property_value(device, "ID_SERIAL_SHORT");
        }
        if (id==NULL) {
            id = udev_device_get_property_value(device, "DM_UUID");
        }
        if (id!=NULL) {
            serial = id;
            qDebug("found Disk Serial ID: %s",id);
        }
        else {
            qDebug("Found no Disk Serial ID");
        }
        // Cleanup
        udev_device_unref(device);
        udev_unref(context);
        return serial;
    }

}
