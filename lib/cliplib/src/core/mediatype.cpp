#include "mediatype.h"
#include <QRegularExpression>

namespace ClipsterCore {

    MediaType::MediaType(const QString &mimetype) {
        updateParameters(mimetype);
    }

    void MediaType::updateParameters(const QString &mimetype) {
        QStringList parts = mimetype.split(QRegularExpression("\\s*;\\s*"));
        bool first = true;
        foreach (QString p, parts) {
            if (first) {
                p = p.toLower();
                QStringList type_parts = p.split("/");
                if (!m_fulltype.isEmpty() && m_fulltype==p) {
                    // Updating an already initialized MediaType instanse with
                    // parameters from a different media type. We don't want that
                    break;
                }
                m_fulltype = p;
                if (type_parts.length()==2) {
                    m_toptype = type_parts[0];
                    m_subtype = type_parts[1];
                }
                else {
                    m_toptype = type_parts[0];
                }
                first = false;
                continue;
            }
            QStringList param_parts = p.split("=");
            if (param_parts.length()>1) {
                m_parameters[param_parts[0]] = QStringList(param_parts.mid(1)).join("=");
            }
        }
        m_mediatype = m_toptype + (m_subtype.isEmpty() ? "" : "/" + m_subtype);
        QStringList params;
        foreach (QString pkey, m_parameters.keys()) {
            params << pkey + "=" + m_parameters[pkey];
        }
        if (params.length()) {
            m_mediatype += ";" + params.join(";");
        }
    }

    QString MediaType::fulltype() const {
        return m_fulltype;
    }

    QString MediaType::toplevelType() const {
        return m_toptype;
    }

    QString MediaType::subType() const {
        return m_subtype;
    }

    QStringList MediaType::paramKeys() const {
        return m_parameters.keys();
    }

    QString MediaType::paramValue(const QString &param_key) const {
        return m_parameters[param_key];
    }

    const QMap<QString, QString> &MediaType::parameters() const {
        return m_parameters;
    }

    const QString &MediaType::mediaType() const {
        return m_mediatype;
    }
}
