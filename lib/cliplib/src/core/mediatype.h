#ifndef MEDIATYPE_H
#define MEDIATYPE_H

#include "../cliplib_global.h"

#include <QMap>
#include <QStringList>

namespace ClipsterCore {

    class CLIPLIBSHARED_EXPORT MediaType {
    public:
        MediaType(const QString &mimetype);

        void updateParameters(const QString &mimetype);

        QString fulltype() const;
        QString toplevelType() const;
        QString subType() const;
        const QString &mediaType() const;

        QStringList paramKeys() const;
        QString paramValue(const QString &param_key) const;
        const QMap<QString,QString> &parameters() const;

        QString m_fulltype;
        QString m_toptype;
        QString m_subtype;
        QString m_mediatype;
        QMap<QString, QString> m_parameters;
    };

    typedef QMap<QString, MediaType *> MediaTypeMap;

}

#endif
