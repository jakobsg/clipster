#include "mediatypehelper.h"

#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QMimeData>
#include <QImage>
#include <QBuffer>
#include <QList>

namespace ClipsterCore {

    MediaTypeHelper *MediaTypeHelper::s_instance = 0;

    MediaTypeHelper *MediaTypeHelper::instance() {
        if (s_instance==0) {
            s_instance = new MediaTypeHelper();
        }
        return s_instance;
    }

    MediaTypeHelper::MediaTypeHelper() {
        QFile jsonFile(":/mt-matchrules.json");
        if (jsonFile.open(QFile::ReadOnly)) {
            QJsonDocument json_rulesets = QJsonDocument::fromJson(jsonFile.readAll());
            foreach (QJsonValue val, json_rulesets.array()) {
                MediaTypeMatchRuleSet *mrs = new MediaTypeMatchRuleSet();
                mrs->fromJson(val.toObject());
                m_rulesets[mrs->mediaTypeName()] = mrs;
            }
        }
    }

    const MediaTypeMatchRuleSet *MediaTypeHelper::detectMediaType(const MediaTypeSet &mts) {
        QList<const MediaTypeMatchRuleSet *> matches;
        foreach (MediaTypeMatchRuleSet *mrs, instance()->m_rulesets) {
            bool match = mts.checkMatchRules(mrs->detectionRules());
            if (match) {
                matches.append(mrs);
            }
        }
        float score = 0.0;
        const MediaTypeMatchRuleSet *selected_msr = 0;
        foreach (const MediaTypeMatchRuleSet *mrs, matches) {
            if (mrs->score()>score) {
                score = mrs->score();
                selected_msr = mrs;
            }
        }
        return selected_msr;
    }

    const MediaTypeMatchRuleSet *MediaTypeHelper::detectMediaType(const QStringList &formats) {
        ClipsterCore::MediaTypeSet mts(formats);
        return detectMediaType(mts);
    }

    /**
     * @brief Copy mime data "intelligently"
     * This Function Analyzes the source mime data using media-type match rules. If a ruleset matches
     * it will be used to perform a smart reduced copy of the data.
     * @param source The mime data to be analyzed and smart-copied
     * @param dest   Empty mime data object to recieve the copied data
     * @return Return match rule set if one could be detected.
     */
    const MediaTypeMatchRuleSet *MediaTypeHelper::smartCopyMimeData(const QMimeData *source, QMimeData *dest) {
        const MediaTypeMatchRuleSet *mtrs = detectMediaType(source->formats());
        if (mtrs) {
            if (mtrs->isPureImage()) {
                QImage image = qvariant_cast<QImage>(source->imageData());
                if (!image.isNull()) {

                    dest->setImageData(image);
                    QBuffer buf;
                    if (image.hasAlphaChannel()) {
                        image.save(&buf,"png");
                        dest->setData("image/png",buf.data());
                    }
                    else {
                        image.save(&buf,"jpg");
                        dest->setData("image/jpeg",buf.data());
                    }
                }
                else {
                    // Could not extract a image, probably because this is from
                    // cache mimedata. Just copy all formats
                    foreach(QString format, source->formats()) {
                        if (format.startsWith("image/")) {
                            QImage img = QImage::fromData(source->data(format),format.mid(6).toLatin1().data());
                            dest->setImageData(img);
                        }
                        else {
                            dest->setData(format, source->data(format));
                        }
                    }
                }
            }
            else {
                QList<const MediaTypeMatchRule *> used_rules;
                foreach(QString format, source->formats()) {
                    foreach (const MediaTypeMatchRule *mtmr, mtrs->copyRules()) {
                        if (used_rules.contains(mtmr)) {
                            continue;
                        }
                        if (mtmr->matchRuleRegExp().indexIn(format) == 0) {
                            // Adding MIME type
                            MediaType mt(format);
                            dest->setData(mt.fulltype(), source->data(format));
                            // Mark this matchrules as used
                            used_rules.append(mtmr);
                            break;
                        }
                    }
                }
            }
        }
        return mtrs;
    }

}
