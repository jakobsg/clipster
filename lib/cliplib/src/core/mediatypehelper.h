#ifndef MEDIATYPEHELPER_H
#define MEDIATYPEHELPER_H

#include "../cliplib_global.h"
#include "mediatypematchruleset.h"
#include "mediatypeset.h"

#include <QMap>

class QMimeData;

namespace ClipsterCore {

    class CLIPLIBSHARED_EXPORT MediaTypeHelper {

    public:
        static MediaTypeHelper *instance();
        static const MediaTypeMatchRuleSet *detectMediaType(const MediaTypeSet &mts);
        static const MediaTypeMatchRuleSet *detectMediaType(const QStringList &formats);
        static const MediaTypeMatchRuleSet *smartCopyMimeData(const QMimeData *source, QMimeData *dest);

    private:
        MediaTypeHelper();

        QMap<QString, MediaTypeMatchRuleSet *> m_rulesets;
        static MediaTypeHelper *s_instance ;

    };

}

#endif // MEDIATYPEHELPER_H
