#include "mediatypematcheddata.h"
#include "mediatypehelper.h"
#include "mediatypematchruleset.h"
#include "pathentrymanager.h"
#include "pathtools.h"

#include <QMimeData>
#include <QCryptographicHash>
#include <QApplication>
#include <QFileInfo>
#include <QUrl>
#include <QImage>
#include <QDir>
#include <QBuffer>

namespace ClipsterCore {

    MediaTypeMatchedData::MediaTypeMatchedData(const QMimeData *mimedata, QObject *parent, const QString &forced_data_hash) : QObject(parent) {
        m_mimedata = new QMimeData;
        m_mtrs = MediaTypeHelper::detectMediaType(mimedata->formats());
        if (m_mtrs) {
            MediaTypeHelper::smartCopyMimeData(mimedata, m_mimedata);
        }
        if (!forced_data_hash.isEmpty()) {
            m_data_hash = forced_data_hash;
        }
    }

    MediaTypeMatchedData::~MediaTypeMatchedData() {
        delete m_mimedata;
    }

    const MediaTypeMatchRuleSet *MediaTypeMatchedData::mediaTypeMatchRuleSet() const {
        return m_mtrs;
    }

    const QMimeData *MediaTypeMatchedData::mimeData() const {
        return m_mimedata;
    }

    QMimeData *MediaTypeMatchedData::mimeData() {
        return m_mimedata;
    }

    bool MediaTypeMatchedData::isMatched() const {
        return m_mtrs!=0;
    }

    PathEntryManager *MediaTypeMatchedData::toPathEntryManager(QObject *parent) {
        if (m_mimedata->hasUrls()) {
            ClipsterCore::PathEntryManager *fl = new ClipsterCore::PathEntryManager(parent);
            foreach (QUrl url, m_mimedata->urls()) {
                if (url.isLocalFile()) {
                    fl->addPathEntry(url.path());
                }
            }
            return fl;
        }
        return 0;
    }

    QString MediaTypeMatchedData::calculateDataHash() const {
        // I know this is ugly but data_hash should only be calculated once and is not really a change in terms of data
        // Therefore I allow myself to do this ugly hack whilst letting the method being const.
        MediaTypeMatchedData *only_for_data_hash_buffering = reinterpret_cast<MediaTypeMatchedData *>((void *)this);
        if (!m_data_hash.isEmpty()) {
            return m_data_hash;
        }
        if (m_mtrs->isFileList()) {
            // Fill a path entry manager
            ClipsterCore::PathEntryManager fl;
            foreach (QUrl url, m_mimedata->urls()) {
                if (url.isLocalFile()) {
                    fl.addPathEntry(url.path());
                }
            }
            only_for_data_hash_buffering->m_data_hash = fl.dataHash();
            return m_data_hash;
        }
        else {
            QCryptographicHash h(QCryptographicHash::Sha1);
            h.reset();
            if (m_mtrs->isPureImage()) {
                QImage image = qvariant_cast<QImage>(m_mimedata->imageData());
                if (!image.isNull()) {
                    QBuffer buf;
                    if (image.hasAlphaChannel()) {
                        image.save(&buf,"png");
                        h.addData(buf.data());
                    }
                    else {
                        image.save(&buf,"jpg");
                        h.addData(buf.data());
                    }
                }
            }
            else {
                QStringList formats = m_mimedata->formats();
                formats.sort();
                foreach(QString format, formats) {
                    foreach (const ClipsterCore::MediaTypeMatchRule *mtmr, m_mtrs->copyRules()) {
                        if (mtmr->matchRuleRegExp().indexIn(format) == 0) {
                            // Retrieving data
                            h.addData(m_mimedata->data(format));
                            break;
                        }
                    }
                }
            }
            only_for_data_hash_buffering->m_data_hash = h.result().toHex().data();
            return m_data_hash;
        }
    }

    QString MediaTypeMatchedData::saveToCache() {
        QString data_hash = calculateDataHash();

        if (!PathTools::dataDirExists(data_hash)) {
            // Mime data hash for mime-data does not exist on local disk cache
            // Start serializing it

            QDir data_dir(ClipsterCore::PathTools::dataDir(data_hash));
            // Add the mimedata parts
            foreach (QString format, mimeData()->formats()) {
                MediaType mt(format);
                QStringList path_parts;
                path_parts << "mimedata" << mt.toplevelType();
                data_dir.mkpath(path_parts.join(QDir::separator()));
                path_parts << mt.subType();

                // Write data file
                QFile f(data_dir.filePath(path_parts.join(QDir::separator())));
                if (f.open(QFile::WriteOnly)) {
                    f.write(mimeData()->data(format));
                }
            }
        }
        return data_hash;
    }

    MediaTypeMatchedData *MediaTypeMatchedData::createFromCache(const QString &data_hash, QObject *parent) {
        QDir data_dir(PathTools::dataDir(data_hash));
        QMimeData mimedata;
        QString file_data_path = data_dir.filePath("files");
        QString mime_data_path = data_dir.filePath("mimedata");
        if (QFileInfo::exists(file_data_path)) {
            // Setup file copy
            QDir files_dir(file_data_path);
            QList<QUrl> urls;
            foreach (QFileInfo fi, files_dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files)) {
                urls << QUrl::fromLocalFile(fi.absoluteFilePath());
            }
            mimedata.setUrls(urls);
        }
        else if (QFileInfo::exists(mime_data_path)) {
            // Setup Mimedata copy
            QDir mimedata_dir(mime_data_path);
            foreach (QFileInfo top_fi, mimedata_dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files)) {
                if (top_fi.isDir()) {
                    QDir top_dir(top_fi.filePath());
                    foreach (QFileInfo sub_fi, top_dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files)) {
                        if (sub_fi.isFile()) {
                            QFile data(sub_fi.filePath());
                            if (data.open(QFile::ReadOnly)) {
                                QString format = QString("%1/%2").arg(top_fi.fileName()).arg(sub_fi.fileName());
                                mimedata.setData(format,data.readAll());
                            }
                        }
                    }
                }
            }
        }
        MediaTypeMatchedData *mtmd = new MediaTypeMatchedData(&mimedata, parent, data_hash);
        if (mtmd->mediaTypeMatchRuleSet()==0) {
            delete mtmd;
            mtmd = 0;
        }
        return mtmd;
    }


}

