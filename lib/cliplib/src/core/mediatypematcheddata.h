#ifndef MEDIATYPEMATCHEDDATA_H
#define MEDIATYPEMATCHEDDATA_H

#include "../cliplib_global.h"

#include <QObject>

class QMimeData;

namespace ClipsterCore {

    class MediaTypeMatchRuleSet;
    class PathEntryManager;

    class CLIPLIBSHARED_EXPORT MediaTypeMatchedData : public QObject {

        Q_OBJECT

    public:
        MediaTypeMatchedData(const QMimeData *mimedata, QObject *parent = 0, const QString &forced_data_hash=QString());
        ~MediaTypeMatchedData();

        const MediaTypeMatchRuleSet *mediaTypeMatchRuleSet() const;
        const QMimeData *mimeData() const;
        QMimeData *mimeData();
        QString calculateDataHash() const;
        QString saveToCache();
        bool isMatched() const;
        PathEntryManager *toPathEntryManager(QObject *parent=0);

        static MediaTypeMatchedData *createFromCache(const QString &data_hash, QObject *parent = 0);

    signals:

    public slots:
    private:
        QMimeData *m_mimedata;
        QString m_data_hash;
        const MediaTypeMatchRuleSet *m_mtrs;
    };

}

#endif // MEDIATYPEMATCHEDDATA_H
