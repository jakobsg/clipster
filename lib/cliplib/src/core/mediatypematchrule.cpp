#include "mediatypematchrule.h"

namespace ClipsterCore {

    MediaTypeMatchRule::MediaTypeMatchRule(const QString &toptype_pat, const QString &subtype_pat) {
        m_match_expression.setPatternSyntax(QRegExp::Wildcard);
        setMediaType(toptype_pat, subtype_pat);
    }

    MediaTypeMatchRule::MediaTypeMatchRule(const QString &toptype_pat, const QString &subtype_pat, const QString &pname_pat, const QString &pval_pat) {
        m_toptype_pattern = toptype_pat;
        m_subtype_pattern = subtype_pat;
        m_match_expression.setPatternSyntax(QRegExp::Wildcard);
        setParameterMatchRule(pname_pat, pval_pat);
    }

    MediaTypeMatchRule::MediaTypeMatchRule(const MediaTypeMatchRule &mr) {
        m_toptype_pattern = mr.m_toptype_pattern;
        m_subtype_pattern = mr.m_subtype_pattern;
        m_pname_pattern = mr.m_pname_pattern;
        m_pval_pattern = mr.m_pval_pattern;
        m_match_expression.setPatternSyntax(QRegExp::Wildcard);
        setParameterMatchRule(m_pname_pattern, m_pval_pattern);
    }

    bool MediaTypeMatchRule::operator==(const MediaTypeMatchRule &rhs) {
        return (
            m_toptype_pattern==rhs.m_toptype_pattern &&
            m_subtype_pattern==rhs.m_subtype_pattern &&
            m_pname_pattern==rhs.m_pname_pattern &&
            m_pval_pattern==rhs.m_pval_pattern);
    }

    void MediaTypeMatchRule::updateRegExp() {
        if (!m_pname_pattern.isEmpty()) {
            m_match_expression.setPattern(QString("%1%2%3;*%4=%5").arg(
                m_toptype_pattern,
                m_subtype_pattern.isEmpty() ? "" : "/",
                m_subtype_pattern,
                m_pname_pattern,
                m_pval_pattern));
        }
        else {
                m_match_expression.setPattern(QString("%1%2%3").arg(
                    m_toptype_pattern,
                    m_subtype_pattern.isEmpty() ? "" : "/",
                    m_subtype_pattern));
        }
    }

    void MediaTypeMatchRule::setMediaType(const QString &toptype_pat, const QString &subtype_pat) {
        m_toptype_pattern = toptype_pat;
        m_subtype_pattern = subtype_pat;
        if (!m_pname_pattern.isEmpty()) {
            setParameterMatchRule(m_pname_pattern, m_pval_pattern);
        }
        else {
            updateRegExp();
        }
    }

    void MediaTypeMatchRule::setParameterMatchRule(const QString &pname_pat, const QString &pval_pat) {
        m_pname_pattern = pname_pat;
        m_pval_pattern = pval_pat;
        updateRegExp();
    }

    const QRegExp &MediaTypeMatchRule::matchRuleRegExp() const {
        return m_match_expression;
    }

    const QString &MediaTypeMatchRule::topType() const {
        return m_toptype_pattern;
    }

    const QString &MediaTypeMatchRule::subType() const {
        return m_subtype_pattern;
    }

    const QString &MediaTypeMatchRule::parameterName() const {
        return m_pname_pattern;
    }

    const QString &MediaTypeMatchRule::parameterValue() const {
        return m_pval_pattern;
    }

    void MediaTypeMatchRule::setTopType(const QString &toptype) {
        setMediaType(toptype, m_subtype_pattern);
    }

    void MediaTypeMatchRule::setSubType(const QString &subtype) {
        setMediaType(m_toptype_pattern, subtype);
    }

    void MediaTypeMatchRule::setParameterName(const QString &pname) {
        setParameterMatchRule(pname, m_pval_pattern);
    }

    void MediaTypeMatchRule::setParameterValue(const QString &pvalue) {
        setParameterMatchRule(m_pname_pattern, pvalue);
    }
}
