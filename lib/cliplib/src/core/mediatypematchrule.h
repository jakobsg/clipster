#ifndef MEDIATYPEMATCHRULE_H
#define MEDIATYPEMATCHRULE_H

#include "../cliplib_global.h"

#include <QString>
#include <QRegExp>

namespace ClipsterCore {

    class CLIPLIBSHARED_EXPORT MediaTypeMatchRule
    {
    public:
        MediaTypeMatchRule(
            const QString &toptype_pat,
            const QString &subtype_pat);
        MediaTypeMatchRule(
            const QString &toptype_pat,
            const QString &subtype_pat,
            const QString &pname_pat,
            const QString &pval_pat="*");
        MediaTypeMatchRule(const MediaTypeMatchRule &mr);
        bool operator==(const MediaTypeMatchRule &rhs);
        void setMediaType(const QString &toptype, const QString &subtype);
        void setParameterMatchRule(const QString &pname_pat, const QString &pval_pat="*");
        const QRegExp &matchRuleRegExp() const;
        const QString &topType() const;
        const QString &subType() const;
        const QString &parameterName() const;
        const QString &parameterValue() const;
        void setTopType(const QString &toptype);
        void setSubType(const QString &subtype);
        void setParameterName(const QString &pname);
        void setParameterValue(const QString &pvalue);

    private:
        void updateRegExp();
        QString m_toptype_pattern;
        QString m_subtype_pattern;
        QString m_pname_pattern;
        QString m_pval_pattern;
        QRegExp m_match_expression;
        bool m_expr_uptodate;
    };

}

#endif // MEDIATYPEMATCHRULE_H
