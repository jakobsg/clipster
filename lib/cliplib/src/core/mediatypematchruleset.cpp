#include "mediatypematchruleset.h"
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonValue>
#include <QStringList>
#include <math.h>

namespace ClipsterCore {

    MediaTypeMatchRuleSet::MediaTypeMatchRuleSet() {
        m_is_pure_image = false;
        m_is_file_list = false;
    }

    MediaTypeMatchRuleSet::~MediaTypeMatchRuleSet() {
        foreach (MediaTypeMatchRule *mr, m_owned_by_this) {
            delete mr;
        }
    }

    QJsonObject MediaTypeMatchRuleSet::serialize() const {
        QJsonObject ruleset;
        ruleset["mediatype_name"] = m_mediatype_name;
        ruleset["icon_resource_url"] = m_icon_resource_url;
        ruleset["is_pure_image"] = m_is_pure_image;
        ruleset["is_file_list"] = m_is_file_list;
        QJsonArray det_array;
        foreach (const MediaTypeMatchRule *mr, m_detection_rules) {
            QJsonObject mrule;
            mrule["toptype"] = mr->topType();
            mrule["subtype"] = mr->subType();
            mrule["pname"] = mr->parameterName();
            mrule["pval"] = mr->parameterValue();
            det_array.append(mrule);
        }
        ruleset["detection_rules"] = det_array;
        QJsonArray copy_array;
        foreach (const MediaTypeMatchRule *mr, m_copy_rules) {
            QJsonObject mrule;
            mrule["toptype"] = mr->topType();
            mrule["subtype"] = mr->subType();
            mrule["pname"] = mr->parameterName();
            mrule["pval"] = mr->parameterValue();
            copy_array.append(mrule);
        }
        ruleset["copy_rules"] = copy_array;
        return ruleset;
    }

    void MediaTypeMatchRuleSet::addDetectionRule(const MediaTypeMatchRule *rule) {
        m_detection_rules.append(rule);
    }

    void MediaTypeMatchRuleSet::addCopyRule(const MediaTypeMatchRule *rule) {
        m_copy_rules.append(rule);
    }

    void MediaTypeMatchRuleSet::setDetectionRules(QList<const MediaTypeMatchRule *> &rules) {
        m_detection_rules = rules;
    }

    void MediaTypeMatchRuleSet::setCopyRules(QList<const MediaTypeMatchRule *> &rules) {
        m_copy_rules = rules;
    }

    void MediaTypeMatchRuleSet::setMediaTypeName(const QString mt_name) {
        m_mediatype_name = mt_name;
    }

    void MediaTypeMatchRuleSet::setIconResourceUrl(const QString img_url) {
        m_icon_resource_url = img_url;
    }

    void MediaTypeMatchRuleSet::setIsPureImage(bool is_image) {
        m_is_pure_image = is_image;
    }

    void MediaTypeMatchRuleSet::setIsFileList(bool is_filelist) {
        m_is_file_list = is_filelist;
    }

    const QList<const MediaTypeMatchRule *> &MediaTypeMatchRuleSet::detectionRules() const {
        return m_detection_rules;
    }

    const QList<const MediaTypeMatchRule *> &MediaTypeMatchRuleSet::copyRules() const {
        return m_copy_rules;
    }

    const QString &MediaTypeMatchRuleSet::mediaTypeName() const {
        return m_mediatype_name;
    }

    const QString &MediaTypeMatchRuleSet::iconResourceUrl() const {
        return m_icon_resource_url;
    }

    float MediaTypeMatchRuleSet::score() const {
        float score = 0;
        foreach (const MediaTypeMatchRule *mr, m_detection_rules) {
            QStringList vals;
            int idx = 0;
            vals << mr->topType() << mr->subType() << mr->parameterName() << mr->parameterValue();
            foreach (QString val, vals) {
                bool has_wildcard = val.contains("*");
                if (val.isEmpty() || val=="*") {
                    continue;
                }
                int mlen = val.length();
                if (mlen>100) {
                    mlen=100;
                }
                score += (idx*100.0) + (50.0+pow(mlen,1.306)) * (has_wildcard ? 0.66 : 1.0);
                idx++;
            }
        }
        return score;
    }

    bool MediaTypeMatchRuleSet::isPureImage() const {
        return m_is_pure_image;
    }

    bool MediaTypeMatchRuleSet::isFileList() const {
        return m_is_file_list;
    }

    void MediaTypeMatchRuleSet::fromJson(const QJsonObject &json) {
        m_mediatype_name = json["mediatype_name"].toString();
        m_icon_resource_url = json["icon_resource_url"].toString();
        m_is_pure_image = json.keys().contains("is_pure_image") && json["is_pure_image"].toBool();
        m_is_file_list = json.keys().contains("is_file_list") && json["is_file_list"].toBool();
        foreach (QJsonValue val, json["detection_rules"].toArray()) {
            QJsonObject jmr = val.toObject();
            MediaTypeMatchRule *mr = new MediaTypeMatchRule(
                jmr["toptype"].toString(),
                jmr["subtype"].toString(),
                jmr["pname"].toString(),
                jmr["pval"].toString());
            m_detection_rules.append(mr);
            m_owned_by_this.append(mr);
        }
        foreach (QJsonValue val, json["copy_rules"].toArray()) {
            QJsonObject jmr = val.toObject();
            MediaTypeMatchRule *mr = new MediaTypeMatchRule(
                jmr["toptype"].toString(),
                jmr["subtype"].toString(),
                jmr["pname"].toString(),
                jmr["pval"].toString());
            m_copy_rules.append(mr);
            m_owned_by_this.append(mr);
        }
    }

}
