#ifndef MEDIATYPEMATCHRULESET_H
#define MEDIATYPEMATCHRULESET_H

#include "../cliplib_global.h"
#include "mediatypematchrule.h"

#include <QList>
#include <QJsonObject>

class QMimeData;

namespace ClipsterCore {

    class CLIPLIBSHARED_EXPORT MediaTypeMatchRuleSet
    {
    public:
        MediaTypeMatchRuleSet();
        ~MediaTypeMatchRuleSet();
        QJsonObject serialize() const;

        void addDetectionRule(const MediaTypeMatchRule *rule);
        void addCopyRule(const MediaTypeMatchRule *rule);
        void setDetectionRules(QList<const MediaTypeMatchRule *> &rules);
        void setCopyRules(QList<const MediaTypeMatchRule *> &rules);
        void setMediaTypeName(const QString mt_name);
        void setIconResourceUrl(const QString img_url);
        void setIsPureImage(bool is_image=true);
        void setIsFileList(bool is_filelist=true);

        const QList<const MediaTypeMatchRule *> &detectionRules() const;
        const QList<const MediaTypeMatchRule *> &copyRules() const;
        const QString &mediaTypeName() const;
        const QString &iconResourceUrl() const;

        float score() const;
        bool isPureImage() const;
        bool isFileList() const;

        void fromJson(const QJsonObject &json);

    private:
        QString m_mediatype_name;
        QString m_icon_resource_url;
        QList<const MediaTypeMatchRule *> m_detection_rules;
        QList<const MediaTypeMatchRule *> m_copy_rules;
        QList<MediaTypeMatchRule *> m_owned_by_this;
        bool m_is_pure_image;
        bool m_is_file_list;
    };

}

#endif // MEDIATYPEMATCHRULESET_H
