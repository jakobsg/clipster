#include "mediatypeset.h"
#include <QStringList>

namespace ClipsterCore {

    MediaTypeSet::MediaTypeSet(const QStringList &formats) {
        foreach (QString fmt, formats) {
            QString fulltype = fmt.split(";")[0].toLower();
            if (m_media_types.contains(fulltype)) {
                // Update existing mediatype
                m_media_types[fulltype]->updateParameters(fmt);
            }
            else {
                m_media_types[fulltype] = new MediaType(fmt);
            }
        }
    }

    const MediaTypeMap &MediaTypeSet::mediaTypes() const {
        return m_media_types;
    }

    bool MediaTypeSet::checkMatchRules(const QList<const MediaTypeMatchRule *> &match_rules) const {
        QList<const MediaTypeMatchRule *> mrs = match_rules;
        foreach (MediaType *mt, m_media_types) {
            QString mediatype = mt->mediaType();
            foreach (const MediaTypeMatchRule *mtmr, match_rules) {
    //            qDebug("%s -> %s", mediatype.toUtf8().data(), mtmr->matchRuleRegExp().pattern().toUtf8().data());
                if (mtmr->matchRuleRegExp().indexIn(mediatype) == 0) {
                    mrs.removeOne(mtmr);
                }
            }
            if (mrs.count()==0) {
                break;
            }
        }
        return mrs.count()==0;
    }

    MediaTypeSet::~MediaTypeSet() {
        foreach (MediaType *mt, m_media_types.values()) {
            delete mt;
        }
    }

}
