#ifndef MEDIADATASET_H
#define MEDIADATASET_H

#include "../cliplib_global.h"
#include "mediatypematchrule.h"
#include "mediatype.h"

namespace ClipsterCore {

    class CLIPLIBSHARED_EXPORT MediaTypeSet {
    public:
        MediaTypeSet(const QStringList &formats);
        ~MediaTypeSet();
        const MediaTypeMap &mediaTypes() const;
        bool checkMatchRules(const QList<const MediaTypeMatchRule *> &match_rules) const;

    private:
        MediaTypeMap m_media_types;
    };

}
#endif
