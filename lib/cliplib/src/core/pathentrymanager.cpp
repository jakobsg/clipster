#include "pathentrymanager.h"
#include "pathtools.h"
#include "platformtools.h"

#include <QDir>
#include <QTemporaryDir>
#include <QDateTime>
#include <QStringList>
#include <QCryptographicHash>

namespace ClipsterCore {

    PathEntryManager::PathEntryManager(QObject *parent) : QObject(parent) {
    }

    PathEntryManager::~PathEntryManager() {
        foreach (FileList fl, m_path_entry_files.values()) {
            foreach (QFile *f, fl) {
                delete f;
            }
        }
        m_path_entry_files.clear();
    }

    void PathEntryManager::addPathEntry(const QString &fpath) {
        QFileInfo info(fpath);
        static QFileInfo empty_finfo;
        if (m_path_entries.contains(fpath)) {
            // Reject path entry that has already been added.
            return;
        }
        // Add new path entry
        m_path_entries.append(fpath);

        if (info.isFile()) {
            // Add file entry
            addPathEntry_meta(info,empty_finfo);
        }
        else if (info.isDir()) {
            // Add directory entry
            addPathEntry_meta(info,info);
        }
    }

    QStringList PathEntryManager::pathEntries() const {
        return m_path_entry_files.keys();
    }

    void PathEntryManager::addPathEntry_meta(const QFileInfo &info, const QFileInfo &base) {
        if (info.fileName()==".." || info.fileName()==".") {
            // Always ignore placeholder directories
            return;
        }
        if (info.isFile()) {
            QFile *f = new QFile(info.filePath(),this);
            m_path_entry_files[base.filePath()].append(f);
            // connect(f, SIGNAL(destroyed(QObject*)), this, SLOT(deleting(QObject*)));
        }
        else if (info.isDir()) {
            QDir d(info.filePath());
            foreach (QFileInfo subinfo, d.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files)) {
                addPathEntry_meta(subinfo, base);
            }
        }
    }

    FileList PathEntryManager::files(const QString &path_entry) const {
        return m_path_entry_files[path_entry];
    }

    qint64 PathEntryManager::size() const {
        qint64 size = 0;
        foreach (FileList fl, m_path_entry_files.values()) {
            foreach (QFile *f, fl) {
                size += f->size();
            }
        }
        return size;
    }

    QString PathEntryManager::dataHash() const {
        QStringList entrylist;
        foreach (QString base, m_path_entry_files.keys()) {
            QDir base_dir(base);
            base_dir.cd("..");
            foreach (QFile *f, m_path_entry_files.value(base)) {
                QFileInfo fi(*f);
                QString entry_path = fi.fileName();
                if (!base.isEmpty()) {
                    entry_path = base_dir.relativeFilePath(fi.filePath());
                }
                entry_path = entry_path.replace("\\","/");
                entrylist << (QString("%1;%2").arg(entry_path).arg(fi.size()));
            }
        }
        entrylist.sort();

        QCryptographicHash hash(QCryptographicHash::Sha1);
        hash.reset();
        hash.addData(entrylist.join(":").toUtf8());
        return hash.result().toHex().data();
    }

    PathEntryManager::FreezeResult PathEntryManager::freeze(const QString &data_hash, bool hardlink, qint64 maxbytes) const {
        QTemporaryDir temp_copy;
        //QStringList entrylist;
        qint64 totalbytes = 0;
        QDir temp_dir(temp_copy.path());
        foreach (QString base, m_path_entry_files.keys()) {
            QDir base_dir(base);
            base_dir.cd("..");
            foreach (QFile *f, m_path_entry_files.value(base)) {
                QFileInfo fi(*f);
                QString entry_path = fi.fileName();
                if (!base.isEmpty()) {
                    entry_path = base_dir.relativeFilePath(fi.filePath());
                    QFileInfo ff(entry_path);
                    if (!ff.dir().exists()) {
                        temp_dir.mkpath(ff.dir().path());
                    }
                }
                if (maxbytes>-1 && totalbytes + f->size() > maxbytes) {
                    return SizeLimitExceeded;
                }
                if (hardlink) {
                    if (!createHardLink(fi.absoluteFilePath(),temp_dir.filePath(entry_path))) {
                        return HardLinkCreationFailed;
                    }
                }
                else {
                    if (!f->copy(temp_dir.filePath(entry_path))) {
                        return CopyFileFailed;
                    }
                }
                QFileInfo fdest(temp_dir.filePath(entry_path));
                totalbytes += fdest.size();
                //entry_path = entry_path.replace("\\","/");
                //entrylist << (QString("%1;%2").arg(entry_path).arg(fdest.size()));
            }
        }
        //entrylist.sort();

        //QCryptographicHash hash(QCryptographicHash::Sha1);
        //hash.reset();
        //hash.addData(entrylist.join(":").toUtf8());
        //QString data_hash = hash.result().toHex().data();

        QDir data_dir(PathTools::dataDir(data_hash));
        if (!data_dir.exists("files")) {
            if (!data_dir.rename(temp_copy.path(), "files")) {
                return MoveFreezeDataFailed;
            }
            temp_copy.setAutoRemove(false);
        }
        return Success;
    }

    void PathEntryManager::deleting(QObject *obj) {
        qDebug("Deleting... %p", obj);
    }

}
