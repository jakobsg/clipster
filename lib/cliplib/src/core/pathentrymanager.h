#ifndef FILELIST_H
#define FILELIST_H

#include "../cliplib_global.h"

#include <QObject>
#include <QFile>
#include <QFileInfo>
#include <QMap>

typedef QList<QFile *> FileList;

namespace ClipsterCore {

    class CLIPLIBSHARED_EXPORT PathEntryManager : public QObject {
        Q_OBJECT

    public:

        enum FreezeResult {
            Success,
            FrozenAlready,
            InvalidDataHash,
            SizeLimitExceeded,
            HardLinkCreationFailed,
            CopyFileFailed,
            MoveFreezeDataFailed
        };

        explicit PathEntryManager(QObject *parent = 0);
        ~PathEntryManager();
        void addPathEntry(const QString &fpath);

        QStringList pathEntries() const;
        FileList files(const QString &path_entry) const;
        qint64 size() const;
        QString dataHash() const;
        FreezeResult freeze(const QString &data_hash, bool hardlink=false, qint64 maxbytes=-1) const;

    signals:

    public slots:
        void deleting(QObject *obj);

    private:
        void addPathEntry_meta(const QFileInfo &info, const QFileInfo &base);

        typedef QMap<QString, FileList> PathEntryMap;
        PathEntryMap m_path_entry_files;
        QStringList m_path_entries;
    };

}

#endif // FILELIST_H
