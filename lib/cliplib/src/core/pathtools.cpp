#include "pathtools.h"
#include "mediatypematcheddata.h"
#include "clipdetails.h"

#include <crashlib.h>

#include <QDir>
#include <QUrl>
#include <QStandardPaths>
#include <QtConcurrentRun>

DataHashFeezeResult freezeDataHash(QString data_hash, qint64 maxbytes) {
    if (!ClipsterCore::PathTools::detailsJsonFileExists(data_hash)) {
        return DataHashFeezeResult(data_hash,ClipsterCore::PathEntryManager::InvalidDataHash);
    }
    ClipsterCore::PathEntryManager::FreezeResult freeze_result = ClipsterCore::PathEntryManager::Success;
    ClipsterCore::ClipDetails cd(data_hash);
    if (!cd.isFrozen()) {
        bool frozen = true;
        ClipsterCore::MediaTypeMatchedData *data = ClipsterCore::MediaTypeMatchedData::createFromCache(data_hash);
        if (data) {
            ClipsterCore::PathEntryManager *pem = data->toPathEntryManager(data);
            if (pem) {
                ClipsterCore::PathEntryManager::FreezeResult res = pem->freeze(data_hash, false, maxbytes);
                freeze_result = res;
                if (res != ClipsterCore::PathEntryManager::Success) {
                    frozen = false;
                }
                else {
                    ClipsterCore::PathTools::removeDir(ClipsterCore::PathTools::mimeDataDir(data_hash,false));
                }
                delete data;
            }
        }
        cd.setIsFrozen(frozen);
        cd.save();
    }
    else {
        freeze_result = ClipsterCore::PathEntryManager::FrozenAlready;
    }
    return DataHashFeezeResult(data_hash,freeze_result);
}

namespace ClipsterCore {

    PathTools *PathTools::s_instance = 0;

    PathTools *PathTools::instance() {
        if (s_instance==0) {
            s_instance = new PathTools;
        }
        return s_instance;
    }

    PathTools::PathTools() {
        CALL_LOGGER();
        // User data location
        m_base_dir = QStandardPaths::writableLocation(QStandardPaths::DataLocation);
        QDir adl(m_base_dir);
        // Create if not exists
        if (!QFileInfo::exists(m_base_dir)) {
            adl.mkdir(m_base_dir);
        }
        // Clipster data dir
        m_data_dir = adl.filePath("data");
        // Create if not exists
        if (!QFileInfo::exists(m_data_dir)) {
            adl.mkdir(m_data_dir);
        }
    }

    QString PathTools::dataBaseDir() {
        return instance()->m_data_dir;
    }

    QString PathTools::dataDir(const QString &data_hash, bool create) {
        QDir datadir(instance()->m_data_dir);
        QString data_hash_dir = datadir.filePath(data_hash);
        if (!QFileInfo::exists(data_hash_dir) && create) {
            // Create if not exists
            datadir.mkdir(data_hash_dir);
        }
        return data_hash_dir;
    }

    bool PathTools::dataDirExists(const QString &data_hash) {
        return QFileInfo::exists(PathTools::dataDir(data_hash, false));
    }

    QString PathTools::filesDir(const QString &data_hash, bool create) {
        QDir datadir(PathTools::dataDir(data_hash, create));
        QString filesdir = datadir.filePath("files");
        if (!QFileInfo::exists(filesdir) && create) {
            // Create if not exists
            datadir.mkdir("files");
        }
        return filesdir;
    }

    QString PathTools::mimeDataDir(const QString &data_hash, bool create) {
        QDir datadir(PathTools::dataDir(data_hash, create));
        QString mimedata = datadir.filePath("mimedata");
        if (!QFileInfo::exists(mimedata ) && create) {
            // Create if not exists
            datadir.mkdir("mimedata");
        }
        return mimedata;
    }

    QString PathTools::detailsJsonFile(const QString &data_hash){
        QDir datadir(PathTools::dataDir(data_hash, false));
        return datadir.filePath("details.json");
    }

    QString PathTools::previewPngFile(const QString &data_hash) {
        QDir datadir(PathTools::dataDir(data_hash, false));
        return datadir.filePath("preview.png");
    }

    bool PathTools::filesDirExists(const QString &data_hash) {
        return QFileInfo::exists(PathTools::filesDir(data_hash, false));
    }

    bool PathTools::mimeDataDirExists(const QString &data_hash) {
        return QFileInfo::exists(PathTools::mimeDataDir(data_hash, false));
    }

    bool PathTools::detailsJsonFileExists(const QString &data_hash) {
        return QFileInfo::exists(PathTools::detailsJsonFile(data_hash));
    }

    bool PathTools::previewPngFileExists(const QString &data_hash) {
        return QFileInfo::exists(PathTools::previewPngFile(data_hash));
    }

    QFuture<DataHashFeezeResult> PathTools::backgroundFreeze(QString data_hash, qint64 maxbytes) {
        return QtConcurrent::run(freezeDataHash,data_hash,maxbytes);
    }

    bool PathTools::removeDir(const QString &dirName) {
        bool result = true;
        QDir dir(dirName);

        if (dir.exists(dirName)) {
            foreach(QFileInfo info, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst)) {
                if (info.isDir()) {
                    result = removeDir(info.absoluteFilePath());
                }
                else {
                    result = QFile::remove(info.absoluteFilePath());
                }

                if (!result) {
                    return result;
                }
            }
            result = dir.rmdir(dirName);
        }

        return result;
    }

}
