#ifndef PATHTOOLS_H
#define PATHTOOLS_H

#include "../cliplib_global.h"
#include "pathentrymanager.h"

#include <QString>
#include <QPair>
#include <QFuture>

typedef QPair<QString, ClipsterCore::PathEntryManager::FreezeResult> DataHashFeezeResult;
DataHashFeezeResult freezeDataHash(QString data_hash, qint64 maxbytes=-1);

namespace ClipsterCore {

    class CLIPLIBSHARED_EXPORT PathTools
    {
    public:
        static PathTools *instance();
        static QString dataBaseDir();
        static QString dataDir(const QString &data_hash, bool create=true);
        static QString filesDir(const QString &data_hash, bool create=true);
        static QString mimeDataDir(const QString &data_hash, bool create=true);
        static QString detailsJsonFile(const QString &data_hash);
        static QString previewPngFile(const QString &data_hash);

        static bool dataDirExists(const QString &data_hash);
        static bool filesDirExists(const QString &data_hash);
        static bool mimeDataDirExists(const QString &data_hash);
        static bool detailsJsonFileExists(const QString &data_hash);
        static bool previewPngFileExists(const QString &data_hash);

        static bool removeDir(const QString &dirName);

        static QFuture< DataHashFeezeResult > backgroundFreeze(QString data_hash, qint64 maxbytes=-1);

    private:
        PathTools();
        static PathTools *s_instance;
        QString m_base_dir;
        QString m_data_dir;

    };

}
#endif // PATHTOOLS_H
