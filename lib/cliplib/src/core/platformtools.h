#ifndef PLATFORMTOOLS_H
#define PLATFORMTOOLS_H

#include "../cliplib_global.h"

#include <QString>

namespace ClipsterCore {

    bool CLIPLIBSHARED_EXPORT createHardLink(const QString &oldpath, const QString &newpath, QString *error=0);

}

#endif // PLATFORMTOOLS_H

