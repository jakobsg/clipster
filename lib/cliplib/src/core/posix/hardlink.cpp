#include "../platformtools.h"

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

namespace ClipsterCore {

    bool createHardLink(const QString &oldpath, const QString &newpath, QString *error) {
        link(oldpath.toUtf8().data(), newpath.toUtf8().data());
        if (error) {
            *error = QString::fromUtf8(strerror(errno));
            return false;
        }
        return true;
    }

}
