#include "singleapplication.h"
#include <QCoreApplication>
#include <string>
#include <cctype>
#include <crashlib.h>

#ifdef WINDOWS_OS
#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <psapi.h>

bool application_pid_exists(int pid) {
    CALL_LOGGER();
    HANDLE hProcess = OpenProcess( PROCESS_QUERY_INFORMATION |
                                   PROCESS_VM_READ,
                                   FALSE, pid );
    DWORD dwExitCode = 0;
    if(::GetExitCodeProcess(hProcess, &dwExitCode)) {
        if(dwExitCode == STILL_ACTIVE) {
            return true;
        }
    }
    return false;
}

#elif LINUX_OS
#include <sys/types.h>
#include <signal.h>

bool application_pid_exists(int pid) {
    CALL_LOGGER();
    int res = kill(pid,0);
    return res==0;
}
#elif DARWIN_OS
#include <sys/types.h>
#include <signal.h>

bool application_pid_exists(int pid) {
    int res = kill(pid,0);
    return res==0;
}
#endif

namespace ClipsterCore {

    SingleApplication::SingleApplication(bool unique_by_path) {
        CALL_LOGGER();
        m_already_running = false;
        if (unique_by_path) {
            m_singleapp_pid.setKey(QString("LIMBOSOFT_SINGLEAPP_%1").arg(QCoreApplication::applicationFilePath()));
        } else {
            m_singleapp_pid.setKey(QString("LIMBOSOFT_SINGLEAPP_%1").arg(QCoreApplication::applicationName()));
        }
        bool attach_ok = m_singleapp_pid.attach();
        if (attach_ok) {
            m_singleapp_pid.lock();
            char *read_from = (char*)m_singleapp_pid.data();
            std::string pid = read_from;
            if (pid.length()>0) {
                // Check if the content is a number
                std::string::const_iterator it = pid.begin();
                while (it != pid.end() && std::isdigit(*it)) ++it;
                if (!pid.empty() && it == pid.end()) {
                    if (application_pid_exists(atoi(pid.c_str()))) {
                        m_already_running = true;
                    }
                }
            }
            m_singleapp_pid.unlock();
        }
        bool create_ok = false;
        if (!attach_ok) {
            create_ok = m_singleapp_pid.create(4096);
        }
        if (!m_already_running && (attach_ok||create_ok)) {
            m_singleapp_pid.lock();
            std::string pid_str = QString::number(QCoreApplication::applicationPid()).toStdString();
            char *write_to = (char*)m_singleapp_pid.data();
            memcpy(write_to, pid_str.c_str(), pid_str.length());
            m_singleapp_pid.unlock();
        }
    }

    SingleApplication::~SingleApplication() {
        CALL_LOGGER();
        m_singleapp_pid.detach();
    }

    bool SingleApplication::alreadyRunning() {
        CALL_LOGGER();
        return m_already_running;
    }

}
