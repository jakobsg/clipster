#ifndef SINGLEAPPLICATION_H
#define SINGLEAPPLICATION_H

#include "../cliplib_global.h"
#include <QSharedMemory>

namespace ClipsterCore {

    class CLIPLIBSHARED_EXPORT SingleApplication {
    public:
        SingleApplication(bool unique_by_path=false);
        ~SingleApplication();
        bool alreadyRunning();
    private:
        QSharedMemory m_singleapp_pid;
        bool m_already_running;
    };

}

#endif // SINGLEAPPLICATION_H
