#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <intrin.h>
#include <iphlpapi.h>

#include <map>
#include <iostream>
#include <fstream>
#include <string>

#include <comdef.h>
#include <Wbemidl.h>
#include <crashlib.h>
# pragma comment(lib, "wbemuuid.lib")

#define USERNAME_ENV "USERNAME"

namespace ClipsterCore {

    std::string getPhysicalDiskSerialNumber(const char *indrive=0) {
        CALL_LOGGER();
        HRESULT hres;

        char drive[PATHLEN+1];

        if (!indrive) {
            std::string sysdrive = "c:";
            char *csysdrive;
            csysdrive = getenv("SystemDrive");
            if (csysdrive) {
                sysdrive = csysdrive;
            }
            strncpy(drive,sysdrive.c_str(),256);
        }
        else {
            strncpy(drive,indrive,256);
        }
        // Step 3: ---------------------------------------------------
        // Obtain the initial locator to WMI -------------------------

        IWbemLocator *pLoc = NULL;

        hres = CoCreateInstance(
            CLSID_WbemLocator,
            0,
            CLSCTX_INPROC_SERVER,
            IID_IWbemLocator, (LPVOID *) &pLoc);

        if (FAILED(hres))
        {
            return "";                 // Program has failed.
        }

        // Step 4: -----------------------------------------------------
        // Connect to WMI through the IWbemLocator::ConnectServer method

        IWbemServices *pSvc = NULL;

        // Connect to the root\cimv2 namespace with
        // the current user and obtain pointer pSvc
        // to make IWbemServices calls.
        hres = pLoc->ConnectServer(
                _bstr_t(L"ROOT\\CIMV2"), // Object path of WMI namespace
                NULL,                    // User name. NULL = current user
                NULL,                    // User password. NULL = current
                0,                       // Locale. NULL indicates current
                NULL,                    // Security flags.
                0,                       // Authority (e.g. Kerberos)
                0,                       // Context object
                &pSvc                    // pointer to IWbemServices proxy
                );

        if (FAILED(hres))
        {
            pLoc->Release();
            return "";                // Program has failed.
        }

        // Step 5: --------------------------------------------------
        // Set security levels on the proxy -------------------------

        hres = CoSetProxyBlanket(
            pSvc,                        // Indicates the proxy to set
            RPC_C_AUTHN_WINNT,           // RPC_C_AUTHN_xxx
            RPC_C_AUTHZ_NONE,            // RPC_C_AUTHZ_xxx
            NULL,                        // Server principal name
            RPC_C_AUTHN_LEVEL_CALL,      // RPC_C_AUTHN_LEVEL_xxx
            RPC_C_IMP_LEVEL_IMPERSONATE, // RPC_C_IMP_LEVEL_xxx
            NULL,                        // client identity
            EOAC_NONE                    // proxy capabilities
        );

        if (FAILED(hres))
        {
            pSvc->Release();
            pLoc->Release();
            return "";               // Program has failed.
        }

        // Step 6: --------------------------------------------------
        // Use the IWbemServices pointer to make requests of WMI ----

        // For example, get the name of the operating system
        IEnumWbemClassObject* pEnumerator = NULL;
        IEnumWbemClassObject* pEnumerator1 = NULL;
        IEnumWbemClassObject* pEnumerator2 = NULL;
        IEnumWbemClassObject* pEnumerator3 = NULL;


        IWbemClassObject *pclsObj = NULL;
        IWbemClassObject *pclsObj1 = NULL;
        IWbemClassObject *pclsObj2 = NULL;
        IWbemClassObject *pclsObj3 = NULL;
        ULONG uReturn = 0;
        std::string serial_number;
        std::string serial_number_diskdrive;
        std::string serial_number_physicalmedia;
        std::string pnp_device_id;


        TCHAR infoBuf[64];
        if (strlen(drive)) {
            strncpy((char*)infoBuf,drive,3);
        }
        else {
            UINT res = GetWindowsDirectory( infoBuf, 64 );
            if (!res) {
                return "";
            }
        }
        infoBuf[2] = 0;
        std::cout << infoBuf << "\n";

        char query[1024];

        sprintf(query,"Select * from Win32_LogicalDisk Where DeviceID=\"%s\"",infoBuf);
        std::cout << query << "\n";
        hres = pSvc->ExecQuery(
            bstr_t("WQL"),
            bstr_t(query),
            WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY,
            NULL,
            &pEnumerator);
        if(SUCCEEDED(hres) && pEnumerator)
        {
            // get the first Win32_DiskPartition
            HRESULT hr = pEnumerator->Next(WBEM_INFINITE, 1, &pclsObj, &uReturn);

            if(SUCCEEDED(hr) && 0 != uReturn)
            {
                VARIANT vtProp;

                // Get the value of the partition's DeviceID property
                hr = pclsObj->Get(L"DeviceID", 0, &vtProp, 0, 0);
                if(SUCCEEDED(hr))
                {
                    std::string device_id;
                    if(vtProp.vt == VT_BSTR) {
                        std::wstring wstr((BSTR)V_BSTR(&vtProp));
                        std::string str(wstr.begin(), wstr.end());
                        str.assign(wstr.begin(), wstr.end());
                        device_id = str;
                    }
                    VariantClear(&vtProp);

                    // "join" Win32_DiskPartition to Win32_DiskDrive
                    sprintf(query,"ASSOCIATORS OF {Win32_LogicalDisk.DeviceID=\"%s\"} WHERE AssocClass = Win32_LogicalDiskToPartition",device_id.c_str());
                    std::cout << query << "\n";

                    hres = pSvc->ExecQuery(
                        bstr_t("WQL"),
                        bstr_t(query),
                        WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY,
                        NULL,
                        &pEnumerator1);

                    if(SUCCEEDED(hres) && pEnumerator1)
                    {
                        // get the first Win32_DiskDrive
                        hr = pEnumerator1->Next(WBEM_INFINITE, 1, &pclsObj1, &uReturn);

                        if(SUCCEEDED(hr) && 0 != uReturn)
                        {
                            // Get the value of the disk-drive's DeviceID
                            hr = pclsObj1->Get(L"DeviceID", 0, &vtProp, 0, 0);
                            if(SUCCEEDED(hr))
                            {
                                if(vtProp.vt == VT_BSTR)
                                {
                                    std::wstring wstr((BSTR)V_BSTR(&vtProp));
                                    std::string str(wstr.begin(), wstr.end());
                                    str.assign(wstr.begin(), wstr.end());
                                    device_id = str;
                                }
                                VariantClear(&vtProp);

                                // "join" Win32_DiskDrive to Win32_PhysicalMedia
                                sprintf(query,"ASSOCIATORS OF {Win32_DiskPartition.DeviceID='%s'} WHERE ResultClass=Win32_DiskDrive",device_id.c_str());
                                std::cout << query << "\n";
                                hres = pSvc->ExecQuery(
                                    bstr_t("WQL"),
                                    bstr_t(query),
                                    WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY,
                                    NULL,
                                    &pEnumerator2);

                                if(SUCCEEDED(hres) && pEnumerator2)
                                {
                                    // get the first Win32_PhysicalMedia
                                    hr = pEnumerator2->Next(WBEM_INFINITE, 1, &pclsObj2, &uReturn);

                                    if(SUCCEEDED(hr) && 0 != uReturn)
                                    {
                                        // get the PhysicalMedia's SerialNumber
                                        hr = pclsObj2->Get(L"SerialNumber", 0, &vtProp, 0, 0);
                                        if(SUCCEEDED(hr))
                                        {
                                            if(vtProp.vt == VT_BSTR)
                                            {
                                                std::wstring wstr((BSTR)V_BSTR(&vtProp));
                                                std::string str(wstr.begin(), wstr.end());
                                                str.assign(wstr.begin(), wstr.end());
                                                serial_number_diskdrive = str;
                                            }
                                            VariantClear(&vtProp);
                                        }

                                        hr = pclsObj2->Get(L"DeviceID", 0, &vtProp, 0, 0);
                                        if(SUCCEEDED(hr))
                                        {
                                            if(vtProp.vt == VT_BSTR)
                                            {
                                                std::wstring wstr((BSTR)V_BSTR(&vtProp));
                                                std::string str(wstr.begin(), wstr.end());
                                                str.assign(wstr.begin(), wstr.end());
                                                device_id = str;
                                                //wcscpy(wmihddsn,vtProp.bstrVal);
                                            }
                                            VariantClear(&vtProp);

                                            // "join" Win32_DiskDrive to Win32_PhysicalMedia
                                            sprintf(query,"ASSOCIATORS OF {Win32_DiskDrive.DeviceID='%s'} WHERE ResultClass=Win32_PhysicalMedia",device_id.c_str());
                                            std::cout << query << "\n";
                                            hres = pSvc->ExecQuery(
                                                bstr_t("WQL"),
                                                bstr_t(query),
                                                WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY,
                                                NULL,
                                                &pEnumerator3);

                                            if(SUCCEEDED(hres) && pEnumerator3)
                                            {
                                                // get the first Win32_PhysicalMedia
                                                hr = pEnumerator3->Next(WBEM_INFINITE, 1, &pclsObj3, &uReturn);

                                                if(SUCCEEDED(hr) && 0 != uReturn)
                                                {
                                                    // get the PhysicalMedia's SerialNumber
                                                    hr = pclsObj3->Get(L"SerialNumber", 0, &vtProp, 0, 0);
                                                    if(SUCCEEDED(hr))
                                                    {
                                                        if(vtProp.vt == VT_BSTR)
                                                        {
                                                            std::wstring wstr((BSTR)V_BSTR(&vtProp));
                                                            std::string str(wstr.begin(), wstr.end());
                                                            str.assign(wstr.begin(), wstr.end());
                                                            serial_number_physicalmedia = str;
                                                        }
                                                        VariantClear(&vtProp);
                                                    }

                                                    // get the PhysicalMedia's SerialNumber
                                                    hr = pclsObj3->Get(L"PNPDeviceID", 0, &vtProp, 0, 0);
                                                    if(SUCCEEDED(hr))
                                                    {
                                                        if(vtProp.vt == VT_BSTR)
                                                        {
                                                            std::wstring wstr((BSTR)V_BSTR(&vtProp));
                                                            std::string str(wstr.begin(), wstr.end());
                                                            str.assign(wstr.begin(), wstr.end());
                                                            pnp_device_id = str;
                                                        }
                                                        VariantClear(&vtProp);
                                                    }
                                                }
                                                if(pclsObj3) pclsObj3->Release();
                                            }
                                            if(pEnumerator3) pEnumerator3->Release();
                                        }

                                    }

                                    if(pclsObj2) pclsObj2->Release();
                                }
                                if(pEnumerator2) pEnumerator2->Release();
                            } // get disk-drive's DeviceID
                        }

                        if(pclsObj1) pclsObj1->Release();
                    }
                    if(pEnumerator1) pEnumerator1->Release();

                } // get partition's DeviceID
            }
            if(pclsObj) pclsObj->Release();
        } // if succeeded first query
        if(pEnumerator) pEnumerator->Release();

        // Cleanup
        // ========

        pSvc->Release();
        pLoc->Release();
        std::cout << "SERIAL-NUMBER DISKDRIVE: " << serial_number_diskdrive << "\n";
        std::cout << "SERIAL-NUMBER PHYSICALMEDIA: " << serial_number_physicalmedia << "\n";
        if (!serial_number_diskdrive.empty()) {
            serial_number = serial_number_diskdrive;
        }
        else if (!serial_number_physicalmedia.empty()) {
            serial_number = serial_number_physicalmedia;
        }
        else if (!pnp_device_id.empty()) {
            serial_number = pnp_device_id;
        }
        std::cout << "CHOSEN SERIAL-NUMBER: " << serial_number << "\n";
        return serial_number;
    }

}
