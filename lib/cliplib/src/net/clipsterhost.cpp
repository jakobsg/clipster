#include "clipsterhost.h"

ClipsterNet::ClipsterHost *ClipsterNet::ClipsterHost::s_instance = 0;

namespace ClipsterNet {

    ClipsterHost *ClipsterHost::instance() {
        if (s_instance==0) {
            s_instance = new ClipsterHost;
        }
        return s_instance;
    }

    const QString &ClipsterHost::hostName() {
        return instance()->m_host_name;
    }

    const QString &ClipsterHost::schema() {
        return instance()->m_schema;
    }

    const QUrl &ClipsterHost::serviceUrl(const QString &service_class) {
        ClipsterHost *inst = instance();
        if (!inst->m_service_urls.contains(service_class)) {
            inst->m_service_urls[service_class] = QUrl(
                QString("%1/service/v%2/%3/jsonwsp")
                    .arg(inst->m_home_page_url.toString())
                    .arg(CLIPSTER_SERVICE_VERSION)
                    .arg(service_class));
        }
        qDebug("service url: %s", inst->m_service_urls[service_class].toString().toUtf8().data());
        return inst->m_service_urls[service_class];
    }

    const QUrl &ClipsterHost::homePageUrl() {
        return instance()->m_home_page_url;
    }

    const QUrl &ClipsterHost::teamsPageUrl() {
        return instance()->m_teams_page_url;
    }

    const QUrl &ClipsterHost::loginPageUrl() {
        return instance()->m_login_page_url;
    }

    int ClipsterHost::serviceVersion() {
        return CLIPSTER_SERVICE_VERSION;
    }

    ClipsterHost::ClipsterHost() {
        m_host_name = CLIPSTER_HOST;
        m_schema = CLIPSTER_HTTP_SCHEMA;
        m_home_page_url = QUrl(QString("%1://%2").arg(CLIPSTER_HTTP_SCHEMA).arg(CLIPSTER_HOST));
        m_teams_page_url = QUrl(QString("%1/teams").arg(m_home_page_url.toString()));
        m_login_page_url = QUrl(QString("%1/login").arg(m_home_page_url.toString()));
    }

}
