#ifndef CLIPSTERHOST_H
#define CLIPSTERHOST_H

#ifdef USE_CLIPSTER_DEV_SERVER
#define CLIPSTER_HTTP_SCHEMA "http"
#define CLIPSTER_HOST "dev.clipster.info"
#else
#define CLIPSTER_HTTP_SCHEMA "http"
#define CLIPSTER_HOST "clipster.info"
#endif

#include "../cliplib_global.h"
#include <QUrl>
#include <QMap>

namespace ClipsterNet {

    class CLIPLIBSHARED_EXPORT ClipsterHost {

    public:
        static ClipsterHost *instance();
        static const QString &hostName();
        static const QString &schema();
        static const QUrl &serviceUrl(const QString &service_class);
        static const QUrl &homePageUrl();
        static const QUrl &teamsPageUrl();
        static const QUrl &loginPageUrl();
        static int serviceVersion();

    private:
        ClipsterHost();

        static ClipsterHost *s_instance;
        QString m_host_name;
        QString m_schema;
        QUrl m_home_page_url;
        QUrl m_teams_page_url;
        QUrl m_login_page_url;
        QMap<QString,QUrl> m_service_urls;

    };

}

#endif // CLIPSTERHOST_H
