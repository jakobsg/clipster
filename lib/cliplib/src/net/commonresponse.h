#ifndef COMMONRESPONSE_H
#define COMMONRESPONSE_H

#include "../cliplib_global.h"
#include <QString>
#include <QJsonObject>
#include <QNetworkReply>

namespace ClipsterNet {

    class CLIPLIBSHARED_EXPORT CommonResponse {
    public:
        CommonResponse();
        CommonResponse(const CommonResponse &other);
        ~CommonResponse();

        void setData(const QJsonObject &data);
        QJsonObject data() const;

        int m_res_code;
        QString m_res_msg;
        QJsonObject m_json_response;

        bool m_serverfault;
        bool m_servicefault;
        QNetworkReply::NetworkError m_networkerror;
        int m_http_status_code;
        QString m_http_reason;
        QString m_reflection;
        QJsonObject *m_data;
    };

}

#endif // COMMONRESPONSE_H
