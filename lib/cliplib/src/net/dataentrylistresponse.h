#ifndef DATAENTRYLISTRESPONSE_H
#define DATAENTRYLISTRESPONSE_H

#include "../cliplib_global.h"
#include "commonresponse.h"
#include <QJsonArray>

namespace ClipsterNet {

    class CLIPLIBSHARED_EXPORT DataEntryListResponse : public CommonResponse {
    public:
        DataEntryListResponse();
        DataEntryListResponse(const CommonResponse &resp);

        ~DataEntryListResponse();
        QJsonArray m_data_entries;
        qint64 m_latest_timestamp;
    };

}

#endif // DATAENTRYLISTRESPONSE_H
