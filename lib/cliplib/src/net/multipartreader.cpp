#include "multipartreader.h"
#include "transferreplywrapper.h"

#include <QRegularExpression>
#include <QTemporaryFile>

namespace ClipsterNet {

    MultiPartReader::MultiPartReader(TransferReplyWrapper *parent) : QObject(parent) {
        m_not_multipart = false;
        m_boundary = "";
        m_charset = "UTF-8";
        m_current = 0;
        m_done = false;
        m_reading_headers = false;
        if (parent) {
            parent->setMultiPartReader(this);
        }
    }

    MultiPartReader::~MultiPartReader() {
        //qDebug("Deleting MultiPartReader: %p", this);
    }

    QList<QTemporaryFile *> MultiPartReader::partFiles() {
        return m_partfiles;
    }

    QList<MultiPartReader::RawHeaderList> MultiPartReader::partHeaders() {
        return m_partheaders;
    }

    QTemporaryFile *MultiPartReader::partFile(int n) {
        return m_partfiles[n];
    }

    MultiPartReader::RawHeaderList MultiPartReader::partHeader(int n) {
        return m_partheaders[n];
    }

    int MultiPartReader::count() const {
        return m_partfiles.count();
    }

    int MultiPartReader::nextBoundary(int offset) {
        int idx = m_buffer.indexOf(m_boundary,offset);
        if (m_buffer.length()<idx+m_boundary.length()+2) {
            // Special situation:
            // Boundary found at end of byte array, therefore no split/end control character
            // we interpret this as no match.
            return -1;
        }
        return idx;
    }

    void MultiPartReader::flush(int size) {
        QByteArray flush_bytes;
        if (size<0) {
            //qDebug("FLUSHING:\n%s",m_buffer.left(m_buffer.size()-size).data());
            flush_bytes = m_buffer.left(m_buffer.size()+size);
            m_buffer = m_buffer.right(-size);
        }
        else {
            //qDebug("FLUSHING:\n%s",m_buffer.left(size).data());
            flush_bytes = m_buffer.left(size);
            m_buffer = m_buffer.right(m_buffer.size()-size);
        }
        if (m_reading_headers) {
            int idx1 = flush_bytes.indexOf("\n\n");
            if (idx1>-1) {
                idx1 += 2;
            }
            int idx2 = flush_bytes.indexOf("\r\n\r\n");
            if (idx2>-1) {
                idx2 += 4;
            }
            int idx = (idx1>-1 && idx2>-1) ? qMin(idx1, idx2) : qMax(idx1, idx2);
            if (idx==-1) {
                m_buffer.prepend(flush_bytes);
                return;
            }
            QByteArray header_bytes = flush_bytes.left(idx).data();
            RawHeaderList headers;
            foreach (QByteArray hline, header_bytes.split('\n')) {
                int colon_idx = hline.indexOf(":");
                if (colon_idx>-1) {
                    headers.append(QNetworkReply::RawHeaderPair(
                        hline.left(colon_idx).trimmed(),
                        hline.right(hline.length()-colon_idx-1).trimmed()));
                }
            }
            m_partheaders << headers;
//            foreach (QNetworkReply::RawHeaderPair hpair, headers) {
//                if (hpair.first.contains("Disposition")) {
//                    qDebug(QString("HNAME: %1, HVALUE: %2").arg(hpair.first.data()).arg(hpair.second.data()).toUtf8().data());
//                }
//            }

            flush_bytes = flush_bytes.right(flush_bytes.length() - idx);
            m_reading_headers = false;
        }
        if (m_current) {
            m_current->write(flush_bytes);
        }
    }

    void MultiPartReader::readyReadConsumer(TransferReplyWrapper *reply) {
        if (m_not_multipart || m_done) {
            return;
        }
        if (!m_boundary.length()) {
            m_mainheaders = reply->networkReply()->rawHeaderPairs();
            QString content_type = reply->networkReply()->header(QNetworkRequest::ContentTypeHeader).toString();
            // Intercept boundary format: "Content-Type: multipart/related; boundary=a78b1240cb8d3f2b086dff77681cf01e; charset=UTF-8"
            QRegularExpression rx_boundary("boundary=([^;]+)");
            QRegularExpressionMatch boundary_match = rx_boundary.match(content_type);
            if (!rx_boundary.captureCount()) {
                m_not_multipart = true;
                return;
            }
            QRegularExpression rx_charset("charset=([^;]+)");
            QRegularExpressionMatch charset_match = rx_charset.match(content_type);
            if (rx_charset.captureCount()) {
                m_charset = charset_match.captured(1);
            }
            m_boundary = "--" + boundary_match.captured(1);
        }
        m_buffer.append(reply->networkReply()->readAll());

        int idx = nextBoundary();
        if (idx>-1) {
            // found at least one boundary within the incoming data
            while (idx>-1) {
                if (idx>0) {
                    flush(idx-1);
                    m_buffer = m_buffer.right(m_buffer.size()-1);
                }
                int nl_idx = m_buffer.indexOf('\n');
                if (nl_idx==-1) {
                    m_done = true;
                    break;
                }
                if (m_current) {
                    m_current->close();
                }
                m_reading_headers = true;
                m_current = new QTemporaryFile(this);
                m_current->open();
                m_partfiles << m_current;
                m_buffer = m_buffer.right(m_buffer.length()-nl_idx-1);
                idx = nextBoundary();
            }
        }
        else {
            // Read data with no boundaries. Flush until the latest 128 bytes
            flush(-128);
        }
    }

}
