#ifndef MULTIPARTREADER_H
#define MULTIPARTREADER_H

#include <QObject>
#include <QNetworkReply>
#include <QTemporaryFile>

class QNetworkReply;

namespace ClipsterNet {

    class TransferReplyWrapper;

    class MultiPartReader : public QObject
    {
        Q_OBJECT

    public:
        typedef QList<QNetworkReply::RawHeaderPair> RawHeaderList;

        MultiPartReader(TransferReplyWrapper *parent = 0);
        ~MultiPartReader();
        QList<QTemporaryFile *> partFiles();
        QList<RawHeaderList> partHeaders();
        QTemporaryFile *partFile(int n);
        RawHeaderList partHeader(int n);
        int count() const;

    signals:

    public slots:
        void readyReadConsumer(ClipsterNet::TransferReplyWrapper *reply);
    private:
        int nextBoundary(int offset = 0);
        void flush(int size);
        bool m_not_multipart;
        bool m_reading_headers;
        bool m_done;
        QByteArray m_buffer;
        QString m_boundary;
        QString m_charset;
        RawHeaderList m_mainheaders;
        QList<QTemporaryFile *> m_partfiles;
        QList<RawHeaderList> m_partheaders;
        QTemporaryFile *m_current;
    };

}
#endif // MULTIPARTREADER_H
