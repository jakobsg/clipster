#ifndef SUCCESSRESPONSE_H
#define SUCCESSRESPONSE_H

#include "../cliplib_global.h"
#include "commonresponse.h"

namespace ClipsterNet {

    class CLIPLIBSHARED_EXPORT SuccessResponse : public CommonResponse {
    public:
        SuccessResponse();
        SuccessResponse(const CommonResponse &resp);
        ~SuccessResponse();

        bool m_success;
    };

}

#endif
