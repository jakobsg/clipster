#include "teamlistresponse.h"
#include <crashlib.h>

namespace ClipsterNet {

    TeamListResponse::TeamListResponse()
    : CommonResponse() {
    }

    TeamListResponse::TeamListResponse(const CommonResponse &resp)
    : CommonResponse(resp) {
    }

    TeamListResponse::~TeamListResponse() {
    }

}
