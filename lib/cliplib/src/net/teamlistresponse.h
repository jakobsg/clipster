#ifndef TEAMLISTRESPONSE_H
#define TEAMLISTRESPONSE_H

#include "../cliplib_global.h"
#include "commonresponse.h"
#include <QJsonArray>

namespace ClipsterNet {

    class CLIPLIBSHARED_EXPORT TeamListResponse : public CommonResponse {
    public:
        TeamListResponse();
        TeamListResponse(const CommonResponse &resp);

        ~TeamListResponse();
        QJsonArray m_teams;
    };

}
#endif // TEAMENTRYLISTRESPONSE_H
