#include "transferreplywrapper.h"

#include <QJsonObject>

#define TIMEOUT_MSECS 10000

namespace ClipsterNet {

    TransferReplyWrapper::TransferReplyWrapper(QNetworkReply *rep, const QString &data_hash) : QObject(rep) {
        m_data_hash = data_hash;
        m_reply = rep;
        m_multi_part_reader = 0;
        m_data = 0;
        connect(rep,SIGNAL(downloadProgress(qint64,qint64)),this,SLOT(passOnDownloadProgress(qint64,qint64)));
        connect(rep,SIGNAL(uploadProgress(qint64,qint64)),this,SLOT(passOnUploadProgress(qint64,qint64)));
        connect(rep,SIGNAL(finished()),this,SLOT(passOnFinished()));
        connect(rep,SIGNAL(readyRead()),this,SLOT(passOnReadyRead()));
        m_timeout_timer = new QTimer(this);
        m_timeout_timer->setSingleShot(true);
        connect(m_timeout_timer,SIGNAL(timeout()),this,SLOT(requestTimeout()));
        m_timeout_timer->start(TIMEOUT_MSECS);
    }

    TransferReplyWrapper::~TransferReplyWrapper() {
        if (m_data) {
            delete m_data;
        }
        //qDebug("Deleting TransferReplyWrapper: %p", this);
    }

    void TransferReplyWrapper::setMultiPartReader(MultiPartReader *mpr) {
        m_multi_part_reader = mpr;
        disconnect(this,SIGNAL(readyRead(ClipsterNet::TransferReplyWrapper *)),mpr,SLOT(readyReadConsumer(ClipsterNet::TransferReplyWrapper *)));
        connect(this,SIGNAL(readyRead(ClipsterNet::TransferReplyWrapper *)),mpr,SLOT(readyReadConsumer(ClipsterNet::TransferReplyWrapper *)));
        mpr->setParent(this);
    }

    MultiPartReader *TransferReplyWrapper::multiPartReader() {
        return m_multi_part_reader;
    }

    void TransferReplyWrapper::passOnDownloadProgress(qint64 progress, qint64 total) {
        m_timeout_timer->stop();
        m_timeout_timer->start(TIMEOUT_MSECS);
        emit downloadProgress(this, progress, total);
    }

    void TransferReplyWrapper::passOnUploadProgress(qint64 progress, qint64 total) {
        m_timeout_timer->stop();
        m_timeout_timer->start(TIMEOUT_MSECS);
        emit uploadProgress(this, progress, total);
    }

    void TransferReplyWrapper::passOnFinished() {
        m_timeout_timer->stop();
        emit finished(this);
    }

    void TransferReplyWrapper::passOnReadyRead() {
        emit readyRead(this);
    }

    void TransferReplyWrapper::requestTimeout() {
        qDebug("Timeout occured!");
        m_reply->abort();
    }

    QNetworkReply *TransferReplyWrapper::networkReply() {
        return m_reply;
    }

    void TransferReplyWrapper::setData(const QJsonObject &data) {
        if (m_data==0) {
            m_data = new QJsonObject;
        }
        *m_data = data;
    }

    QJsonObject TransferReplyWrapper::data() const {
        if (m_data) {
            return *m_data;
        }
        return QJsonObject();
    }

    const QString &TransferReplyWrapper::dataHash() const {
        return m_data_hash;
    }

}
