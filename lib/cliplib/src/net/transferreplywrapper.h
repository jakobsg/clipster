#ifndef TRANSFERREPLYHANDLER_H
#define TRANSFERREPLYHANDLER_H

#include "../cliplib_global.h"
#include "multipartreader.h"

#include <QNetworkReply>
#include <QTimer>

class QJsonObject;

namespace ClipsterNet {

    class CLIPLIBSHARED_EXPORT TransferReplyWrapper : public QObject
    {
        Q_OBJECT

    public:
        explicit TransferReplyWrapper(QNetworkReply *rep, const QString &data_hash=QString());
        ~TransferReplyWrapper();

        void setMultiPartReader(MultiPartReader *mpr);
        MultiPartReader *multiPartReader();
        QNetworkReply *networkReply();
        void setData(const QJsonObject &data);
        QJsonObject data() const;
        const QString &dataHash() const;

    private slots:
        void passOnDownloadProgress(qint64 progress, qint64 total);
        void passOnUploadProgress(qint64 progress, qint64 total);
        void passOnFinished();
        void passOnReadyRead();
        void requestTimeout();

    signals:
        void downloadProgress(ClipsterNet::TransferReplyWrapper *reply, qint64 progress, qint64 total);
        void uploadProgress(ClipsterNet::TransferReplyWrapper *reply, qint64 progress, qint64 total);
        void readyRead(ClipsterNet::TransferReplyWrapper *reply);
        void finished(ClipsterNet::TransferReplyWrapper *reply);

    private:
        QTimer *m_timeout_timer;
        MultiPartReader *m_multi_part_reader;
        QString m_data_hash;
        QNetworkReply *m_reply;
        QJsonObject *m_data;
    };

}

#endif // TRANSFERREPLYHANDLER_H
