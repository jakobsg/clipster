#include "webserviceclient.h"
#include "clipsterhost.h"
#include "transferreplywrapper.h"
#include "commonresponse.h"
#include "multipartreader.h"

#include "../core/pathentrymanager.h"
#include "../core/mediatypematchruleset.h"
#include "../core/mediatypeset.h"
#include "../core/mediatypehelper.h"
#include "../core/pathtools.h"
#include "../core/deviceinfo.h"
#include "../core/configmanager.h"
#include "../core/clipdetails.h"
#include "../core/clipboardtools.h"

#include <crashlib.h>

#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QHttpMultiPart>
#include <QRegularExpression>
#include <QDir>
#include <QDataStream>
#include <QBuffer>
#include <QUrl>
#include <QHostInfo>

#include <QDebug>

namespace ClipsterNet {

    WebServiceClient::WebServiceClient(QObject *parent) : QObject(parent) {
        m_device_hash = ClipsterCore::ConfigManager::configValue("device_hash").toString();
        qDebug() << "Device Hash: " << m_device_hash;
        if (!m_device_hash.isEmpty()) {
            store_device_hash(m_device_hash);
        }
        m_manager = new QNetworkAccessManager(this);
    }

    WebServiceClient::~WebServiceClient() {
    }

    QHttpMultiPart *WebServiceClient::createMultiPartPayload(QJsonDocument jsonwsp_part, const QMimeData *mdata) {
        QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::RelatedType);
        multiPart->setBoundary(QByteArray("CLIPSTER_BOUNDARY_987654321"));
        // Add the JSON-WSP part
        QByteArray utf8json = jsonwsp_part.toJson(QJsonDocument::Compact);
        QHttpPart textPart;
        textPart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json, charset=UTF-8"));
        textPart.setBody(utf8json);
        multiPart->append(textPart);
        // Add the mimedata parts
        foreach (QString format, mdata->formats()) {
            QHttpPart part;
            part.setHeader(QNetworkRequest::ContentTypeHeader, QVariant(format));
            part.setBody(mdata->data(format));
            multiPart->append(part);
        }

        return multiPart;
    }

    QHttpMultiPart *WebServiceClient::createMultiPartPayload(QJsonDocument jsonwsp_part, const QStringList &file_list) {
        QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::RelatedType);
        multiPart->setBoundary(QByteArray("CLIPSTER_BOUNDARY_987654321"));

        // Add the JSON-WSP part
        QByteArray utf8json = jsonwsp_part.toJson(QJsonDocument::Compact);
        QHttpPart textPart;
        textPart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json, charset=UTF-8"));
        textPart.setBody(utf8json);
        multiPart->append(textPart);

        // Prepare path entries
        ClipsterCore::PathEntryManager *fl = new ClipsterCore::PathEntryManager(multiPart);
        foreach (QString fpath, file_list) {
            fl->addPathEntry(fpath);
        }

        // Add file parts
        foreach (QString pe, fl->pathEntries()) {
            QDir path_entry(pe);
            foreach (QFile *file, fl->files(pe)) {
                bool open_success = file->open(QIODevice::ReadOnly);
                qDebug("%s, %p",file->fileName().toUtf8().data(), file);
                if (!open_success) {
                    // File did not open for input - Action: skip
                    // TODO: Maybe some kind of notification to user
                    qDebug("File %s could not be opened - Error: %s (%d)", file->fileName().toUtf8().data(), file->errorString().toUtf8().data(), (int)file->error());
                    emit uploadError(file->errorString());
                    delete multiPart;
                    return 0;
                }

                QHttpPart part;
                part.setHeader(QNetworkRequest::ContentTypeHeader, QVariant(QString("application/octet-stream")));
                QFileInfo finfo(*file);
                if (!pe.isEmpty()) {
                    part.setRawHeader(QString("ROOT-ENTRY-NAME").toUtf8(), path_entry.dirName().toUtf8());
                    part.setRawHeader(QString("SUB-DIRECTORY").toUtf8(), path_entry.relativeFilePath(finfo.path()).toUtf8());
                }
                part.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(QString("attachment; filename=\"%1\"").arg(finfo.fileName())));
                part.setBodyDevice(file);
                multiPart->append(part);
            }
        }
        return multiPart;
    }

    void WebServiceClient::commonReplyHandler(QNetworkReply *reply, CommonResponse *resp, const QByteArray &ext_jsonwsp) {
        CALL_LOGGER();
        QJsonParseError parse_error;
        if (reply->error()>0) {
            LOG_ERROR(QString("Network Error: %1").arg(reply->errorString()));
            resp->m_networkerror = reply->error();
            return;
        }
        int http_status_code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
        resp->m_http_reason = reply->attribute( QNetworkRequest::HttpReasonPhraseAttribute ).toString();
        resp->m_http_status_code = http_status_code;
        if (http_status_code==501) {
            LOG_ERROR(QString("Server Error: 501"));
            resp->m_serverfault = true;
            return;
        }
        // JsonWSP
        QByteArray json_response;
        if (!ext_jsonwsp.isEmpty()) {
            json_response = ext_jsonwsp;
        }
        else {
            json_response = reply->readAll();
        }
        qDebug(json_response.data());

        QJsonDocument resp_doc = QJsonDocument::fromJson(json_response,&parse_error);
        if (parse_error.error != QJsonParseError::NoError) {
            LOG_ERROR(QString("Response JSON parse error: %1").arg(parse_error.errorString()));
            resp->m_serverfault = true;
            return;
        }
        resp->m_json_response = resp_doc.object();
        resp->m_reflection = resp->m_json_response["reflection"].toString();
        if (resp->m_json_response["fault"].isUndefined()) {
            LOG_ERROR(QString("Server Fault: %1").arg(json_response.data()));
            // if we reach to here with no exception, then we have a service fault
            resp->m_servicefault = true;
            return;
        }
        LOG_TRACE(QString("Response: %1").arg(json_response.data()));
        QJsonObject result_obj = resp->m_json_response["result"].toObject();
        if (result_obj["method_result"].isUndefined()) {
            resp->m_servicefault = true;
            return;
        }
        QJsonObject method_result = result_obj["method_result"].toObject();
        int res_code = method_result["res_code"].toInt();
        QString res_msg = method_result["res_msg"].toString();
        resp->m_res_code = res_code;
        resp->m_res_msg = res_msg;
    }

    void WebServiceClient::loginReply(TransferReplyWrapper *reply) {
        CommonResponse resp;
        commonReplyHandler(reply->networkReply(), &resp);
        if (resp.m_res_code==0) {
            QJsonObject result = resp.m_json_response["result"].toObject();
            m_login_token = result["login_token"].toString();
            qDebug("LOGIN TOKEN: %s", m_login_token.toUtf8().data());
            registerDevice();
        }
        reply->networkReply()->deleteLater();
    }

    void WebServiceClient::registerDeviceReply(TransferReplyWrapper *reply) {
        SuccessResponse sresp;
        commonReplyHandler(reply->networkReply(), &sresp);
        if (sresp.m_res_code==0) {
            QJsonObject result = sresp.m_json_response["result"].toObject();
            m_device_hash = result["device_hash"].toString();
            if (!m_device_hash.isEmpty()) {
                store_device_hash(m_device_hash);
            }
            qDebug("DEVICE HASH: %s", m_device_hash.toUtf8().data());
            ClipsterCore::ConfigManager::setConfigValue("device_hash",m_device_hash);
            sresp.m_success = true;
        }
        emit registerDeviceResponseReady(sresp);
        reply->networkReply()->deleteLater();
    }

    void WebServiceClient::whoamiReply(TransferReplyWrapper *reply) {
        WhoamiResponse resp;
        commonReplyHandler(reply->networkReply(), &resp);
        if (resp.m_res_code==0) {
            QJsonObject result = resp.m_json_response["result"].toObject();
            QJsonObject user_info = result["user_info"].toObject();
            if (!user_info.isEmpty()) {
                resp.m_username = user_info["username"].toString();
                store_username(resp.m_username);
            }
        }
        else if (resp.m_res_code==10301) {
            // Invalid device hash
            ClipsterCore::ConfigManager::removeConfigValue("device_hash");
        }

        emit whoamiResponseReady(resp);
        reply->networkReply()->deleteLater();
    }

    void WebServiceClient::listDataEntriesReply(TransferReplyWrapper *reply) {
        DataEntryListResponse resp;
        commonReplyHandler(reply->networkReply(), &resp);
        if (resp.m_res_code==0) {
            QJsonObject result = resp.m_json_response["result"].toObject();
            resp.m_data_entries = result["data_entries"].toArray();
            if (resp.m_data_entries.count()) {
                resp.m_latest_timestamp = result["latest_timestamp"].toDouble();
                ClipsterCore::ConfigManager::setConfigValue("data_timestamp", resp.m_latest_timestamp);
                ClipsterCore::ClipDetails::saveClipDataEntries(resp.m_data_entries);
            }
            emit dataEntryListResponseReady(resp);
        }
        reply->networkReply()->deleteLater();
    }

    void WebServiceClient::listTeamDataEntriesReply(TransferReplyWrapper *reply) {
        DataEntryListResponse resp;
        commonReplyHandler(reply->networkReply(), &resp);
        if (resp.m_res_code==0) {
            QJsonObject result = resp.m_json_response["result"].toObject();
            resp.m_data_entries = result["data_entries"].toArray();
            if (resp.m_data_entries.count()) {
                resp.m_latest_timestamp = result["latest_timestamp"].toDouble();
                ClipsterCore::ConfigManager::setConfigValue("team_data_timestamp", resp.m_latest_timestamp);
                ClipsterCore::ClipDetails::saveClipDataEntries(resp.m_data_entries);
            }
            emit dataEntryListResponseReady(resp);
        }
        reply->networkReply()->deleteLater();
    }

    void WebServiceClient::listTeamsReply(TransferReplyWrapper *reply) {
        TeamListResponse resp;
        commonReplyHandler(reply->networkReply(), &resp);
        if (resp.m_res_code==0) {
            QJsonObject result = resp.m_json_response["result"].toObject();
            resp.m_teams = result["teams"].toArray();
            emit teamListResponseReady(resp);
        }
        reply->networkReply()->deleteLater();
    }

    void WebServiceClient::pendingTeamInvitationsReply(TransferReplyWrapper *reply) {
        CommonResponse resp;
        commonReplyHandler(reply->networkReply(), &resp);
        if (resp.m_res_code==0) {
            QJsonObject result = resp.m_json_response["result"].toObject();
            int number = result["number"].toInt();
            emit pendingTeamInvitationsResponseReady(number);
        }
        reply->networkReply()->deleteLater();
    }

    void WebServiceClient::pendingTeamMemberAcceptsReply(TransferReplyWrapper *reply) {
        CommonResponse resp;
        commonReplyHandler(reply->networkReply(), &resp);
        if (resp.m_res_code==0) {
            QJsonObject result = resp.m_json_response["result"].toObject();
            int number = result["number"].toInt();
            emit pendingTeamMemberAcceptsResponseReady(number);
        }
        reply->networkReply()->deleteLater();
    }

    void WebServiceClient::teamShareDataEntryReply(TransferReplyWrapper *reply) {
        SuccessResponse resp;
        resp.setData(reply->data());
        commonReplyHandler(reply->networkReply(), &resp);
        if (resp.m_res_code==0) {
            QJsonObject result = resp.m_json_response["result"].toObject();
            resp.m_success = result["success"].toBool();
        }
        emit teamShareDataEntryResponseReady(resp);
        reply->networkReply()->deleteLater();
    }

    TransferReplyWrapper *WebServiceClient::upload(const QString &data_hash) {

        QJsonDocument jdoc;
        QJsonObject jobj;
        jobj["type"] = "jsonwsp/request";
        jobj["version"] = "1.0";
        jobj["methodname"] = "upload";
        QJsonObject jargs;
        jargs["device_hash"] = m_device_hash;
        jargs["data_hash"] = data_hash;
        jobj["args"] = jargs;

        jdoc.setObject(jobj);

        QHttpMultiPart *multiPart = 0;

        QMimeData mimedata;
        ClipsterCore::ClipboardTools::createFromCache(data_hash, mimedata);
        if (mimedata.hasUrls()) {
            // Create file copy upload
            QStringList file_list;
            foreach (QUrl url, mimedata.urls()) {
                if (url.isLocalFile()) {
                    file_list << url.path();
                }
            }
            multiPart = createMultiPartPayload(jdoc, file_list);
        }
        else {
            // Create data copy upload
            multiPart = createMultiPartPayload(jdoc, &mimedata);
        }
        if (multiPart==0) {
            return 0;
        }

        QNetworkRequest request(ClipsterHost::serviceUrl("TransferService"));

        QNetworkAccessManager *manager = new QNetworkAccessManager(this);
        QNetworkReply *reply = manager->post(request, multiPart);
        multiPart->setParent(reply); // delete the multiPart with the reply

        TransferReplyWrapper *trw = new TransferReplyWrapper(reply, data_hash);
        connect(trw, SIGNAL(finished(ClipsterNet::TransferReplyWrapper *)),this, SLOT(uploadReply(ClipsterNet::TransferReplyWrapper*)));
        return trw;
    }

//    TransferReplyWrapper *WebServiceClient::upload(const ClipsterCore::MediaTypeMatchedData *data) {

//        QJsonDocument jdoc;
//        QJsonObject jobj;
//        jobj["type"] = "jsonwsp/request";
//        jobj["version"] = "1.0";
//        jobj["methodname"] = "upload";
//        QJsonObject jargs;
//        jargs["device_hash"] = m_device_hash;
//        jargs["data_hash"] = data->calculateDataHash();
//        jobj["args"] = jargs;

//        jdoc.setObject(jobj);

//        QHttpMultiPart *multiPart = 0;

//        if (data->mediaTypeMatchRuleSet()->isFileList()) {
//            // Create file copy upload
//            QStringList file_list;
//            foreach (QUrl url, data->mimeData()->urls()) {
//                if (url.isLocalFile()) {
//                    file_list << url.path();
//                }
//            }
//            multiPart = createMultiPartPayload(jdoc, file_list);
//        }
//        else {
//            // Create data copy upload
//            multiPart = createMultiPartPayload(jdoc, data->mimeData());
//        }
//        if (multiPart==0) {
//            return 0;
//        }

//        QUrl url(QString("%1://%2/service/v%3/TransferService/jsonwsp").arg(CLIPSTER_HTTP_SCHEMA).arg(CLIPSTER_HOST).arg(CLIPSTER_SERVICE_VERSION));
//        QNetworkRequest request(url);

//        QNetworkAccessManager *manager = new QNetworkAccessManager(this);
//        QNetworkReply *reply = manager->post(request, multiPart);
//        multiPart->setParent(reply); // delete the multiPart with the reply

//        TransferReplyWrapper *trw = new TransferReplyWrapper(reply, data->calculateDataHash());
//        connect(trw, SIGNAL(finished(ClipsterNet::TransferReplyWrapper *)),this, SLOT(uploadReply(ClipsterNet::TransferReplyWrapper*)));
//        return trw;
//    }

    void WebServiceClient::uploadReply(TransferReplyWrapper *reply) {
        CommonResponse cr;
        commonReplyHandler(reply->networkReply(), &cr);
        reply->deleteLater();
    }

    TransferReplyWrapper *WebServiceClient::download(const QString &data_hash) {
        ClipsterCore::ClipDetails cd(data_hash);
        if (cd.dataAvaiable()) {
            return 0;
        }
        QJsonDocument jdoc;
        QJsonObject jobj;
        jobj["type"] = "jsonwsp/request";
        jobj["version"] = "1.0";
        jobj["methodname"] = "download";
        jobj["mirror"] = data_hash;
        QJsonObject jargs;
        jargs["device_hash"] = m_device_hash;
        jargs["data_hash"] = data_hash;

        jobj["args"] = jargs;
        jdoc.setObject(jobj);
        QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);

        QNetworkRequest req(ClipsterHost::serviceUrl("TransferService"));
        req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
        req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
        QNetworkReply *reply = m_manager->post(req,utf8json);

        TransferReplyWrapper *trw = new TransferReplyWrapper(reply, data_hash);
        new MultiPartReader(trw);
        connect(trw, SIGNAL(finished(ClipsterNet::TransferReplyWrapper *)),this, SLOT(downloadReply(ClipsterNet::TransferReplyWrapper *)));
        return trw;
    }

    void WebServiceClient::downloadReply(TransferReplyWrapper *reply) {
        MultiPartReader *mpr = reply->multiPartReader();
        if (mpr->count()) {
            // Handle the JSON-WSP part
            QTemporaryFile *jsonwsp_temp = mpr->partFile(0);
            QFile jsonwsp_file(jsonwsp_temp->fileName());
            jsonwsp_file.open(QFile::ReadOnly);
            // Parse the networkReply
            CommonResponse cr;
            commonReplyHandler(reply->networkReply(), &cr, jsonwsp_file.readAll());

            // Handle each part
            QRegularExpression rx_content_disp("filename=\"([^\"]+)\"");
            QDir data_dir(ClipsterCore::PathTools::dataDir(cr.m_reflection));

            for (int pidx=1; pidx<mpr->count(); pidx++) {
                MultiPartReader::RawHeaderList rl = mpr->partHeader(pidx);
                QTemporaryFile *tmp = mpr->partFile(pidx);
                QString content_type;
                QString root_entry_name;
                QString sub_directory;
                QString name;
                bool is_file = false;
                foreach (QNetworkReply::RawHeaderPair rhp, rl) {
                    if (rhp.first == "Content-Type") {
                        content_type = QString::fromUtf8(rhp.second);
                    }
                    else if (rhp.first == "Root-Entry-Name") {
                        root_entry_name = QString::fromUtf8(rhp.second);
                    }
                    else if (rhp.first == "Sub-Directory") {
                        sub_directory = QString::fromUtf8(rhp.second);
                    }
                    else if (rhp.first == "Content-Disposition") {
                        is_file = true;
                        QRegularExpressionMatch m = rx_content_disp.match(QString::fromUtf8(rhp.second));
                        if (m.hasMatch()) {
                            name = m.captured(1);
                        }
                    }
                }
                if (is_file) {
                    QStringList path_parts;
                    path_parts << "files";
                    if (!root_entry_name.isEmpty()) {
                        path_parts << root_entry_name;
                        if (!sub_directory.isEmpty()) {
                            path_parts << QDir::toNativeSeparators(sub_directory);
                        }
                        data_dir.mkpath(path_parts.join(QDir::separator()));
                    }
                    path_parts << name;
                    qDebug(data_dir.filePath(path_parts.join(QDir::separator())).toUtf8().data());
                    tmp->close();
                    if (!tmp->rename(data_dir.filePath(path_parts.join(QDir::separator())))) {
                        qDebug(tmp->errorString().toUtf8().data());
                    }
                    tmp->setAutoRemove(false);
                }
                else {
                    ClipsterCore::MediaType mt(content_type);
                    QStringList path_parts;
                    path_parts << "mimedata" << mt.toplevelType();
                    data_dir.mkpath(path_parts.join(QDir::separator()));
                    path_parts << mt.subType();
                    tmp->close();
                    if (!tmp->rename(data_dir.filePath(path_parts.join(QDir::separator())))) {
                        qDebug(tmp->errorString().toUtf8().data());
                    }
                    tmp->setAutoRemove(false);
                }
            }
            emit downloadComplete(reply);
        }
        reply->networkReply()->deleteLater();
    }

    TransferReplyWrapper *WebServiceClient::removeDataEntry(const QString &data_hash) {
        QJsonDocument jdoc;
        QJsonObject jobj;
        jobj["type"] = "jsonwsp/request";
        jobj["version"] = "1.0";
        jobj["methodname"] = "removeDataEntry";
        jobj["mirror"] = data_hash;
        QJsonObject jargs;
        jargs["device_hash"] = m_device_hash;
        jargs["data_hash"] = data_hash;

        jobj["args"] = jargs;
        jdoc.setObject(jobj);
        QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);

        QNetworkRequest req(ClipsterHost::serviceUrl("TransferService"));
        req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
        req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
        QNetworkReply *reply = m_manager->post(req,utf8json);
        TransferReplyWrapper *trw = new TransferReplyWrapper(reply, data_hash);
        connect(trw, SIGNAL(finished(ClipsterNet::TransferReplyWrapper *)),this, SLOT(removeDataEntryReply(ClipsterNet::TransferReplyWrapper *)));
        return trw;
    }

    void WebServiceClient::removeDataEntryReply(TransferReplyWrapper *reply) {
        SuccessResponse sresp;
        commonReplyHandler(reply->networkReply(), &sresp);
        if (sresp.m_res_code==0) {
            QJsonObject result = sresp.m_json_response["result"].toObject();
            sresp.m_success = result["success"].toBool();
        }
        emit dataEntryRemoved(reply->dataHash(), sresp);
        reply->networkReply()->deleteLater();
    }

    TransferReplyWrapper *WebServiceClient::login(const QString &username, const QString &password) {
        QJsonDocument jdoc;
        QJsonObject jobj;
        jobj["type"] = "jsonwsp/request";
        jobj["version"] = "1.0";
        jobj["methodname"] = "login";
        QJsonObject jargs;
        jargs["username"] = username;
        jargs["password"] = password;
        jobj["args"] = jargs;
        jdoc.setObject(jobj);
        QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);

        QNetworkRequest req(ClipsterHost::serviceUrl("UserService"));
        req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
        req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
        QNetworkReply *reply = m_manager->post(req,utf8json);

        TransferReplyWrapper *trw = new TransferReplyWrapper(reply);
        connect(trw, SIGNAL(finished(ClipsterNet::TransferReplyWrapper *)),this, SLOT(loginReply(ClipsterNet::TransferReplyWrapper *)));
        return trw;
    }

    TransferReplyWrapper *WebServiceClient::registerDevice(const QString &login_token) {
        if (!login_token.isEmpty()) {
            m_login_token = login_token;
        }
        QJsonDocument jdoc;
        QJsonObject jobj;
        jobj["type"] = "jsonwsp/request";
        jobj["version"] = "1.0";
        jobj["methodname"] = "registerDevice";
        QJsonObject jargs;
        jargs["login_token"] = m_login_token;
        QString hwsig = ClipsterCore::hardwareSignature();
        if (hwsig.isEmpty()) {
            qDebug("Unable to construct a hardware signature");
            return 0;
        }
        jargs["hardware_signature"] = hwsig;
        jargs["hostname"] = QHostInfo::localHostName();
        jargs["os_type"] = QSysInfo::productType();
        jargs["os_version"] = QSysInfo::productVersion();

        jobj["args"] = jargs;
        jdoc.setObject(jobj);
        QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);

        QNetworkRequest req(ClipsterHost::serviceUrl("DeviceService"));
        req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
        req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
        QNetworkReply *reply = m_manager->post(req,utf8json);

        TransferReplyWrapper *trw = new TransferReplyWrapper(reply);
        connect(trw, SIGNAL(finished(ClipsterNet::TransferReplyWrapper *)),this, SLOT(registerDeviceReply(ClipsterNet::TransferReplyWrapper *)));
        return trw;
    }

    TransferReplyWrapper *WebServiceClient::whoami() {
        QJsonDocument jdoc;
        QJsonObject jobj;
        jobj["type"] = "jsonwsp/request";
        jobj["version"] = "1.0";
        jobj["methodname"] = "whoami";
        QJsonObject jargs;
        jargs["device_hash"] = m_device_hash;

        jobj["args"] = jargs;
        jdoc.setObject(jobj);
        QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);

        QNetworkRequest req(ClipsterHost::serviceUrl("UserService"));
        req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
        req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
        QNetworkReply *reply = m_manager->post(req,utf8json);

        TransferReplyWrapper *trw = new TransferReplyWrapper(reply);
        connect(trw, SIGNAL(finished(ClipsterNet::TransferReplyWrapper *)),this, SLOT(whoamiReply(ClipsterNet::TransferReplyWrapper *)));
        return trw;
    }

    TransferReplyWrapper *WebServiceClient::listDataEntries() {
        QJsonDocument jdoc;
        QJsonObject jobj;
        jobj["type"] = "jsonwsp/request";
        jobj["version"] = "1.0";
        jobj["methodname"] = "listDataEntries";
        QJsonObject jargs;
        jargs["device_hash"] = m_device_hash;
        qDebug() << "DEVICE_HASH: " << m_device_hash;
        jargs["since_timestamp"] = ClipsterCore::ConfigManager::configValue("data_timestamp",0).toDouble();
        jobj["args"] = jargs;
        jdoc.setObject(jobj);
        QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);

        QNetworkRequest req(ClipsterHost::serviceUrl("TransferService"));
        req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
        req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
        QNetworkReply *reply = m_manager->post(req,utf8json);

        TransferReplyWrapper *trw = new TransferReplyWrapper(reply);
        connect(trw, SIGNAL(finished(ClipsterNet::TransferReplyWrapper *)),this, SLOT(listDataEntriesReply(ClipsterNet::TransferReplyWrapper *)));
        return trw;
    }

    TransferReplyWrapper *WebServiceClient::listTeamDataEntries() {
        QJsonDocument jdoc;
        QJsonObject jobj;
        jobj["type"] = "jsonwsp/request";
        jobj["version"] = "1.0";
        jobj["methodname"] = "listTeamDataEntries";
        QJsonObject jargs;
        jargs["device_hash"] = m_device_hash;
        jargs["since_timestamp"] = ClipsterCore::ConfigManager::configValue("team_data_timestamp",0).toDouble();
        jobj["args"] = jargs;
        jdoc.setObject(jobj);
        QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);

        QNetworkRequest req(ClipsterHost::serviceUrl("TransferService"));
        req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
        req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
        QNetworkReply *reply = m_manager->post(req,utf8json);

        TransferReplyWrapper *trw = new TransferReplyWrapper(reply);
        connect(trw, SIGNAL(finished(ClipsterNet::TransferReplyWrapper *)),this, SLOT(listTeamDataEntriesReply(ClipsterNet::TransferReplyWrapper *)));
        return trw;
    }

    TransferReplyWrapper *WebServiceClient::listTeams() {
        CALL_LOGGER();
        QJsonDocument jdoc;
        QJsonObject jobj;
        jobj["type"] = "jsonwsp/request";
        jobj["version"] = "1.0";
        jobj["methodname"] = "listOwnMemberships";
        QJsonObject jargs;
        jargs["device_hash"] = m_device_hash;
        jobj["args"] = jargs;
        jdoc.setObject(jobj);
        QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);

        QNetworkRequest req(ClipsterHost::serviceUrl("TeamService"));
        req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
        req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
        QNetworkReply *reply = m_manager->post(req,utf8json);

        TransferReplyWrapper *trw = new TransferReplyWrapper(reply);
        connect(trw, SIGNAL(finished(ClipsterNet::TransferReplyWrapper *)),this, SLOT(listTeamsReply(ClipsterNet::TransferReplyWrapper *)));
        return trw;
    }

    TransferReplyWrapper *WebServiceClient::pendingTeamInvitations() {
        QJsonDocument jdoc;
        QJsonObject jobj;
        jobj["type"] = "jsonwsp/request";
        jobj["version"] = "1.0";
        jobj["methodname"] = "pendingInvitations";
        QJsonObject jargs;
        jargs["device_hash"] = m_device_hash;
        jobj["args"] = jargs;
        jdoc.setObject(jobj);
        QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);

        QNetworkRequest req(ClipsterHost::serviceUrl("TeamService"));
        req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
        req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
        QNetworkReply *reply = m_manager->post(req,utf8json);

        TransferReplyWrapper *trw = new TransferReplyWrapper(reply);
        connect(trw, SIGNAL(finished(ClipsterNet::TransferReplyWrapper *)),this, SLOT(pendingTeamInvitationsReply(ClipsterNet::TransferReplyWrapper *)));
        return trw;
    }

    TransferReplyWrapper *WebServiceClient::pendingTeamMemberAccepts() {
        QJsonDocument jdoc;
        QJsonObject jobj;
        jobj["type"] = "jsonwsp/request";
        jobj["version"] = "1.0";
        jobj["methodname"] = "pendingMemberAccepts";
        QJsonObject jargs;
        jargs["device_hash"] = m_device_hash;
        jobj["args"] = jargs;
        jdoc.setObject(jobj);
        QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);

        QNetworkRequest req(ClipsterHost::serviceUrl("TeamService"));
        req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
        req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
        QNetworkReply *reply = m_manager->post(req,utf8json);

        TransferReplyWrapper *trw = new TransferReplyWrapper(reply);
        connect(trw, SIGNAL(finished(ClipsterNet::TransferReplyWrapper *)),this, SLOT(pendingTeamMemberAcceptsReply(ClipsterNet::TransferReplyWrapper *)));
        return trw;
    }

    TransferReplyWrapper *WebServiceClient::teamShareDataEntry(const QString &team_owner, const QString &teamname, const QString &data_hash) {
        QJsonDocument jdoc;
        QJsonObject jobj;
        jobj["type"] = "jsonwsp/request";
        jobj["version"] = "1.0";
        jobj["methodname"] = "shareDataEntry";
        QJsonObject jargs;
        jargs["device_hash"] = m_device_hash;
        jargs["team_owner"] = team_owner;
        jargs["teamname"] = teamname;
        jargs["data_hash"] = data_hash;
        jobj["args"] = jargs;
        jdoc.setObject(jobj);
        QByteArray utf8json = jdoc.toJson(QJsonDocument::Compact);

        QNetworkRequest req(ClipsterHost::serviceUrl("TeamService"));
        req.setHeader(QNetworkRequest::ContentLengthHeader,utf8json.size());
        req.setHeader(QNetworkRequest::ContentTypeHeader,"application/json, charset=UTF-8");
        QNetworkReply *reply = m_manager->post(req,utf8json);

        TransferReplyWrapper *trw = new TransferReplyWrapper(reply, data_hash);
        trw->setData(jargs);
        connect(trw, SIGNAL(finished(ClipsterNet::TransferReplyWrapper *)),this, SLOT(teamShareDataEntryReply(ClipsterNet::TransferReplyWrapper *)));
        return trw;
    }

}

