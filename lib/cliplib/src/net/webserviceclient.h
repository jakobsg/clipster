#ifndef WEBSERVICECLIENT_H
#define WEBSERVICECLIENT_H

#include "whoamiresponse.h"
#include "dataentrylistresponse.h"
#include "successresponse.h"
#include "teamlistresponse.h"
#include "../cliplib_global.h"
#include "../core/mediatypehelper.h"
#include "../core/mediatypematcheddata.h"

#include <QObject>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QHttpMultiPart>
#include <QMimeData>

namespace  ClipsterNet {

    class CommonResponse;
    class TransferReplyWrapper;

    class CLIPLIBSHARED_EXPORT WebServiceClient : public QObject
    {
        Q_OBJECT
    public:
        explicit WebServiceClient(QObject *parent = 0);
        ~WebServiceClient();

        QHttpMultiPart *createMultiPartPayload(QJsonDocument jsonwsp_part, const QMimeData *mdata);
        QHttpMultiPart *createMultiPartPayload(QJsonDocument jsonwsp_part, const QStringList &file_list);

        TransferReplyWrapper *upload(const QString &data_hash);
        TransferReplyWrapper *download(const QString &data_hash);
        TransferReplyWrapper *removeDataEntry(const QString &data_hash);
        TransferReplyWrapper *login(const QString &username, const QString &password);
        TransferReplyWrapper *registerDevice(const QString &login_token=QString());
        TransferReplyWrapper *whoami();
        TransferReplyWrapper *listDataEntries();
        TransferReplyWrapper *listTeamDataEntries();
        TransferReplyWrapper *listTeams();
        TransferReplyWrapper *pendingTeamInvitations();
        TransferReplyWrapper *pendingTeamMemberAccepts();
        TransferReplyWrapper *teamShareDataEntry(const QString &team_owner, const QString &teamname, const QString &data_hash);

    signals:
        void downloadComplete(ClipsterNet::TransferReplyWrapper *reply);
        void dataEntryRemoved(const QString &data_hash, const ClipsterNet::SuccessResponse &response);
        void uploadError(const QString &error_String);
        void whoamiResponseReady(const ClipsterNet::WhoamiResponse &response);
        void registerDeviceResponseReady(const ClipsterNet::SuccessResponse &response);
        void dataEntryListResponseReady(const ClipsterNet::DataEntryListResponse &response);
        void pendingTeamInvitationsResponseReady(int count);
        void pendingTeamMemberAcceptsResponseReady(int count);
        void teamListResponseReady(const ClipsterNet::TeamListResponse &response);
        void teamShareDataEntryResponseReady(const ClipsterNet::SuccessResponse &response);

    public slots:
        void uploadReply(ClipsterNet::TransferReplyWrapper *reply);
        void downloadReply(ClipsterNet::TransferReplyWrapper *reply);
        void removeDataEntryReply(ClipsterNet::TransferReplyWrapper *reply);
        void loginReply(ClipsterNet::TransferReplyWrapper *reply);
        void registerDeviceReply(ClipsterNet::TransferReplyWrapper *reply);
        void whoamiReply(ClipsterNet::TransferReplyWrapper *reply);
        void listDataEntriesReply(ClipsterNet::TransferReplyWrapper *reply);
        void listTeamDataEntriesReply(ClipsterNet::TransferReplyWrapper *reply);
        void listTeamsReply(ClipsterNet::TransferReplyWrapper *reply);
        void pendingTeamInvitationsReply(ClipsterNet::TransferReplyWrapper *reply);
        void pendingTeamMemberAcceptsReply(ClipsterNet::TransferReplyWrapper *reply);
        void teamShareDataEntryReply(ClipsterNet::TransferReplyWrapper *reply);

    private:
        void commonReplyHandler(QNetworkReply *reply, CommonResponse *resp, const QByteArray &ext_jsonwsp=QByteArray());
        QNetworkAccessManager *m_manager;
        QString m_login_token;
        QString m_device_hash;

    };

}
#endif // WEBSERVICECLIENT_H
