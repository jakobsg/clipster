#ifndef WHOAMIRESPONSE_H
#define WHOAMIRESPONSE_H

#include "../cliplib_global.h"
#include "commonresponse.h"

namespace ClipsterNet {

    class CLIPLIBSHARED_EXPORT WhoamiResponse : public CommonResponse {
    public:
        WhoamiResponse();
        WhoamiResponse(const CommonResponse &resp);
        ~WhoamiResponse();

        QString m_username;
        QString m_firstname;
        QString m_lastname;

    };

}

#endif // WHOAMIRESPONSE_H
