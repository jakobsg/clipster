#-------------------------------------------------
#
# Project created by QtCreator 2015-08-03T19:33:14
#
#-------------------------------------------------

QT += gui widgets network

TARGET = crashlib
DESTDIR = bin
TEMPLATE = lib

SOURCES += src/crashlib.cpp \
    src/calllogger.cpp \
    src/profileinfo.cpp \
	src/callevent.cpp \
	src/crashinfo_common.cpp \
    src/customevent.cpp \
    src/logevent.cpp

HEADERS += src/crashlib.h\
    src/crashlib_global.h \
    src/calllogger.h \
    src/profileinfo.h \
    src/callevent.h \
    src/customevent.h \
    src/logevent.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
win32 {
    HEADERS += \
        src/win/qstackwalker.h

    SOURCES += \
        src/win/StackWalker.cpp \
		src/win/qstackwalker.cpp \
		src/win/crashinfo_win.cpp
    LIBS +=  dbghelp.lib
    DEFINES += CRASHLIB_LIBRARY
    QMAKE_CFLAGS_RELEASE += -Zi
    QMAKE_CXXFLAGS_RELEASE += -Zi
    QMAKE_LFLAGS_RELEASE += /DEBUG /OPT:REF
    QMAKE_CXXFLAGS_EXCEPTIONS_ON = /EHa
    QMAKE_CXXFLAGS_STL_ON = /EHa
}
!win32 {
        SOURCES += src/x11/crashinfo_x11.cpp
}

unix:!macx {
    DEFINES += LINUX_OS
}
macx {
    DEFINES += DARWIN_OS
}
win32 {
    DEFINES += WINDOWS_OS
}
