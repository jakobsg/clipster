#include "callevent.h"
#include <QJsonValue>
#include <QDateTime>

CallEvent::CallEvent(const QString &fname, const QString &filename, int line)
: CustomEvent(fname,filename,line){
	m_etimer.start();
    m_call_done = false;
}

void CallEvent::setCallEnded() {
    m_duration = m_etimer.nsecsElapsed();
    m_call_done = true;
}

QJsonObject CallEvent::toJson() {
	QJsonObject jobj = CustomEvent::toJson();
    jobj["duration"] = m_duration;
    return jobj;
}

CallEvent::~CallEvent() {
}

