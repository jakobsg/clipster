#ifndef CALLEVENT_H
#define CALLEVENT_H

#include "crashlib_global.h"
#include "customevent.h"
#include <QString>
#include <QJsonObject>
#include <QElapsedTimer>

class CallEvent : public CustomEvent {
public:
	CallEvent(const QString &fname, const QString &filename, int line);
	~CallEvent();

    void setCallEnded();
	QJsonObject toJson();

    QElapsedTimer m_etimer;
    qint64 m_duration;
    bool m_call_done;
};

#endif // CALLEVENT_H
