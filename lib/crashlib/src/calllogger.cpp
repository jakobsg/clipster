#include "calllogger.h"
#include <QMutex>
#include <time.h>

#define CALL_HISTORY_SIZE 300

CallEventByThread *CallLogger::m_call_hist = 0;
ProfileInfoByThread *CallLogger::m_profile_info = 0;
QMutex *CallLogger::m_mutex = 0;

CallLogger::CallLogger(const QString &fname, const QString &filename, int line) {
	m_callevent = new CallEvent(fname, filename, line);
    callHistory().append(m_callevent);
}

CallEventList &CallLogger::callHistory() {
    mutex().lock();
    maybeInitCallEvent();
    CallEventList &call_hist = (*m_call_hist)[m_callevent->m_thread_id];
    mutex().unlock();
    return call_hist;
}



ProfileInfoMap &CallLogger::profileInfoMap() {
    mutex().lock();
    maybeInitProfileInfo();
    ProfileInfoMap &profile_info_map = (*m_profile_info)[m_callevent->m_thread_id];
    mutex().unlock();
    return profile_info_map;
}

CallLogger::~CallLogger() {
    m_callevent->setCallEnded();
    ProfileInfoMap &pmap = profileInfoMap();
    if (!pmap.contains(m_callevent->m_fname)) {
        pmap[m_callevent->m_fname] = new ProfileInfo(*m_callevent);
    }
    else {
        pmap[m_callevent->m_fname]->addCallEvent(*m_callevent);
    }
    CallEventList &cil = callHistory();
    if (cil.length()>CALL_HISTORY_SIZE) {
        foreach (CallEvent *ci, cil) {
            if (ci->m_call_done) {
                cil.removeOne(ci);
                delete ci;
                break;
            }
        }
    }
}

QMutex &CallLogger::mutex() {
    if (m_mutex==0) {
        m_mutex = new QMutex;
    }
    return *m_mutex;
}

QList<OS_THREAD_ID> CallLogger::threadIds() {
    mutex().lock();
    maybeInitCallEvent();
    QList<OS_THREAD_ID> treadids = m_call_hist->keys();
    mutex().unlock();
    return treadids;
}

CallEventList &CallLogger::callHistoryByThreadId(OS_THREAD_ID thread_id) {
    mutex().lock();
    maybeInitCallEvent();
    CallEventList &call_hist = (*m_call_hist)[thread_id];
    mutex().unlock();
    return call_hist;
}

ProfileInfoMap &CallLogger::profileInfoByThreadId(OS_THREAD_ID thread_id) {
    mutex().lock();
    maybeInitProfileInfo();
    ProfileInfoMap &profile_info_map = (*m_profile_info)[thread_id];
    mutex().unlock();
    return profile_info_map;
}

void CallLogger::destroyAllCallHistory() {
    if (m_call_hist==0) {
        return;
    }
    mutex().lock();
    foreach (CallEventList cil, *m_call_hist) {
        foreach (CallEvent *ci, cil) {
            qDebug("Deleting Callinfo");
            delete ci;
        }
    }
    m_call_hist->clear();
    mutex().unlock();
}

void CallLogger::destroyAllProfileInfo() {
    if (m_profile_info==0) {
        return;
    }
    mutex().lock();
    foreach (ProfileInfoMap pim, *m_profile_info) {
        foreach (ProfileInfo *pi, pim) {
            qDebug("Deleting ProfileInfo");
            delete pi;
        }
    }
    m_profile_info->clear();
    mutex().unlock();
}

void CallLogger::maybeInitCallEvent() {
    if (m_call_hist==0) {
        m_call_hist = new CallEventByThread;
    }
}

void CallLogger::maybeInitProfileInfo() {
    if (m_profile_info==0) {
        m_profile_info = new ProfileInfoByThread;
    }
}
