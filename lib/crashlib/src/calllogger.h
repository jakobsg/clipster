#ifndef CALLLOGGER_H
#define CALLLOGGER_H

#include "crashlib_global.h"
#include "callevent.h"
#include "profileinfo.h"

#include <QMutex>
#include <QMap>

typedef QList<CallEvent *> CallEventList;
typedef QMap<QString, ProfileInfo *> ProfileInfoMap;
typedef QMap<OS_THREAD_ID, CallEventList> CallEventByThread;
typedef QMap<OS_THREAD_ID, ProfileInfoMap> ProfileInfoByThread;

class CRASHLIBSHARED_EXPORT CallLogger {
public:
	CallLogger(const QString &fname, const QString &filename, int line);
    ~CallLogger();

    static QList<OS_THREAD_ID> threadIds();
	static CallEventList &callHistoryByThreadId(OS_THREAD_ID thread_id);
    static ProfileInfoMap &profileInfoByThreadId(OS_THREAD_ID thread_id);
    static void destroyAllCallHistory();
    static void destroyAllProfileInfo();

private:
	CallEvent *m_callevent;
	static void maybeInitCallEvent();
    static void maybeInitProfileInfo();
    static QMutex &mutex();
	CallEventList &callHistory();
    ProfileInfoMap &profileInfoMap();
    static QMutex *m_mutex;
	static CallEventByThread *m_call_hist;
    static ProfileInfoByThread *m_profile_info;
};

#endif // CALLLOGGER_H
