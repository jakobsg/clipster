#include "crashlib_global.h"
#include "crashlib.h"
#include <QHostInfo>
#include <QSysInfo>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QTemporaryFile>
#include <QDir>
#include <QProcess>
#include <QCoreApplication>

QJsonDocument crash_info_to_json(int sig, const QStringList &simple_callstack, const QStringList &detailed_callstack) {
    QJsonDocument doc;
    QJsonObject root_obj;
    root_obj["device_hash"] = device_hash();
    root_obj["git_version"] = git_version();
    //root_obj["hardware_signature"] = hardware_signature();
    if (sig==SIGSTDE) {
        root_obj["crash_type"] = "std::exception";
    }
    else {
        root_obj["crash_type"] = "segfault";
    }
	QJsonObject callevent_object;
    foreach (OS_THREAD_ID thread_id, CallLogger::threadIds()) {
		CallEventList call_hist = CallLogger::callHistoryByThreadId(thread_id);
        ProfileInfoMap &profile_info_map = CallLogger::profileInfoByThreadId(thread_id);
		QJsonArray callevent_array;
		foreach (CallEvent *callevent_line, call_hist) {
			if (!callevent_line->m_call_done) {
				callevent_line->setCallEnded();
				if (!profile_info_map.contains(callevent_line->m_fname)) {
					profile_info_map[callevent_line->m_fname] = new ProfileInfo(*callevent_line);
                }
                else {
					profile_info_map[callevent_line->m_fname]->addCallEvent(*callevent_line);
                }
            }

			callevent_array.append(callevent_line->toJson());
        }
		callevent_object[QString::number((qulonglong)thread_id)] = callevent_array;
    }
	root_obj["call_info"] = callevent_object;

    QJsonObject profile_object;
    foreach (OS_THREAD_ID thread_id, CallLogger::threadIds()) {
        ProfileInfoMap profile_info_map = CallLogger::profileInfoByThreadId(thread_id);
        QJsonArray profile_array;
        foreach (ProfileInfo *profline, profile_info_map) {
			profile_array.append(profline->toJson());
        }
        profile_object[QString::number((qulonglong)thread_id)] = profile_array;
    }
    root_obj["profile_info"] = profile_object;
    CallLogger::destroyAllCallHistory();
    CallLogger::destroyAllProfileInfo();

    root_obj["simple_callstack"] = QJsonArray::fromStringList(simple_callstack);
    root_obj["detailed_callstack"] = QJsonArray::fromStringList(detailed_callstack);
    root_obj["hostname"] = QHostInfo::localHostName();
    root_obj["os_type"] = QSysInfo::productType();
    root_obj["os_version"] = QSysInfo::productVersion();
    root_obj["username"] = username();
	QJsonArray log_events;
	foreach (LogEvent *le, LogEvent::logEvents()) {
		log_events.append(le->toJson());
	}
	root_obj["log_events"] = log_events;
    doc.setObject(root_obj);
    return doc;
}

void tmpfile_seg_handler( int sig ) {
    QStringList simple_callstack;
    QStringList detailed_callstack;
    collect_crash_info(sig, simple_callstack, detailed_callstack);
    QJsonDocument doc = crash_info_to_json(sig, simple_callstack, detailed_callstack);
    QTemporaryFile tmpfile;
    tmpfile.setAutoRemove(false);
    tmpfile.open();
    tmpfile.write(doc.toJson(QJsonDocument::Indented));

//    foreach (QString dbgline, debugstrings) {
//        s << dbgline << "\n";
//    }
    tmpfile.close();
    qDebug("Crash detected info dumped to: %s", tmpfile.fileName().toUtf8().data());
    QDir app_dir(QCoreApplication::applicationDirPath());
    QProcess::startDetached(app_dir.filePath("crashinfo"),QStringList(tmpfile.fileName()));
    exit(1);
}

void tmpfile_std_handler( void ) {
    tmpfile_seg_handler(SIGSTDE);
}
