#include "crashlib.h"
#include <signal.h>

static QString glob_hw_signature = QString::null;
void store_hardware_signature(const QString &hw_sig) {
    glob_hw_signature = hw_sig;
}

const QString &hardware_signature() {
    return glob_hw_signature;
}


static QString glob_device_hash = QString::null;
void store_device_hash(const QString &device_hash) {
    glob_device_hash = device_hash;
}

const QString &device_hash() {
    return glob_device_hash;
}


static QString glob_username = QString::null;
void store_username(const QString &username) {
    glob_username = username;
}

const QString &username() {
    return glob_username;
}


static QString glob_git_version = QString::null;
void store_git_version(const QString &git_version) {
    glob_git_version = git_version;
}

const QString &git_version() {
    return glob_git_version;
}


#ifdef WINDOWS_OS
#include <windows.h>

BOOL WINAPI DllMain(
  _In_ HINSTANCE hinstDLL,
  _In_ DWORD     fdwReason,
  _In_ LPVOID    lpvReserved
) {
    if (fdwReason==DLL_THREAD_ATTACH) {
		::SetUnhandledExceptionFilter(UnhandledCrashHandler);
        signal(SIGILL, tmpfile_seg_handler);
        signal(SIGSEGV, tmpfile_seg_handler);
        signal(SIGFPE, tmpfile_seg_handler);
		signal(SIGABRT, tmpfile_seg_handler);
		signal(SIGABRT_COMPAT, tmpfile_seg_handler);
		std::set_terminate( tmpfile_std_handler );
    }
    if (fdwReason==DLL_PROCESS_ATTACH) {
		::SetUnhandledExceptionFilter(UnhandledCrashHandler);
		signal(SIGILL, tmpfile_seg_handler);
        signal(SIGSEGV, tmpfile_seg_handler);
        signal(SIGFPE, tmpfile_seg_handler);
		signal(SIGABRT, tmpfile_seg_handler);
		signal(SIGABRT_COMPAT, tmpfile_seg_handler);
		std::set_terminate( tmpfile_std_handler );
    }
    return TRUE;
}
#endif
