#ifndef CRASHLIB_H
#define CRASHLIB_H

#include "crashlib_global.h"
#include "calllogger.h"
#include "logevent.h"
#include <QStringList>
#include <QMap>
#include <time.h>

extern void collect_crash_info(int sig, QStringList &simple_callstack, QStringList &detailed_callstack);

#ifdef WINDOWS_OS
#include <windows.h>
#define CALL_LOGGER() CallLogger call_logger(__FUNCSIG__, __FILE__, __LINE__);
#define LOG_WARNING(x) newLogEvent(__FUNCSIG__, __FILE__, __LINE__, x, LogEvent::LOG_WARNING);
#define LOG_ERROR(x) newLogEvent(__FUNCSIG__, __FILE__, __LINE__, x, LogEvent::LOG_ERROR);
#define LOG_FATAL(x) newLogEvent(__FUNCSIG__, __FILE__, __LINE__, x, LogEvent::LOG_FATAL);
#define LOG_INFO(x) newLogEvent(__FUNCSIG__, __FILE__, __LINE__, x, LogEvent::LOG_INFO);
#define LOG_DEBUG(x) newLogEvent(__FUNCSIG__, __FILE__, __LINE__, x, LogEvent::LOG_DEBUG);
#define LOG_TRACE(x) newLogEvent(__FUNCSIG__, __FILE__, __LINE__, x, LogEvent::LOG_TRACE);
extern LONG CRASHLIBSHARED_EXPORT WINAPI UnhandledCrashHandler(EXCEPTION_POINTERS * /*ExceptionInfo*/);
#else
#define CALL_LOGGER() CallLogger call_logger(__PRETTY_FUNCTION__, __FILE__, __LINE__)
#define LOG_WARNING(x) newLogEvent(__PRETTY_FUNCTION__, __FILE__, __LINE__, x, LogEvent::LOG_WARNING);
#define LOG_ERROR(x) newLogEvent(__PRETTY_FUNCTION__, __FILE__, __LINE__, x, LogEvent::LOG_ERROR);
#define LOG_FATAL(x) newLogEvent(__PRETTY_FUNCTION__, __FILE__, __LINE__, x, LogEvent::LOG_FATAL);
#define LOG_INFO(x) newLogEvent(__PRETTY_FUNCTION__, __FILE__, __LINE__, x, LogEvent::LOG_INFO);
#define LOG_DEBUG(x) newLogEvent(__PRETTY_FUNCTION__, __FILE__, __LINE__, x, LogEvent::LOG_DEBUG);
#define LOG_TRACE(x) newLogEvent(__PRETTY_FUNCTION__, __FILE__, __LINE__, x, LogEvent::LOG_TRACE);
#endif

#define SIGSTDE 10000

extern void CRASHLIBSHARED_EXPORT seg_handler(int sig);
extern void CRASHLIBSHARED_EXPORT std_handler( void );
extern void CRASHLIBSHARED_EXPORT tmpfile_seg_handler(int sig);
extern void CRASHLIBSHARED_EXPORT tmpfile_std_handler( void );
void CRASHLIBSHARED_EXPORT store_hardware_signature(const QString &hw_sig );
const QString &hardware_signature();
void CRASHLIBSHARED_EXPORT store_device_hash(const QString &device_hash );
const QString &device_hash();
void CRASHLIBSHARED_EXPORT store_username(const QString &username );
const QString &username();
void CRASHLIBSHARED_EXPORT store_git_version(const QString &git_version );
const QString &git_version();

#endif // CRASHLIB_H
