#ifndef CRASHLIB_GLOBAL_H
#define CRASHLIB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(CRASHLIB_LIBRARY)
#  define CRASHLIBSHARED_EXPORT Q_DECL_EXPORT
#else
#  define CRASHLIBSHARED_EXPORT Q_DECL_IMPORT
#endif

#ifdef WINDOWS_OS
#include <windows.h>
#define OS_THREAD_ID DWORD
#else
#define OS_THREAD_ID pthread_t
#endif



#endif // CRASHLIB_GLOBAL_H
