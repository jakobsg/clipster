#include "customevent.h"
#include <QMutex>
#include <QJsonValue>
#include <QDateTime>

class Counter {
public:
	Counter() { n = 0; }

	int increment() { QMutexLocker locker(&mutex); return ++n; }
	int decrement() { QMutexLocker locker(&mutex); return --n; }
	int value() const { QMutexLocker locker(&mutex); return n; }

private:
	mutable QMutex mutex;
	int n;
};

static Counter counter;

CustomEvent::CustomEvent(const QString &fname, const QString &filename, int line) {
	m_time = QDateTime::currentMSecsSinceEpoch();
	m_fname = fname;
	m_filename = filename;
	m_line = line;
#ifdef WINDOWS_OS
	m_thread_id =  GetCurrentThreadId();
#else
	m_thread_id = pthread_self();
#endif
	m_event_seq = counter.increment();

}

QJsonObject CustomEvent::toJson() {
	QJsonObject jobj;
	jobj["fname"] = m_fname;
	jobj["file"] = m_filename;
	jobj["line"] = m_line;
	jobj["thread"] = QString::number((qulonglong)m_thread_id);
	jobj["seq"] = m_event_seq;
	jobj["time"] = m_time;
	return jobj;
}
