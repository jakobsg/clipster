#ifndef CUSTOMEVENT_H
#define CUSTOMEVENT_H

#include "crashlib_global.h"
#include <QString>
#include <QJsonObject>

class CustomEvent
{
	public:
		CustomEvent(const QString &fname, const QString &filename, int line);
		QJsonObject toJson();

		QString m_fname;
		QString m_filename;
		int m_line;
		OS_THREAD_ID m_thread_id;
		int m_event_seq;
		qint64 m_time;

};

#endif // CUSTOMEVENT_H
