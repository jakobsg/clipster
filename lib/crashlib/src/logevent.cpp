#include "logevent.h"

#include <QJsonValue>
#include <QJsonDocument>

LogEventList *LogEvent::m_log_events = 0;
LogEventCallbackList *LogEvent::m_add_log_event_callbacks = 0;
LogEventCallbackList *LogEvent::m_remove_log_event_callbacks = 0;
QMutex *LogEvent::m_mutex = 0;


LogEvent *newLogEvent(const QString &fname, const QString &filename, int line, const QString &msg, LogEvent::LogLevel lvl) {
	LogEvent *le = new LogEvent(fname, filename, line, msg, lvl);
	LogEvent::addLogEvent(le);
        if (lvl<=LogEvent::LOG_ERROR) {
            // Always print log events error or worse to stdout if available
            QJsonDocument doc;
            doc.setObject(le->toJson());

            qDebug(QString("LOG_ERROR: %1").arg(doc.toJson(QJsonDocument::Indented).data()).toUtf8().data());
        }
	return le;
}

LogEvent::LogEvent(const QString &fname, const QString &filename, int line, const QString &msg, LogLevel lvl)
: CustomEvent(fname, filename, line){
	m_msg = msg;
	m_loglevel = lvl;
}

QJsonObject LogEvent::toJson() {
	QJsonObject jobj = CustomEvent::toJson();
	jobj["log_msg"] = m_msg;
	jobj["log_lvl"] = (int) m_loglevel;
	return jobj;
}

LogEventList &LogEvent::logEvents() {
	return *m_log_events;
}

QMutex &LogEvent::mutex() {
	if (m_mutex==0) {
		m_mutex = new QMutex;
	}
	return *m_mutex;
}

void LogEvent::addLogEvent(LogEvent *le) {
	mutex().lock();
	maybeInitLogEventList();
	m_log_events->append(le);
    foreach (LogEventCallback add_cb, *m_add_log_event_callbacks) {
        add_cb(le);
    }
    if (m_log_events->count()>1000) {
        foreach (LogEventCallback rm_cb, *m_remove_log_event_callbacks) {
            rm_cb(m_log_events->first());
        }
        delete m_log_events->takeFirst();
	}
    mutex().unlock();
}

void LogEvent::registerAddLogEventCallback(LogEventCallback cb) {
    mutex().lock();
    maybeInitLogEventList();
    m_add_log_event_callbacks->append(cb);
    mutex().unlock();
}

void LogEvent::registerRemoveLogEventCallback(LogEventCallback cb) {
    mutex().lock();
    maybeInitLogEventList();
    m_remove_log_event_callbacks->append(cb);
    mutex().unlock();
}

void LogEvent::maybeInitLogEventList() {
	if (m_log_events==0) {
		m_log_events = new LogEventList;
        m_add_log_event_callbacks = new LogEventCallbackList;
        m_remove_log_event_callbacks = new LogEventCallbackList;
    }
}
