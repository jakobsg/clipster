#ifndef LOGEVENT_H
#define LOGEVENT_H

#include "crashlib_global.h"
#include "customevent.h"
#include <QJsonObject>
#include <QMutex>
#include <QMap>

class LogEvent;

typedef QList<LogEvent *> LogEventList;

typedef bool (*LogEventCallback)(const LogEvent *le);
typedef QList<LogEventCallback> LogEventCallbackList;

class CRASHLIBSHARED_EXPORT LogEvent : public CustomEvent {

public:
	enum LogLevel {
		LOG_FATAL = 1,
		LOG_ERROR = 2,
		LOG_WARNING = 4,
		LOG_INFO = 8,
		LOG_DEBUG = 16,
		LOG_TRACE = 32
	};

	LogEvent(const QString &fname, const QString &filename, int line, const QString &msg, LogLevel lvl);
	static void addLogEvent(LogEvent *le);
    static void registerAddLogEventCallback(LogEventCallback cb);
    static void registerRemoveLogEventCallback(LogEventCallback cb);
    QJsonObject toJson();
	static LogEventList &logEvents();
    QString m_msg;
    LogLevel m_loglevel;

private:
	static QMutex &mutex();
	static void maybeInitLogEventList();
	static LogEventList *m_log_events;
    static LogEventCallbackList *m_add_log_event_callbacks;
    static LogEventCallbackList *m_remove_log_event_callbacks;
    static QMutex *m_mutex;
};

LogEvent CRASHLIBSHARED_EXPORT *newLogEvent(const QString &fname, const QString &filename, int line, const QString &msg, LogEvent::LogLevel lvl);

#endif // LOGEVENT_H
