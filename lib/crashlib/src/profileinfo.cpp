#include "profileinfo.h"
#include <QJsonValue>
#include <QJsonArray>

ProfileInfo::ProfileInfo(const CallEvent &cevent) {
    m_fname = cevent.m_fname;
    addCallEvent(cevent);
}


ProfileInfo::~ProfileInfo() {
}

void ProfileInfo::addCallEvent(const CallEvent &cevent) {
    m_durations.append(cevent.m_duration);
}


QJsonObject ProfileInfo::toJson() {
    QJsonObject jobj;
    jobj["fname"] = m_fname;
    jobj["durations"] = QJsonArray::fromVariantList(m_durations);
    return jobj;
}
