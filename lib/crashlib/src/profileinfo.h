#ifndef PROFILEINFO_H
#define PROFILEINFO_H

#include "callevent.h"
#include <QString>
#include <QJsonObject>
#include <QList>
#include <QVariant>

class ProfileInfo {

public:
	ProfileInfo(const CallEvent &cevent);
    ~ProfileInfo();

	void addCallEvent(const CallEvent &cevent);
	QJsonObject toJson();

    QString m_fname;
    QList<QVariant> m_durations;

};

#endif
