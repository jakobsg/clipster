#include <windows.h>
#include "qstackwalker.h"
#include "../crashlib.h"
#include "../calllogger.h"
#include <signal.h>
#include <DbgHelp.h>
#include <iostream>
#include <qdebug.h>
#include <QStringList>
#include <stdio.h>

void collect_crash_info(int sig, QStringList &simple_callstack, QStringList &detailed_callstack) {
    unsigned int   i;
    void         * stack[ 100 ];
    unsigned short frames;
    SYMBOL_INFO  * symbol;
    HANDLE         process;
    char dbg_line[4096];

    process = GetCurrentProcess();
    SymInitialize( process, NULL, TRUE );
    frames               = CaptureStackBackTrace( 0, 100, stack, NULL );
    symbol               = ( SYMBOL_INFO * )calloc( sizeof( SYMBOL_INFO ) + 256 * sizeof( char ), 1 );
    symbol->MaxNameLen   = 255;
    symbol->SizeOfStruct = sizeof( SYMBOL_INFO );

    for( i = 0; i < frames; i++ ) {
        SymFromAddr( process, ( DWORD64 )( stack[ i ] ), 0, symbol );
        _sprintf_p( dbg_line, 4096, "%i: %s - 0x%0X\0", frames - i - 1, symbol->Name, symbol->Address );
        simple_callstack.append(dbg_line);
    }
    free( symbol );

    QStackWalker sw(detailed_callstack);
    sw.ShowCallstack(GetCurrentThread());
}

void seg_handler( int sig ) {
    QStringList simple_callstack;
    QStringList detailed_callstack;
    collect_crash_info(sig, simple_callstack, detailed_callstack);
    exit(1);
}


void std_handler( void ) {
     seg_handler(SIGSTDE);
}

LONG WINAPI UnhandledCrashHandler(EXCEPTION_POINTERS * /*ExceptionInfo*/)
{
	qDebug("sdfiohsiodf iuwhe iwe");

	return EXCEPTION_EXECUTE_HANDLER;
}
