#include "qstackwalker.h"

QStackWalker::QStackWalker(QStringList &stringlist) :
m_output(stringlist) {

}

QStackWalker::~QStackWalker() {

}

void QStackWalker::OnOutput(LPCSTR szText) {
    QString dbgline(szText);
    m_output.append(dbgline.trimmed());
    StackWalker::OnOutput(szText);
}
