#ifndef QSTACKWALKER_H
#define QSTACKWALKER_H

#include "StackWalker.h"
#include <QStringList>

class QStackWalker : public StackWalker
{
public:
    QStackWalker(QStringList &stringlist);
    ~QStackWalker();
protected:
    virtual void OnOutput(LPCSTR szText);
private:
    QStringList &m_output;
};

#endif // QSTACKWALKER_H
