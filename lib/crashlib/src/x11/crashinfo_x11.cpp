#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <QStringList>

void seg_handler(int sig) {
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
}

#include <execinfo.h>
#include <errno.h>
#include <cxxabi.h>

static inline void printStackTrace( QStringList &callstack, unsigned int max_frames = 63 )
{
    // storage array for stack trace address data
    void* addrlist[max_frames+1];

    // retrieve current stack addresses
    unsigned int addrlen = backtrace( addrlist, sizeof( addrlist ) / sizeof( void* ));

    if ( addrlen == 0 ) {
        return;
    }

    // resolve addresses into strings containing "filename(function+address)",
    // Actually it will be ## program address function + offset
    // this array must be free()-ed
    char** symbollist = backtrace_symbols( addrlist, addrlen );

    size_t funcnamesize = 1024;
    char funcname[1024];
    char stackline[1100];

    // iterate over the returned symbol lines. skip the first, it is the
    // address of this function.
    for ( unsigned int i = 4; i < addrlen; i++ ) {
        char* begin_name   = NULL;
        char* begin_offset = NULL;
        char* end_offset   = NULL;
        // find parentheses and +address offset surrounding the mangled name
#ifdef DARWIN_OS
        // OSX style stack trace
        for ( char *p = symbollist[i]; *p; ++p ) {
            if (( *p == '_' ) && ( *(p-1) == ' ' ))
                begin_name = p-1;
            else if ( *p == '+' )
                begin_offset = p-1;
        }

        if ( begin_name && begin_offset && ( begin_name < begin_offset )) {
            *begin_name++ = '\0';
            *begin_offset++ = '\0';

            // mangled name is now in [begin_name, begin_offset) and caller
            // offset in [begin_offset, end_offset). now apply
            // __cxa_demangle():
            int status;
            char* ret = abi::__cxa_demangle( begin_name, &funcname[0],
                    &funcnamesize, &status );
            if ( status == 0 ) {
                snprintf(funcname, 1024, ret); // use possibly realloc()-ed string
                snprintf( stackline, 1100,"  %-30s %-40s %s",
                          symbollist[i], funcname, begin_offset );
                callstack << stackline;
            } else {
                // demangling failed. Output function name as a C function with
                // no arguments.
                snprintf( stackline, 1100, "  %-30s %-38s() %s",
                          symbollist[i], begin_name, begin_offset );
                callstack << stackline;
            }

#else // !DARWIN - but is posix
        // not OSX style
        // ./module(function+0x15c) [0x8048a6d]
        for ( char *p = symbollist[i]; *p; ++p ) {
            if ( *p == '(' )
                begin_name = p;
            else if ( *p == '+' )
                begin_offset = p;
            else if ( *p == ')' && ( begin_offset || begin_name ))
                end_offset = p;
        }

        if ( begin_name && end_offset && ( begin_name < end_offset )) {
            *begin_name++   = '\0';
            *end_offset++   = '\0';
            if ( begin_offset )
                *begin_offset++ = '\0';

            // mangled name is now in [begin_name, begin_offset) and caller
            // offset in [begin_offset, end_offset). now apply
            // __cxa_demangle():

            int status = 0;
            char* ret = abi::__cxa_demangle( begin_name, funcname,
                                             &funcnamesize, &status );
            char* fname = begin_name;
            if ( status == 0 )
                fname = ret;

            if ( begin_offset ) {
                snprintf( stackline, 1100, "  %-30s ( %-40s  + %-6s) %s",
                         symbollist[i], fname, begin_offset, end_offset );
                callstack << stackline;
            } else {
                snprintf( stackline, 1100, "  %-30s ( %-40s    %-6s) %s",
                         symbollist[i], fname, "", end_offset );
                callstack << stackline;
            }
#endif  // !DARWIN - but is posix
        } else {
            // couldn't parse the line? print the whole line.
            snprintf(stackline, 1100, "  %-40s", symbollist[i]);
            callstack << stackline;
        }
    }

    free(symbollist);
}

void std_handler( void ) {
    void *trace_elems[20];
    int trace_elem_count(backtrace( trace_elems, 20 ));
    char **stack_syms(backtrace_symbols( trace_elems, trace_elem_count ));
    for ( int i = 0 ; i < trace_elem_count ; ++i ) {
        std::cout << stack_syms[i] << "\n";
    }
    free( stack_syms );
}

void collect_crash_info(int sig, QStringList &simple_callstack, QStringList &detailed_callstack) {
    void *trace_elems[20];
    int trace_elem_count(backtrace( trace_elems, 20 ));
    char **stack_syms(backtrace_symbols( trace_elems, trace_elem_count ));
    for ( int i = 0 ; i < trace_elem_count ; ++i ) {
        simple_callstack << stack_syms[i];
    }
    free( stack_syms );
    printStackTrace(detailed_callstack);
}
