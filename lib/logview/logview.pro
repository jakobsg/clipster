#-------------------------------------------------
#
# Project created by QtCreator 2015-08-03T19:33:14
#
#-------------------------------------------------

QT += gui widgets network

TARGET = logview
DESTDIR = bin
TEMPLATE = lib

SOURCES += src/logview.cpp \
    src/logeventmodel.cpp

HEADERS += src/logview.h \
    src/logeventmodel.h

INCLUDEPATH += ../crashlib/include

LIBS += \
    -L../crashlib/bin -lcrashlib

unix {
    target.path = /usr/lib
    INSTALLS += target
}
win32 {
    DEFINES += LOGVIEW_LIBRARY
    QMAKE_CFLAGS_RELEASE += -Zi
    QMAKE_CXXFLAGS_RELEASE += -Zi
    QMAKE_LFLAGS_RELEASE += /DEBUG /OPT:REF
    QMAKE_CXXFLAGS_EXCEPTIONS_ON = /EHa
    QMAKE_CXXFLAGS_STL_ON = /EHa
}
!win32 {
}

unix:!macx {
    DEFINES += LINUX_OS
}
macx {
    DEFINES += DARWIN_OS
}
win32 {
    DEFINES += WINDOWS_OS
}
