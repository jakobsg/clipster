#include "logeventmodel.h"
#include <QColor>
#include <QDateTime>

LogEventModel::LogEventModel(QObject *parent)
: QAbstractTableModel(parent) {

}

int LogEventModel::rowCount(const QModelIndex &idx) const {
    return m_log_events.count();
}

int LogEventModel::columnCount(const QModelIndex &idx) const {
    return 6;
}

QVariant LogEventModel::headerData(int section, Qt::Orientation orientation, int role) const {
    Q_UNUSED(role)
    if (role!=Qt::DisplayRole) {
        return QVariant();
    }
    if (orientation==Qt::Horizontal) {
        if (section==0) {
            return "Timestamp";
        }
        else if (section==1) {
            return "Level";
        }
        else if (section==2) {
            return "Message";
        }
        else if (section==3) {
            return "Filename";
        }
        else if (section==4) {
            return "Line No";
        }
        else if (section==5) {
            return "Thread ID";
        }
    }
    return section;
}

QVariant LogEventModel::data(const QModelIndex &index, int role) const {
    const LogEvent *lv = m_log_events[index.row()];
    if (role==Qt::DisplayRole) {
        if (index.column()==0) {
            return QDateTime::fromMSecsSinceEpoch(lv->m_time).toString("yyyy-MM-dd HH:mm:ss.zzz");
        }
        else if (index.column()==1) {
            switch (lv->m_loglevel) {
                case LogEvent::LOG_FATAL:
                    return "FATAL";
                case LogEvent::LOG_ERROR:
                    return "ERROR";
                case LogEvent::LOG_WARNING:
                    return "WARNING";
                case LogEvent::LOG_INFO:
                    return "INFO";
                case LogEvent::LOG_DEBUG:
                    return "DEBUG";
                case LogEvent::LOG_TRACE:
                    return "TRACE";
            }
        }
        else if (index.column()==2) {
            return lv->m_msg;
        }
        else if (index.column()==3) {
            return lv->m_filename;
        }
        else if (index.column()==4) {
            return lv->m_line;
        }
        else if (index.column()==5) {
            return QString::number((qulonglong) lv->m_thread_id);
        }
        return "";
    }
    else if (role==Qt::ForegroundRole) {
        if (lv->m_loglevel==LogEvent::LOG_FATAL) {
            return QVariant(QColor(Qt::red));
        }
        else if (lv->m_loglevel==LogEvent::LOG_ERROR) {
            return QVariant(QColor(Qt::red));
        }
        else if (lv->m_loglevel==LogEvent::LOG_WARNING) {
            return QVariant(QColor(255,204,0));
        }
        else if (lv->m_loglevel==LogEvent::LOG_INFO) {
            return QVariant(QColor(0,0,0));
        }
        else if (lv->m_loglevel==LogEvent::LOG_DEBUG) {
            return QVariant(QColor(153,153,153));
        }
        else if (lv->m_loglevel==LogEvent::LOG_TRACE) {
            return QVariant(QColor(153,153,100));
        }
    }
    return QVariant();
}

void LogEventModel::addLogEvent(const LogEvent *lv) {
    int idx = m_log_events.count();
    beginInsertRows(QModelIndex(),idx,idx);
    m_log_events.push_back(lv);
    endInsertRows();
}

void LogEventModel::removeLogEvent(const LogEvent *lv) {
    int idx = m_log_events.indexOf(lv);
    if (idx>-1) {
        beginRemoveRows(QModelIndex(),idx,idx);
        removeRow(idx);
        m_log_events.takeAt(idx);
        endRemoveRows();
    }
}

