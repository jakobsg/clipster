#ifndef LOGEVENTMODEL_H
#define LOGEVENTMODEL_H

#include <crashlib.h>

#include <QAbstractTableModel>

typedef QList<const LogEvent*> ConstLogEventList;

class LogEventModel : public QAbstractTableModel
{
public:
    LogEventModel(QObject *parent = 0);
    int rowCount(const QModelIndex &idx) const;
    int columnCount(const QModelIndex &idx) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

    void addLogEvent(const LogEvent *lv);
    void removeLogEvent(const LogEvent *lv);

signals:

public slots:

private:
    ConstLogEventList m_log_events;
};

#endif // LOGEVENTMODEL_H
