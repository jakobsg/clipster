#include "logview.h"
#include <QHeaderView>
#include <QScrollBar>
#include <QApplication>
#include <QDesktopWidget>

LogView *LogView::s_instance = 0;
bool hooks_installed = false;

bool add_log_event(const LogEvent *le) {
    if (!LogView::hasInstance()) {
        return false;
    }
    QTableView *tv = LogView::instance()->logTableView();
    bool table_at_bottom = tv->verticalScrollBar()->maximum() == tv->verticalScrollBar()->sliderPosition();
    LogView::instance()->logTableModel()->addLogEvent(le);
    for (int i=0; i<6; i++) {
        if (i==2) {
            continue;
        }
        tv->resizeColumnToContents(i);
    }
    if (table_at_bottom) {
        tv->scrollToBottom();
    }
    return true;
}

bool remove_log_event(const LogEvent *le) {
    if (!LogView::hasInstance()) {
        return true;
    }
    LogView::instance()->logTableModel()->removeLogEvent(le);
    return true;
}

LogView *LogView::instance() {
    if (s_instance==0) {
        s_instance = new LogView();
    }
    return s_instance;
}

bool LogView::hasInstance() {
    return s_instance!=0;
}

void LogView::toggleVisible() {
    if (s_instance) {
        s_instance->close();
    }
    else {
        LogView *lv = instance();
        lv->setVisible(true);
        lv->raise();
        lv->resize(1024,800);
        lv->move(QApplication::desktop()->screen()->rect().center() - lv->rect().center());
        lv->raise();
    }
}

void LogView::closeEvent(QCloseEvent *ce) {
    s_instance->destroy();
    s_instance = 0;
}

LogView::LogView(QWidget *parent)
: QWidget(parent) {
    setWindowTitle(tr("Clipster Log View"));
    if (!hooks_installed) {
        LogEvent::registerAddLogEventCallback(add_log_event);
        LogEvent::registerRemoveLogEventCallback(remove_log_event);
        hooks_installed = true;
    }
    m_logtable = new QTableView(this);
    m_logmodel = new LogEventModel(this);
    m_logtable->setModel(m_logmodel);
    m_layout = new QGridLayout(this);
    m_wraptext = new QCheckBox(tr("Wrap text"),this);
    connect(m_wraptext, SIGNAL(toggled(bool)),this,SLOT(enableWordWrap(bool)));
    m_layout->addWidget(m_wraptext,0,0,1,1);
    m_layout->addWidget(m_logtable,1,0,1,1);
    m_logtable->verticalHeader()->setHidden(true);
    m_logtable->setWordWrap(false);
    m_logtable->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
    m_logtable->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    m_logtable->horizontalHeader()->setSectionResizeMode(2,QHeaderView::Stretch);
    m_logtable->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_logtable->setSelectionMode(QAbstractItemView::ExtendedSelection);
}

LogEventModel *LogView::logTableModel() {
    return m_logmodel;
}

QTableView *LogView::logTableView() {
    return m_logtable;
}

void LogView::enableWordWrap(bool enable) {
    m_logtable->setWordWrap(enable);
}
