#ifndef LOGVIEW_H
#define LOGVIEW_H

#include "logview_global.h"
#include "logeventmodel.h"

#include <crashlib.h>

#include <QWidget>
#include <QTableView>
#include <QLayout>
#include <QCheckBox>

class LOGVIEWSHARED_EXPORT LogView : public QWidget {

    Q_OBJECT

public:
    static LogView *instance();
    static bool hasInstance();
    static void toggleVisible();
    LogEventModel *logTableModel();
    QTableView *logTableView();

public slots:
    void enableWordWrap(bool enable);

protected:
    void closeEvent(QCloseEvent *ce);

private:
    LogView(QWidget *parent=0);
    QTableView *m_logtable;
    QGridLayout *m_layout;
    LogEventModel *m_logmodel;
    QCheckBox *m_wraptext;
    static LogView *s_instance;

};

#endif // LOGVIEW_H
