#include "widget.h"
#include <QApplication>
#include <QStyle>
#include <QStyleFactory>
#include <QDirIterator>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QStyle *style = QStyleFactory::create("windows");
    QApplication::setStyle(style);
    Widget w;
    w.show();
    return a.exec();
}
