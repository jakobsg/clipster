#include "mediatypeoverview.h"
#include "ui_mediatypeoverview.h"
#include <QDirIterator>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>


MediaTypeOverview::MediaTypeOverview(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MediaTypeOverview)
{
    m_current_mrs = 0;
    ui->setupUi(this);
    QListWidgetItem *item = new QListWidgetItem(QIcon(""),NULL,ui->m_icon_list);
    item->setData(Qt::ToolTipRole, tr("No selection"));
    item->setData(Qt::UserRole, "");
    ui->m_icon_list->addItem(item);
    ui->m_icon_list->setResizeMode(QListView::Adjust);
    QDirIterator it(":/cliplib-images", QDirIterator::Subdirectories);
    while (it.hasNext()) {
        QString icon = it.next();
        item = new QListWidgetItem(QIcon(icon),NULL,ui->m_icon_list);
        item->setData(Qt::ToolTipRole, icon);
        item->setData(Qt::UserRole, icon);
        ui->m_icon_list->addItem(item);
    }
    QFile jsonFile("mt-matchrules.json");
    ui->m_mediatype_name->setText("");
    if (jsonFile.open(QFile::ReadOnly)) {
        qDebug("loading from overview");
        QJsonDocument json_rulesets = QJsonDocument::fromJson(jsonFile.readAll());
        foreach (QJsonValue val, json_rulesets.array()) {
            ClipsterCore::MediaTypeMatchRuleSet *mrs = new ClipsterCore::MediaTypeMatchRuleSet();
            mrs->fromJson(val.toObject());
            m_rulesets[mrs->mediaTypeName()] = mrs;
            QTreeWidgetItem *titem = new QTreeWidgetItem(ui->m_mediatype_tree);
            titem->setText(0,mrs->mediaTypeName());
            titem->setIcon(0,QIcon(mrs->iconResourceUrl()));
            titem->setData(0, Qt::UserRole, qVariantFromValue((void *) mrs));
            ui->m_mediatype_tree->addTopLevelItem(titem);
        }
    }
    connect(ui->m_mediatype_tree, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)),this,SLOT(mediaTypeItemActivated(QTreeWidgetItem*)));
    connect(ui->m_icon_list, SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)),this,SLOT(iconCurrentItemChanged(QListWidgetItem*)));
    connect(ui->m_mediatype_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged()));
    connect(ui->m_pure_image, SIGNAL(toggled(bool)), this, SLOT(isPureImageToggled(bool)));
    connect(ui->m_file_list, SIGNAL(toggled(bool)), this, SLOT(isFileListToggled(bool)));
    connect(ui->m_ok_button, SIGNAL(clicked(bool)),this,SLOT(save()));
}

MediaTypeOverview::~MediaTypeOverview() {
    delete ui;
}

void MediaTypeOverview::mediaTypeItemActivated(QTreeWidgetItem *item) {
    ClipsterCore::MediaTypeMatchRuleSet *mrs = (ClipsterCore::MediaTypeMatchRuleSet *) item->data(0,Qt::UserRole).value<void *>();
    m_current_mrs = mrs;
    ui->m_mediatype_name->setText(mrs->mediaTypeName());
    ui->m_pure_image->setChecked(mrs->isPureImage());
    ui->m_file_list->setChecked(mrs->isFileList());
    bool found = false;
    for (int idx=0; idx<ui->m_icon_list->count(); idx++) {
        QListWidgetItem *icon = ui->m_icon_list->item(idx);
        if (icon->data(Qt::UserRole).toString()==mrs->iconResourceUrl()) {
            ui->m_icon_list->setCurrentRow(idx);
            found = true;
            break;
        }
    }
    if (!found) {
        ui->m_icon_list->setCurrentRow(-1);
    }
}

void MediaTypeOverview::iconCurrentItemChanged(QListWidgetItem *current) {
    qDebug("%s", current->data(Qt::UserRole).toString().toUtf8().data());
    if (m_current_mrs) {
        m_current_mrs->setIconResourceUrl(current->data(Qt::UserRole).toString());
        ui->m_mediatype_tree->currentItem()->setIcon(0,QIcon(current->data(Qt::UserRole).toString()));
    }
}

void MediaTypeOverview::nameChanged() {
    m_current_mrs->setMediaTypeName(ui->m_mediatype_name->text());
}

void MediaTypeOverview::isPureImageToggled(bool toggle) {
    m_current_mrs->setIsPureImage(toggle);
}

void MediaTypeOverview::isFileListToggled(bool toggle) {
    m_current_mrs->setIsFileList(toggle);
}

void MediaTypeOverview::save() {
    QJsonArray a;
    foreach (const ClipsterCore::MediaTypeMatchRuleSet *mrs, m_rulesets) {
        a.append(mrs->serialize());
    }

    QJsonDocument doc(a);
    QFile f("mt-matchrules.json");
    if (f.open(QFile::WriteOnly)) {
        qDebug("saving from overview");
        f.write(doc.toJson(QJsonDocument::Indented));
    }
    accept();
}
