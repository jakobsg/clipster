#ifndef MEDIATYPEOVERVIEW_H
#define MEDIATYPEOVERVIEW_H

#include <QDialog>
#include <mediatype.h>
class QTreeWidgetItem;
class QListWidgetItem;

namespace Ui {
class MediaTypeOverview;
}

class MediaTypeOverview : public QDialog
{
    Q_OBJECT

public:
    explicit MediaTypeOverview(QWidget *parent = 0);
    ~MediaTypeOverview();

public slots:
    void mediaTypeItemActivated(QTreeWidgetItem * item);
    void iconCurrentItemChanged(QListWidgetItem * current);
    void nameChanged();
    void isPureImageToggled(bool toggle);
    void isFileListToggled(bool toggle);
    void save();

private:
    Ui::MediaTypeOverview *ui;
    QMap<QString, ClipsterCore::MediaTypeMatchRuleSet *> m_rulesets;
    ClipsterCore::MediaTypeMatchRuleSet *m_current_mrs;
};

#endif // MEDIATYPEOVERVIEW_H
