#-------------------------------------------------
#
# Project created by QtCreator 2015-06-19T12:01:01
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = mt-manager
TEMPLATE = app


SOURCES += main.cpp\
    widget.cpp \
    mediatypeoverview.cpp

HEADERS  += widget.h \
    mediatypeoverview.h

FORMS    += widget.ui \
    mediatypeoverview.ui

RESOURCES +=

DISTFILES += \
    mt-matchrules.json

LIBS += \
        -L../lib/cliplib/bin -lcliplib -L../lib/crashlib/bin -lcrashlib

INCLUDEPATH += \
    ../lib/cliplib/include
