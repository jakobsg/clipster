#include "widget.h"
#include "mediatypeoverview.h"
#include <mediatype.h>
#include "ui_widget.h"
#include <QMimeData>
#include <QFile>
#include <QImage>
#include <QClipboard>
#include <QMimeData>
#include <QGroupBox>
#include <QLayout>
#include <QPushButton>
#include <QFontMetrics>
#include <QMimeType>
#include <QLabel>
#include <QTreeWidgetItem>
#include <QJsonArray>
#include <QMessageBox>
#include <QFile>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget) {
    ui->setupUi(this);

    connect(QApplication::clipboard(), SIGNAL(dataChanged()), this, SLOT(detectDataType()));
    connect(ui->detection_tree,SIGNAL(itemChanged(QTreeWidgetItem*,int)),this,SLOT(itemChanged(QTreeWidgetItem*,int)));
    connect(ui->copy_tree,SIGNAL(itemChanged(QTreeWidgetItem*,int)),this,SLOT(itemChanged(QTreeWidgetItem*,int)));
    connect(ui->m_detection_rules, SIGNAL(cellChanged(int,int)),this,SLOT(detectionCellChanged(int,int)));
    connect(ui->m_copy_rules, SIGNAL(cellChanged(int,int)),this,SLOT(copyCellChanged(int,int)));
    connect(ui->m_save_button, SIGNAL(clicked(bool)), this, SLOT(saveMediaTypeRuleSet()));
    connect(ui->m_mediatype_name, SIGNAL(textChanged(QString)), this, SLOT(updateSaveButton()));
    connect(ui->m_overview, SIGNAL(clicked(bool)), this, SLOT(showMediaTypeOverview()));
}

Widget::~Widget() {
    foreach (ClipsterCore::MediaTypeMatchRule *mr, m_detection_rules) {
        delete mr;
    }
    foreach (ClipsterCore::MediaTypeMatchRuleSet *mrs, m_rulesets.values()) {
        delete mrs;
    }
    delete ui;
}

void Widget::testFunc() {
//    QMimeData d;
//    QFile f("/home/jakob/dev/limbosoft/bus.jpg");
//    if (f.open(QFile::ReadOnly)) {
//        QImage i("/home/jakob/dev/limbosoft/ball.png");
//        qDebug("Has alpha: %d", i.hasAlphaChannel());
//        d.setData("image/jpg",f.readAll());
//    }
//    qDebug("has image: %s", d.hasImage() ? "yes":"no");
    ClipsterCore::MediaTypeMatchRuleSet rs;
    rs.addDetectionRule(new ClipsterCore::MediaTypeMatchRule("text","html"));
    rs.addDetectionRule(new ClipsterCore::MediaTypeMatchRule("application","x-openoffice-objectdescriptor-xml","width"));
    rs.addDetectionRule(new ClipsterCore::MediaTypeMatchRule("application","x-openoffice-objectdescriptor-xml"));
    rs.addDetectionRule(new ClipsterCore::MediaTypeMatchRule("application","x-openoffice-objectdescriptor-xml","typename","\"calc*\""));
}

void Widget::detectDataType() {
    const QMimeData *md = QApplication::clipboard()->mimeData();
    m_current_formats = md->formats();
    foreach (const ClipsterCore::MediaTypeMatchRule *mr, m_selected_detection_rules) {
        delete mr;
    }
    m_selected_detection_rules.clear();
    foreach (const ClipsterCore::MediaTypeMatchRule *mr, m_selected_copy_rules) {
        delete mr;
    }
    m_selected_copy_rules.clear();
    updateTree(ui->detection_tree,m_detection_rules);
    updateTree(ui->copy_tree, m_copy_rules);
    ui->m_save_button->setEnabled(false);

    foreach (ClipsterCore::MediaTypeMatchRuleSet *mrs, m_rulesets.values()) {
        delete mrs;
    }
    m_rulesets.clear();
    QFile jsonFile("mt-matchrules.json");
    ui->m_mediatype_name->setText("");
    if (jsonFile.open(QFile::ReadOnly)) {
        qDebug("loading from main");
        m_json_rulesets = QJsonDocument::fromJson(jsonFile.readAll());
        foreach (QJsonValue val, m_json_rulesets.array()) {
            ClipsterCore::MediaTypeMatchRuleSet *mrs = new ClipsterCore::MediaTypeMatchRuleSet();
            mrs->fromJson(val.toObject());
            m_rulesets[mrs->mediaTypeName()] = mrs;
        }
    }
    ClipsterCore::MediaTypeSet mdp(m_current_formats);
    QList<const ClipsterCore::MediaTypeMatchRuleSet *> matches;
    foreach (ClipsterCore::MediaTypeMatchRuleSet *mrs, m_rulesets) {
        bool match = mdp.checkMatchRules(mrs->detectionRules());
        if (match) {
            matches.append(mrs);
        }
    }
    float score = 0.0;
    const ClipsterCore::MediaTypeMatchRuleSet *selected_msr = 0;
    foreach (const ClipsterCore::MediaTypeMatchRuleSet *mrs, matches) {
        if (mrs->score()>score) {
            score = mrs->score();
            selected_msr = mrs;
        }
    }
    if (selected_msr) {
        const ClipsterCore::MediaTypeMatchRuleSet *mrs = selected_msr;
        ui->m_mediatype_name->setText(mrs->mediaTypeName());
        ui->m_pure_image->setChecked(selected_msr->isPureImage());
        ui->m_file_list->setChecked(selected_msr->isFileList());
        m_icon_resource_url = selected_msr->iconResourceUrl();
        QPixmap icon(selected_msr->iconResourceUrl());
        icon = icon.scaled(QSize(32,32),Qt::KeepAspectRatio,Qt::SmoothTransformation);
        ui->m_image_label->setPixmap(icon);
        markupTreeWidget(ui->detection_tree, mrs->detectionRules());
        foreach (const ClipsterCore::MediaTypeMatchRule *mr, mrs->detectionRules()) {
            m_selected_detection_rules.append(new ClipsterCore::MediaTypeMatchRule(*mr));
            updateSelectionTable(ui->m_detection_rules, m_selected_detection_rules);
        }
    }
}

void Widget::updateTree(QTreeWidget *targettree, QList<ClipsterCore::MediaTypeMatchRule *> &rules_list) {
    targettree->clear();
    foreach (ClipsterCore::MediaTypeMatchRule *mr, rules_list) {
        delete mr;
    }
    rules_list.clear();
    ClipsterCore::MediaTypeSet mdp(m_current_formats);
    foreach (ClipsterCore::MediaType *mt, mdp.mediaTypes()) {
        QTreeWidgetItem *mtype = new QTreeWidgetItem(targettree);
        mtype->setText(0,mt->fulltype());
        mtype->setFlags(mtype->flags()|Qt::ItemIsUserCheckable);
        mtype->setCheckState(0,Qt::Unchecked);
        ClipsterCore::MediaTypeMatchRule *mr = new ClipsterCore::MediaTypeMatchRule(mt->toplevelType(),mt->subType());
        mtype->setData(0, Qt::UserRole, qVariantFromValue((void *) mr));
        rules_list.append(mr);
        foreach (QString key, mt->paramKeys()) {
            QString val = mt->parameters()[key];
            QTreeWidgetItem *param = new QTreeWidgetItem();
            param->setText(0,key);
            param->setText(1,val);
            param->setFlags(param->flags()|Qt::ItemIsUserCheckable);
            param->setCheckState(0,Qt::Unchecked);
            param->setCheckState(1,Qt::Unchecked);
            ClipsterCore::MediaTypeMatchRule *mr_on_pkey = new ClipsterCore::MediaTypeMatchRule(mt->toplevelType(),mt->subType(),key);
            param->setData(0, Qt::UserRole, qVariantFromValue((void *) mr_on_pkey));
            rules_list.append(mr_on_pkey);
            ClipsterCore::MediaTypeMatchRule *mr_on_pval = new ClipsterCore::MediaTypeMatchRule(mt->toplevelType(),mt->subType(),key, val);
            param->setData(1, Qt::UserRole, qVariantFromValue((void *) mr_on_pval));
            rules_list.append(mr_on_pval);
            mtype->addChild(param);
        }
        targettree->addTopLevelItem(mtype);
    }
    targettree->resizeColumnToContents(0);
    targettree->expandAll();
}

void Widget::updateSaveButton() {
    ClipsterCore::MediaTypeSet mdp(m_current_formats);
    bool ready_to_save = m_selected_detection_rules.count()>0 &&
        mdp.checkMatchRules(m_selected_detection_rules) &&
        mdp.checkMatchRules(m_selected_copy_rules) &&
        !ui->m_mediatype_name->text().isEmpty();
    ui->m_save_button->setEnabled(ready_to_save);
}

void Widget::showMediaTypeOverview()
{
    MediaTypeOverview dlg(this);
    dlg.show();
    dlg.exec();
    detectDataType();
}

void Widget::itemChanged(QTreeWidgetItem *item, int column) {
    QTableWidget *target_rules_table = ui->m_detection_rules;
    QList<const ClipsterCore::MediaTypeMatchRule *> *selection_rules = &m_selected_detection_rules;
    QLabel *target_rules_table_check = ui->m_detection_rules_check;
    if (item->treeWidget()==ui->copy_tree) {
        target_rules_table = ui->m_copy_rules;
        selection_rules = &m_selected_copy_rules;
        target_rules_table_check = ui->m_copy_rules_check;
    }
    ClipsterCore::MediaTypeMatchRule *mr = (ClipsterCore::MediaTypeMatchRule *) item->data(column,Qt::UserRole).value<void *>();

    if (item->checkState(column) == Qt::Checked) {
        selection_rules->append(new ClipsterCore::MediaTypeMatchRule(*mr));
    }
    else {
        foreach (const ClipsterCore::MediaTypeMatchRule *mr2, *selection_rules) {
            if (*mr==*mr2) {
                selection_rules->removeOne(mr2);
                delete mr2;
                break;
            }
        }
    }
    updateSelectionTable(target_rules_table, *selection_rules);
    ClipsterCore::MediaTypeSet mdp(m_current_formats);
    bool match = mdp.checkMatchRules(*selection_rules);
    target_rules_table_check->setText(
        match ? tr("<font color=\"#008000\">Rules match successful</font>") :
                tr("<font color=\"#800000\">Rules match failed</font>"));
    updateSaveButton();
}

void Widget::detectionCellChanged(int row, int column) {
    rulesCellChanged(row, column, false);
}

void Widget::copyCellChanged(int row, int column) {
    rulesCellChanged(row, column, true);
}

void Widget::saveMediaTypeRuleSet() {
    ClipsterCore::MediaTypeMatchRuleSet *ms = new ClipsterCore::MediaTypeMatchRuleSet();
    ms->setMediaTypeName(ui->m_mediatype_name->text());
    ms->setDetectionRules(m_selected_detection_rules);
    ms->setIsPureImage(ui->m_pure_image->isChecked());
    ms->setIsFileList(ui->m_file_list->isChecked());
    ms->setIconResourceUrl(m_icon_resource_url);
    QList <const ClipsterCore::MediaTypeMatchRule *> copy_rules;
    copy_rules.append(m_selected_detection_rules);
    copy_rules.append(m_selected_copy_rules);
    ms->setCopyRules(copy_rules);
    m_rulesets[ms->mediaTypeName()] = ms;
    QJsonArray a;
    foreach (const ClipsterCore::MediaTypeMatchRuleSet *mrs, m_rulesets) {
        a.append(mrs->serialize());
    }

    QJsonDocument doc(a);
    QFile f("mt-matchrules.json");
    if (f.open(QFile::WriteOnly)) {
        qDebug("saving from main");
        f.write(doc.toJson(QJsonDocument::Indented));
    }
    // REMEMBER OVERRIDE m_ruleset version and delete the old one which is referenced only by this
}

void Widget::rulesCellChanged(int row, int column, bool copy_rules_change) {
    QTableWidget *target_rules_table = ui->m_detection_rules;
    QTreeWidget *target_tree = ui->detection_tree;
    QList<const ClipsterCore::MediaTypeMatchRule *> *selection_rules = &m_selected_detection_rules;
    QLabel *target_rules_table_check = ui->m_detection_rules_check;
    if (copy_rules_change) {
        target_rules_table = ui->m_copy_rules;
        target_tree = ui->copy_tree;
        selection_rules = &m_selected_copy_rules;
        target_rules_table_check = ui->m_copy_rules_check;
    }
    ClipsterCore::MediaTypeMatchRule *mr = (ClipsterCore::MediaTypeMatchRule *) target_rules_table->item(row, 0)->data(Qt::UserRole).value<void *>();
    if (mr==0) {
        return;
    }
    if (column==0) {
        mr->setTopType(target_rules_table->item(row, column)->text());
    }
    else if (column==1) {
        mr->setSubType(target_rules_table->item(row, column)->text());
    }
    else if (column==2) {
        mr->setParameterName(target_rules_table->item(row, column)->text());
    }
    else if (column==3) {
        mr->setParameterValue(target_rules_table->item(row, column)->text());
    }

    ClipsterCore::MediaTypeSet mdp(m_current_formats);
    bool match = mdp.checkMatchRules(*selection_rules);
    target_rules_table_check->setText(
        match ? tr("<font color=\"#008000\">Rules match successful</font>") :
                tr("<font color=\"#800000\">Rules match failed</font>"));
    markupTreeWidget(target_tree, *selection_rules);
    updateSaveButton();
}

void Widget::markupTreeWidget(QTreeWidget *targettree, const QList<const ClipsterCore::MediaTypeMatchRule *> &rules, bool noitemchange) {
    if (noitemchange) {
        disconnect(targettree,SIGNAL(itemChanged(QTreeWidgetItem*,int)),this,SLOT(itemChanged(QTreeWidgetItem*,int)));
    }
    for (int idx=0; idx<targettree->topLevelItemCount(); idx++) {
        QTreeWidgetItem *item = targettree->topLevelItem(idx);
        bool found = false;
        ClipsterCore::MediaTypeMatchRule *mr11 = (ClipsterCore::MediaTypeMatchRule *) item->data(0,Qt::UserRole).value<void *>();
        foreach (const ClipsterCore::MediaTypeMatchRule *mr12, rules) {
            if (*mr11==*mr12) {
                found = true;
                break;
            }
        }
        item->setCheckState(0, found ? Qt::Checked : Qt::Unchecked);
        for (int cidx=0; cidx<item->childCount(); cidx++) {
            QTreeWidgetItem *citem = item->child(cidx);
            for (int colidx=0; colidx<citem->columnCount(); colidx++) {
                ClipsterCore::MediaTypeMatchRule *mr21 = (ClipsterCore::MediaTypeMatchRule *) citem->data(colidx,Qt::UserRole).value<void *>();
                found = false;
                foreach (const ClipsterCore::MediaTypeMatchRule *mr22, rules) {
                    if (*mr21==*mr22) {
                        found = true;
                        break;
                    }
                }
                citem->setCheckState(colidx, found ? Qt::Checked : Qt::Unchecked);
            }
        }
    }
    if (noitemchange) {
        connect(targettree,SIGNAL(itemChanged(QTreeWidgetItem*,int)),this,SLOT(itemChanged(QTreeWidgetItem*,int)));
    }
}

void Widget::updateSelectionTable(QTableWidget *target_rules_table, const QList<const ClipsterCore::MediaTypeMatchRule *> &rules) {
    target_rules_table->clearContents();
    target_rules_table->setRowCount(rules.count());
    int idx = 0;
    foreach (const ClipsterCore::MediaTypeMatchRule *smr, rules) {
        QTableWidgetItem *toptype = new QTableWidgetItem(smr->topType());
        QTableWidgetItem *subtype = new QTableWidgetItem(smr->subType());
        QTableWidgetItem *pkey = new QTableWidgetItem(smr->parameterName());
        QTableWidgetItem *vkey = new QTableWidgetItem(smr->parameterValue());
        target_rules_table->setItem(idx,0,toptype);
        target_rules_table->setItem(idx,1,subtype);
        target_rules_table->setItem(idx,2,pkey);
        target_rules_table->setItem(idx,3,vkey);
        toptype->setData(Qt::UserRole, qVariantFromValue((void *) smr));
        idx++;
    }
    target_rules_table->resizeColumnsToContents();
}
