#ifndef WIDGET_H
#define WIDGET_H

#include <mediatype.h>
#include <QWidget>
#include <QJsonDocument>
#include <QMap>

namespace Ui {
class Widget;
}

class QTreeWidget;
class QTreeWidgetItem;
class QTableWidget;

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
    void updateTree(QTreeWidget *targettree, QList<ClipsterCore::MediaTypeMatchRule *> &rules_list);
    void rulesCellChanged(int row, int column, bool copy_rules_change);
    void markupTreeWidget(QTreeWidget *targettree, const QList<const ClipsterCore::MediaTypeMatchRule *> &rules, bool noitemchange=true);
    void updateSelectionTable(QTableWidget *targettable, const QList<const ClipsterCore::MediaTypeMatchRule *> &rules);

public slots:
    void testFunc();
    void detectDataType();
    void itemChanged(QTreeWidgetItem *item, int column);
    void detectionCellChanged(int row, int column);
    void copyCellChanged(int row, int column);
    void saveMediaTypeRuleSet();
    void updateSaveButton();
    void showMediaTypeOverview();

private:
    Ui::Widget *ui;
    QList <ClipsterCore::MediaTypeMatchRule *> m_detection_rules;
    QList <const ClipsterCore::MediaTypeMatchRule *> m_selected_detection_rules;
    QList <ClipsterCore::MediaTypeMatchRule *> m_copy_rules;
    QList <const ClipsterCore::MediaTypeMatchRule *> m_selected_copy_rules;
    QString m_icon_resource_url;
    QJsonDocument m_json_rulesets;
    QStringList m_current_formats;
    QMap<QString, ClipsterCore::MediaTypeMatchRuleSet *> m_rulesets;
};

#endif // WIDGET_H
