CREATE EXTENSION citext;

create sequence common;

create table user_account (
	user_account_id INT PRIMARY KEY default nextval('common'),
	username TEXT UNIQUE NOT NULL,
	password TEXT,
	firstname CITEXT,
	lastname CITEXT,
	facebook_id TEXT UNIQUE,
	google_id TEXT UNIQUE,
	activated BOOLEAN NOT NULL DEFAULT false,
	last_login TIMESTAMP,
	verification_code TEXT,
	created timestamp without time zone not null default (now() at time zone 'utc')
);
insert into user_account (user_account_id,username,firstname,lastname)
select 0, 'admin', 'Admin', 'User';

create table device (
	device_id INT PRIMARY KEY default nextval('common'),
	hardware_signature TEXT NOT NULL,
	device_hash TEXT NOT NULL,
	new_device_hash TEXT,
	hostname TEXT NOT NULL,
	os_type TEXT NOT NULL,
	os_version TEXT NOT NULL,
	fk_user_account_id INT NOT NULL,
	latest_check timestamp without time zone not null default (now() at time zone 'utc'),
	CONSTRAINT fk_device_on_user_account FOREIGN KEY (fk_user_account_id) REFERENCES user_account (user_account_id),
	CONSTRAINT uni_hw_sig UNIQUE (hardware_signature),
	CONSTRAINT uni_dev_hash UNIQUE (device_hash),
	CONSTRAINT uni_new_dev_hash UNIQUE (new_device_hash)
);

create table cb_entry (
	cb_entry_id INT PRIMARY KEY default nextval('common'),
	data_hash TEXT NOT NULL,
	copytime timestamp without time zone not null,
	text_preview CITEXT,
	image_preview BYTEA,
	files BOOLEAN NOT NULL default false,
	removetime timestamp without time zone,
	size int not null,
	fk_owner_id INT NOT NULL,
	fk_device_id INT NOT NULL,
	CONSTRAINT fk_user_account FOREIGN KEY (fk_owner_id) REFERENCES user_account (user_account_id),
	CONSTRAINT fk_cb_entry_on_device FOREIGN KEY (fk_device_id) REFERENCES device (device_id),
	CONSTRAINT uni_hash UNIQUE (data_hash, fk_owner_id)
);

create table media_type (
	media_type_id INT PRIMARY KEY default nextval('common'),
	mime_type TEXT UNIQUE NOT NULL
);

create table cb_entry_media_type (
	fk_cb_entry_id INT NOT NULL,
	fk_media_type_id INT NOT NULL,
	CONSTRAINT fk_cb_entry FOREIGN KEY (fk_cb_entry_id) REFERENCES cb_entry (cb_entry_id),
	CONSTRAINT fk_media_type FOREIGN KEY (fk_media_type_id) REFERENCES media_type (media_type_id)
);

insert into media_type (mime_type)
select 'text/plain' union
select 'text/html' union
select 'image/png';

create table team (
	team_id int PRIMARY KEY default nextval('common'),
	teamname CITEXT NOT NULL,
	teamicon BYTEA,
	private boolean not null default true,
	searchable boolean not null default true,
	fk_owner_id INT NOT NULL,
	CONSTRAINT fk_team_on_user FOREIGN KEY (fk_owner_id) REFERENCES user_account (user_account_id),
	CONSTRAINT uni_teamname UNIQUE (fk_owner_id, teamname)
);

create table team_member (
	team_member_id int PRIMARY KEY default nextval('common'),
	fk_team_id INT NOT NULL,
	fk_user_account_id INT NOT NULL,
	owner_accepted boolean not null default false,
	member_accepted boolean not null default false,
	CONSTRAINT uni_membership UNIQUE (fk_team_id, fk_user_account_id),
	CONSTRAINT fk_team_member_on_user FOREIGN KEY (fk_user_account_id) REFERENCES user_account (user_account_id),
	CONSTRAINT fk_team_member_on_team FOREIGN KEY (fk_team_id) REFERENCES team (team_id)
);

create table team_cb_entry (
	team_cb_entry_id int PRIMARY KEY default nextval('common'),
	fk_team_id INT NOT NULL,
	fk_cb_entry_id INT NOT NULL,
	sharetime timestamp without time zone,
	removetime timestamp without time zone,
	CONSTRAINT uni_team_clip UNIQUE (fk_team_id, fk_cb_entry_id),
	CONSTRAINT fk_team_clip_on_team FOREIGN KEY (fk_team_id) REFERENCES team (team_id),
	CONSTRAINT fk_team_clip_in_clip FOREIGN KEY (fk_cb_entry_id) REFERENCES cb_entry (cb_entry_id)
);

create table share_cb_entry_public (
	share_cb_entry_public_id int PRIMARY KEY default nextval('common'),
	hash TEXT NOT NULL,
	message TEXT,
	password TEXT,
	remaining_access int NOT NULL default 1,
	fk_owner_id INT NOT NULL,
	fk_cb_entry_id INT NOT NULL,
	CONSTRAINT fk_share_clip_on_user FOREIGN KEY (fk_owner_id) REFERENCES user_account (user_account_id),
	CONSTRAINT fk_share_clip_in_clip FOREIGN KEY (fk_cb_entry_id) REFERENCES cb_entry (cb_entry_id)	
);



create table crash_summary (
	crash_summary_id int PRIMARY KEY default nextval('common'),
	reported_ts timestamp without time zone not null default (now() at time zone 'utc'),
	os_type text not null,
	os_version text not null,
	crash_type text not null,
	contact_ok boolean not null default false,
	crash_file text not null,
	git_version text,
	fk_device_id int,
	username text,
	contact_mail text,
	user_details text,
	hostname text,
	processed boolean not null default false,
	CONSTRAINT fk_crash_on_device FOREIGN KEY (fk_device_id) REFERENCES device (device_id)
);

create table crash_call_info (
	crash_call_info_id int PRIMARY KEY default nextval('common'),
	fk_crash_summary_id int,
	seq bigint not null,
	fname text not null,
	file text not null,
	line text not null,
	thread text not null,
	start text not null,
	duration text not null,
	CONSTRAINT fk_cinfo_on_crash FOREIGN KEY (fk_crash_summary_id) REFERENCES crash_summary (crash_summary_id)
);

create table crash_profile_info (
	crash_profile_info_id  int PRIMARY KEY default nextval('common'),
	fk_crash_summary_id int,
	fname text not null,
	call_count bigint not null,
	acc_duration bigint not null,
	min_duration bigint not null,
	max_duration bigint not null,
	avg_duration decimal not null,
	durations bigint[],
	CONSTRAINT fk_pinfo_on_crash FOREIGN KEY (fk_crash_summary_id) REFERENCES crash_summary (crash_summary_id)
);
