# -*- coding: utf-8 -*-
from pysite.conf import PySiteConfiguration
from pysite.compat import PORTABLE_STRING

class TestSite(PySiteConfiguration):
	sitename = 'clipster-adm'
	logfile = PORTABLE_STRING('/tmp/clipster-adm.log')
	translations = ['en']
	sitetitle = PORTABLE_STRING('Clipster Administration')
	
	def __init__(self,basedir):
		super(TestSite, self).__init__(basedir)

siteconf = TestSite
