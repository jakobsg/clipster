from pysite.wsgi import PySiteApplication
from os.path import dirname,abspath,join

application = PySiteApplication(abspath(dirname(__file__)))
