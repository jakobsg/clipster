# -*- coding: utf-8 -*-
from pysite.conf import PySiteConfiguration
from pysite.compat import PORTABLE_STRING

class TestSite(PySiteConfiguration):
	sitename = 'clipster'
	translations = ['en','da']
	logfile = PORTABLE_STRING('/tmp/clipster.log')
	
	def __init__(self,basedir):
		super(TestSite, self).__init__(basedir)

siteconf = TestSite
