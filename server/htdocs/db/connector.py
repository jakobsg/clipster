# -*- coding: utf-8 -*-
import psycopg2
import psycopg2.extras
psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
import clipster_conf

import sys
if sys.version_info.major==2:
	from thread import get_ident
if sys.version_info.major==3:
	from threading import get_ident
                                                                                                                                                                                                                                                                               
conn_pool = {}                                                                                                                                                                                                                                                                 
                                                                                                                                                                                                                                                                               
def get_connection():                                                                                                                                                                                                                                                           
	global conn_pool
	if get_ident() not in conn_pool:
		conn_pool[get_ident()] = psycopg2.connect(
			host=clipster_conf.db_host,
			user=clipster_conf.db_user,
			password=clipster_conf.db_passwd,
			database=clipster_conf.db_name)
	conn_pool[get_ident()]
	return conn_pool[get_ident()]                                                                                                                                                                                                                                   
                                                                                                                                                                                                                                                                               
def get_cursor(as_dict=False):
	conn = get_connection()
	if as_dict==True:
		cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	else:
		cur = conn.cursor()
	return cur

def commit():
	conn = get_connection()
	conn.commit()


def rollback():
	conn = get_connection()
	conn.rollback()
