# -*- coding: utf-8 -*-

select_device = u'''
select device_id
from device
where
  device_hash=%(device_hash)s
'''

insert_crash_summary = u'''
insert into crash_summary (
  os_type, os_version, crash_type,
  contact_ok, fk_device_id, username,
  contact_mail, user_details, hostname,
  crash_file, git_version)
select
  %(os_type)s, %(os_version)s, %(crash_type)s,
  %(contact_ok)s, %(device_id)s, %(username)s,
  %(contact_mail)s, %(user_details)s, %(hostname)s,
  %(crash_file)s, %(git_version)s
returning crash_summary_id
'''

insert_crash_call_info = u'''
insert into crash_call_info (
  fk_crash_summary_id, seq, fname,
  file, line, thread,
  start, duration)
select
  %(crash_summary_id)s, %(seq)s, %(fname)s,
  %(file)s, %(line)s, %(thread)s,
  %(start)s, %(duration)s
'''

insert_crash_profile_info = u'''
insert into crash_profile_info (
  fk_crash_summary_id, fname, call_count,
  acc_duration, min_duration, max_duration,
  avg_duration, durations )
select
  %(crash_summary_id)s, %(fname)s, %(call_count)s,
  %(acc_duration)s, %(min_duration)s, %(max_duration)s,
  %(avg_duration)s, %(durations)s
'''