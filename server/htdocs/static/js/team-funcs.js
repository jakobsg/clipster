function modalTeamListQueryParams(params) {
	var args = {
		methodname: 'listTeams',
		args: {
			device_hash: device_hash,
			search_frase: params.search ? '*'+params.search+'*' : '',
			pagenumber: params.offset/params.limit,
			pagesize: params.limit,
			minimum_search_frase_length: 3
		}
	};
	return args;
}

function modalTeamListResponseHandler(res) {
	return { total: res.result.full_count, rows: res.result.teams };
}

function find_team_dialog(title, rowclick_cb) {
	var displayFields = [];
	var click_to_select = false;
	displayFields.push(
		{key: "teamname", visible: true, switchable: false, name: "Team name"},
		{key: "owner", visible: true, switchable: false, name: "Team owner"});
	
	var result = $.objectList({
		queryParams: modalTeamListQueryParams,
		displayFields: displayFields,
		responseHandler: modalTeamListResponseHandler,
		tableOptions: {
			onClickRow: function(row,elem) {
				if (rowclick_cb) {
					rowclick_cb(row);
				}
			}
		},
		url: "/service/v1/TeamService/jsonwsp",
		tableId: "modal-team-list-table",
		show_search: true,
		show_view_toggler: false,
		show_column_toggler: false,
		click_to_select: click_to_select
	});
	var dlg = $.bsModal('open',{id: "modal_teams"});
	dlg.title.html(title);
	dlg.body.html(result.container);
	setTimeout(function(){$("#bsmodal-modal_teams input[placeholder=Search]").focus()},750);
}
