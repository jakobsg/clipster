function modalUserListQueryParams(params) {
	var args = {
		methodname: 'listUsers',
		args: {
			device_hash: device_hash,
			search_frase: params.search ? '*'+params.search+'*' : '',
			pagenumber: params.offset/params.limit,
			pagesize: params.limit,
			minimum_search_frase_length: 3
		}
	};
	return args;
}

function modalUserListResponseHandler(res) {
	return { total: res.result.full_count, rows: res.result.users };
}

function find_user_dialog(title, rowclick_cb) {
	var displayFields = [];
	var click_to_select = false;
	displayFields.push(
		{key: "username", visible: true, switchable: false, name: "Username"},
		{key: "firstname", visible: true, switchable: false, name: "First name"},
		{key: "lastname", visible: true, switchable: false, name: "Last name"});
	
	var result = $.objectList({
		queryParams: modalUserListQueryParams,
		displayFields: displayFields,
		responseHandler: modalUserListResponseHandler,
		tableOptions: {
			onClickRow: function(row,elem) {
				if (rowclick_cb) {
					rowclick_cb(row);
				}
			}
		},
		url: "/service/v1/UserService/jsonwsp",
		tableId: "modal-user-list-table",
		show_search: true,
		show_view_toggler: false,
		show_column_toggler: false,
		click_to_select: click_to_select
	});
	var dlg = $.bsModal('open',{id: "modal_users"});
	dlg.title.html(title);
	dlg.body.html(result.container);
	setTimeout(function(){$("#bsmodal-modal_users input[placeholder=Search]").focus()},750);
}
