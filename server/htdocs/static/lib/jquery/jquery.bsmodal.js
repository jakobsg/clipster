/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-langpicker
 *
 * Copyright 2006, 2014 Jakob Simon-Gaarde
 * Released under the MIT license
 */
(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		// CommonJS
		factory(require('jquery'));
	} else {
		// Browser globals
		factory(jQuery);
	}
}(function ($) {
	$.bsModal = function (cmd, options) {
		if (cmd=="closeall") {
			$("div.modal[aria-hidden=false]").each(function(idx,obj){$.bsModal('close',{id:$(obj).attr("id").slice(8)})});
			return;
		}
		var common_defaults = {
			id: 'default'
		};
		var creation_defaults = {
			title: "Dialog Title",
			body: "Body of dialog."
		}
		if (cmd==null) {
			cmd = "open";
		}
		if (arguments.length==1) {
			if (typeof(cmd)=="object") {
				options = cmd;
				cmd = "open";
			}
		}
		options = $.extend({size:'lg'}, common_defaults, options);
		var dialog_id = "bsmodal-"+options.id;
		var modal_container = $("#"+dialog_id);
		var modal_dialog = null;
		var modal_content = null;
		var modal_header = null;
		var modal_title = null;
		var modal_body = null;
		if (modal_container.length==0) {
			options = $.extend({}, creation_defaults, options);
			modal_container = $('<div id="'+dialog_id+'" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"></div>');
			modal_valign_helper = $('<div class="vertical-alignment-helper"></div>');
			modal_dialog = $('<div class="modal-dialog modal-'+options.size+' vertical-align-center"></div>');
			modal_content = $('<div class="modal-content"></div>');
			modal_header = $('<div class="modal-header"></div>');
			modal_header.append('<button type="button" class="close" data-dismiss="modal"><span style="font-size: 1.5em" class="glyphicon glyphicon-remove"></span></button>');
			modal_title = $('<h4 class="modal-title" id="myModalLabel"></h4>');
			modal_header.append(modal_title);
			modal_body = $('<div class="modal-body"></div>');
			modal_content.append(modal_header);
			modal_content.append(modal_body);
			modal_dialog.append(modal_content);
			modal_valign_helper.append(modal_dialog);
			modal_container.append(modal_valign_helper);
			$('body').append(modal_container);
			modal_body.css("max-height",$(window).height()-100);
		}
		else {
			modal_dialog = modal_container.find('.modal-dialog');
			modal_content = modal_container.find('.modal-content');
			modal_header = modal_container.find('.modal-header');
			modal_title = modal_container.find('.modal-title');
			modal_body = modal_container.find('.modal-body');
			modal_body.css("max-height",$(window).height()-110);
		}
		if (options.title) {
			modal_title.html(options.title);
		}
		if (options.body) {
			modal_body.html(options.body);
		}
		
		if (cmd=="open") {
			$(".modal").css("z-index","1040");
			modal_container.css("z-index","1041");
			modal_container.modal('show');
		}
		else if (cmd=="close") {
			modal_container.modal('hide');
		}
		else if (cmd=="toggle") {
			modal_container.modal('toggle');
		}
		return {title:modal_title, body:modal_body};
	};

}));


