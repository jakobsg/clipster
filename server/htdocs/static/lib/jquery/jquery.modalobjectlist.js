/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-langpicker
 *
 * Copyright 2006, 2014 Jakob Simon-Gaarde
 * Released under the MIT license
 */

var example_data = [
	{country: 'DK', city: 'Copenhagen'},
	{country: 'DK', city: 'Odense'},
	{country: 'SE', city: 'Malmö'},
	{country: 'DE', city: 'Berlin'},
	{country: 'DE', city: 'Hamburg'},
	{country: 'NO', city: 'Oslo'},
	{country: 'GB', city: 'London'},
	{country: 'FR', city: 'Paris'},
	{country: 'ES', city: 'Madrid'},
	{country: 'IT', city: 'Rome'},
	{country: 'US', city: 'Washington'},
	{country: 'ZA', city: 'Johannesburg'},
	{country: 'JP', city: 'Tokyo'},
];

function olListExample(options, data_callback) {
	setTimeout(function() {
		var result = $.grep(
			example_data,
			function(a) {
				if (options.search_frase==null) {
					return true;
				}
				return (a.country.indexOf(options.search_frase)>-1 || a.city.indexOf(options.search_frase)>-1);
			}
		);
		var full_count = result.length;
		if (options.pagesize!=null) {
			var pagenumber = options.pagenumber!=null ? options.pagenumber : 0;
			data_callback({total:full_count, rows: result.splice(pagenumber*options.pagesize,options.pagesize)});
		}
		else {
			data_callback({total:full_count, rows: result});
		}
	},500);
}

function olResultExample(res) {
// 	console.info(res);
}

function objectListExample() {
	$.objectList({
		listObjectsCallback: olListExample,
		displayFields: [
			{key: 'city', switchable: false, visible: true, name: "City" },
			{key: 'country', switchable: true, visible: false, name: "Country" }],
		resultCallback: olResultExample,
		title: "Example object picker",
		id: "example"
	});
}


var objectpicker_timer = null;
var objectpicker_callbacks = {};

function testing(a,b,c) {
// 	console.debug(a);
// 	console.debug(b);
// 	console.debug(c);
}

function response_handler(res) {
// 	console.debug(res);
	return { total: res.result.full_count, rows: res.result.locations };
}

function objectListQueryParams(params) {
	return JSON.stringify({
		methodname: 'listLocations',
		args: {
			session_id: '',
			search_frase: params.search,
			search_attribs: ['name', 'address', 'postal_code', 'city'],
			pagenumber: params.offset/params.limit,
			pagesize: params.limit
		}
	});
}

(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		// CommonJS
		factory(require('jquery'));
	} else {
		// Browser globals
		factory(jQuery);
	}
}(function ($) {

	$.objectList = function (options) {
		var common_defaults = {};
		options = $.extend({}, common_defaults, options);
		objectpicker_callbacks[options.id] = options.resultCallback;
		
		modal_body = $.bsModal('update',{
			id: options.id,
			title: options.title,
			body: ''}).body;
		
		if (modal_body.find('input').length==0) {
			var objects_list_container = $('<div class="col-xs-12 col-sm-12 col-lg-12 panel-container"></div>');
			var objects_list_table = $('<table class="object-list-table"></table>');
			objects_list_table.attr( {
				"data-query-params": objectListQueryParams,
				"data-method": "post",
				"data-url": "/service/v1/CALService/jsonwsp",
				"data-response-handler": "response_handler",
				"data-side-pagination": "server",
				"data-toggle": "table",
				"data-cache": "false",
				"data-search": "true",
				"data-show-toggle": "true",
				"data-show-columns": "true",
				"data-toolbar": "#location-toolbar",
				"data-pagination": "true",
				"data-id-field": "location_id"} );
			objects_list_container.append(objects_list_table);
			var objects_table_head = $("<thead></thead>");
			objects_list_table.append(objects_table_head);
			var table_head_row = $("<tr></tr>");
			objects_table_head.append(table_head_row);
			for (var idx in options.displayFields) {
				var field = options.displayFields[idx];
				var title = field.name!=null ? field.name : field.key;
				var th = $("<th>"+title+"</th>");
				th.attr('data-field', field.key);
				th.attr('data-visible', field.visible==true ? true : false);
				th.attr('data-switchable', field.switchable==true ? true : false);
				table_head_row.append(th);
			}
			modal_body.append(objects_list_container);
		}
		modal_body.find("table.object-list-table").bootstrapTable('destroy');
		modal_body.find("table.object-list-table").bootstrapTable();
		$.bsModal('open',{id:options.id});
		return modal_body;
	};
}));
