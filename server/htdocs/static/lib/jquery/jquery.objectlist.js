function olClickExample(row,elem) {
// 	console.info(res);
}

function olResponseHandlerExample(res) {
	return { total: res.result.full_count, rows: res.result.locations };
}

function olQueryParamsExample(params) {
	return JSON.stringify({
		methodname: 'listLocations',
		args: {
			session_id: '',
			search_frase: params.search ? '*'+params.search+'*' : '',
			search_attribs: ['name', 'address', 'postal_code', 'city'],
			pagenumber: params.offset/params.limit,
			pagesize: params.limit
		}
	});
}

function objectListExample() {
	result = $.objectList({
		queryParams: olQueryParamsExample,
		displayFields: [
			{key: 'city', switchable: false, visible: true, name: "City" },
			{key: 'country', switchable: true, visible: false, name: "Country" }],
		responseHandler: olResponseHandlerExample,
		tableOptions: {
			onClickRow: olClickExample
		},
		url: "/service/v1/CALService/jsonwsp",
		tableId: "debtor-list-table"
	});
	result.toolbar.append('<button id="add-location-button" type="button" class="btn btn-default"><i class="glyphicon glyphicon-plus"></i></button>');
	return result;
}

// function format_edit_button(value, row, index) {
//     return '<a class="edit" href="javascript:void(0)" title="Edit"><i class="table-action glyphicon glyphicon-edit"></i></a>'
// }
// 
// window.objectListEvents = {
// 	'click a.edit': function(e,value,row,index) {
// 		e.stopPropagation();
// 		if (row.location_id!=null) {
// 			$.bsModal("closeall");
// 			location_tools(row);
// 		}
// 	}
// }

var ol_toolbar_counter = 1;

(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		// CommonJS
		factory(require('jquery'));
	} else {
		// Browser globals
		factory(jQuery);
	}
}(function ($) {

	$.objectList = function (options) {
		var common_defaults = {
			show_search: true,
			show_view_toggler: true,
			show_column_toggler: true,
			click_to_select: false,
// 			show_edit_button: false
		};
		options = $.extend({}, common_defaults, options);
		var objects_list_container = $('<div></div>');
		if (options.toolbar==null) {
			var toolbar_id = "ol-toolbar-" + ol_toolbar_counter++;
			var toolbar = $('<div id="'+toolbar_id+'" class="btn-group"></div>');
			$("body").append(toolbar);
			options.toolbar = "div#" + toolbar_id;
		}
		
		var objects_list_table = $('<table class="object-list-table"></table>');
		objects_list_container.append(objects_list_table);
		objects_list_table.attr( {
			"data-query-params": options.queryParams.name,
			"data-method": "post",
			"data-url": options.url,
			"data-response-handler": options.responseHandler.name,
			"data-side-pagination": "server",
			"data-toggle": "table",
			"data-cache": "false",
			"data-search": options.show_search,
			"data-search-align": "left",
			"data-show-toggle": options.show_view_toggler,
			"data-show-columns": options.show_column_toggler,
			"data-click-to-select": options.click_to_select,
			"data-toolbar": options.toolbar,
			"data-pagination": "true",
			"data-id-field": "location_id"} );
		if (options.containerId) {
			objects_list_container.attr("id",options.containerId);
		}
		if (options.tableId) {
			objects_list_table.attr("id",options.tableId);
		}
		var objects_table_head = $("<thead></thead>");
		objects_list_table.append(objects_table_head);
		var table_head_row = $("<tr></tr>");
		objects_table_head.append(table_head_row);
		for (var idx in options.displayFields) {
			var field = options.displayFields[idx];
			var title = field.name!=null ? field.name : field.key;
			var th = $("<th>"+title+"</th>");
			th.attr('data-field', field.key);
			th.attr('data-visible', field.visible==true ? true : false);
			th.attr('data-switchable', field.switchable==true ? true : false);
			th.attr('data-checkbox', field.checkable==true ? true : false);
			th.attr('data-sortable',true);
			table_head_row.append(th);
		}
// 		if (options.show_edit_button==true) {
// 			var th = $("<th></th>");
// 			th.attr('data-field', field.key);
// 			th.attr('data-visible', true);
// 			th.attr('data-switchable', false);
// 			th.attr('data-sortable',false);
// 			th.attr('data-formatter',"format_edit_button");
// 			th.attr('data-events',"objectListEvents");
// 			table_head_row.append(th);
// 		}
		objects_list_table.bootstrapTable( options.tableOptions );
		return {
			container: objects_list_container,
			toolbar: $(toolbar),
			table: objects_list_table
		};
	};
}));
