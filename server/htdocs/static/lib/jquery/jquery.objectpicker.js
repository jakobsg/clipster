 
/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-langpicker
 *
 * Copyright 2006, 2014 Jakob Simon-Gaarde
 * Released under the MIT license
 */

var example_data = [
	{country: 'DK', city: 'Copenhagen'},
	{country: 'DK', city: 'Odense'},
	{country: 'SE', city: 'Malmö'},
	{country: 'DE', city: 'Berlin'},
	{country: 'DE', city: 'Hamburg'},
	{country: 'NO', city: 'Oslo'},
	{country: 'GB', city: 'London'},
	{country: 'FR', city: 'Paris'},
	{country: 'ES', city: 'Madrid'},
	{country: 'IT', city: 'Rome'},
	{country: 'US', city: 'Washington'},
	{country: 'ZA', city: 'Johannesburg'},
	{country: 'JP', city: 'Tokyo'},
];

function searchExample(search_string, data_callback) {
	setTimeout(function() {
		var result = $.grep(
			example_data,
			function(a) {
				return (a.country.indexOf(search_string)>-1 || a.city.indexOf(search_string)>-1);
			}
		);
		data_callback(result);
	},500);
}

function resultExample(res) {
// 	console.info(res);
}

function formatExample(idx,obj) {
	return "<b>"+idx+".</b> " + obj.city;
}

function objectPickerExample(withformat) {
	if (withformat) {
		$.objectPicker({
			searchCallback: searchExample,
			formatCallback: formatExample,
			resultCallback: resultExample,
			title: "Example object picker",
			id: "example"
		});
	}
	else {
		$.objectPicker({
			searchCallback: searchExample,
			displayFields: ['city','country'],
			resultCallback: resultExample,
			title: "Example object picker",
			id: "example"
		});
	}
}


var objectpicker_timer = null;
var objectpicker_callbacks = {};

(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		// CommonJS
		factory(require('jquery'));
	} else {
		// Browser globals
		factory(jQuery);
	}
}(function ($) {

	$.objectPicker = function (options) {
		var common_defaults = {};
		options = $.extend({}, common_defaults, options);
		objectpicker_callbacks[options.id] = options.resultCallback;
		
		modal_body = $.bsModal('update',{
			id: options.id,
			title: options.title,
			body: ''}).body;
		
		if (modal_body.find('input').length==0) {
			var search_field = $('<div class="inner-addon left-addon"><i class="glyphicon glyphicon-search"></i><input type="text" class="form-control"></input></div>');
			modal_body.append(search_field);
			var objects_list_container = $('<div class="container-fluid objects-list"></div>');
			var objects_list_grid = $('<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>');
			var objects_list_row = $('<div class="row"></div>');
			objects_list_grid.append(objects_list_row);
			objects_list_container.append(objects_list_grid);
			modal_body.append(objects_list_container);
			var search_input = search_field.find('input');
			search_input.keyup( function() {
				if (objectpicker_timer) {
					clearTimeout(objectpicker_timer);
				}
				objectpicker_timer = setTimeout(function(){
					options.searchCallback(search_input.val(),function(result) {
						objects_list_row.children().remove();
						$(result).each(function(idx,obj) {
							var object_div = $('<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3"></div>');
							var object_anchor = $('<a class="list-group-item force-pointer"></a>');
							object_div.append(object_anchor);
							if (options.displayFields) {
								object_anchor.html($(options.displayFields).map(function(idx,key) {return obj[key]}).toArray().join(' '));
							}
							else if (options.formatCallback) {
								object_anchor.html(options.formatCallback(idx,obj));
							}
							object_div.prop('obj',obj);
							objects_list_row.append(object_div);
							object_div.click( function() {
								$.bsModal('close',{id:options.id});
								if (objectpicker_callbacks[options.id]) {
									objectpicker_callbacks[options.id]($(this).prop('obj'));
								}
							});
						});
					});
				}, 300 );
			});
		}
		$.bsModal('open',{id:options.id});
		setTimeout( function() {
			modal_body.find('input').focus();
			modal_body.find('input').select();
		},500);
		return modal_body;
	};
}));
