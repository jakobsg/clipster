# -*- coding: utf-8 -*-
import pysite.subhandlers
import os
import json
import clipster_conf
import datetime
from db.connector import get_cursor,rollback,commit
from db.crash_sql import *


class UploadCrashInfo(pysite.subhandlers.BaseHandler):

	def init(self):
		crashinfo = self.environ['wsgi.input'].read().strip()
		crashdoc = json.loads(crashinfo)
		device_hash = 'no_dev_hash'
		if 'device_hash' in crashdoc:
			device_hash = crashdoc['device_hash']
			self.template_info['result'] = u'Accepted crash info for device: %s\nThank you for your help!' % device_hash
		else:
			self.template_info['result'] = u'Accepted crash info with missing device hash\nThank you for your help!';

		crashinfodir = os.path.join(clipster_conf.crashinfodir,device_hash)
		if not os.path.exists(crashinfodir):
			os.makedirs(crashinfodir)
			
		crashfile = os.path.join(crashinfodir,datetime.datetime.now().isoformat())
		open(crashfile,'w').write(crashinfo)

		cur = get_cursor()
		cur.execute(select_device, crashdoc)
		res = cur.fetchone()
		crashdoc['device_id'] = None
		if res is not None:
			crashdoc['device_id'] = res[0]

		crashdoc['crash_file'] = crashfile
		cur.execute(insert_crash_summary, crashdoc)
		res = cur.fetchone()
		if res is None:
			self.logger.warning('Failed to insert crash_summary into the database for: %s' % crashfile)
			rollback()
			return
		
		crash_summary_id = res[0]
		for t,cinfo in crashdoc['call_info'].items():
			for ci in cinfo:
				ci['crash_summary_id'] = crash_summary_id
				ci['start'] = ci['time']
			cur.executemany(insert_crash_call_info,cinfo)
			
		for t,pinfo in crashdoc['profile_info'].items():
			for pi in pinfo:
				pi['crash_summary_id'] = crash_summary_id
				durations = pi['durations']
				pi['acc_duration'] = sum(durations)
				pi['avg_duration'] = float(pi['acc_duration'])/len(durations)
				pi['min_duration'] = min(durations)
				pi['max_duration'] = max(durations)
				pi['call_count'] = len(durations)
			cur.executemany(insert_crash_profile_info,pinfo)
		
		commit()

subhandler = UploadCrashInfo
