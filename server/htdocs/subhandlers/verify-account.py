# -*- coding: utf-8 -*-
import pysite.subhandlers

class VerifyAccount(pysite.subhandlers.BaseHandler):

	def init(self):
		self.template_info['verification_code'] = self.req_data.get_any_value('verification_code')
                
subhandler = VerifyAccount
