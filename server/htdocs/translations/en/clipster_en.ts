<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>main-fb</name>
    <message>
        <location filename="../../templates/main-fb.jinja" line="285"/>
        <source>Download the Clipster Client</source>
        <translation>Download the Clipster Client</translation>
    </message>
    <message>
        <location filename="../../templates/main-fb.jinja" line="252"/>
        <source>Please provide a username (e-mail) and password for your Clipster profile.</source>
        <translation>Please provide a username (e-mail) and password for your Clipster profile.</translation>
    </message>
    <message>
        <location filename="../../templates/main-fb.jinja" line="122"/>
        <source>Show downloads for all systems</source>
        <translation>Show downloads for all systems</translation>
    </message>
    <message>
        <location filename="../../templates/main-fb.jinja" line="126"/>
        <source>Show only your system&apos;s download</source>
        <translation>Show only your system&apos;s download</translation>
    </message>
    <message>
        <location filename="../../templates/main-fb.jinja" line="240"/>
        <source>New profile</source>
        <translation>New profile</translation>
    </message>
    <message>
        <location filename="../../templates/main-fb.jinja" line="245"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
</context>
<context>
    <name>verify-account</name>
    <message>
        <location filename="../../templates/verify-account.jinja" line="56"/>
        <source>Activating your Clipster profile.</source>
        <translation>Activating your Clipster profile.</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../../templates/main.jinja" line="271"/>
        <source>Download the Clipster Client</source>
        <translation>Download the Clipster Client</translation>
    </message>
    <message>
        <location filename="../../templates/main.jinja" line="238"/>
        <source>Please provide a username (e-mail) and password for your Clipster profile.</source>
        <translation>Please provide a username (e-mail) and password for your Clipster profile.</translation>
    </message>
    <message>
        <location filename="../../templates/main.jinja" line="125"/>
        <source>Show downloads for all systems</source>
        <translation>Show downloads for all systems</translation>
    </message>
    <message>
        <location filename="../../templates/main.jinja" line="129"/>
        <source>Show only your system&apos;s download</source>
        <translation>Show only your system&apos;s download</translation>
    </message>
    <message>
        <location filename="../../templates/main.jinja" line="226"/>
        <source>New profile</source>
        <translation>New profile</translation>
    </message>
    <message>
        <location filename="../../templates/main.jinja" line="231"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <source>Existing profile</source>
        <translation type="obsolete">Existing profile</translation>
    </message>
    <message>
        <source>Show all downloads</source>
        <translation type="obsolete">Show all downloads</translation>
    </message>
    <message>
        <source>Download the Clipster client for your system.</source>
        <translation type="obsolete">Download the Clipster client for your system.</translation>
    </message>
    <message>
        <source>Download client</source>
        <translation type="obsolete">Download client</translation>
    </message>
</context>
<context>
    <name>common</name>
    <message>
        <location filename="../../templates/main.jinja" line="253"/>
        <source>Verify password</source>
        <translation>Verify password</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="435"/>
        <source>Create</source>
        <translation>Create</translation>
    </message>
    <message>
        <location filename="../../templates/login.jinja" line="173"/>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="434"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="602"/>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="../../templates/main.jinja" line="246"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../../templates/main.jinja" line="239"/>
        <source>Username (valid e-mail)</source>
        <translation>Username (valid e-mail)</translation>
    </message>
</context>
<context>
    <name>teams</name>
    <message>
        <location filename="../../templates/teams.jinja" line="721"/>
        <source>Make this team searchable for all users</source>
        <translation>Make this team searchable for all users</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="184"/>
        <source>Leave team</source>
        <translation>Leave team</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="293"/>
        <source>Member</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="278"/>
        <source>Exclude member</source>
        <translation>Exclude member</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="167"/>
        <source>Are you sure you want to remove the team</source>
        <translation>Are you sure you want to remove the team</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="202"/>
        <source>Are you sure you want to exclude the team member </source>
        <translation>Are you sure you want to exclude the team member </translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="700"/>
        <source>Username</source>
        <translation>Username</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="563"/>
        <source>Find user</source>
        <translation>Find user</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="695"/>
        <source>Team members</source>
        <translation>Team members</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="545"/>
        <source>Find team</source>
        <translation>Find team</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="691"/>
        <source>Invite a user</source>
        <translation>Invite a user</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="660"/>
        <source>Invitations</source>
        <translation>Invitations</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="284"/>
        <source>Team owner</source>
        <translation>Team owner</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="672"/>
        <source>Pending memberships</source>
        <translation>Pending memberships</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="701"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="644"/>
        <source>Join a team</source>
        <translation>Join a team</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="690"/>
        <source>Back to teams</source>
        <translation>Back to teams</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="648"/>
        <source>Memberships</source>
        <translation>Memberships</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="643"/>
        <source>Create a team</source>
        <translation>Create a team</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="275"/>
        <source>Accept membership</source>
        <translation>Accept membership</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="266"/>
        <source>Accept invitation</source>
        <translation>Accept invitation</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="167"/>
        <source>Remove team</source>
        <translation>Remove team</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="202"/>
        <source>Exclude team member</source>
        <translation>Exclude team member</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="439"/>
        <source>Create team</source>
        <translation>Create team</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="287"/>
        <source>Pending your accept</source>
        <translation>Pending your accept</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="654"/>
        <source>Owner</source>
        <translation>Owner</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="653"/>
        <source>Team name</source>
        <translation>Team name</translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="184"/>
        <source>Are you sure you want to leave the team </source>
        <translation>Are you sure you want to leave the team </translation>
    </message>
    <message>
        <location filename="../../templates/teams.jinja" line="290"/>
        <source>Pending user accept</source>
        <translation>Pending user accept</translation>
    </message>
</context>
</TS>
