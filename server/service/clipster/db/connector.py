import psycopg2
import psycopg2.extras
psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
import clipster.conf

import sys
if sys.version_info.major==2:
	from thread import get_ident
if sys.version_info.major==3:
	from threading import get_ident
                                                                                                                                                                                                                                                                               
class Connection(object):
	
	def __init__(self):
		self.conn = psycopg2.connect(
			host=clipster.conf.db_host,
			user=clipster.conf.db_user,
			password=clipster.conf.db_passwd,
			database=clipster.conf.db_name,
			port=6543)
	
	def cursor(self, as_dict=False):
		if as_dict==True:
			cur = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		else:
			cur = self.conn.cursor()
		return cur
	
	def commit(self):
		return self.conn.commit()
	
	def rollback(self):
		return self.conn.rollback()

conn_pool = {}                                                                                                                                                                                                                                                                 
                                                                                                                                                                                                                                                                               
def get_connection():                                                                                                                                                                                                                                                           
	global conn_pool
	if get_ident() not in conn_pool:
		conn_pool[get_ident()] = psycopg2.connect(
			host=clipster.conf.db_host,
			user=clipster.conf.db_user,
			password=clipster.conf.db_passwd,
			database=clipster.conf.db_name)
	conn_pool[get_ident()]
	return conn_pool[get_ident()]                                                                                                                                                                                                                                   
                                                                                                                                                                                                                                                                               
def get_cursor(as_dict=False):
	conn = get_connection()
	if as_dict==True:
		cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	else:
		cur = conn.cursor()
	return cur

def commit():
	conn = get_connection()
	conn.commit()


def rollback():
	conn = get_connection()
	conn.rollback()
