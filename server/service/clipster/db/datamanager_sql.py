# -*- coding: utf-8 -*-

sql_insert_cb_entry = u"""
	insert into cb_entry (
		data_hash, copytime, size, 
		text_preview, image_preview,
		files, fk_device_id, fk_owner_id )
	select 
		%(data_hash)s, (now() at time zone 'utc'), %(size)s,
		%(text_preview)s, %(image_preview)s,
		%(files)s, d.device_id, d.fk_user_account_id
	from
		device d
	where
		d.device_hash = %(device_hash)s and
		not exists (
		select 1
		from cb_entry
		where
			data_hash = %(data_hash)s and
			fk_owner_id = d.fk_user_account_id
		union
		select 1
		from
			device d,
			cb_entry c,
			team_cb_entry tc,
			team_member tm
		where
			d.device_hash = %(device_hash)s and
			c.data_hash = %(data_hash)s and
			tm.fk_user_account_id = d.fk_user_account_id and
			tm.member_accepted = true and
			tm.owner_accepted = true and
			tc.fk_team_id = tm.fk_team_id and
			tc.fk_cb_entry_id = c.cb_entry_id and
			not tc.sharetime is null)
	returning cb_entry_id
"""

sql_update_cb_entry_removed = u"""
	update cb_entry ce
	set
		removetime = (now() at time zone 'utc'),
		text_preview = null,
		image_preview = null,
		files = false
	where
		ce.data_hash = %(data_hash)s and
		ce.fk_owner_id = (
			select fk_user_account_id
			from device d
			where device_hash = %(device_hash)s )
"""

sql_update_cb_entry_unremove = u"""
	update cb_entry ce
	set
		removetime = null,
		copytime = (now() at time zone 'utc'),
		text_preview = %(text_preview)s, 
		image_preview = %(image_preview)s,
		files = %(files)s
	where
		ce.data_hash = %(data_hash)s and
		ce.fk_owner_id = (
			select fk_user_account_id
			from device d
			where device_hash = %(device_hash)s )
"""

sql_insert_media_type = u"""
	insert into media_type (
		mime_type)
	select
		%(mime_type)s
	where
		not exists (
			select 1
			from 
				media_type
			where
				mime_type = %(mime_type)s )
"""

sql_insert_cb_media_type = u"""
	insert into cb_entry_media_type (
		fk_cb_entry_id,
		fk_media_type_id)
	select
		%(cb_entry_id)s,
		media_type_id
	from
		media_type
	where
		mime_type = %(mime_type)s
"""

sql_select_cb_entries = u"""
	select
		data_hash, copytime, text_preview,
		image_preview,files,ua.username,
		d2.hostname, d2.os_type, ARRAY(
			select
				mime_type
			from
				media_type mt,
				cb_entry_media_type cbmt
			where
				cbmt.fk_cb_entry_id = c.cb_entry_id and
				mt.media_type_id = cbmt.fk_media_type_id) mime_types,
		(case when not removetime is null THEN true ELSE false end) removed,
		removetime
	from
		cb_entry c, device d1, device d2, user_account ua
	where
		d1.device_hash = %(device_hash)s and
		ua.user_account_id = d1.fk_user_account_id and
		c.fk_owner_id = ua.user_account_id and
		d2.device_id = c.fk_device_id
		<filter_clause>
	order by copytime, removetime, cb_entry_id asc
	<limit_clause>
"""

sql_select_team_cb_entries = u"""
select
	cb.data_hash, tc.sharetime copytime, cb.text_preview,
	cb.image_preview,
	cb.files,ua1.username,
	d2.hostname, d2.os_type,
	ua2.username team_owner, t.teamname, ARRAY(
		select
			mime_type
		from
			media_type mt,
			cb_entry_media_type cbmt
		where
			cbmt.fk_cb_entry_id = cb.cb_entry_id and
			mt.media_type_id = cbmt.fk_media_type_id) mime_types,
	(case when not tc.removetime is null THEN true ELSE false end) removed,
	tc.removetime
	from
		device d1,
		device d2,
		user_account ua1,
		user_account ua2,
		team_member tm,
		team t,
		team_cb_entry tc,
		cb_entry cb
	where
		d1.device_hash = %(device_hash)s and
		tm.fk_user_account_id = d1.fk_user_account_id and
		t.team_id = tm.fk_team_id and
		ua2.user_account_id = t.fk_owner_id and
		tm.member_accepted = true and
		tm.owner_accepted = true and
		tc.fk_team_id = t.team_id and
		cb.cb_entry_id = tc.fk_cb_entry_id and
		ua1.user_account_id = cb.fk_owner_id and
		d2.device_id = cb.fk_device_id
		<filter_clause>
	order by copytime, removetime, cb_entry_id asc
	<limit_clause>
"""

sql_check_cb_access = u"""
	select 1
	from
		cb_entry c,
		device d
	where
		d.device_hash = %(device_hash)s and
		c.data_hash = %(data_hash)s and
		c.fk_owner_id = d.fk_user_account_id
	union
	select 1
	from
		cb_entry c,
		device d,
		team_cb_entry tc,
		team_member tm
	where
		d.device_hash = %(device_hash)s and
		c.data_hash = %(data_hash)s and
		tm.fk_user_account_id = d.fk_user_account_id and
		tm.member_accepted = true and
		tm.owner_accepted = true and
		tc.fk_team_id = tm.fk_team_id and
		tc.fk_cb_entry_id = c.cb_entry_id and
		not tc.sharetime is null
"""