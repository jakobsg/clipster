# -*- coding: utf-8 -*-

sql_regdev_insert = u"""
	insert into device (
		hardware_signature, device_hash, hostname,
		os_type, os_version, fk_user_account_id)
	select
		%(hardware_signature)s, %(device_hash)s, %(hostname)s,
		%(os_type)s, %(os_version)s, %(user_account_id)s
	where not exists (
		select 1
		from device
		where
			hardware_signature = %(hardware_signature)s )
"""

sql_regdev_update = u"""
	update device
	set
		device_hash = %(device_hash)s,
		hostname = %(hostname)s,
		os_type = %(os_type)s,
		os_version = %(os_version)s,
		fk_user_account_id = %(user_account_id)s
	where
		hardware_signature = %(hardware_signature)s
"""