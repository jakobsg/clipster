# -*- coding: utf-8 -*-

sql_team_insert = u'''
	insert into team (
		teamname,
		teamicon,
		private,
		searchable,
		fk_owner_id)
	select
		%(teamname)s,
		%(teamicon)s,
		%(private)s,
		%(searchable)s,
		d.fk_user_account_id
	from
		device d
	where
		d.device_hash = %(device_hash)s and
		not exists (
			select 1
			from team
			where
				teamname = %(teamname)s and
				fk_owner_id = d.fk_user_account_id )
	returning team_id
'''

sql_team_delete = u'''
	delete from team t
	where
		t.teamname = %(teamname)s and
		t.fk_owner_id = (
			select fk_user_account_id
			from
				device d
			where
				d.device_hash = %(device_hash)s )
'''

sql_team_members_delete_all = u'''
	delete from team_member
	where
		fk_team_id = (select t.team_id
	from
		team t,
		device d
	where
		d.device_hash = %(device_hash)s and
		t.teamname=%(teamname)s and
		t.fk_owner_id=d.fk_user_account_id)
'''

sql_team_member_delete = u'''
	delete from team_member
	where
		(fk_team_id, fk_user_account_id) in (
			select
				t.team_id, ua.user_account_id
			from
				team t,
				device d,
				user_account ua
			where
				d.device_hash = %(device_hash)s and
				t.fk_owner_id = d.fk_user_account_id and
				t.teamname = %(teamname)s and
				ua.username = %(member_username)s )
'''

sql_team_member_delete2 = u'''
	delete from team_member
	where
		(fk_team_id, fk_user_account_id) in (
			select
				t.team_id, d.fk_user_account_id
			from
				team t,
				device d,
				user_account ua
			where
				d.device_hash = %(device_hash)s and
				ua.username = %(team_owner)s and
				t.fk_owner_id = ua.user_account_id and 
				t.teamname = %(teamname)s )
'''

sql_team_id = u"""
	select t.team_id
	from
		team t
	where
		t.teamname=%(teamname)s and
		t.fk_owner_id=%(fk_owner_id)s
"""

sql_own_team_id = u"""
	select t.team_id
	from
		team t,
		device d
	where
		d.device_hash = %(device_hash)s and
		t.teamname=%(teamname)s and
		t.fk_owner_id=d.fk_user_account_id
"""

sql_select_teams = u'''
	select
		teamname,
		teamicon,
		private,
		searchable,
		ua.username,
		tm.owner_accepted,
		tm.member_accepted,
		(select count(*) from team_member tm2 where tm2.fk_team_id=t.team_id and tm2.owner_accepted=false) pending_owner_accepts,
		count(*) over() as full_count
	from
		team t
		cross join device d
		left outer join team_member tm on
			tm.fk_user_account_id = d.fk_user_account_id and
			tm.fk_team_id = t.team_id,
		user_account ua
	where
		d.device_hash = %(device_hash)s and
		(t.searchable=true or t.fk_owner_id=d.fk_user_account_id or tm.owner_accepted=true) and
		ua.user_account_id = t.fk_owner_id
		<filter_clause>
	order by teamname
	<limit_clause>
'''

sql_select_teams2 = u'''
	select
		teamname,
		teamicon,
		private,
		searchable,
		ua.username,
		tm.owner_accepted,
		tm.member_accepted,
		(select count(*) from team_member tm2 where tm2.fk_team_id=t.team_id and owner_accepted=false) pending_owner_accepts,
		count(*) over() as full_count
	from
		device d,
		team t,
		team_member tm,
		user_account ua
	where
		d.device_hash = %(device_hash)s and
		tm.fk_user_account_id = d.fk_user_account_id and
		t.team_id = tm.fk_team_id and
		(t.searchable=true or t.fk_owner_id=d.fk_user_account_id or tm.owner_accepted=true) and
		ua.user_account_id = t.fk_owner_id
		<filter_clause>
	order by teamname
	<limit_clause>
'''

sql_select_members = u'''
	select
		ua2.username,
		tm.owner_accepted,
		tm.member_accepted,
		count(*) over() as full_count
	from
		user_account ua1,
		team t,
		team_member tm,
		user_account ua2
	where
		ua1.username = %(team_owner)s and
		t.fk_owner_id = ua1.user_account_id and
		t.teamname = %(teamname)s and
		tm.fk_team_id = t.team_id and
		ua2.user_account_id = tm.fk_user_account_id
	order by username, tm.team_member_id
	<limit_clause>
'''

sql_team_member_insert = u'''
	insert into team_member (
		fk_team_id,
		fk_user_account_id,
		owner_accepted,
		member_accepted
	)
	select
		t.team_id,
		d.fk_user_account_id,
		%(owner_accepted)s,
		%(member_accepted)s
	from
		user_account ua1,
		device d,
		team t
	where
		ua1.username = %(team_owner)s and
		t.fk_owner_id = ua1.user_account_id and
		d.device_hash = %(device_hash)s and
		t.teamname = %(teamname)s and
		not exists (
			select 1
			from team_member
			where
				fk_team_id = t.team_id and
				fk_user_account_id = d.fk_user_account_id )
	returning team_member_id
'''

sql_team_member_self = u'''
	insert into team_member (
		fk_team_id,
		fk_user_account_id,
		owner_accepted,
		member_accepted
	)
	select
		%(fk_team_id)s,
		d.fk_user_account_id,
		%(owner_accepted)s,
		%(member_accepted)s
	from
		device d
	where
		d.device_hash = %(device_hash)s and
		not exists (
			select 1
			from team_member tm
			where
				tm.fk_team_id = %(fk_team_id)s and
				tm.fk_user_account_id = d.fk_user_account_id )
	returning team_member_id
'''

sql_team_invite_insert = u'''
	insert into team_member (
		fk_team_id,
		fk_user_account_id,
		owner_accepted,
		member_accepted
	)
	select
		t.team_id,
		ua.user_account_id,
		%(owner_accepted)s,
		%(member_accepted)s
	from
		user_account ua,
		device d,
		team t
	where
		ua.username = %(invitee_username)s and
		d.device_hash = %(device_hash)s and
		t.fk_owner_id = d.fk_user_account_id and
		t.teamname = %(teamname)s and
		not exists (
			select 1
			from team_member
			where
				fk_team_id = t.team_id and
				fk_user_account_id = ua.user_account_id )
	returning team_member_id
'''

sql_accept_membership = u'''
	update team_member
	set
		owner_accepted = true
	where
		(fk_team_id, fk_user_account_id) in (
			select
				t.team_id, ua.user_account_id
			from
				team t,
				device d,
				user_account ua
			where
				d.device_hash = %(device_hash)s and
				t.fk_owner_id = d.fk_user_account_id and
				t.teamname = %(teamname)s and
				ua.username = %(member_username)s )
'''

sql_accept_invitation = u'''
	update team_member
	set
		member_accepted = true
	where
		(fk_team_id, fk_user_account_id) in (
			select
				t.team_id, d.fk_user_account_id
			from
				team t,
				device d,
				user_account ua
			where
				d.device_hash = %(device_hash)s and
				ua.username = %(team_owner)s and
				t.fk_owner_id = ua.user_account_id and 
				t.teamname = %(teamname)s )
'''

sql_pending_invitations = u'''
	select count(*)
	from
		device d,
		team_member tm
	where
		d.device_hash = %(device_hash)s and
		tm.fk_user_account_id = d.fk_user_account_id and
		tm.owner_accepted = true and
		tm.member_accepted = false
'''

sql_pending_member_accepts = u'''
	select count(*)
	from
		device d,
		team t,
		team_member tm
	where
		d.device_hash = %(device_hash)s and
		t.fk_owner_id = d.fk_user_account_id and
		tm.fk_team_id = t.team_id and
		tm.owner_accepted = false and
		tm.member_accepted = true
'''

sql_team_cb_entry_insert = u'''
	insert into team_cb_entry (
		fk_team_id,
		fk_cb_entry_id,
		sharetime
	)
	select
		t.team_id,
		ce.cb_entry_id,
		now()
	from
		user_account ua,
		team t,
		cb_entry ce
	where
		ua.username = %(team_owner)s and
		t.fk_owner_id = ua.user_account_id and
		t.teamname = %(teamname)s and
		ce.data_hash = %(data_hash)s and
		not exists (
			select 1
			from team_cb_entry tce
			where
				tce.fk_team_id = t.team_id and
				tce.fk_cb_entry_id = ce.cb_entry_id) and
		exists (
			-- Check team access
			select 1
			from
				device d,
				team_member tm
			where
				d.device_hash = %(device_hash)s and
				tm.fk_user_account_id = d.fk_user_account_id and
				tm.fk_team_id = t.team_id and
				tm.owner_accepted=true and
				tm.member_accepted=true )
	returning team_cb_entry_id
'''

sql_team_cb_entry_update = u'''
	update team_cb_entry tce
	set
		sharetime = <sharetime>,
		removetime = <removetime>
	from
		user_account ua,
		team t,
		cb_entry ce
	where
		ua.username = %(team_owner)s and
		t.fk_owner_id = ua.user_account_id and
		t.teamname = %(teamname)s and
		ce.data_hash = %(data_hash)s and
		exists (
			-- Check team access
			select 1
			from
				device d,
				team_member tm
			where
				d.device_hash = %(device_hash)s and
				tm.fk_user_account_id = d.fk_user_account_id and
				tm.fk_team_id = t.team_id and
				tm.owner_accepted=true and
				tm.member_accepted=true ) and
	tce.fk_team_id = t.team_id and
	tce.fk_cb_entry_id = ce.cb_entry_id
'''