#-*- coding: utf-8 -*-

def mk_filter_clause(search_frase, search_attribs, ignore_case=False, table_alias=u''):
	filter_clause = u''
	if search_frase:
		search_frase = search_frase.replace('*','%')
		op = u'='
		if search_frase.find('%')>-1:
			op = u'like' if not ignore_case else u'ilike'
		filter_clause = u'and (' + (u' or '.join(map(lambda x: '%(p)s%(x)s %(op)s %%(search_frase)s' % {'p':table_alias, 'x':x,'op':op},search_attribs))) + u')'
	return filter_clause

def mk_insert_parts(attribs):
	return u', '.join(attribs.keys()), u'select ' + (u', '.join(map(lambda x: u'%%(%s)s' % x, attribs.keys())))

def mk_update_part(attribs):
	return u', '.join(map(lambda k: u'%(k)s=%%(%(k)s)s' % {'k':k}, attribs.keys()))

def mk_limit_clause(pagenumber=0,pagesize=None):
	if pagesize:
		return u'limit %d offset %d' % (pagesize, pagenumber*pagesize)
	return u''
