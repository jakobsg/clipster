# -*- coding: utf-8 -*-

sql_init_insert = u"""
	insert into user_account (
		username, password, verification_code)
	select 
		%(username)s, %(password_hash)s, %(verification_code)s
	where not exists (
		select 1
		from user_account
		where
			username = %(username)s )
"""

sql_init_update = u"""
	update user_account
	set
		password = %(password_hash)s,
		verification_code = %(verification_code)s
	where 
		username = %(username)s
"""
 
sql_check_created = u"""
	select
		1
	from
		user_account
	where
		username = %(username)s and
		activated = true
"""

sql_select_users = u'''
	select
		username,
		firstname,
		lastname,
		count(*) over() as full_count
	from
		user_account ua
	where
		1=1
		<filter_clause>
	order by username, user_account_id
	<limit_clause>
'''


sql_activate_user = u"""
	update user_account
	set
		activated = true,
		created = NOW()
	where 
		verification_code = %(verification_code)s
"""

sql_passwd_authenticate = u"""
	select user_account_id
	from user_account
	where
		username = %(username)s and
		password = %(password_hash)s
"""

sql_user_id = u"""
	select user_account_id
	from user_account
	where
		username=%(username)s
"""

sql_user_by_device = u"""
	select
		ua.user_account_id, ua.username,
		ua.firstname, ua.lastname,
		ua.facebook_id, ua.google_id
	from
		user_account ua,
		device d
	where
		ua.user_account_id = d.fk_user_account_id and
		d.device_hash = %(device_hash)s
"""
