# -*- coding: utf-8 -*-

from clipster.db.connector import Connection
from clipster.db.datamanager_sql import *
from clipster.tools.payloadutil import Sha1Producer, simplify_string
from clipster.db.tools import mk_filter_clause, mk_update_part, mk_limit_clause
import clipster.conf as conf
from datetime import datetime
import os,shutil
from PIL import Image
from StringIO import StringIO
from ladon.tools.log import info
import re, cchardet
from ladon.compat import PORTABLE_STRING

rx_content_disp = re.compile("filename=\"([^\"]+)\"")

class DataManager(object):
	
	def store_data(self, device_hash, data_hash, attachments):
		info("Store data: %s" % str(data_hash))
		if not data_hash:
			sp = Sha1Producer()
		
		mime_formats = []
		text_preview = None
		image_preview = None
		has_files = False
		max_preview_size = 512
		total_size = 0
		for a_id,a_info in attachments.items():
			if os.path.exists(a_info.get('path','/no-way')):
				mime_format = a_info.get('headers',{}).get('CONTENT_TYPE',u'application/unknown')
				total_size += os.stat(a_info.get('path')).st_size
				if not data_hash:
					sp.add_data_file(a_info.get('path'))
				if mime_format=='text/plain':
					text_preview = open(a_info.get('path'),'rb').read()
					cd = cchardet.detect(text_preview)
					text_preview = PORTABLE_STRING(text_preview, cd['encoding'])
					if len(text_preview)>max_preview_size:
						text_preview = text_preview[:max_preview_size/2]+text_preview[-(max_preview_size/2):]
				elif mime_format=='image/png':
					i = Image.open(a_info.get('path'))
					i.thumbnail((64,48))
					fp = StringIO()
					i.save(fp,'PNG')
					image_preview = bytearray(fp.getvalue())
				elif mime_format=='image/jpeg' or mime_format=='image/jpg':
					i = Image.open(a_info.get('path'))
					i.thumbnail((64,48))
					fp = StringIO()
					i.save(fp,'PNG')
					image_preview = bytearray(fp.getvalue())
				elif mime_format=='application/octet-stream':
					has_files = True
					mime_format = 'text/uri-list'
				mime_formats = list(set(mime_formats + [mime_format]))
		if not data_hash:
			data_hash = sp.hexdigest()
		dbargs = {
			'data_hash': data_hash,
			'text_preview': text_preview,
			'image_preview': image_preview,
			'files': has_files,
			'device_hash': device_hash,
			'removetime': None,
			'size': total_size
		}
		conn = Connection()
		conn.rollback()
		cur = conn.cursor();
		cur.execute(sql_update_cb_entry_unremove,dbargs)
		
		if (cur.rowcount==0):
			# Pre-removed cb_entry does not exist, insert new
			cur.execute(sql_insert_cb_entry,dbargs)
			res = cur.fetchone()
			if res==None:
				return False
			cb_entry_id = res[0]
			
			# Add mime-types
			for mime_type in mime_formats:
				cur.execute(sql_insert_cb_media_type,{
					'mime_type': mime_type,
					'cb_entry_id': cb_entry_id
				})
				if cur.rowcount==0:
					cur.execute(sql_insert_media_type, { 'mime_type': mime_type })
					cur.execute(sql_insert_cb_media_type,{
						'mime_type': mime_type,
						'cb_entry_id': cb_entry_id
					})

		# save data
		for a_id,a_info in attachments.items():
			open('/tmp/debug-save','a').write(str(a_info)+str(a_id)+"\n")
			if os.path.exists(a_info.get('path','/no-way')):
				headers = a_info.get('headers',{})
				if not headers:
					contine
				content_disposition = headers.get('CONTENT_DISPOSITION')
				if content_disposition:
					m = rx_content_disp.search(content_disposition)
					if m is None:
						# Could not probe filename
						continue
					# File upload
					filename = m.groups()[0]
					root_dir_entry = headers.get('ROOT_ENTRY_NAME')
					entry_sub_dir = headers.get('SUB_DIRECTORY')
					if entry_sub_dir:
						# ensure posix path separators
						entry_sub_dir = entry_sub_dir.replace("\\","/")
					# Move into place
					data_path = os.path.join(conf.datadir,data_hash,'files',*map(lambda x : x.decode('utf-8'), filter(lambda x : x, (root_dir_entry, entry_sub_dir, filename))))
					data_dir = os.path.abspath(os.path.join(data_path,'..'))
					if not os.path.exists(data_dir):
						os.makedirs(data_dir)
					os.rename(a_info.get('path'),data_path)
					
				else:
					# Mime data upload
					mime_format = a_info.get('headers',{}).get('CONTENT_TYPE',u'application/unknown')
					data_path = os.path.join(conf.datadir,data_hash,'mimedata',mime_format)
					data_dir = os.path.abspath(os.path.join(data_path,'..'))
					if not os.path.exists(data_dir):
						os.makedirs(data_dir)
					os.rename(a_info.get('path'),data_path)
		
		conn.commit()
		return data_hash
	
	
	def remove_data_entry(self, device_hash, data_hash):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor();
		dbargs = {
			'device_hash': device_hash,
			'data_hash': data_hash
		}
		cur.execute(sql_update_cb_entry_removed,dbargs)
		if cur.rowcount==1:
			conn.commit()
			data_path = os.path.join(conf.datadir, data_hash)
			try:
				shutil.rmtree(data_path)
			except:
				pass
			return True
		return False
		
	
	def list_data_entries(self, device_hash, since_timestamp=None, pagenumber=0, pagesize=None):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor(True)
		dbargs = {
			'device_hash': device_hash
		}
		sql = sql_select_cb_entries.replace('<limit_clause>',mk_limit_clause(pagenumber,pagesize))
		if since_timestamp:
			sql = sql.replace('<filter_clause>'," and greatest(copytime,removetime) > TIMESTAMP WITHOUT TIME ZONE 'epoch' + %(epoch)s * INTERVAL '1 msecond'" % {'epoch':since_timestamp})
		else:
			sql = sql.replace('<filter_clause>','')
		cur.execute(sql,dbargs)
		while True:
			res = cur.fetchmany(50)
			if not res:
				break
			for row in res:
				yield row

	def list_team_data_entries(self, device_hash, since_timestamp=None, pagenumber=0, pagesize=None):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor(True)
		dbargs = {
			'device_hash': device_hash
		}
		sql = sql_select_team_cb_entries.replace('<limit_clause>',mk_limit_clause(pagenumber,pagesize))
		if since_timestamp:
			sql = sql.replace('<filter_clause>'," and greatest(tc.sharetime,tc.removetime) > TIMESTAMP WITHOUT TIME ZONE 'epoch' + %(epoch)s * INTERVAL '1 msecond'" % {'epoch':since_timestamp})
		else:
			sql = sql.replace('<filter_clause>','')
		cur.execute(sql,dbargs)
		while True:
			res = cur.fetchmany(50)
			if not res:
				break
			for row in res:
				yield row

	def data_mime_parts(self, device_hash, data_hash):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor()
		dbargs = {
			'device_hash': device_hash,
			'data_hash': data_hash
		}
		cur.execute(sql_check_cb_access,dbargs)
		mimeparts = {}
		if cur.fetchone():
			data_dir = os.path.join(conf.datadir,data_hash,'files')
			if os.path.exists(data_dir):
				# File copy
				for root, dirs, files in os.walk(data_dir):
					for name in files:
						full_path = os.path.join(root, name)
						reldir_split = os.path.relpath(root, data_dir).split(os.path.sep)
						root_entry_name = None if reldir_split[0]=='.' else reldir_split[0]
						sub_directory = None if len(reldir_split)<2 else os.path.sep.join(reldir_split[1:])
						content_disposition = u'attachment; filename="%s"' % name
						mimeparts[(root_entry_name,sub_directory,content_disposition)] = open(full_path, 'rb')
			else:
				# Mime data
				data_dir = os.path.join(conf.datadir,data_hash,'mimedata')
				for d,dirs,files in os.walk(data_dir):
					for f in files:
						mimeparts[os.path.relpath(os.path.join(d,f),data_dir)] = open(os.path.join(d,f),'rb')
		return mimeparts
	
