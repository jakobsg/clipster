from clipster.db.connector import Connection
from clipster.db.devicemanager_sql import *
from datetime import datetime
from hashlib import sha1
import time
import os

class DeviceManager(object):

	def register_device(self, user_account_id, hardware_signature, hostname, os_type, os_version):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor()
		device_hash = sha1()
		device_hash.update(str(time.time()) + hostname + hardware_signature + os_type + os_version)
		dbargs = {
			'user_account_id': user_account_id,
			'hardware_signature': hardware_signature,
			'hostname': hostname,
			'os_type': os_type,
			'os_version': os_version,
			'device_hash': device_hash.hexdigest()
		}
		cur.execute(sql_regdev_insert,dbargs)
		if cur.rowcount==0:
			cur.execute(sql_regdev_update,dbargs)
			if cur.rowcount==0:
				return 1, None # Unknown problem
		conn.commit()
		return 0, device_hash.hexdigest() # Device registered
