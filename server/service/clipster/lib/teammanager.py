# -*- coding: utf-8 -*-

from clipster.db.connector import Connection
from clipster.db.teammanager_sql import *
import clipster.lib.usermanager as uman
from clipster.db.tools import mk_filter_clause, mk_update_part, mk_limit_clause

um = uman.UserManager()

class TeamManager(object):
	
	def create_team(self, device_hash, teamname, private, searchable):
		conn = Connection()
		conn.rollback()
		dbargs = {
			'device_hash': device_hash,
			'teamname': teamname,
			'teamicon': None,
			'private': private,
			'searchable': searchable
		}
		cur = conn.cursor()
		cur.execute(sql_team_insert, dbargs)
		res = cur.fetchone()
		if res and len(res)==1:
			team_id = res[0]
		else:
			conn.rollback()
			return False
		
		dbargs['fk_team_id'] = team_id
		dbargs['owner_accepted'] = True
		dbargs['member_accepted'] = True
		cur.execute(sql_team_member_self, dbargs)
		conn.commit()
		return True
		
	def remove_team(self, device_hash, teamname):
		conn = Connection()
		conn.rollback()
		dbargs = {
			'device_hash': device_hash,
			'teamname': teamname
		}
		cur = conn.cursor()
		cur.execute(sql_team_members_delete_all, dbargs)
		cur.execute(sql_team_delete, dbargs)
		if cur.rowcount>0:
			conn.commit()
			return True
		else:
			return False
		
		
	def list_teams(self, device_hash, searchfrase=u'', only_owned=False, pagenumber=0, pagesize=None):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor(True)
		dbargs = {
			'device_hash': device_hash
		}
		user = um.user_by_device(device_hash)
		if len(user):
			sql = sql_select_teams.replace('<limit_clause>',mk_limit_clause(pagenumber,pagesize))
			clauses = []
			if only_owned:
				clauses += [ 't.fk_owner_id = %d' % user['user_account_id'] ]
			if searchfrase:
				clauses += [ "lower(t.teamname) like '%s'" % searchfrase.replace('*','%%').lower() ]
			if len(clauses):
				sql = sql.replace('<filter_clause>', 'and ' + (' and '.join(clauses)))
			else:
				sql = sql.replace('<filter_clause>', '')
				
			cur.execute(sql,dbargs)
			while True:
				res = cur.fetchmany(50)
				if not res:
					break
				for row in res:
					yield row

	def list_members(self, device_hash, team_owner, teamname, pagenumber=0, pagesize=None):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor(True)
		dbargs = {
			'device_hash': device_hash,
			'team_owner': team_owner,
			'teamname': teamname
		}
		sql = sql_select_members.replace('<limit_clause>',mk_limit_clause(pagenumber,pagesize))
		cur.execute(sql,dbargs)
		while True:
			res = cur.fetchmany(50)
			if not res:
				break
			for row in res:
				yield row


	def list_own_memberships(self, device_hash, searchfrase=u'', pagenumber=0, pagesize=None):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor(True)
		dbargs = {
			'device_hash': device_hash
		}
		sql = sql_select_teams2.replace('<limit_clause>',mk_limit_clause(pagenumber,pagesize))
		clauses = []
		if searchfrase:
			clauses += [ "lower(t.teamname) like '%s'" % searchfrase.replace('*','%%').lower() ]
		if len(clauses)>0:
			sql = sql.replace('<filter_clause>', 'and ' + (' and '.join(clauses)))
		else:
			sql = sql.replace('<filter_clause>', '')
		cur.execute(sql,dbargs)
		while True:
			res = cur.fetchmany(50)
			if not res:
				break
			for row in res:
				yield row

	def request_membership(self, device_hash, team_owner, teamname):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor()
		dbargs = {
			'device_hash': device_hash,
			'teamname': teamname,
			'team_owner': team_owner,
			'owner_accepted': False,
			'member_accepted': True
		}
		cur.execute(sql_team_member_insert, dbargs)
		res = cur.fetchone()
		if not res is None:
			conn.commit()
			return True
		else:
			return False

	def send_invitation(self, device_hash, teamname, invitee_username):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor()
		dbargs = {
			'device_hash': device_hash,
			'teamname': teamname,
			'invitee_username': invitee_username,
			'owner_accepted': True,
			'member_accepted': False
		}
		cur.execute(sql_team_invite_insert, dbargs)
		res = cur.fetchone()
		if not res is None:
			conn.commit()
			return True
		else:
			return False

	def exclude_member(self, device_hash, teamname, member_username):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor()
		dbargs = {
			'device_hash': device_hash,
			'teamname': teamname,
			'member_username': member_username
		}
		
		cur.execute(sql_team_member_delete, dbargs)
		if cur.rowcount:
			conn.commit()
			return True
		else:
			return False

	def withdraw_membership(self, device_hash, team_owner, teamname):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor()
		dbargs = {
			'device_hash': device_hash,
			'teamname': teamname,
			'team_owner': team_owner
		}
		cur.execute(sql_team_member_delete2, dbargs)
		if cur.rowcount:
			conn.commit()
			return True
		else:
			return False

	def accept_membership(self, device_hash, teamname, member_username):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor()
		dbargs = {
			'device_hash': device_hash,
			'teamname': teamname,
			'member_username': member_username
		}
		cur.execute(sql_accept_membership, dbargs)
		if cur.rowcount:
			conn.commit()
			return True
		return False
	
	def accept_invitation(self, device_hash, team_owner, teamname):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor()
		dbargs = {
			'device_hash': device_hash,
			'teamname': teamname,
			'team_owner': team_owner
		}
		cur.execute(sql_accept_invitation, dbargs)
		if cur.rowcount:
			conn.commit()
			return True
		return False

	def pending_invitations(self, device_hash):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor()
		dbargs = {
			'device_hash': device_hash
		}
		cur.execute(sql_pending_invitations, dbargs)
		res = cur.fetchone()
		if not res is None:
			return res[0]
		return 0
	
	def pending_member_accepts(self, device_hash):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor()
		dbargs = {
			'device_hash': device_hash
		}
		cur.execute(sql_pending_member_accepts, dbargs)
		res = cur.fetchone()
		if not res is None:
			return res[0]
		return 0
	
	def share_cb_entry(self, device_hash, team_owner, teamname, data_hash):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor()
		dbargs = {
			'device_hash': device_hash,
			'team_owner': team_owner,
			'teamname': teamname,
			'data_hash': data_hash
		}
		cur.execute(sql_team_cb_entry_insert, dbargs)
		res = cur.fetchone()
		if not res is None:
			conn.commit()
			return True
		else:
			sql = sql_team_cb_entry_update
			sql = sql.replace('<sharetime>','now()')
			sql = sql.replace('<removetime>','null')
			cur.execute(sql, dbargs)
			if cur.rowcount>0:
				conn.commit()
				return True
		return False

	def unshare_cb_entry(self, device_hash, team_owner, teamname, data_hash):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor()
		dbargs = {
			'device_hash': device_hash,
			'team_owner': team_owner,
			'teamname': teamname,
			'data_hash': data_hash
		}
		sql = sql_team_cb_entry_update
		sql = sql.replace('<sharetime>','null')
		sql = sql.replace('<removetime>','now()')
		cur.execute(sql, dbargs)
		if cur.rowcount>0:
			conn.commit()
			return True
		return False
