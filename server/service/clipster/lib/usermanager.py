# -*- coding: utf-8 -*-

from clipster.db.connector import Connection
from clipster.db.usermanager_sql import *
from clipster.db.tools import mk_filter_clause, mk_update_part, mk_limit_clause
from datetime import datetime
from hashlib import sha1
import time
import os

class UserManager(object):
	
	pw_salt = 'U2oeeWqMc6FfPYDUojJo'


	def init_user(self, username, password):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor()
		code = sha1()
		code.update(str(time.time())+username+password)
		password_hash = sha1()
		password_hash.update(password + self.pw_salt)
		dbargs = {
			'username': username,
			'password_hash': password_hash.hexdigest(),
			'verification_code': code.hexdigest()
		}
		cur.execute(sql_check_created,dbargs)
		if cur.fetchone():
			return 1, None # User already exists
		cur.execute(sql_init_insert,dbargs)
		if cur.rowcount==0:
			cur.execute(sql_init_update,dbargs)
			if cur.rowcount==0:
				return 2, None # Unknown problem
		conn.commit()
		return 0, code.hexdigest() # User initialized


	def verify_user(self, verification_code):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor()
		dbargs = {
			'verification_code': verification_code
		}
		cur.execute(sql_activate_user,dbargs)
		if cur.rowcount==0:
			return 1
		conn.commit()
		return 0 # User activated


	def authenticate(self, username, password):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor()
		password_hash = sha1()
		password_hash.update(password + self.pw_salt)
		dbargs = {
			'username': username,
			'password_hash': password_hash.hexdigest()
		}
		cur.execute(sql_passwd_authenticate,dbargs)
		res = cur.fetchone()
		if res==None:
			return None
		return res[0] # login succesful return user_account_id

	def list_users(self, device_hash, search_frase=u'*', pagenumber=0, pagesize=None):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor(True)
		dbargs = {
			'device_hash': device_hash
		}
		user = self.user_by_device(device_hash)
		if len(user):
			sql = sql_select_users.replace('<limit_clause>',mk_limit_clause(pagenumber,pagesize))
			clauses = []
			if search_frase:
				ors = []
				ors += [ u"lower(ua.username) like '%s'" % search_frase.replace('*','%%').lower() ]
				ors += [ u"lower(ua.firstname) like '%s'" % search_frase.replace('*','%%').lower() ]
				ors += [ u"lower(ua.lastname) like '%s'" % search_frase.replace('*','%%').lower() ]
				clauses += [ u' or '.join( ors ) ]
			if len(clauses):
				sql = sql.replace('<filter_clause>', 'and ' + (' and '.join(clauses)))
			else:
				sql = sql.replace('<filter_clause>', '')
			open('/tmp/bla','w').write(sql)
			cur.execute(sql,dbargs)
			while True:
				res = cur.fetchmany(50)
				if not res:
					break
				for row in res:
					yield row


	def user_id(self, username):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor()
		dbargs = {
			'username': username
		}
		cur.execute(sql_user_id,dbargs)
		res = cur.fetchone()
		if res==None:
			return 0
		return res[0] # login succesful


	def user_by_device(self, device_hash):
		conn = Connection()
		conn.rollback()
		cur = conn.cursor(True)
		dbargs = {
			'device_hash': device_hash
		}
		cur.execute(sql_user_by_device,dbargs)
		res = cur.fetchone()
		if res==None:
			return None
		
		return dict(res)
		