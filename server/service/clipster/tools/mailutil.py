# -*- coding: utf-8 -*-
from ladon.compat import PORTABLE_STRING
import smtplib

def send_mail(fromaddr,toaddrs,subject,body):
	if type(fromaddr)==PORTABLE_STRING:
		fromaddr = fromaddr.encode('utf-8')
	if type(toaddrs) not in [list,tuple]:
		toaddrs = [toaddrs]
	toaddrs_csv = (u", ".join(toaddrs)).encode('utf-8')
	if type(body)==PORTABLE_STRING:
		body = body.encode('utf-8')
	if type(subject)==PORTABLE_STRING:
		subject = subject.encode('utf-8')
	
	msg = """From: %(fromaddr)s
To: %(toaddr)s
MIME-Version: 1.0
Content-type: text/html; charset=UTF-8
Subject: %(subject)s

	%(body)s
	""" % {
		'fromaddr': fromaddr,
		'toaddr': toaddrs_csv,
		'subject': subject,
		'body': body }

	server = smtplib.SMTP('localhost')
	server.set_debuglevel(1)
	server.sendmail(fromaddr, toaddrs, msg)
	server.quit()
	
