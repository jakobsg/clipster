import memcache,os
import ConfigParser
import sys
import clipster_conf

if sys.version_info.major==2:
	from thread import get_ident
if sys.version_info.major==3:
	from threading import get_ident

mc_server = getattr(clipster_conf,'memcache_host','127.0.0.1')
mc_port = getattr(clipster_conf,'memcache_port',11211)
mc_max_value_size = getattr(clipster_conf,'max_value_size',2^12)
conn_pool = {}                                                                                                                                                                                                                                                                 

memcache_server = '%s:%s' % (mc_server, mc_port)
                                                                                                                                                                              
def get_mc_client():                                                                                                                                                                                                                                                           
	global conn_pool, memcache_server, mc_max_value_size
	if get_ident() not in conn_pool or len(conn_pool[get_ident()].get_stats())==0:
		conn_pool[get_ident()] = memcache.Client([memcache_server])
		conn_pool[get_ident()].server_max_value_length = mc_max_value_size
	return conn_pool[get_ident()]                                                                                                                                                                                                                                   
