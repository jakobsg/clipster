# -*- coding: utf-8 -*-

import hashlib, re

class Sha1Producer(object):
	
	def __init__(self):
		self.sha1 = hashlib.sha1()
		
	def add_data_file(self, filename, blocksize=65536):
		with open(filename, "r+b") as f:
			for block in iter(lambda: f.read(blocksize), ""):
				self.sha1.update(block)

	def hexdigest(self):
		return self.sha1.hexdigest()


def simplify_string(filename, max_size=512, blocksize=65536):
	final = ""
	with open(filename, "r") as f:
		for block in iter(lambda: f.read(blocksize), ""):
			final += ' '.join(block.split())
	if len(final)>max_size:
		return final[:max_size/2]+final[-(max_size/2):]
	else:
		return final
 
