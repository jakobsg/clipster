#-*- coding: utf-8 -*-
from ladon.types.ladontype import LadonType
from ladon.compat import PORTABLE_STRING, PORTABLE_BYTES
from ladon.exceptions.service import ClientFault
import re,time

def check_isodate(isodate):
	if re.match('^\d{4}-\d{2}-\d{2}$',isodate):
		try:
			time.strptime(isodate,'%Y-%m-%d')
		except:
			raise ClientFault('Date is out of range')
	else:
		raise ClientFault('Bad date format')
	return isodate

def check_isotimestamp(isotimestamp):
	if re.match('^\d{4}-\d{2}-\d{2}$ \d{2}:\d{2}$:\d{2}$',isotimestamp):
		try:
			time.strptime(isotimestamp,'%Y-%m-%d %H:%M:%S')
		except:
			raise ClientFault('Timestamp is out of range')
	else:
		raise ClientFault('Bad timestamp format, must be ISO timestamp: YYYY-MM-DD HH:MM:SS')
	return isotimestamp

mandatory_int = {
	'type': int,
	'optional': False
}

default_nullable_int = {
	'type': int,
	'nullable': True,
	'default': None
}

default_nullable_float = {
	'type': float,
	'nullable': True,
	'default': None
}

mandatory_string = {
	'type': PORTABLE_STRING,
	'optional': False
}

default_nullable_string = {
	'type': PORTABLE_STRING,
	'nullable': True,
	'default': None
}

default_nullable_bytes = {
	'type': PORTABLE_BYTES,
	'nullable': True,
	'default': None
}

mandatory_isodate = {
	'type': PORTABLE_STRING,
	'nullable': False,
	'doc': 'Enter a valid ISO-8601 (YYYY-MM-DD)',
	'filters': {
		'incoming': [check_isodate]
	}
}

mandatory_isotimestamp = {
	'type': PORTABLE_STRING,
	'nullable': False,
	'doc': 'Enter a valid ISO-8601 (YYYY-MM-DD HH:MM:SS)',
	'filters': {
		'incoming': [check_isotimestamp]
	}
}

mandatory_bool = {
	'type': bool,
	'optional': False
}

nullable_bool = {
	'type': bool,
	'nullable': True,
	'default': None
}

class MethodResult(LadonType):
	
	res_code = mandatory_int
	res_msg = mandatory_string
	res_details = default_nullable_string
	
	def __init__(self, res_code, res_msg, **kw):
		self.res_code = res_code
		self.res_msg = res_msg
		super(MethodResult, self).__init__(**kw)


class KeyValue(LadonType):
	key = PORTABLE_STRING
	value = PORTABLE_STRING


class CommonResponse(LadonType):
	
	method_result = MethodResult
	
	def __init__(self, res_code=0, res_msg=u'Success', **kw):
		self.method_result = MethodResult(res_code, res_msg)
		super(CommonResponse, self).__init__(**kw)


class SuccessResponse(CommonResponse):
	success = bool

def check_sort_direction(sort_direction):
	if sort_direction in [u'asc',u'desc']:
		return sort_direction
	raise ClientFault(u'The sort direction must be "asc" or "desc" got "%s"' % sort_direction)

class NumberResponse(CommonResponse):
	number = mandatory_int
