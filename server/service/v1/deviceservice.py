# -*- coding: utf-8 -*-

from ladon.ladonizer import ladonize
from ladon.compat import PORTABLE_STRING
from deviceservice_types import *
from common_types import SuccessResponse
import os,hashlib,re
from clipster.tools.memcacheutil import get_mc_client
import clipster.lib.devicemanager as dman

dm = dman.DeviceManager()

class DeviceService(object):

	@ladonize(PORTABLE_STRING, PORTABLE_STRING, PORTABLE_STRING, PORTABLE_STRING, PORTABLE_STRING, rtype=RegisterDeviceResponse)
	def registerDevice(self, login_token, hardware_signature, os_type, os_version, hostname, **export):
		resp = RegisterDeviceResponse()
		resp.success = False
		mc = get_mc_client()
		user_account_id = mc.get(login_token.encode('utf-8'))
		if user_account_id==None:
			resp.method_result.res_code = 20001
			resp.method_result.res_msg = u'Invalid login_token'
			return resp
		if user_account_id:
			res, device_hash = dm.register_device(user_account_id, hardware_signature, hostname, os_type, os_version)
			if res==1:
				resp.method_result.res_code = 20002
				resp.method_result.res_msg = u'Internal error: Unable to register device'
			resp.device_hash = device_hash
			resp.success = True
		return resp
