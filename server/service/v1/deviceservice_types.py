# -*- coding: UTF-8 -*- 

from ladon.types.ladontype import LadonType
from common_types import *

class RegisterDeviceResponse(SuccessResponse):
	device_hash = default_nullable_string

