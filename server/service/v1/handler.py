from ladon.server.wsgi import LadonWSGIApplication
from os.path import abspath,dirname
from ladon.tools.log import set_loglevel,set_logfile
import sys

sys.path.append(dirname(abspath(__file__)))
from clipster.conf import service_logfile, service_loglevel, service_logging_flags

set_logfile(service_logfile)
set_loglevel(service_loglevel)

 
application = LadonWSGIApplication(
  ['transferservice','userservice','deviceservice','teamservice'],
  [dirname(abspath(__file__))],
  catalog_name='Customer Activity Log web service catalog',
  catalog_desc='Customer Activity Log web service provides methods to manage customers and customer activities',
  logging=reduce(lambda x,y: x|y, service_logging_flags))
