# -*- coding: utf-8 -*-

from ladon.ladonizer import ladonize
from ladon.compat import PORTABLE_STRING
from teamservice_types import *
from common_types import SuccessResponse
import os,re,time
import clipster.lib.teammanager as tman
#import clipster.lib.usermanager as uman
#from clipster.tools.mailutil import send_mail
from clipster.tools.memcacheutil import get_mc_client
from hashlib import sha1
#um = uman.UserManager()

tm = tman.TeamManager()

class TeamService(object):
	"""
	Dette er team service
	"""
	@ladonize(PORTABLE_STRING, PORTABLE_STRING, bool, bool, rtype=SuccessResponse)
	def createTeam(self, device_hash, teamname, private=True, searchable=True):
		resp = SuccessResponse()
		resp.success = tm.create_team(device_hash, teamname, private, searchable)
		return resp
			
	@ladonize(PORTABLE_STRING, PORTABLE_STRING, rtype=SuccessResponse)
	def removeTeam(self, device_hash, teamname):
		resp = SuccessResponse()
		resp.success = tm.remove_team(device_hash, teamname)
		return resp

	@ladonize(PORTABLE_STRING, PORTABLE_STRING, bool, int, int, int, rtype=ListTeamsResponse)
	def listTeams(self, device_hash, search_frase=u'*', only_owned=False, minimum_search_frase_length=3, pagenumber=0, pagesize=-1, **env):
		resp = ListTeamsResponse()
		resp.teams = []
		resp.full_count = 0
		if pagesize==-1:
			pagesize = None
		if search_frase==u'*':
			search_frase = u''
		if (len(search_frase)<minimum_search_frase_length):
			return resp
		for team in tm.list_teams(
			device_hash,
			search_frase,
			only_owned,
			pagenumber=pagenumber,
			pagesize=pagesize):
			
			t = Team()
			t.teamname = team['teamname']
			t.private = team['private']
			t.owner = team['username']
			t.searchable = team['searchable']
			t.icon = team['teamicon']
			t.owner_accepted = team['owner_accepted']
			t.member_accepted = team['member_accepted']
			t.pending_accept = True if t.owner_accepted==False or t.member_accepted==False else False
			t.pending_owner_accepts = team['pending_owner_accepts']
			resp.teams += [t]
		if 'team' in locals():
			resp.full_count = team['full_count']
		return resp
	
	@ladonize(PORTABLE_STRING, rtype=NumberResponse)
	def pendingInvitations(self, device_hash):
		resp = NumberResponse()
		resp.number = tm.pending_invitations(device_hash)
		return resp
		
	
	@ladonize(PORTABLE_STRING, rtype=NumberResponse)
	def pendingMemberAccepts(self, device_hash):
		resp = NumberResponse()
		resp.number = tm.pending_member_accepts(device_hash)
		return resp
	
	@ladonize(PORTABLE_STRING, PORTABLE_STRING, int, int, rtype=ListTeamsResponse)
	def listOwnMemberships(self, device_hash, search_frase=u'*', pagenumber=0, pagesize=-1, **env):
		resp = ListTeamsResponse()
		resp.teams = []
		if pagesize==-1:
			pagesize = None
		if search_frase==u'*':
			search_frase = u''
		for team in tm.list_own_memberships(
			device_hash,
			search_frase,
			pagenumber=pagenumber,
			pagesize=pagesize):
			
			t = Team()
			t.teamname = team['teamname']
			t.private = team['private']
			t.owner = team['username']
			t.searchable = team['searchable']
			t.icon = team['teamicon']
			t.owner_accepted = team['owner_accepted']
			t.member_accepted = team['member_accepted']
			t.pending_accept = True if t.owner_accepted==False or t.member_accepted==False else False
			t.pending_owner_accepts = team['pending_owner_accepts']
			resp.teams += [t]
		if 'team' in locals():
			resp.full_count = team['full_count']
		else:
			resp.full_count = 0
		return resp
	
	@ladonize(PORTABLE_STRING, PORTABLE_STRING, PORTABLE_STRING, int, int, rtype=ListMembersResponse)
	def listMembers(self, device_hash, team_owner, teamname, pagenumber=0, pagesize=-1, **env):
		resp = ListMembersResponse()
		resp.members = []
		if pagesize==-1:
			pagesize = None
		for member in tm.list_members(
			device_hash,
			team_owner,
			teamname,
			pagenumber=pagenumber,
			pagesize=pagesize):
			
			m = Member()
			m.username = member['username']
			m.owner_accepted = member['owner_accepted']
			m.member_accepted = member['member_accepted']
			resp.members += [m]
		if 'member' in locals():
			resp.full_count = member['full_count']
		else:
			resp.full_count = 0
		return resp

	@ladonize(PORTABLE_STRING, PORTABLE_STRING, PORTABLE_STRING, rtype=RequestMembershipResponse)
	def requestMembership(self, device_hash, team_owner, teamname, **export):
		resp = RequestMembershipResponse()
		resp.pending_accept = True
		resp.success = tm.request_membership(device_hash, team_owner, teamname)
		return resp

	@ladonize(PORTABLE_STRING, PORTABLE_STRING, PORTABLE_STRING, rtype=SuccessResponse)
	def withdrawMembership(self, device_hash, team_owner, teamname, **export):
		resp = SuccessResponse()
		resp.success = tm.withdraw_membership(device_hash, team_owner, teamname)
		return resp

	@ladonize(PORTABLE_STRING, PORTABLE_STRING, PORTABLE_STRING, rtype=SuccessResponse)
	def sendInvitation(self, device_hash, teamname, invitee_username, **export):
		resp = SuccessResponse()
		resp.success = tm.send_invitation(device_hash, teamname, invitee_username)
		return resp

	@ladonize(PORTABLE_STRING, PORTABLE_STRING, PORTABLE_STRING, rtype=SuccessResponse)
	def excludeMember(self, device_hash, teamname, member_username, **export):
		resp = SuccessResponse()
		resp.success = tm.exclude_member(device_hash, teamname, member_username)
		return resp

	@ladonize(PORTABLE_STRING, PORTABLE_STRING, PORTABLE_STRING, rtype=SuccessResponse)
	def acceptMembership(self, device_hash, teamname, member_username,  **export):
		resp = SuccessResponse()
		resp.success = tm.accept_membership(device_hash, teamname, member_username)
		return resp

	@ladonize(PORTABLE_STRING, PORTABLE_STRING, PORTABLE_STRING, rtype=SuccessResponse)
	def acceptInvitation(self, device_hash, team_owner, teamname, **export):
		resp = SuccessResponse()
		resp.success = tm.accept_invitation(device_hash, team_owner, teamname)
		return resp

	@ladonize(PORTABLE_STRING, PORTABLE_STRING, PORTABLE_STRING, PORTABLE_STRING, rtype=SuccessResponse)
	def shareDataEntry(self, device_hash, team_owner, teamname, data_hash):
		resp = SuccessResponse()
		resp.success = tm.share_cb_entry(device_hash, team_owner, teamname, data_hash)
		return resp

	@ladonize(PORTABLE_STRING, PORTABLE_STRING, PORTABLE_STRING, PORTABLE_STRING, rtype=SuccessResponse)
	def unshareDataEntry(self, device_hash, team_owner, teamname, data_hash):
		resp = SuccessResponse()
		resp.success = tm.unshare_cb_entry(device_hash, team_owner, teamname, data_hash)
		return resp
