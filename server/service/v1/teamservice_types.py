# -*- coding: UTF-8 -*- 

from ladon.server.customresponse import CustomResponse
from ladon.types.ladontype import LadonType
from common_types import *
import os

class Team(LadonType):
	owner = mandatory_string
	teamname = mandatory_string
	icon = default_nullable_bytes
	private = mandatory_bool
	searchable = mandatory_bool
	owner_accepted = nullable_bool
	member_accepted = nullable_bool
	pending_accept = nullable_bool
	pending_owner_accepts = mandatory_int

class Member(LadonType):
	username = mandatory_string
	owner_accepted = mandatory_bool
	member_accepted = mandatory_bool

class ListTeamsResponse(CommonResponse):
	teams = [ Team ]
	full_count = mandatory_int

class RequestMembershipResponse(SuccessResponse):
	pending_accept = mandatory_bool

class ListMembersResponse(CommonResponse):
	members = [ Member ]
	full_count = mandatory_int