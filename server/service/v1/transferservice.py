from ladon.ladonizer import ladonize
from ladon.compat import PORTABLE_STRING
from transferservice_types import *
from common_types import SuccessResponse
import os,hashlib,re,calendar
import clipster.lib.datamanager as dman
from ladon.types.attachment import attachment

dm = dman.DataManager()

class TransferService(object):

	@ladonize(PORTABLE_STRING, PORTABLE_STRING, rtype=UploadResponse)
	def upload(self,device_hash, data_hash=u'', **export):
		resp = UploadResponse()
		resp.success = False
		if 'attachments' in export:
			res = dm.store_data(device_hash, data_hash, export.get('attachments'))
		if res:
			resp.success = True
			resp.data_hash = res
		return resp

	@ladonize(PORTABLE_STRING, int, int, int, rtype=DataEntryListResponse)
	def listDataEntries(self, device_hash, since_timestamp=-1, pagenumber=0, pagesize=-1, **env):
		resp = DataEntryListResponse()
		resp.data_entries = []
		if pagesize==-1:
			pagesize = None
		if since_timestamp==-1:
			since_timestamp = None
		latest_timestamp = 0
		for data_entry in dm.list_data_entries(
			device_hash,
			since_timestamp=since_timestamp,
			pagenumber=pagenumber,
			pagesize=pagesize):
			
			de = DataEntry()
			de.data_hash = data_entry['data_hash']
			ct = data_entry['copytime']
			de.copytime = calendar.timegm(ct.timetuple())*1000 + ct.microsecond/1000
			rt = data_entry['removetime']
			if rt:
				removetime = calendar.timegm(rt.timetuple())*1000 + rt.microsecond/1000
				latest_timestamp = max(latest_timestamp, removetime)
			else:
				latest_timestamp = max(latest_timestamp,de.copytime)
			latest_timestamp = max(latest_timestamp,de.copytime)
			de.image_preview = None if data_entry['image_preview']==None else str(data_entry['image_preview']).encode('base64')
			de.text_preview = data_entry['text_preview']
			de.files = data_entry['files']
			de.mime_types = map(lambda x: PORTABLE_STRING(x), data_entry['mime_types'])
			de.username = data_entry['username']
			de.os_type = data_entry['os_type']
			de.hostname = data_entry['hostname']
			de.removed = data_entry['removed']
			resp.data_entries += [de]
		resp.latest_timestamp = latest_timestamp + 1
		return resp

	@ladonize(PORTABLE_STRING, int, int, int, rtype=DataEntryListResponse)
	def listTeamDataEntries(self, device_hash, since_timestamp=-1, pagenumber=0, pagesize=-1, **env):
		resp = DataEntryListResponse()
		resp.data_entries = []
		if pagesize==-1:
			pagesize = None
		if since_timestamp==-1:
			since_timestamp = None
		latest_timestamp = 0
		for data_entry in dm.list_team_data_entries(
			device_hash,
			since_timestamp=since_timestamp,
			pagenumber=pagenumber,
			pagesize=pagesize):
			
			de = DataEntry()
			de.data_hash = data_entry['data_hash']
			ct = data_entry['copytime']
			if ct:
				de.copytime = calendar.timegm(ct.timetuple())*1000 + ct.microsecond/1000
			else:
				de.copytime = 0
			rt = data_entry['removetime']
			if rt:
				removetime = calendar.timegm(rt.timetuple())*1000 + rt.microsecond/1000
				latest_timestamp = max(latest_timestamp, removetime)
			else:
				latest_timestamp = max(latest_timestamp,de.copytime)
			latest_timestamp = max(latest_timestamp,de.copytime)
			de.image_preview = None if data_entry['image_preview']==None else str(data_entry['image_preview']).encode('base64')
			de.text_preview = data_entry['text_preview']
			de.files = data_entry['files']
			de.mime_types = map(lambda x: PORTABLE_STRING(x), data_entry['mime_types'])
			de.team_owner = data_entry['team_owner']
			de.teamname = data_entry['teamname']
			de.username = data_entry['username']
			de.os_type = data_entry['os_type']
			de.hostname = data_entry['hostname']
			de.removed = data_entry['removed']
			resp.data_entries += [de]
		resp.latest_timestamp = latest_timestamp + 1
		return resp

	@ladonize(PORTABLE_STRING, PORTABLE_STRING, rtype=DownloadResponse)
	def download(self, device_hash, data_hash, **env):
		resp = DownloadResponse()
		resp.attachments = []
		mimeparts = dm.data_mime_parts(device_hash,data_hash)
		for mime_type,fp in mimeparts.items():
			if type(mime_type)==tuple:
				# File
				header_names = ('Root-Entry-Name','Sub-Directory','Content-Disposition')
				resp.attachments += [ attachment(fp,headers=dict(filter(lambda x: x[1] is not None,zip(header_names,mime_type)))) ]
			else:
				# Mime-type
				resp.attachments += [ attachment(fp,headers={'Content-Type':mime_type}) ]
		return resp

	@ladonize(PORTABLE_STRING, PORTABLE_STRING, rtype=SuccessResponse)
	def removeDataEntry(self, device_hash, data_hash):
		resp = SuccessResponse()
		resp.success = dm.remove_data_entry(device_hash, data_hash)
		return resp
		