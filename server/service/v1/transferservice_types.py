# -*- coding: UTF-8 -*- 

from ladon.types.attachment import attachment
from ladon.types.ladontype import LadonType
from common_types import *


class UploadResponse(SuccessResponse):
	data_hash = default_nullable_string

class DataEntry(LadonType):
	data_hash = mandatory_string
	copytime = mandatory_int
	text_preview = default_nullable_string
	image_preview = default_nullable_bytes
	files = {
		'type': bool,
		'nullable': False,
		'default': False
	}
	team_owner = default_nullable_string
	teamname = default_nullable_string
	mime_types = [ PORTABLE_STRING ]
	username = mandatory_string
	os_type = mandatory_string
	hostname = mandatory_string
	removed = mandatory_bool
	

class DataEntryListResponse(CommonResponse):
	data_entries = [ DataEntry ]
	latest_timestamp = default_nullable_int

class DownloadResponse(CommonResponse):
	attachments = [ attachment ]
