# -*- coding: utf-8 -*-

from ladon.ladonizer import ladonize
from ladon.compat import PORTABLE_STRING
from userservice_types import *
from common_types import SuccessResponse
import os,re,time
import clipster.lib.usermanager as uman
from clipster.tools.mailutil import send_mail
from clipster.tools.memcacheutil import get_mc_client
from hashlib import sha1
um = uman.UserManager()

verification_subject = u'Clipster Account Verification'
verification_body = u'''<h2>Thank you for signing up.</h2>
<p>
Please click this link to complete your Clipster registration:
<a href="http://clipster-dev.limbosoft.com/verify-account?verification_code=%(verification_code)s">Verify account</a>
</p>
'''

def check_email(email):
	if not re.match('^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$',email):
		return False
	return True

class UserService(object):

	@ladonize(PORTABLE_STRING, PORTABLE_STRING, rtype=SuccessResponse)
	def initUserAccount(self, username, password, **export):
		resp = SuccessResponse()
		resp.success = False
		username = username.lower()
		valid_email = check_email(username)
		if not valid_email:
			resp.method_result.res_code = 10003
			resp.method_result.res_msg = u'Username must be a valid and functional e-mail address'
			return resp
		res, verification_code = um.init_user(username,password)
		if res==1:
			resp.method_result.res_code = 10001
			resp.method_result.res_msg = u'User account is already in use'
			return resp
		if res==2:
			resp.method_result.res_code = 10002
			resp.method_result.res_msg = u'For unknown reason the account could not be initialized'
			return resp
		send_mail(
			u'Clipster <clipster@limbosoft.com>',
			username,
			verification_subject,
			verification_body % {'verification_code': verification_code})
		resp.method_result.res_msg = u'Almost done! Check your e-mail for a Clipster account verification message.'
		resp.success = True
		return resp

	@ladonize(PORTABLE_STRING, rtype=SuccessResponse)
	def verifyUserAccount(self, verification_code, **export):
		resp = SuccessResponse()
		resp.success = False
		res = um.verify_user(verification_code)
		if res==1:
			resp.method_result.res_code = 10101
			resp.method_result.res_msg = u'The verification code is invalid. Maybe the account has already been activated?'
			return resp
		resp.method_result.res_msg = u"You're Clipster profile has been activated."
		resp.success = True
		return resp
	
	@ladonize(PORTABLE_STRING, PORTABLE_STRING, rtype=LoginResponse)
	def login(self, username, password):
		resp = LoginResponse()
		resp.success = False
		user_account_id = um.authenticate(username, password)
		if not user_account_id:
			resp.method_result.res_code = 10201
			resp.method_result.res_msg = u'No Clipster account found having this username/password combination'
			return resp
		resp.success = True 
		login_token = sha1()
		login_token.update(username + password + str(time.time()))
		mc = get_mc_client()
		resp.login_token = login_token.hexdigest()
		mc.set(resp.login_token,user_account_id)
		return resp
	
	@ladonize(PORTABLE_STRING, PORTABLE_STRING, int, int, int, rtype=ListUsersResponse)
	def listUsers(self, device_hash, search_frase=u'*', minimum_search_frase_length=3, pagenumber=0, pagesize=-1, **env):
		resp = ListUsersResponse()
		resp.users = []
		resp.full_count = 0
		if pagesize==-1:
			pagesize = None
		if search_frase==u'*':
			search_frase = u''
		if (len(search_frase)<minimum_search_frase_length):
			return resp
		for user in um.list_users(
			device_hash,
			search_frase,
			pagenumber=pagenumber,
			pagesize=pagesize):
			
			u = User()
			u.username = user['username']
			u.firstname = user['firstname']
			u.lastname = user['lastname']
			resp.users += [u]
		if 'user' in locals():
			resp.full_count = user['full_count']
		return resp
	
	@ladonize(PORTABLE_STRING, rtype=WhoamiResponse)
	def whoami(self, device_hash):
		resp = WhoamiResponse()
		resp.user_info = User()
		res = um.user_by_device(device_hash)
		if not res:
			resp.method_result.res_code = 10301
			resp.method_result.res_msg = u'Invalid device hash'
			return resp
		resp.user_info.username = res['username']
		resp.user_info.firstname = res['firstname']
		resp.user_info.lastname = res['lastname']
		return resp
