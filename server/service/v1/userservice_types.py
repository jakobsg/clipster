# -*- coding: UTF-8 -*- 

from ladon.server.customresponse import CustomResponse
from ladon.types.ladontype import LadonType
from common_types import *
import os

class User(LadonType):
	username = default_nullable_string
	firstname = default_nullable_string
	lastname = default_nullable_string

class LoginResponse(SuccessResponse):
	login_token = default_nullable_string

class WhoamiResponse(CommonResponse):
	user_info = User

class ListUsersResponse(CommonResponse):
	users = [ User ]